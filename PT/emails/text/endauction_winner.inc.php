Caro(a) {S_NAME},
Parabéns! O seu item acabou de ser vendido.

Detalhes indicados abaixo.

Leilão: {A_TITLE}
Preço de venda: {A_CURRENTBID}
Quantidade: {A_QTY}
Data de encerramento: {A_ENDS}
URL do leilão: {A_URL}

Aceda a {SITENAME}: {SITE_URL}user_menu.php

Informação do comprador
{B_REPORT}