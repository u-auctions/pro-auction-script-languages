Caro(a) {C_NAME},

Esta é uma fatura para regularizar o saldo da sua conta no nosso site, {SITENAME}.

O seu saldo é: {BALANCE}

Por favor clique no link abaixo para aceder à página de pagamento:
{LINK}