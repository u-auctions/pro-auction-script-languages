Caro(a) {W_NAME},

Venceu o leilão em {SITENAME}

Dados do leilão

Título: {A_TITLE}
Descrição: {A_DESCRIPTION}

A sua licitação: {A_CURRENTBID} ({W_WANTED} items)
Obteve: {W_GOT} items
Data de encerramento do leilão: {A_ENDS}
URL: {A_URL}

Informação do vendedor

{S_NICK}
{S_EMAIL}

Detalhes de pagamento do vendedor:
{S_PAYMENT}


Se recebeu esta mensagem por engano, por favor responda a este e-mail, 
escreva para {ADMINMAIL}, ou visite {SITENAME} em {SITE_URL}.

