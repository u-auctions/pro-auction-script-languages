Dear Support Team,<br>
<br>
You have received a support ticket.<br>
<strong>User:</strong> {USER}<br>
<br>
<strong>Website:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Subject:</strong><br>
{SUBJECT}
<br><br>
<strong>Message:</strong><br>
{MESSAGE}