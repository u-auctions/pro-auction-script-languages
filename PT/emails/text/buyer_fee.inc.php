Caro(a) {C_NAME},

Esta é uma fatura pelo pagamento do valor de {ITEM_NAME}.

Total a pagar: {BALANCE}

Por favor clique no link abaixo para aceder à página de pagamento:
{LINK}

Toda a sua atividade no site ficará suspensa até o pagamento ser efetuado.