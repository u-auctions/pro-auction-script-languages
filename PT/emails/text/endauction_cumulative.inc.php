Caro(a) {S_NAME},

Os seguintes leilões que colocou em {SITENAME}, encerraram ontem.

{REPORT}


Foi enviado um email ao(s) vencedor(es) com o seu endereço de e-mail.

Se recebeu esta mensagem por engano, por favor responda a este e-mail, escreva para {ADMINMAIL}, ou visite {SITENAME} em {SITE_URL}.