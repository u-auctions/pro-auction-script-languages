Utilizador {NAME} ({NICK}),
O proprietário de uma conta de comprador solicitou a troca para uma conta de vendedor.

Detalhes do utilizador

ID do utilizador: {ID}
Nome do utilizador: {NAME}
Nickname do utilizador: {NICK}
E-mail do utilizador: {EMAIL}