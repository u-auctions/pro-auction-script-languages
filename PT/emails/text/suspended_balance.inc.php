Caro(a){NAME},

A sua conta em {SITENAME} foi suspensa porque excedeu o limite máximo de dívida permitido.

O seu saldo é: {BALANCE}

Para reativar a sua conta, terá de regularizar o saldo. 
Por favor visite o link abaixo para aceder à página de pagamento.
{OUTSTANDING}