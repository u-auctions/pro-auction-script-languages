Caro(a) {C_NAME},
O seu item foi listado em {SITENAME}. O item aparecerá automaticamente nas próximas pesquisas.

Apresentam-se os detalhes da listagem:

Leilão: {A_TITLE}
Tipo do leilão: {A_TYPE}
Item n.º: {A_ID}
Base de licitação: {A_MINBID}
Valor de reserva: {A_RESERVE}
Valor comprar agora: {A_BNPRICE}
Termina a: {A_ENDS}

{SITE_URL}item.php?id={A_ID}
    
Tem outro item para listar?
{SITE_URL}select_category.php