Caro(a) {REALNAME},
Esta é uma resposta automática do seu alerta de licitações.
Alguém licitou num item que adicionou à lista de acompanhados.

Leilão: {TITLE}
Licitação atual: {BID}

URL do leilão: {AUCTION_URL}