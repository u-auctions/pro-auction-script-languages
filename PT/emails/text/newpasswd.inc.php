Olá {REALNAME},
De acordo com o solicitado, criámos uma nova password para a sua conta.

A sua nova password: {NEWPASS}

Utilize-a para aceder a {SITENAME} e lembre-se de a alterar para algo que consiga lembrar-se. 