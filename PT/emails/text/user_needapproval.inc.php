Caro(a) {C_NAME},
Para começar a vender e/ou comprar em {SITENAME}, a sua conta terá de ser aprovada pelo administrador do nosso site e que de momento está a ser analisada. 

Receberá um e-mail assim que a sua conta ficar ativa.

A sua informação encontra-se indicada em baixo: 

Nome completo: {C_NAME}
Username: {C_NICK}
<!-- IF C_ADDRESS ne '' -->
Utilizador: {C_ADDRESS}
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
Cidade: {C_CITY}
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
Distrito: {C_PROV}
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
País: {C_COUNTRY}
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
Código Postal: {C_ZIP}
<!-- ENDIF -->
Email: {C_EMAIL}