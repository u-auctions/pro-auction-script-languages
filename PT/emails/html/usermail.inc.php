<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Complete o seu registo no {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Olá <b>{C_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		Clique em "Active-me" para completar o seu registo. Assim que o fizer, poderá começar a comprar/vender em {SITENAME}
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top"><br /><br />
		<table border="0" width="100%">
			<tr>
				<td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Se o link não funcionar:</b></td>

			</tr>
			<tr>
				<td style="font-size: 12px;">Por favor envie-nos um e-mail para <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">Continue para Confirmar</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{CONFIRMURL}">
		<img border="0" src="{SITEURL}images/email_alerts/Active_Acct_Btn.jpg" width="120" height="32"></a></td>
	</tr>
</table>