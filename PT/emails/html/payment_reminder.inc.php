Caro(a) {C_NAME},<br>
<br>
Esta é uma fatura para regularizar o saldo da sua conta no nosso website, {SITENAME}.<br>
<br>
O seu saldo é: {BALANCE}<br>
<br>
Por favor [ <a href="{LINK}">clique aqui</a> ] para aceder à página de pagamento.