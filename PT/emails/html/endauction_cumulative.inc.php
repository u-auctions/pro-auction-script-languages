Caro(a) {S_NAME},<br>
<br>
Os seguintes leilões que colocou em <a href="{SITE_URL}">{SITENAME}</a> encerraram ontem.<br>
<br>
{REPORT}
<br>
<br>
Foi enviado um email ao(s) vencedor(es) com o seu endereço de e-mail.<br>
<br>
Se recebeu esta mensagem por engano, por favor responda a este e-mail, escreva para <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>, ou visite {SITENAME} em <a href="{SITE_URL}">{SITE_URL}</a>.
