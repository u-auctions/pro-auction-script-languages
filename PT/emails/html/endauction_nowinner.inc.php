<table border="0" width="100%">
	<tr>
		<td colspan="3" height="35"><div style="font-size: 14px; font-weight: bold;">Seu leilão terminou sem vencedor no {SITENAME}</div></td>
	</tr>
	<tr>
		<td colspan="3" style="font-size: 12px;">Caro(a) <b>{S_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="3" height="40" style="font-size: 12px; padding-right: 6px;">
		Lamentamos mas o leilão que criou em {SITENAME} encerrou sem vencedor</td>
	</tr>
	<tr>
		<td width="9%" rowspan="2"><img width="150px" height="150px" border="0" src="{A_PICURL}"></td>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td colspan="2" style="font-size: 12px;"><a href="{A_URL}">{A_TITLE}</a></td>

			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Item nº:</td>
				<td align="left" style="font-size: 12px;">{A_ID}</td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Terminou a:</td>
				<td align="left" style="font-size: 12px;">{A_END}</td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">URL do Leilão:</td>
				<td align="left" style="font-size: 12px;"><a href="{A_URL}">{A_URL}</a></td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">&nbsp;</td>
				<td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Aceder a {SITENAME}</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="120" valign="top">&nbsp;
		</td>
	</tr>
</table>