Caro(a) {W_NAME},<br>
<br>
Venceu o leilão seguinte em <a href="{SITE_URL}">{SITENAME}</a> <br>
<br>
Dados do leilão<br>
------------------------------------<br>
Título: {A_TITLE}<br>
Descrição: {A_DESCRIPTION}<br>
<br>
A sua licitação: {A_CURRENTBID} ({W_WANTED} items)<br>
Obteve: {W_GOT} items<br>
Data de encerramento do leilão: {A_ENDS}<br>
URL: <a href="{A_URL}">{A_URL}</a><br>
<br>
=============<br>
Informação do vendedor<br>
=============<br>
<br>
{S_NICK}<br>
<a href="mailto:{S_EMAIL}">{S_EMAIL}</a><br>
Detalhes de pagamento do vendedor:<br>
{S_PAYMENT}<br>		 
<br>
<br>
Se recebeu esta mensagem por engano, por favor responda a este e-mail,<br>
escreva para <a href="mailto:{ADMINEMAIL}">{ADMINEMAIL}</a>,ou visite <a href="{SITE_URL}">{SITENAME}</a> at <a href="{SITE_URL}">{SITE_URL}</a>.
