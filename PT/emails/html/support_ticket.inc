Caro(a) {NAME},<br>
<br>
Recebeu uma resposta da equipa de suporte.<br>
<br>
<strong>{SITENAME} Suporte:</strong> <a href="{A_URL}">Meu Suporte</a><br>
<br>
<strong>Website:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Assunto:</strong><br>
{SUBJECT}
<br><br>
<strong>Messagem:</strong><br>
{MESSAGE}