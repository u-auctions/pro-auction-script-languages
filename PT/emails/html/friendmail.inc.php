<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Alguém recomendou-lhe um leilão no website {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Caro(a) <b>{F_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		 Foi-lhe encaminhado um leilão por {S_NAME} (<a href="mailto:{S_EMAIL}">{S_EMAIL}</a>) no {SITENAME}
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px; line-height: 0.6cm;">
			<b>Título do leilão:</b> 	{TITLE} <br />
			<b>Comentário:</b> {S_COMMENT}<br />
		</td>
		<td width="34%" style="font-size: 12px;">Verifique o leilão!</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{URL}"><img src="{SITEURL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
		</td>
	</tr>
</table>