<b>Esta é uma copia do contato efetuado via Formulario Contacte-nos.</b><br><br>

Caro(a) {SELLER_NICK},<br>
<br>
Esta mensagem é enviada do website {SITENAME}.<br>
<br>
Utilizador {SENDER_NAME} tem uma questão para si.<br>
<br>
Email:<br>
{SENDER_EMAIL}<br>
<br>
Assunto:<br>
{SUBJECT}<br><br>
Questão:<br>
{SENDER_QUESTION}<br>
