Caro(a) {NAME},<br>
<br>
A sua conta em {SITENAME} foi suspensa porque excedeu o limite máximo de dívida permitido.<br>
<br>
O seu saldo é: {BALANCE}<br>
<br>
Para reativar a sua conta, terá de regularizar o saldo. <br>
Por favor visite o link abaixo para aceder à página de pagamento.<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a>