Olá {REALNAME},<br>
De acordo com o solicitado, criámos uma nova password para a sua conta.<br>
<br>
A sua nova password: {NEWPASS}<br>
<br>
Utilize-a para aceder a {SITENAME} e lembre-se de a alterar para algo que consiga lembrar-se.