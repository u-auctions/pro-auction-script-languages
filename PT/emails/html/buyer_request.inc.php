Utilizador {NAME} ({NICK}),<br>
O proprietário de uma conta de comprador solicitou a troca para uma conta de vendedor.<br>
<br>
Detalhes do utilizador:<br>
---------------<br>
ID do utilizador: {ID}<br>
Nome do utilizador: {NAME}<br>
Nickname do utilizador: {NICK}<br>
E-mail do utilizador: {EMAIL}