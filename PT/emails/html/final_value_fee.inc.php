Caro(a) {C_NAME},<br>
<br>
Esta é uma fatura pelo pagamento do valor de {ITEM_NAME}.<br>
<br>
Total a pagar: {BALANCE}<br>
<br>
Por favor clique no link abaixo para aceder à página de pagamento:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Toda a sua atividade no site ficará suspensa até o pagamento ser efetuado.