Caro(a) {SELLER_NICK},<br>
<br>
Esta mensagem é enviada de {SITENAME}.<br>
<br>
O {SENDER_NAME} <a href="mailto:{SENDER_EMAIL}">{SENDER_EMAIL}</a> tem uma questão a colocar-lhe sobre o seu leilão {TITLE}.<br>
<br>
Questão:<br>
{SENDER_QUESTION}<br>
<br>
URL do Leilão: <a href="{SITEURL}item.php?id={AID}">{SITEURL}item.php?id={AID}</a><br>
<br>
Obrigado por fazer parte do {SITENAME}<br>
<a href="{SITEURL}">{SITEURL}</a><br>