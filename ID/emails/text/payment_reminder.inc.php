Yang terhormat {C_NAME},

Ini adalah faktur untuk mengosongkan saldo account Anda pada situs kami, {SITENAME}.

Saldo Anda adalah: {BALANCE}

Silakan klik pada tautan di bawah ini untuk mengakses halaman pembayaran:
{LINK}