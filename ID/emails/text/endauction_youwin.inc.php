Yang terhormat {W_NAME},

Anda telah memenangkan lelang berikut ini pada {SITENAME}

Info lelang

Judul: {A_TITLE}
Keterangan: {A_DESCRIPTION}

Tawaran anda: {A_CURRENTBID} ({W_WANTED} barang)
Anda mendapat: {W_GOT} barang
Lelang berakhir pada: {A_ENDS}
Alamat: {A_URL}

Info Penjual

{S_NICK}
{S_EMAIL}
Rincian pembayaran Penjual:
{S_PAYMENT}


Jika Anda telah menerima pesan ini karena kesalahan, silakan balas email ini,
Balas ke {ADMINEMAIL}, atau kunjungi {SITENAME} di {SITE_URL}.
