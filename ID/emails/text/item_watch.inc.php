Yang terhormat {REALNAME},
Ini adalah tanggapan otomatis dari pantauan barang anda. 
Seseorang telah menawar pada barang yang telah ditambahkan ke daftar pantauan anda.

Judul lelang: {TITLE}
Penawaran terakhir: {BID}

Alamat lelang: {AUCTION_URL}