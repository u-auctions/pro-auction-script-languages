Yang terhormat {NAME},

Akun anda pada {SITENAME} telah ditangguhkan karena Anda telah melebihi batas utang maksimum yang diizinkan.

Saldo Anda adalah: {BALANCE}

Untuk mengaktifkan kembali akun, Anda akan perlu untuk menghapus saldo account Anda. 
Silahkan kunjungi tautan di bawah ini untuk mengakses halaman pembayaran.
{OUTSTANDING}