Yang terhormat {C_NAME},
Item Anda telah berhasil terdaftar di {SITENAME}. Item akan ditampilkan langsung di hasil pencarian.

Berikut adalah rinciannya:

Judul lelang: {A_TITLE}
Jenis lelang: {A_TYPE}
Barang #: {A_ID}
Dimulai penawaran: {A_MINBID}
Harga cadangan: {A_RESERVE}
Harga beli: {A_BNPRICE}
Waktu berakhir: {A_ENDS}

{SITE_URL}item.php?id={A_ID}
				
Memiliki barang lain untuk di jual?
{SITE_URL}select_category.php