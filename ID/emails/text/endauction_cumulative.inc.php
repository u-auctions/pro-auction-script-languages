Yang terhormat {S_NAME},

Berikut lelang Anda diposting di {SITENAME} Berakhir kemarin.

{REPORT}


Sebuah email telah dikirim ke pemenang(s) dengan alamat email Anda.

Jika Anda telah menerima pesan ini karena kesalahan, silakan balas email ini,
Balas ke {ADMINMAIL}, atau kunjungi {SITENAME} di {SITE_URL}.
