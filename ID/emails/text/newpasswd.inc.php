Hi {REALNAME},
Seperti yang Anda minta, kami telah membuat kata sandi baru untuk akun Anda.

Kata sandi baru anda: {NEWPASS}

Gunakan itu untuk login ke {SITENAME} dan ingat untuk mengubahnya menjadi yang Anda ingat.