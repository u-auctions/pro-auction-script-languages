Yang terhormat {C_NAME},

Ini adalah faktur untuk pembayaran biaya pembelian {ITEM_NAME}.

Jumlah yang harus di bayar: {BALANCE}

Silahkan klik tautan berikut ini untuk mengakses halaman pembayaran:
{LINK}

Semua aktivitas Anda di situs akan ditangguhkan hingga ini terbayar