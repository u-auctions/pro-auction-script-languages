Yang terhormat {C_NAME},
Dalam rangka untuk mulai menjual/membeli di {SITENAME}, akun Anda harus 
disetujui oleh administrator situs kami yang saat ini sedang ditinjau.

Anda akan menerima e-mail ketika akun Anda telah di setujui. 

Informasi Anda di bawah ini:

Nama lengkap: {C_NAME}
Username: {C_NICK}
<!-- IF C_ADDRESS ne '' -->
Alamat: {C_ADDRESS}
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
Kota: {C_CITY}
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
Provinsi: {C_PROV}
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
Negara: {C_COUNTRY}
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
Kode Pos: {C_ZIP}
<!-- ENDIF -->
Email: {C_EMAIL}