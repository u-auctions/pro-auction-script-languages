Yang terhormat {NAME},<br>
<br>
Akun Anda di {SITENAME} telah ditangguhkan karena Anda telah melebihi batas utang maksimum yang diizinkan.<br>
<br>
Saldo Anda adalah: {BALANCE}<br>
<br>
Untuk mengaktifkan kembali akun, Anda akan perlu untuk menghapus saldo account Anda.<br>
Silahkan kunjungi tautan di bawah ini untuk mengakses halaman pembayaran.<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a>