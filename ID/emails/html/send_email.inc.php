Yang terhormat {SELLER_NICK},<br>
<br>
Pesan ini dikirim dari {SITENAME}.<br>
<br>
{SENDER_NAME} kepada <a href="mailto:{SENDER_EMAIL}">{SENDER_EMAIL}</a> memiliki pertanyaan untuk Anda tentang lelang {TITLE}.<br>
<br>
Pertanyaan:<br>
{SENDER_QUESTION}<br>
<br>
Alamat lelang: <a href="{SITEURL}item.php?id={AID}">{SITEURL}item.php?id={AID}</a><br>
<br>
Terima kasih telah menjadi bagian dari {SITENAME}<br>
<a href="{SITEURL}">{SITEURL}</a><br>