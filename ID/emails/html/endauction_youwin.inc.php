Yang terhormat{W_NAME},<br>
<br>
Anda telah memenangkan lelang berikut ini pada <a href="{SITE_URL}">{SITENAME}</a> <br>
<br>
Data lelang<br>
------------------------------------<br>
Judul: {A_TITLE}<br>
Keterangan: {A_DESCRIPTION}<br>
<br>
Tawaran anda: {A_CURRENTBID} ({W_WANTED} barang)<br>
Anda mendapat: {W_GOT} barang<br>
Lelang berakhir pada: {A_ENDS}<br>
alamat: <a href="{A_URL}">{A_URL}</a><br>
<br>
=============<br>
INFORMASI PENJUAL<br>
=============<br>
<br>
{S_NICK}<br>
<a href="mailto:{S_EMAIL}">{S_EMAIL}</a><br>
Rincian pembayaran Penjual:<br>
{S_PAYMENT}<br>		 
<br>
<br>
Jika Anda telah menerima pesan ini karena kesalahan, silakan balas email ini,<br>
Balas ke <a href="mailto:{ADMINEMAIL}">{ADMINEMAIL}</a>, atau kunjungi <a href="{SITE_URL}">{SITENAME}</a> di <a href="{SITE_URL}">{SITE_URL}</a>.
