Pengguna {NAME} ({NICK}),<br>
pemilik rekening pembeli diminta untuk beralih ke rekening penjual.<br>
<br>
Rincian Pengguna:<br>
---------------<br>
ID Pengguna: {ID}<br>
Nama Pengguna: {NAME}<br>
Panggilan Pengguna: {NICK}<br>
E-Mail Pengguna: {EMAIL}