Yang terhormat {S_NAME},<br>
<br>
Lelang Anda berikut ini telah <a href="{SITE_URL}">{SITENAME}</a> berakhir kemarin.<br>
<br>
{REPORT}
<br>
<br>
Sebuah email telah dikirim kepada pemenang(s) dengan alamat email anda.<br>
<br>
Jika Anda telah menerima pesan ini karena kesalahan, silakan balas email ini,
Balas ke <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>, atau kunjungi {SITENAME} di <a href="{SITE_URL}">{SITE_URL}</a>.
