Yang terhormat {C_NAME},<br>
<br>
Ini adalah faktur untuk pembayaran biaya pembelian{ITEM_NAME}.<br>
<br>
Jumlah yang harus di bayar: {BALANCE}<br>
<br>
Silakan klik pada tautan di bawah ini untuk ke halaman pembayaran:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Semua aktivitas di situs Anda akan ditangguhkan hingga ini terbayar