Yang terhormat {C_NAME},<br>
<br>
Ini adalah faktur untuk pembayaran biaya nilai akhir {ITEM_NAME}.<br>
<br>
Jumlah yang harus di bayar: {BALANCE}<br>
<br>
Silahkan klik tautan di bawah ini untu pembayaran:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Semua aktivitas di situs Anda akan ditangguhkan hingga ini terbayar