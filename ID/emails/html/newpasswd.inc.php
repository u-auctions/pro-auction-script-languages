Hi {REALNAME},<br>
Seperti yang Anda minta, kami telah membuat kata sandi baru untuk akun Anda.<br>
<br>
Kata sandi baru: {NEWPASS}<br>
<br>
Pakai ini untuk login ke {SITENAME} dan ingat untuk mengubahnya dengan yang Anda ingat.