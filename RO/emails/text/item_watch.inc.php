Buna ziua {REALNAME},
Acesta este un raspuns automat de la urmarirea articolului dvs.
Cineva are oferta unui articol pe care dvs. l-ati adaugat in lista.

Titlul Licitatie: {TITLE}
Oferta curenta: {BID}

URL-ul Licitatiei: {AUCTION_URL}
