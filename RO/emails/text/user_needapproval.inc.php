Buna ziua {C_NAME},
Pentru a începe vânzarea și/sau cumpărarea pe {SITENAME}, contul dvs. trebuie să fie
aprobat de către administratorul site-ului nostru, care este în prezent în curs de revizuire.

Veți primi un e-mail la scurt timp atunci când contul dvs. este activ.

Informațiile dvs. de mai jos:

Numele complet: {C_NAME}
Nume utilizator: {C_NICK}
<!-- IF C_ADDRESS ne '' -->
Adresa: {C_ADDRESS}
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
Oras: {C_CITY}
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
Judet: {C_PROV}
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
Tara: {C_COUNTRY}
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
Cod: {C_ZIP}
<!-- ENDIF -->
Email: {C_EMAIL}
