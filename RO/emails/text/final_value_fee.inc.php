Buna ziua {C_NAME},

Aceasta este o factură pentru plata taxei de cumpărare de {ITEM_NAME}.

Total de plata: {BALANCE}

Va rugam accesati link-ul de mai jos pentru a accesa pagina de plata:
{LINK}

Activitatea dvs. de pe acest site va fi suspendata pana cand plata va fi efectuata
