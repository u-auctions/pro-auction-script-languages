Buna ziua {W_NAME},

Ați câștigat următoarea licitație pe {SITENAME}

Data Licitatiei

Titlu: {A_TITLE}
Descriere: {A_DESCRIPTION}

Oferta dvs.: {A_CURRENTBID} ({W_WANTED} items)
Beneficiati de: {W_GOT} articol
Sfarsitul Licitatiei: {A_ENDS}
URL: {A_URL}

Info Vanzator

{S_NICK}
{S_EMAIL}
Detaliile de plata ale vanzatorului:
{S_PAYMENT}


Daca ati primit din greseala acest mesaj, va rugam raspundeti printr-un e-mail,
catre {ADMINEMAIL}, sau vizitati {SITENAME} la {SITE_URL}.
