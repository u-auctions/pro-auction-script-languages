Buna ziua {S_NAME},
Felicitari articolul dvs. tocmai s-a vandut!

Mai jos sunt detaliile.

Titlu Licitatiei: {A_TITLE}
Pretul de vanzare: {A_CURRENTBID}
Cantitatea: {A_QTY}
Sfarsitul Licitatiei: {A_ENDS}
URL-ul Licitatiei: {A_URL}

Dute la {SITENAME}: {SITE_URL}user_menu.php

Informatiile cumparatorului
{B_REPORT}
