Buna ziua {REALNAME},
Asa cum ati cerut, am creat o noua parola pentru contul dvs..

Noua dvs. parola: {NEWPASS}

Folositi-o ca sa va autentificati la {SITENAME} si amintiti-va sa o schimbati intr-una pe care o puteti tine minte.
