Buna ziua {S_NAME},

Urmatoarele licitatii care au fost lansate pe {SITENAME} s-au inchis ieri.

{REPORT}


Un e-mail a fost trimis la câștigător cu adresa ta de email.

Dacă ați primit acest mesaj din greșeală, vă rugăm să răspundeți la acest e-mail,
scrie la {ADMINMAIL}, sau vizitati {SITENAME} si {SITE_URL}.
