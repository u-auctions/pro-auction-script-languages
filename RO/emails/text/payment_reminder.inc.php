Buna ziua {C_NAME},

Aceasta este o factură pentru compensarea soldului contului cu site-ul nostru, {SITENAME}.

Compensarea este: {BALANCE}

Vă rugăm să faceți clic pe link-ul de mai jos pentru a accesa pagina de plată:
{LINK}
