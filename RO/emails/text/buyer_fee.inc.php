Buna ziua {C_NAME},

Aceasta este o factură pentru plata taxei de cumpărare de {ITEM_NAME}.

Total de plata: {BALANCE}

Vă rugăm să faceți clic pe link-ul de mai jos pentru a avea acces la pagina de plată:
{LINK}

Toate activitățile de pe site vor fi suspendate până la această plata
