Buna ziua {C_NAME},
Articolul dumneavoastră a fost listat cu succes pe {SITENAME}. Articolul va fi afișat imediat în rezultatele căutării.

Aici sunt detaliile listarii:

Titlul Licitatiei: {A_TITLE}
Tipul Licitatiei: {A_TYPE}
Articol nr: {A_ID}
Oferta minima: {A_MINBID}
Pretul de Rezerva: {A_RESERVE}
Pretul Cumpara Acum: {A_BNPRICE}
Sfarsit Licitatie: {A_ENDS}

{SITE_URL}item.php?id={A_ID}
				
Mai aveti si alt articol la licitatie?
{SITE_URL}select_category.php
