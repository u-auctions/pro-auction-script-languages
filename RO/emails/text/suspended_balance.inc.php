Buna ziua {NAME},

Contul dvs. de pe {SITENAME} a fost suspendat pentru că ai depășit limita datoriei maxime admisă.

Compensarea dvs.: {BALANCE}

Pentru a reactiva contul, va trebui să se îndepărteze soldul contului dvs..
Vă rugăm să vizitați link-ul de mai jos pentru a accesa pagina de plata.
{OUTSTANDING}
