Buna ziua {C_NAME},<br>
<br>
Aceasta este o factură pentru compensarea soldului contului cu site-ul nostru, {SITENAME}.<br>
<br>
Compensarea este: {BALANCE}<br>
<br>
Va rugam [ <a href="{LINK}">apasa aici</a> ] pentru a accesa pagina de plată.
