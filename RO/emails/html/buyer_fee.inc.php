Buna ziua {C_NAME},<br>
<br>
Aceasta este o factură pentru plata taxei de cumpărare de {ITEM_NAME}.<br>
<br>
Total de plata: {BALANCE}<br>
<br>
Vă rugăm să faceți clic pe link-ul de mai jos pentru a avea acces la pagina de plată:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Toate activitățile de pe site vor fi suspendate până la această plata
