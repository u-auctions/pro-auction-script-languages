Buna ziua {NAME},<br>
<br>
Acesta este un răspuns automat de la urmarirea articolului dvs.<br>
Licitatiea de pe lista de urmarire a articolului a fost inchisa.<br>
<br>
Titlul licitatiei: {TITLE}<br>
URL-ul licitatiei: <a href="{URL}">{URL}</a>
