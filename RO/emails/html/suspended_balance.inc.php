Buna ziua {NAME},<br>
<br>
Contul dvs. de pe {SITENAME} a fost suspendat pentru că ai depășit limita datoriei maxime admisă.<br>
<br>
Compensarea dvs: {BALANCE}<br>
<br>
Pentru a reactiva contul, va trebui să se îndepărteze soldul contului dvs.<br>
Vă rugăm să vizitați link-ul de mai jos pentru a accesa pagina de plata.<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a>
