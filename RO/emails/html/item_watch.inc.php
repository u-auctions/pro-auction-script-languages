Buna ziua {REALNAME},<br>
<br>
Acesta este un raspuns automat de la urmarirea articolului dvs. <br>
Cineva are oferta unui articol pe care dvs. l-ati adaugat in lista.<br>
<br>
Titlul Articolului: {TITLE}<br>
<br>
Oferta curenta: {BID}<br>
<br>
URL-ul Licitatiei: <a href="{AUCTION_URL}">{AUCTION_URL}</a>
