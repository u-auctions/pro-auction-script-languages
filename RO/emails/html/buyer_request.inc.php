Buna ziua {NAME} ({NICK}),<br>
Proprietarul unui cont cumpărător a cerut a se comuta la un cont vânzător.<br>
<br>
Detalii utilizator:<br>
---------------<br>
ID-ul utilizatorului: {ID}<br>
Nume Utilizator: {NAME}<br>
Porecla Utilizator: {NICK}<br>
E-mail Utilizator: {EMAIL}
