<b>Aceasta o copie a formularului Contactati-ne trimis la noi.</b><br><br>

Buna ziua {SELLER_NICK},<br>
<br>
Acest mesaj este trimis de la {SITENAME} site.<br>
<br>
Utilizatorul {SENDER_NAME} are o intrebare pentru tine.<br>
<br>
E-mail trimis de:<br>
{SENDER_EMAIL}<br>
<br>
Subiect:<br>
{SUBJECT}<br><br>
Intrebare:<br>
{SENDER_QUESTION}<br>
