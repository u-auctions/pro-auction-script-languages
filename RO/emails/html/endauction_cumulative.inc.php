Buna ziua {S_NAME},<br>
<br>
Urmatoarele licitatii care au fost lansate pe <a href="{SITE_URL}">{SITENAME}</a> s-au inchis ieri.<br>
<br>
{REPORT}
<br>
<br>
Un e-mail a fost trimis la câștigător cu adresa ta de email.<br>
<br>
Dacă ați primit acest mesaj din greșeală, vă rugăm să răspundeți la acest e-mail,
scrie la <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>, sau vizitati {SITENAME} si <a href="{SITE_URL}">{SITE_URL}</a>.
