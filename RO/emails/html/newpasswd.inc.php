Buna ziua {REALNAME},<br>
Asa cum ati cerut, am creat o noua parola pentru contul dvs.<br>
<br>
Noua dvs. parola: {NEWPASS}<br>
<br>
Folositi-o ca sa va autentificati la {SITENAME} si amintiti-va sa o schimbati intr-una pe care o puteti tine minte.
