Buna ziua {C_NAME},<br>
<br>
Aceasta este o factură pentru plata valorii finale a {ITEM_NAME}.<br>
<br>
Total de Plata: {BALANCE}<br>
<br>
Vă rugăm să faceți clic pe link-ul de mai jos pentru a acces la pagina de plată:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Toate activitățile pe site vor fi suspendat până la această plată
