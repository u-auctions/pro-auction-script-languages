Buna ziua {W_NAME},<br>
<br>
Ați câștigat următoarea licitație pe <a href="{SITE_URL}">{SITENAME}</a> <br>
<br>
Data Licitatiei<br>
------------------------------------<br>
Titlul: {A_TITLE}<br>
Descrierea: {A_DESCRIPTION}<br>
<br>
Oferta dvs: {A_CURRENTBID} ({W_WANTED} items)<br>
Beneficiati de: {W_GOT} articol<br>
Sfarsitul Licitatiei: {A_ENDS}<br>
URL: <a href="{A_URL}">{A_URL}</a><br>
<br>
=============<br>
INFO VANZATOR<br>
=============<br>
<br>
{S_NICK}<br>
<a href="mailto:{S_EMAIL}">{S_EMAIL}</a><br>
Detaliile de plata ale vanzatorului:<br>
{S_PAYMENT}<br>		 
<br>
<br>
Daca ati primit din greseala acest mesaj, va rugam raspundeti printr-un e-mail,<br>
scrie la <a href="mailto:{ADMINEMAIL}">{ADMINEMAIL}</a>, sau viziteaza <a href="{SITE_URL}">{SITENAME}</a> la <a href="{SITE_URL}">{SITE_URL}</a>.
