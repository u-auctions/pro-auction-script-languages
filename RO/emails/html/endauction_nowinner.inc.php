<table border="0" width="100%">
	<tr>
		<td colspan="3" height="35"><div style="font-size: 14px; font-weight: bold;">Licitatia dvs. sa incheiat fara nici  un castigator {SITENAME}</div></td>
	</tr>
	<tr>
		<td colspan="3" style="font-size: 12px;">Buna ziua <b>{S_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="3" height="40" style="font-size: 12px; padding-right: 6px;">
		Ne pare rau, dar licitatia creata de dvs {SITENAME} a fost inchisa fara castigatori</td>
	</tr>
	<tr>
		<td width="9%" rowspan="2"><img width="150px" height="150px" border="0" src="{A_PICURL}"></td>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td colspan="2" style="font-size: 12px;"><a href="{A_URL}">{A_TITLE}</a></td>

			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Articol nr:</td>
				<td align="left" style="font-size: 12px;">{A_ID}</td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Final licitatie:</td>
				<td align="left" style="font-size: 12px;">{A_END}</td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">URL-ul licitatiei:</td>
				<td align="left" style="font-size: 12px;"><a href="{A_URL}">{A_URL}</a></td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">&nbsp;</td>
				<td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Dute la {SITENAME}</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="120" valign="top">&nbsp;
		</td>
	</tr>
</table>
