<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Finalizați înscrierea pe {SITENAME} !</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Buna ziua <b>{C_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		Doar faceți clic pe "Activare Me" pentru a finaliza înregistrarea. Odată ce ai făcut asta sunteți gata să  
		începeti sa cumpărati/vindeti pe {SITENAME}
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top"><br /><br />
		<table border="0" width="100%">
			<tr>
				<td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>În cazul în care link-ul nu funcționează:</b></td>

			</tr>
			<tr>
				<td style="font-size: 12px;">Va rugam sa ne trimiteți un email la <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">Continuă pentru a confirma</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{CONFIRMURL}">
		<img border="0" src="{SITEURL}images/email_alerts/Active_Acct_Btn.jpg" width="120" height="32"></a></td>
	</tr>
</table>
