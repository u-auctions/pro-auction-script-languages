<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Cineva doreste sa vizualizeze licitatia dvs.la {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Buna ziua <b>{F_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		 Ați transmise o licitație de la {S_NAME} (<a href="mailto:{S_EMAIL}">{S_EMAIL}</a>) la {SITENAME}
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px; line-height: 0.6cm;">
			<b>Titlul licitatiei:</b> 	{TITLE} <br />
			<b>Comemntariu:</b> {S_COMMENT}<br />
		</td>
		<td width="34%" style="font-size: 12px;">Verifica Licitație!</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{URL}"><img src="{SITEURL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
		</td>
	</tr>
</table>
