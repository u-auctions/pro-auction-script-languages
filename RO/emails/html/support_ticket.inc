Buna ziua {NAME},<br>
<br>
Ați primit un răspuns de la echipa suport.<br>
<br>
<strong>{SITENAME} Suport:</strong> <a href="{A_URL}">Suportul meu</a><br>
<br>
<strong>Website:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Subiect:</strong><br>
{SUBJECT}
<br><br>
<strong>Message:</strong><br>
{MESSAGE}
