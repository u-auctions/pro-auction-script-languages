Buna ziua {SELLER_NICK},<br>
<br>
acest mesaj este trimis de {SITENAME}.<br>
<br>
{SENDER_NAME} de <a href="mailto:{SENDER_EMAIL}">{SENDER_EMAIL}</a> are o intrebare cu privire la licitatia dvs. {TITLE}.<br>
<br>
Intrebare:<br>
{SENDER_QUESTION}<br>
<br>
URL-ul Licitatiei: <a href="{SITEURL}item.php?id={AID}">{SITEURL}item.php?id={AID}</a><br>
<br>
Vă mulțumim pentru a fi o parte a {SITENAME}<br>
<a href="{SITEURL}">{SITEURL}</a><br>
