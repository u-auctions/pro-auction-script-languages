﻿<b>C'est une copie du message CONTACTEZ-NOUS envoyé de votre part.</b><br><br>

Cher {SELLER_NICK},<br>
<br>
Ce message est envoyé de la part de {SITENAME} site.<br>
<br>
L'utilisateur {SENDER_NAME} a une question pour vous.<br>
<br>
L'envoyeur d-email:<br>
{SENDER_EMAIL}<br>
<br>
Sujet:<br>
{SUBJECT}<br><br>
Question:<br>
{SENDER_QUESTION}<br>
