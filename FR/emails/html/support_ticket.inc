﻿Cher {NAME},<br>
<br>
Vous avez reçu une reponse du Groupe de support.<br>
<br>
<strong>{SITENAME} Support:</strong> <a href="{A_URL}">Mon Support</a><br>
<br>
<strong>Website:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Sujet:</strong><br>
{SUBJECT}
<br><br>
<strong>Message:</strong><br>
{MESSAGE}