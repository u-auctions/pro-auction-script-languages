Tisztelt {C_NAME},
Annak érdkében hogy el tudjon kezdeni eladni és/vagy vásárolni a(z) {SITENAME}, a fiókját még az oldal adminisztrátorának ellenõrizni kell.

Hamarosan kap egy E-Mail-t amiben engedélyezzük a fiókját.

Az Ön információi:

Teljes neve: {C_NAME}
Felhsználóneve: {C_NICK}
<!-- IF C_ADDRESS ne '' -->
Címe: {C_ADDRESS}
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
Város: {C_CITY}
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
Állam/Megye: {C_PROV}
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
Ország: {C_COUNTRY}
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
Irányítószám: {C_ZIP}
<!-- ENDIF -->
E-Mail: {C_EMAIL}