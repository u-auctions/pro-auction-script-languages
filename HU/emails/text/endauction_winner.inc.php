Tisztelt {S_NAME},
Gratulálunk, terméke eladásra került!

Eladás részletei.

Aukció címe: {A_TITLE}
Eladási ár: {A_CURRENTBID}
Mennyiség: {A_QTY}
Lezárás dátuma: {A_ENDS}
Aukció URL: {A_URL}

Ugorjon ide {SITENAME}: {SITE_URL}user_menu.php

Vásárló adatai
{B_REPORT}