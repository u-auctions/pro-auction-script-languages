Tisztelt {NAME},<br>
<br>
Válasz érkezett a támogatói csoporttól.<br>
<br>
<strong>{SITENAME} Támogatás:</strong> <a href="{A_URL}">Támogatásaim</a><br>
<br>
<strong>Weboldal:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Tárgy:</strong><br>
{SUBJECT}
<br><br>
<strong>Üzenet:</strong><br>
{MESSAGE}
