Tisztelt {C_NAME},

Ez egy fizetési emlékeztetõ, hogy rendezze fiókjának egyenlegét a(z) {SITENAME} oldalon.

Az Ön egyenlege: {BALANCE}

Kérem kattintson ide, hogy hozzáférjen a fizetési oldalhoz:
{LINK}