Tisztelt {S_NAME},
Sajnáljuk, de az Ön által {SITENAME} oldalon indított aukciója nyertes nélkül zárult le.

Aukció URL: {A_URL}
Aukció címe: {A_TITLE}
Termék #: {A_ID}
Lezárás idõpontja: {A_END}