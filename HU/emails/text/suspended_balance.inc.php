Tisztelt {NAME},


A(z) {SITENAME} oldalon a felhasználói fiókját felfüggesztettük mert tartozása elérte a maximumot.

Ön egyenlege: {BALANCE}

Annak érdekében, hogy újra használhassa fiókját, ki kell egyenlítenie a tartozását.
Kérem látogasson el az alábbi linken keresztül a fizetési oldalra.
{OUTSTANDING}