Tisztelt {S_NAME},

Az alábbi aukciók melyeket a(z) {SITENAME} oldalon elindított, tegnap lezárultak.

{REPORT}


Egy E-Mail-t elküldtünk a nyertesnek az Ön E-Mail címével.

Amennyiben a kapott E-Mail hibás adatokat,információt tartalmazna kérem továbbítsa
az alábbi E-Mail címre {ADMINMAIL}, vagy látogasson el {SITENAME} oldalunkra. {SITE_URL}
