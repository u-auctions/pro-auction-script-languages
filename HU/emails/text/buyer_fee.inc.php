Tisztelt {C_NAME},

Ez egy számla a fizetésrõl a(z) {ITEM_NAME} termék megvásárlásának díjáról.

Összesen fizetendõ: {BALANCE}

Kérem kattintson az alábbi linkre, hogy a fizetési oldalra ugorjon:
{LINK}

Fizetésig minden mûveletet felfüggesztünk az oldalon