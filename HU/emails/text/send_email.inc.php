Tisztelt {SELLER_NICK},
Ezt az üzenetet a(z) {SITENAME} oldalról küldték.

{SENDER_NAME} {SENDER_EMAIL} az alábbi kérdést tette fel {TITLE} aukciójával kapcsolatban.

Kérdés:
{SENDER_QUESTION}

Aukció URL: {SITEURL}item.php?id={AID}