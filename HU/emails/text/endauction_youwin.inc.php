Tisztelt {W_NAME},

Ön megnyerte az aukciót a(z) {SITENAME} oldalon

Aukció adatai

Cím: {A_TITLE}
Leírás: {A_DESCRIPTION}

Licitje: {A_CURRENTBID} ({W_WANTED} items)
Ön megkapja a: {W_GOT} terméket
Aukció lezárása: {A_ENDS}
URL: {A_URL}

Eladó adatai

{S_NICK}
{S_EMAIL}
Eladó fizetési információi:
{S_PAYMENT}


Amennyiben a kapott E-Mail hibás adatokat,információt tartalmazna kérem továbbítsa
az alábbi E-Mail címre {ADMINMAIL}, vagy látogasson el {SITENAME} oldalunkra. {SITE_URL}
