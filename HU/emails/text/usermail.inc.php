<html>
 <head>
         <style type="text/css">
                 body {
                 font-family: Verdana;
                 }
         </style>
 </head>
 <body>
<table border="0" width="100%">
        <tr>
                <td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;"> A(z) {SITENAME} oldalon a regisztráció véglegesítése!</div></td>
        </tr>
        <tr>
                <td colspan="2" style="font-size: 12px;">Tisztelt <b>{C_NAME}</b>,</td>
        </tr>
        <tr>
                <td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">   Csak kattintson a "Activate Me"-ra a regisztráció visszaigazolásához. Ettõl kezdve már elkezdheti a vásárlást/eladást a(z) {SITENAME} oldalon
                </td>
        </tr>
        <tr>
                <td width="55%" rowspan="2" valign="top"><br /><br />
                <table border="0" width="100%">
                        <tr>
                                <td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Ha a link nem mûködik:</b></td>

                        </tr>
                        <tr>
                                <td style="font-size: 12px;">Kérem küldjön levelet<a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a> címre</td>
                        </tr>
                </table>
                </td>
                <td width="34%" style="font-size: 12px;">Megerõsítés</td>
        </tr>
        <tr>
                <td width="34%" height="176" valign="top">
                <a href="{CONFIRMURL}">
                <img border="0" src="{SITEURL}images/email_alerts/Active_Acct_Btn.jpg" width="120" height="32"></a></td>
        </tr>
 </table>
</body>
</html>