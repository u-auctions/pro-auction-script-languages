Tisztelt {C_NAME},
Az Ön termékét sikeresen feltöltöttük a {SITENAME} oldalra. Terméke azonnal megjelenik a keresési listákon.

Közzétételi adatok:

Aukció címe: {A_TITLE}
Aukció típusa: {A_TYPE}
Termékazonosító #: {A_ID}
Nyitóár: {A_MINBID}
Minimumár: {A_RESERVE}
Azonnali megvásárlási ár: {A_BNPRICE}
Lezárás idõpontja: {A_ENDS}

{SITE_URL}item.php?id={A_ID}

Van másik feltölthetõ terméke?
{SITE_URL}select_category.php