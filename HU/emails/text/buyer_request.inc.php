Tisztelt {NAME} ({NICK}),
Az alábbi vásárlói fiókkal rendelkezõ felhasználó kéri fiókjának váltását Eladói-vá.

Felhasználó adatai

Felhasználói azonosító: {ID}
Felhasználóneve: {NAME}
Felhasználó álneve: {NICK}
Felhasználó E-mail címe: {EMAIL}