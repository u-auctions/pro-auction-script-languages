Helló {REALNAME},
Ahogy kérte, egy új jelszót generáltunk fiókjához.

Új jelszó: {NEWPASS}

Most már be tud lépni az {SITENAME} oldalra és ne felejtse el mielõbb megváltoztatni egy könnyebben megjegyezhetõre.