<html>
 <head>
         <style type="text/css">
                 body {
                 font-family: Verdana;
                 }
         </style>
 </head>
 <body>
<table border="0" width="100%">
        <tr>
                <td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Gratulálunk, Ön most már az {SITENAME} oldal tagja!</div></td>
        </tr>
        <tr>
                <td colspan="2" style="font-size: 12px;">Tisztelt <b>{C_NAME}</b>,</td>
        </tr>
        <tr>
                <td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
                Ez a levél megerõsíti, hogy a regisztrációját a(z) {SITENAME} oldalra elfogadtuk! <br />
                Most már be tud lépni a fiókjába és eladhat és/vagy licitálhat. Köszönjük, hogy csatlakozott!
                </td>
        </tr>
        <tr>
                <td width="55%" rowspan="2" valign="top" style="font-size: 12px;"><b>Elkezdi most?</b><br /><br />
                <a href="{SITE_URL}"><img src="{SITE_URL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
                </td>
                <td width="34%" style="font-size: 12px;">&nbsp;</td>
        </tr>
        <tr>
                <td width="34%" height="176" valign="top">&nbsp;
                </td>
        </tr>
 </table>
</body>
</html>