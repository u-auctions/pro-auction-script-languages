Tisztelt {REALNAME},
Ez egy automatikus figyelmeztetés a megfigyelt termékérõl.
Valaki licitált egy olyan termékre mely szerepel a megfigyelõi listáján.

Aukció címe: {TITLE}
Aktuális licit: {BID}

Aukció URL: {AUCTION_URL}