Tisztelt {NAME} ({NICK}),<br>

kérted a vásárlói fiókod eladói fiókká minõsítését.<br>
<br>
Fiókod adatai:<br>
---------------<br>
Azonosító: {ID}<br>
Felhasználónév: {NAME}<br>
Felhasználói álnév: {NICK}<br>
Email cím: {EMAIL}