Tisztelt {S_NAME},<br>
<br>
Az alábbi aukciód <a href="{SITE_URL}">{SITENAME}</a> lezárult.<br>
<br>
{REPORT}
<br>
<br>
Egy email-t küldtünk az aukciót megnyerõnek a te email címeddel.<br>
<br>
Ha hibaüzenet érkezett a levélben akkor kérlek írj az alábbi címre <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>, vagy látogasd meg az alábbi webhelyet a(z) {SITENAME} oldalon <a href="{SITE_URL}">{SITE_URL}</a>.
