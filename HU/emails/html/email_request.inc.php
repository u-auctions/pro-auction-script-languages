Tisztelt {SELLER_NICK},<br>
<br>
Ezt az üzenetet a(z) {SITENAME} oldal küldte.<br>
<br>
A {SENDER_NAME} felhasználótól kérdés érkezett.<br>
<br>
Küldõ email címe:<br>
{SENDER_EMAIL}<br>
<br>
Tárgy:<br>
{SUBJECT}<br><br>
Kérdés:<br>
{SENDER_QUESTION}<br>
