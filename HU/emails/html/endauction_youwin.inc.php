Tisztelt {W_NAME},<br>
<br>
Megnyert egy aukció meghírdetett terméket <a href="{SITE_URL}">{SITENAME}</a> <br>
<br>
Aukció adatai<br>
------------------------------------<br>
Cím: {A_TITLE}<br>
Leírás: {A_DESCRIPTION}<br>
<br>
Licitje: {A_CURRENTBID} ({W_WANTED} items)<br>
Kapott: {W_GOT} terméket<br>
Aukció lezárásának dátuma: {A_ENDS}<br>
URL: <a href="{A_URL}">{A_URL}</a><br>
<br>
=============<br>
ELADÓ ADATAI <br>
=============<br>
<br>
{S_NICK}<br>
<a href="mailto:{S_EMAIL}">{S_EMAIL}</a><br>
Eladó fizetési adatai:<br>
{S_PAYMENT}<br>
<br>
<br>
Amennyiben a kapott levél hibás vagy helytelen adatokat tartalmaz kérem küldje vissza <a href="mailto:{ADMINEMAIL}">{ADMINEMAIL}</a> E-Mail címre, vagy látogasson el <a href="{SITE_URL}">{SITENAME}</a> at <a href="{SITE_URL}">{SITE_URL}</a> oldalunkra.
