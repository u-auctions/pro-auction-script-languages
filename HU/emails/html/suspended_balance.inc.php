Tisztelt {NAME},<br>
<br>
A(z) {SITENAME} oldalon a felhasználói fiókját felfüggesztettük mert tartozása elérte a maximumot.<br>
<br>
Ön egyenlege: {BALANCE}<br>
<br>
Annak érdekében, hogy újra használhassa fiókját, ki kell egyenlítenie a tartozását.<br>
Kérem látogasson el az alábbi linken keresztül a fizetési oldalra.<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a>