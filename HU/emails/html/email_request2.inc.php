<b>Ez a nekünk küldött levél másolata.</b><br><br>

Tisztelt {SELLER_NICK},<br>
<br>
Ez az üzenet a {SITENAME} oldalról érkezett.<br>
<br>
A(z) {SENDER_NAME} felhasználó az alábbi kérdést tette fel.<br>
<br>
Küldõ email címe:<br>
{SENDER_EMAIL}<br>
<br>
Tárgy:<br>
{SUBJECT}<br><br>
Kérdés:<br>
{SENDER_QUESTION}<br>
