<table border="0" width="100%">
        <tr>
                <td colspan="3" height="35"><div style="font-size: 14px; font-weight: bold;">A terméked feltöltése sikeres volt a {SITENAME} oldalára!</div></td>
        </tr>
        <tr>
                <td colspan="3" style="font-size: 12px;">Tisztelt <b>{C_NAME}</b>,</td>
        </tr>
        <tr>
                <td colspan="3" height="50" style="font-size: 12px; padding-right: 6px;">A terméked a(z) {SITENAME} oldalára feltöltésre került. Hamarosan megtalálható lesz a keresési listákon.<br>Közzétételi információk:</td>
        </tr>
        <tr>
                <td width="9%" rowspan="2"><img width="150" height="150" border="0" src="{SITE_URL}{A_PICURL}"></td>
                <td width="55%" rowspan="2">
                <table border="0" width="100%">
                        <tr>
                                <td colspan="2" style="font-size: 12px;"><a href="{SITE_URL}item.php?id={A_ID}">{A_TITLE}</a></td>

                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;">Aukció típusa:</td>
                                <td align="left" style="font-size: 12px;">{A_TYPE}</td>
                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;">Termék #:</td>
                                <td align="left" style="font-size: 12px;">{A_ID}</td>
                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;">Kezdõ ár:</td>
                                <td align="left" style="font-size: 12px;">{A_MINBID}</td>
                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;">Reserve price:</td>
                                <td align="left" style="font-size: 12px;">{A_RESERVE}</td>
                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;">Fix ár:</td>
                                <td align="left" style="font-size: 12px;">{A_BNPRICE}</td>
                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;">Aukció vége:</td>
                                <td align="left" style="font-size: 12px;">{A_ENDS}</td>
                        </tr>
                        <tr>
                                <td width="22%" style="font-size: 12px;"></td>
                                <td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Fiókom {SITENAME}</a></td>
                        </tr>
                </table>
                </td>
                <td width="34%" style="font-size: 12px;">Van másik terméke is melyet el szeretne adni?</td>
        </tr>
        <tr>
                <td width="34%" height="176" valign="top">
                        <a href="{SITE_URL}select_category.php">
                                <img border="0" src="{SITE_URL}images/email_alerts/Sell_More_Btn.jpg" width="120" height="32">
                        </a>
                </td>
        </tr>
</table>