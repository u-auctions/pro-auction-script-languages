Tisztelt {C_NAME},<br>
<br>

Ez egy kifizetendõ számla mely a {ITEM_NAME} terméked eladási díját tartalmazza.<br>
<br>
Fizetendõ: {BALANCE}<br>
<br>
Kérlek az alábbi linken egyenlítsd ki az tartozásod:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
A weboldalon minden mûveletedet felfüggesztjük a tartzás rendezéséig!