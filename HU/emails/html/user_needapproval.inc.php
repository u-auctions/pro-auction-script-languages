<table border="0" width="100%">
        <tr>
                <td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Köszönjük, hogy regisztrált a(z) {SITENAME} oldalra!</div></td>
        </tr>
        <tr>
                <td colspan="2" style="font-size: 12px;">Tisztelt <b>{C_NAME}</b>,</td>
        </tr>
        <tr>
                <td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
                Annak érdkében hogy el tudjon kezdeni eladni és/vagy vásárolni a(z) {SITENAME}, a fiókját
                még az oldal adminisztrátorának ellenõrizni kell. <br /><br />
                Hamarosan kap egy E-Mail-t amiben engedélyezzük a fiókját.<br>
                </td>
        </tr>
        <tr>
                <td width="55%" rowspan="2">
                <table border="0" width="100%">
                        <tr>
                                <td colspan="2" style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Az Ön információi:</b></td>

                        </tr>
                        <tr>
                                <td width="17%" style="font-size: 12px;">Teljes neve:</td>
                                <td align="left" style="font-size: 12px;">{C_NAME}</td>
                        </tr>
                        <tr>
                                <td width="17%" style="font-size: 12px;">Felhasználó neve:</td>
                                <td align="left" style="font-size: 12px;">{C_NICK}</td>
                        </tr>
<!-- IF C_ADDRESS ne '' -->
                        <tr>
                                <td width="17%" style="font-size: 12px;">Címe:</td>
                                <td align="left" style="font-size: 12px;">{C_ADDRESS}</td>
                        </tr>
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
                        <tr>
                                <td width="17%" style="font-size: 12px;">Város:</td>
                                <td align="left" style="font-size: 12px;">{C_CITY}</td>
                        </tr>
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
                        <tr>
                                <td width="17%" style="font-size: 12px;">Állam/Megye:</td>
                                <td align="left" style="font-size: 12px;">{C_PROV}</td>
                        </tr>
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
                        <tr>
                                <td width="17%" style="font-size: 12px;">Ország:</td>
                                <td align="left" style="font-size: 12px;">{C_COUNTRY}</td>
                        </tr>
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
                        <tr>
                                <td width="17%" style="font-size: 12px;">Irányítószám:</td>
                                <td align="left" style="font-size: 12px;">{C_ZIP}</td>
                        </tr>
<!-- ENDIF -->
                        <tr>
                                <td width="17%" style="font-size: 12px;">E-Mail:</td>
                                <td align="left" style="font-size: 12px;">{C_EMAIL}</td>
                        </tr>
                        <tr>
                                <td width="17%" style="font-size: 12px;"></td>
                                <td align="left" style="font-size: 12px;">&nbsp;</td>
                        </tr>
                </table>
                </td>
                <td width="34%" style="font-size: 12px;">&nbsp;</td>
        </tr>
        <tr>
                <td width="34%" height="176" valign="top">&nbsp;
                </td>
        </tr>
</table>