Tisztelt {C_NAME},<br>
<br>
Ez egy számla a {ITEM_NAME} tétel végleges díjjáról.<br>
<br>
Összesen fizetendõ: {BALANCE}<br>
<br>
Kattintson az alábbi linkre a saját fizetési oldalához:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Minden aktivitását a weboldalunkon korlátozzuk míg nem egyenlíti ki tartozását
