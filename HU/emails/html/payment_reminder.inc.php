Tisztelt {C_NAME},<br>
<br>
Ez egy fizetési emlékeztetõ az Ön egyenlegérõl a(z) {SITENAME} oldalon.<br>
<br>
Jelenlegi egyenleg: {BALANCE}<br>
<br>
Kérem [ <a href="{LINK}">lépjen be</a> ] mielõbb és egyenlítse ki tartozását.