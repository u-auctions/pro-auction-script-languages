<table border="0" width="100%">
        <tr>
                <td colspan="3" height="35"><div style="font-size: 14px; font-weight: bold;">Gratulálunk, terméked elkelt!</div></td>
        </tr>
        <tr>
                <td colspan="3" style="font-size: 12px;">Tisztelt <b>{S_NAME}</b>,</td>
        </tr>
        <tr>
                <td colspan="3" height="50" style="font-size: 12px; padding-right: 6px;"><i>Gratulálunk</i> a terméked elkelt!
                Részletek lent.</td>
        </tr>
        <tr>
                <td width="9%" rowspan="2"><img width="150px" height="150px" border="0" src="{SITE_URL}{A_PICURL}"></td>
                <td width="55%" rowspan="2">
                <table border="0" width="100%">
                        <tr>
                                <td colspan="2" style="font-size: 12px;"><a href="{A_URL}">{A_TITLE}</a></td>

                        </tr>
                        <tr>
                                <td width="18%" style="font-size: 12px;">Eladási ár:</td>
                                <td align="left" style="font-size: 12px;">{A_CURRENTBID}</td>
                        </tr>
                        <tr>
                                <td width="18%" style="font-size: 12px;">Mennyiség:</td>
                                <td align="left" style="font-size: 12px;">{A_QTY}</td>
                        </tr>
                        <tr>
                                <td width="18%" style="font-size: 12px;">Aukció vége:</td>
                                <td align="left" style="font-size: 12px;">{A_ENDS}</td>
                        </tr>
                        <tr>
                                <td width="18%" style="font-size: 12px;">Aukció URL:</td>
                                <td align="left" style="font-size: 12px;"><a href="{A_URL}">{A_URL}</a></td>
                        </tr>
                        <tr>
                                <td width="18%" style="font-size: 12px;"></td>
                                <td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Ugorjon {SITENAME}</a></td>
                        </tr>
                </table>
                </td>
                <td width="34%" style="font-size: 12px;">Fizetési részletek</td>
        </tr>
        <tr>
                <td width="34%" height="90" valign="top">
                <a href="{SITE_URL}buying.php">
                <img border="0" src="{SITE_URL}images/email_alerts/Total_Due_Btn.jpg" width="120" height="32"></a></td>
        </tr>
 </table><br />

<table border="0" width="100%">
        <tr>
                <td style="font-size: 12px;"><b>Vevõ információi</b></td>
        </tr>
        <tr>
                <td style="font-size: 12px;">{B_REPORT}</td>
        </tr>
</table><br />
<div style="font-size: 12px;"><i>Egy levelet küldtünk a gyõztesnek is az Ön E-Mail címével.</i></div>