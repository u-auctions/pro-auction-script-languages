<?php
/*******************************************************************************
 *   copyright                                : (C) 2011 - 2014 uAuctions
 *   site                                        : http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the
 *   ubidzz.com website ask for a refund.
*******************************************************************************
******************************************************************************
 * If you bought this script from the ubidzz.com
 * Please register at u-Auctions.com and contact the uAuctions admin
 * at u-Auctions.com with your order number and name and member name that
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
*******************************************************************************/

if (!defined('InuAuctions')) exit();

// CHARSET ENCODING
// Change the charset according to the language used in this file.
$CHARSET = "UTF-8";
// DOCUMENT DIRECTION
// Change the $DOCDIR variable below according to the document direction needed
// by the language you are using.
// Possible values are:
// - ltr (default) - means left-to-right document (almost any language)
// - rtl - means right-to-left document (i.e. arabic, hebrew, ect).
$DOCDIR = "ltr";
// Error messages and user interface messages are below. Translate them taking care of leaving
// The PHP and HTML tags unchanged.
// Error messages =============================================================
$ERR_001 = "Adatbázis elérési hiba. Kérem lépjen kapcsolatba az oldal üzemeltetõjével.";
$ERR_002 = "Hibás név";
$ERR_003 = "Hibás felhasználónév";
$ERR_004 = "Hibás jelszó";
$ERR_005 = "Kérem, ismételje meg a jelszót";
$ERR_006 = "A jelszavak nem egyeznek";
$ERR_007 = "Az e-mail cím hibás";
$ERR_008 = "Kérem, adjon meg valós e-mail címet";
$ERR_009 = "A megadott felhasználónév már regisztrált";
$ERR_010 = "A felhasználónév túl rövid (min 6 karakter)";
$ERR_011 = "A jelszó túl rövid (min 6 karakter)";
$ERR_012 = "Helytelen cím";
$ERR_013 = "Helytelen város";
$ERR_014 = "Helytelen ország";
$ERR_015 = "Helytelen irányítószám";
$ERR_016 = "Kérem, adjon meg valós irányítószámot";
$ERR_017 = "Helyteleb terméknév";
$ERR_018 = "Helytelen termékleírás";
$ERR_019 = "Helytelen kezdõlicit";
$ERR_020 = "Hibás érték a minimum mennyiségben";
$ERR_021 = "Minimumár";
$ERR_022 = "A megadott Minimumár helytelen";
$ERR_023 = "Válasszon kategóriát a termékhez";
$ERR_024 = "Válasszon fizetési módot";
$ERR_025 = "Hibás felhasználó";
$ERR_026 = "Helytelen jelszó";
$ERR_027 = "Helytelen devizajel";
$ERR_028 = "Kérem, adjon meg valós e-mail címet";
$ERR_029 = "A felhasználói adatok már regisztrálva vannak";
$ERR_030 = "Az alábbi mezõ csak számot tartalmazhat az alábbi formában: nnnn.nn";
$ERR_031 = "Az adatlap kitöltése nem teljes";
$ERR_032 = "Az egyik vagy mindkettõ e-mail címe nem helyes";
$ERR_033 = "Hibás ellenõrzõ link";
$ERR_034 = "A te licitednek többnek kell lennie: ";
$ERR_035 = "A nap mezõbe csak szám írható";
$ERR_036 = "Az eladó nem licitálhat a saját aukciójára";
$ERR_037 = "A keresési kulcsszó nem lehet üres";
$ERR_038 = "Helytelen bejelentkezés";
$ERR_039 = "Te már jóváhagyott regisztrációval rendelkezel.";
$ERR_040 = "Tied a nyerõ licit és nem adhatsz meg alacsonyabb licitet a korábban megadottnál.";
$ERR_041 = "Kérlek, értékeld 1 és 5 között";
$ERR_042 = "A megjegyzése hiányzik";
$ERR_043 = "Hibás mezõformátum: csak szám lehet";
$ERR_044 = "A felhasználónak tagja kell, hogy legyen legalább egy csoportnak";
$ERR_047 = "A szükséges adatok hiányosak";
$ERR_048 = "Hibás belépés";
$ERR_049 = "A kategórianév nem lehet üres";
$ERR_050 = "Legalább egy csoportot 'Automatikus csatlakozás'-sal engedélyezned kell";
$ERR_051 = "Kérem, adja meg a megfelelõ darabszámot a számból";
$ERR_054 = "Kérem, mindkét jelszó mezõt töltse ki";
$ERR_055 = "A <i>%s</i> felhasználó már megtalálható az adatbázisban";
$ERR_056 = "A licitlépcsõ mértéke hibás";
$ERR_057 = "A licitlépcsõ értéke csak szám lehet";
$ERR_058 = "Hibás pénzformátum.";

$ERR_059 = "In Dutch Auctions you may not place a bid if your previous
<b>amount of bid times the quantity</b>
is greater than your
<b>amount of current bid times the quantity</b>.";

$ERR_060 = "A kezdés dátuma nem tehetõ korábbi dátumra.";
$ERR_061 = "A megadott 'Azonnali ár' nem megfelelõ";

$ERR_062 = "A holland aukcióban nem adható meg Minimumár";
$ERR_063 = "A holland aukcióban nem állítható be a licitlépcsõ mértéke";
$ERR_064 = "A holland aukcióban nem használható a 'Vásárlás azonnal'";
$ERR_065 = "Hiba az adatok módosításakor";
$ERR_066 = "Hiba az adatok törlésekor";
$ERR_067 = "Hiányzó kötelezõ adatok (minden adat kitöltése kötelezõ).";
$ERR_068 = "A kiválasztott téma nem elérhetõ";
$ERR_069 = "Az deviza konverzió hibás";
$ERR_070 = "Ez az üzenet nem elérhetõ";
$ERR_071 = "A felhasználónév hibás, csak számok és betûk használhatók";
$ERR_072 = "A licit értékét meg kell adnod";
$ERR_073 = "Licit nem adható meg olyan aukcióra ami még nem indult el";
$ERR_074 = "Az értékelést már megadtad ehhez a tranzakcióhoz";
$ERR_075 = 'Nincs licit';
$ERR_076 = 'Hibás Felhasználónév/E-mail páros';
$ERR_077 = 'Ez a kapcsolódási azonosító lejárt';
$ERR_078 = 'A ÁSZF-t el kell fogadni';
$ERR_079 = 'Az internetes sütikkel kapcsolatos irányelvet el kell fogadni';

$ERR_100 = "Ez a felhasználó nem található";
$ERR_101 = "Hibás jelszó";
$ERR_102 = "Ez a felhasználó nem található";
$ERR_103 = "Nem értékelheted saját magad";
$ERR_104 = "Minden adat kötelezõ";
$ERR_105 = "A felhasználónév nem található";
$ERR_106 = "<br><br>Nincs megadott felhasználó";
$ERR_107 = "A felhasználónév túl rövid";
$ERR_108 = "A jelszó túl rövid";
$ERR_109 = "A jelszavak nem egyeznek";
$ERR_110 = "Az E-mail cím helytelen";
$ERR_111 = "Ilyen felhasználó már létezik";
$ERR_112 = "Hibás adat";
$ERR_113 = "18 éves el kell hogy múlj, hogy regisztrálhass";
$ERR_114 = "Nincs aukció ebben a kategóriában";
$ERR_115 = "Ez az E-mail cím már használatban van";
$ERR_115_a = "Ez E-mail cím által használt domain név nem engedélyezett";
$ERR_115_b = "A fizetési információ már használatban";
$ERR_117 = "Hibás születési dátum";
$ERR_122 = "Aukció nem található";
$ERR_600 = 'Hibás aukciótípus';
$ERR_601 = "A mennyiségi mezó helytelen";
$ERR_602 = "Csak GIF, JPG és PNG képet fogadunk el";
$ERR_603 = "A kép túl nagy.";
$ERR_606 = "Hibás aukció";
$ERR_607 = "A licited nem éri el a minimum licitértéket";
$ERR_608 = "A megadott mennyiség nem érvényes, nem licitálhatsz több mennyiségre annál, mint amennyi létezik";
$ERR_608a = "A megadott mennyiség nem értelmezhetõ, kérem adjon meg valós értéket";
$ERR_609 = "A felhasználó nem található";
$ERR_610 = "Adja meg a felhasználónevét és jelszavát";
$ERR_611 = "Hibás jelszó";
$ERR_612 = "Nem licitálhat, mert Ön az eladó!";
$ERR_614 = "Ez az aukció már lezárt";
$ERR_616 = "A postai irányítószám túl rövid";
$ERR_617 = "Helytelen telefonszám";
$ERR_618 = "A fiók felfüggesztésre került az adminisztrátor által.";
$ERR_619 = "Ez az aukció felfüggesztett";
$ERR_620 = "A fiókja nincs aktiválva, kérem ellenõrizze levelezési fiókját az aktiválás miatt.";
$ERR_621 = "A fiókot az adminisztrátor aktiválta.";
$ERR_622 = "Aukció nem található.";
$ERR_623 = "Az aukció amit el próbál érni, vagy soha nem létezett vagy idõközben eltávolították.";
$ERR_624 = "Nincs elküldhetõ üzenet";
$ERR_700 = "Hibás dátum formátum";
$ERR_701 = "Hibás mennyiség (0-nál nagyobb kell, hogy legyen).";
$ERR_702 = "Ennek a licitnek nagyobbnak kell lennie a minimum licitnél.";
$ERR_704 = "<br>Nem adhat le értékelést errõl a felhasználóról!<br>Ez az aukció még nincs lezárva!";
$ERR_705 = "Csak akkor adhat le értékelést, ha van lezárt aukciója ezzel a felhasználóval!";
$ERR_706 = "<i>A képek maximális darabszáma</i> szám kell, hogy legyen.";
$ERR_707 = "<i>A kép maximális mérete</i> nem lehet nulla.";
$ERR_708 = "<i>A kép maximális filem-mérete</i> szám kell, hogy legyen.";
$ERR_709 = "A feltöltött kép túl nagy. Nem haladhatja meg ";
$ERR_710 = "Hibás file-típus. Elfogadott kép-típusok: GIF, PNG and JPEG";
$ERR_711 = "Nem vásárolhat, mert eladó!";
$ERR_712 = "<b>Fixáras</b> nem elérhetõ ehhez az aukcióhoz";
$ERR_713 = 'Az értéknek kisebbnek kell lennie, mint ';
$ERR_714 = 'A meghosszabbítás értéke szám kell, hogy legyen';
$ERR_715 = 'A Meghosszabítás nem választható sokszor';
$ERR_5000 = "Az üzenetek megjelenítése szám kell, hogy legyen";
$ERR_5001 = "Az üzenetek megjelenítése nem lehet nulla";
$ERR_5002 = "Legalább egy statisztikai típust meg kell adnod (hozzáférés, böngészõ &amp; platform, ország)";
$ERR_5014 = "A tárgy vagy az üzenet hibás";
$ERR_5045 = "A Minimumár nem lehet kevesebb, mint a minimális licit";
$ERR_5046 = "A <b>Vásárlás azonnal</b> ár nem lehet alacsonyabb a minimum licitnél és/vagy a Minimumár";
$ERR_25_0001 = "Kérem válasszon alkategóriát";
$ERR_25_0002 = "<p>Az URL alapú file-hozzáférés ezen a kiszolgálon nem engedélyezett a verzió-ellenõrzõ nem futtatható</p>";

// UI Messages =============================================================
$MSG['000'] = "n.a.";
$MSG['001'] = "Új felhasználó regisztrációja";
$MSG['002'] = "Ön neve";
$MSG['003'] = "Felhasználónév";
$MSG['004'] = "Jelszó";
$MSG['005'] = "Kérem ismételje meg a jelszót";
$MSG['006'] = "Ön E-mail címe";
$MSG['007'] = "Elküld";
$MSG['008'] = "Törlés";
$MSG['009'] = "Lakcím";
$MSG['010'] = "Város";
$MSG['011'] = "Állam/Megye";
$MSG['012'] = "Irányítószám";
$MSG['013'] = "Telefon";
$MSG['014'] = "Ország";
$MSG['015'] = "--Válasszon";
$MSG['017'] = "Tétel címe";
$MSG['018'] = "Tétel leírása";
$MSG['019'] = "Alapértelmezett kép";
$MSG['020'] = "Kikiáltási ár";
$MSG['021'] = "Minimumár";
$MSG['022'] = "Aució hossza";
$MSG['023'] = "Szállítási díj";
$MSG['024'] = "Ön új jelszava";
$MSG['025'] = "Szállítási feltételek";
$MSG['026'] = "Fizetési módok";
$MSG['027'] = "Válasszon kategóriát";
$MSG['028'] = "Tétel eladása";
$MSG['029'] = "Nem";
$MSG['030'] = "Igen";
$MSG['031'] = "Vásárló fizeti a szállítást";
$MSG['032'] = "Eladó fizeti a szállítást";
$MSG['033'] = "Nemzetközi szállítás";
$MSG['034'] = "Aukció elõképe";
$MSG['035'] = "Alaphelyzet";
$MSG['036'] = "Adatok elküldése";
$MSG['037'] = "Kép nem elérhetõ";
$MSG['038'] = "Irányár";
$MSG['039'] = "Nincs Minimumár";
$MSG['040'] = "Aukció elküldése";
$MSG['041'] = "Tétel kategória";
$MSG['043'] = "Nincs nemzetközi szállítás";
$MSG['044'] = "Kérem adja meg a felhasználónevét és jelszavát az adatok elküldéséhez.";
$MSG['045'] = "Felhasználókezelõ";
$MSG['046'] = "Továbblépés a <a href='sell.php?mode=recall'>változtatások érvényesítéséhez</a> az aukciójában";
$MSG['047'] = " új";
$MSG['048'] = "Felhasználói regisztrációs adatlap";
$MSG['049'] = "Amennyiben nem regisztrált, ";
$MSG['050'] = "(minimum 6 karakter)";
$MSG['051'] = "Fõoldal";
$MSG['052'] = "Belép";
$MSG['053'] = "Adminisztrációs E-mail szerkesztése";
$MSG['054'] = "Új E-mail elküldése";

$MSG['055'] = "Edit the admin e-mail address below";

$MSG['056'] = "E-mail cím módosítva";
$MSG['057'] = "Edit the currency symbol below";
$MSG['058'] = "Új pénznem rögzítése";
$MSG['059'] = "Email elküldve";
$MSG['060'] = "Pénznem jele módosítva";
$MSG['067'] = "Nyitott aukciók megjelenítése";
$MSG['068'] = "Egyedi licitlépcsõ engedélyezése";
$MSG['069'] = "Aukció hossza";
$MSG['070'] = "A felhasználó beállíthatja az egyedi licitlépcsõket az aukcióiban(ez a minimum különbség két licit között)";
$MSG['071'] = "Módosít";
$MSG['072'] = " a felhasználó az értékelésedre vár";
$MSG['073'] = "Sorok törlése";
$MSG['074'] = "Használja a törlés pipákat majd a TÖRLÉS gombot a sorok eltávolításához. Egyszerûen szerkessze a szöveges mezõket majd nyomja le a MÓDOSÍT gombot a tároláshoz.";
$MSG['075'] = "Fizetési módok";
$MSG['076'] = "Pénznem jel";
$MSG['077'] = "Adminisztrációs E-mail cím szerkesztése";
$MSG['078'] = "Kategóriák tábla";
$MSG['079'] = "Az aukciód lezárult";
$MSG['080'] = "Újrszinkronizálás vagy a gyorstár törlése";
$MSG['081'] = "Országok tábla";
$MSG['082'] = "Átalakít";
$MSG['083'] = "pénznemnek";
$MSG['084'] = "Üzenet elküldve";
$MSG['085'] = "::: Pénznemváltó :::";
$MSG['086'] = "Kategóriák tábla módosítva";
$MSG['087'] = "Leírás";
$MSG['088'] = "ebbe a pénznembe";

$MSG['089'] = "Process changes";

$MSG['090'] = "Országok tábla módosítva";
$MSG['091'] = "Nyelvezet módosítása";
$MSG['092'] = 'A fizetési módok rögzítéséhez, módosításához, törléséhez használja az alábbi ûrlapot.Ezeket az alternatív fizetési módok beállítások, a beépített fizetések engedélyezéséhez/tíltásához használja a <a href="fee_gateways.php">beállítások</a> oldalt.';
$MSG['093'] = "Fizetési módok tábla módosítva";
$MSG['094'] = "Az országok módosításához/törléséhez/rögzítéséhez használja az alábbi ûrlapot.";
$MSG['095'] = "Üdvözöljük, Ön most már tag!";
$MSG['096'] = "Aktuális nyelvezet";
$MSG['097'] = "Napok";
$MSG['098'] = "Regisztráció megerõsítése";
$MSG['099'] = "Az aukciód jóváhagyásra került";
$MSG['100'] = "<h3>Az aukciód elkészült. A megerõsítõ E-mail-t elküldtük a postafiókodba.</h3>";
$MSG['101'] = "Az aukcióid megjelenítése";
$MSG['102'] = 'A(z) #%s aukciód lezárult %s';
$MSG['103'] = " Keresés ";
$MSG['104'] = "Böngészés ";
$MSG['105'] = "Elõzmények";
$MSG['106'] = "Elküldés barátnak";
$MSG['107'] = "Felhasználói E-mail-k";
$MSG['108'] = "Kép megjelenítése";
$MSG['109'] = "Nap";
$MSG['110'] = "Admin";
$MSG['111'] = "Aukció megnyílt";
$MSG['112'] = "Aukció lezárult";
$MSG['113'] = "Aukció azonosító";
$MSG['114'] = "Kép nemtalálható";
$MSG['115'] = "Licitál most!";
$MSG['116'] = "Aktuális licit";
$MSG['117'] = "Legmagasabb licitáló";
$MSG['118'] = "Ends within";
$MSG['119'] = "# of bids";
$MSG['120'] = "Licitlépcsõ";
$MSG['121'] = "Adja meg a licitjét:";
$MSG['122'] = "A aukció idõtartamok szerkesztéséhez/törléséhez/rögzítéséhez használja az alábbi ûrlapot";
$MSG['123'] = "Idõtartamok tábla módosítva";
$MSG['124'] = "Minimális licit";
$MSG['125'] = "Eladó";
$MSG['126'] = " napok, ";
$MSG['126b'] = " nap, ";
$MSG['126a'] = "nap óta";
$MSG['127'] = "Kikiáltási ár";
$MSG['128'] = "Licitlépcsõ";
$MSG['129'] = "Azonosító";
$MSG['130'] = "Licit";
$MSG['131'] = "Vásárló";
$MSG['132'] = "Kategória honosító tábla";
$MSG['133'] = "Licitlépcsõ tábla";

$MSG['135'] = "Edit, delete or add increments using the form below.<br>
                        Be careful, there's no control over the table's values congruence.
                        You must take care to check it yourself. The only data check performed is over the fields content (must be numeric) but the relation between them is not checked.";

$MSG['136'] = "és";
$MSG['137'] = "Lépések";
$MSG['138'] = "Vissza az aukcióhoz";
$MSG['139'] = "Aukció elküldése egy ismerõsnek";
$MSG['140'] = "Az ismerõs neve";
$MSG['141'] = "Ismerõs E-Mail címe";
$MSG['142'] = "Kiemelt termékek engedélyezése";
$MSG['143'] = "Ön E-Mail címe";
$MSG['144'] = "Megjegyzés hozzáadása";
$MSG['145'] = "Elküldeni ismerõsnek";
$MSG['146'] = "Ezt az aukciót elküldtük az alábbi címre ";
$MSG['147'] = "Elküldeni egy másik ismerõsnek";
$MSG['148'] = "Segítség";
$MSG['149'] = "Az alábbi ûrlappal tudsz kapcsolatba lépni ezzel a felhasználóval.";
$MSG['150'] = "Kérés elküldése";
$MSG['151'] = " A kért E-Mail ";
$MSG['152'] = "Licit jóváhagyása";
$MSG['153'] = "A licitáláshoz regisztrálnod kell.";
$MSG['154'] = "Licitáltál:";
$MSG['155'] = "Termék:";
$MSG['156'] = "Licited:";
$MSG['157'] = "Allows sellers to make their auctions featured on the homepage and category pages";
$MSG['158'] = "Saját licit elküldése";
$MSG['159'] = "Licitáló:";
$MSG['160'] = "Lépések tábla módosítva";
$MSG['161'] = "A kategóriák módosítását/törlését/beszúrását az alábbi ûrlappal végezheti el.";
$MSG['162'] = "Kiemelt termékek engedélyezése";
$MSG['163'] = "Regisztráljon!";

$MSG['164'] = "Allows sellers to make their auctions highlighted (displayed listing in a different colour in search results etc)";

$MSG['165'] = "Kategória: ";
$MSG['166'] = "Fõoldal";
$MSG['167'] = "Kép";
$MSG['168'] = "Aukció";
$MSG['169'] = "Aktuális licit";
$MSG['170'] = "Licitek #";
$MSG['171'] = "Ends in";
$MSG['171a'] = "Befejezve";
$MSG['172'] = "Nincs aktív aukció ebben a kategóriában";
$MSG['173'] = "Keresés eredménye: ";
$MSG['174'] = "Vastagított termékek engedélyezése";
$MSG['175'] = "Dátum és idõpont";
$MSG['176'] = "Licitáló";
$MSG['177'] = "Kategória-index";
$MSG['178'] = "Kapcsolat a licitálóval";
$MSG['179'] = "Ahhoz, hogy megkapja a felhasználó E-Mail címét kérem adja meg felhasználónevét és jelszavát.";
$MSG['180'] = " a(z):";
$MSG['181'] = "Felhasználói belépés";
$MSG['182'] = "Személyes adatok módosítása";
$MSG['183'] = "Személyes adatai módosítva";
$MSG['184'] = "Kategóriák tábla módosítva.";
$MSG['185'] = "Aktuálisan megtekintett visszajelzések ";
$MSG['186'] = "<a href=\"javascript:history.back()\">Vissza</a>";
$MSG['187'] = "Felhasználóneve";
$MSG['188'] = "Jelszava";
$MSG['189'] = "Teljes összeg";
$MSG['190'] = "Saját termékkategóriái";
$MSG['191'] = "Szerzõi szövege";
$MSG['192'] = "Ezt az üzenetet hozzáadjuk mindegyik oldal aljára";
$MSG['193'] = "Aukció idõtartama";
$MSG['194'] = "Vastagított feliratú aukciók engedélyezése az eladóknak (kivastagított betûvel jeleníti meg az aukciót a keresési eredményekben, stb.)";
$MSG['195'] = "Kép URL";
$MSG['196'] = "Termék leírása";
$MSG['197'] = "Aukció címe";
$MSG['198'] = "Termék nem található";
$MSG['199'] = "Keresés";
$MSG['200'] = "Helló, ";
$MSG['201'] = "Új felhasználó";
$MSG['202'] = "Felhasználó adatai";
$MSG['203'] = "Nyitott aukciók";
$MSG['204'] = "Lezárt aukciók";
$MSG['205'] = "Saját vezérlõpult";
$MSG['206'] = "Felhasználói adatlap";
$MSG['207'] = "Értékelés leadása";
$MSG['208'] = "Értékelés megtekintése";
$MSG['209'] = "Regisztrálva: ";
$MSG['210'] = "Kapcsolat ";
$MSG['211'] = "Aukció megnyitása most";
$MSG['212'] = "Aukciók:";
$MSG['213'] = "Nyitott aukciók megtekintése";
$MSG['214'] = "Lezárt aukciók megtekintése";
$MSG['215'] = "Elfelejtette jelszavát?";
$MSG['216'] = "Amennyiben elfelejtette jelszavát, kérem adja meg felhasználónevét és E-Mail címét itt. Egy új jelszót hozunk létre amit E-Mail-ben elküldünk";
$MSG['217'] = "Az új jelszót a feladtuk az E-Mail címére.";
$MSG['218'] = "Felhasználói adatlap megtekintése";
$MSG['219'] = "Nyitott aukciók: ";
$MSG['220'] = "Lezárt aukciók: ";
$MSG['221'] = "Felhasználó belépve";
$MSG['222'] = "Felhasználói értékelés";
$MSG['223'] = "Megjegyzés írása";
$MSG['224'] = "Kérem válasszon 1 és 5 között";
$MSG['225'] = "Köszönjük, hogy megjegyzését elküldte";
$MSG['226'] = "Saját rangja ";
$MSG['227'] = "Megjegyzése ";
$MSG['228'] = "Értéke ";
$MSG['229'] = "Legújabb értékelések:";
$MSG['230'] = "Valamennyi értékelés megjelenítése";
$MSG['231'] = "REGISZTRÁLT FELHASZNÁLÓ";
$MSG['232'] = "AUKCIÓK ";
$MSG['233'] = "Több";
$MSG['234'] = "&lt;&lt; Vissza";
$MSG['235'] = "Regisztráljon most";
$MSG['236'] = "Licit szabályzat";
$MSG['237'] = "Licitszabályzat aktiválása?";
$MSG['238'] = "Amennyiben ezt a beállítást engedélyezi a licitálók kiléte ismeretlen mindenkinek, kivéve az eladónak";
$MSG['239'] = "Aukciók";
$MSG['240'] = "Tól";
$MSG['241'] = "Ig";
$MSG['242'] = "Elküldve";
$MSG['243'] = "Amennyiben módosítani kívánja jelszavát, kérem az alábbi két mezõt töltse ki. Ellenkezõ esetben hagyja üresen.";
$MSG['244'] = "Beállítások szerkesztése";
$MSG['245'] = "Kilépés";
$MSG['246'] = "Belépve ";
$MSG['247'] = "Licitszabályzati beállítások módosítva";
$MSG['248'] = "Regisztráció megerõsítése";
$MSG['249'] = "Megerõsítés";
$MSG['250'] = "Visszautasítva";
$MSG['251'] = "---- Válasszon alább";
$MSG['252'] = "Születési dátum";
$MSG['253'] = "(mm/dd/yyyy)";
$MSG['254'] = "Utalás új kategóriára";
$MSG['255'] = "Aukció azonosító";
$MSG['256'] = "Vagy válasszon képet amit fel szeretne tölteni (opciónális)";
$MSG['257'] = "Aukció fajtája";
$MSG['258'] = "Termék mennyisége";
$MSG['259'] = "Termék";
$MSG['260'] = "Megnyitás ekkor:";
$MSG['261'] = "Aukció fajtája";
$MSG['262'] = "Javaslatod";
$MSG['263'] = "Díj összege";
$MSG['264'] = "Valóban ";
$MSG['265'] = "el akarod végezni a változtatásokat";
$MSG['266'] = " ezen az aukción";

$MSG['267'] = "If you reached this page, you or someone claiming to be you, signed up at this site.
                        <br>To confirm your registration simply press the <b>Confirm</b> button below.
                        <br>If you didn't want to register and want to delete your data from our database, use the <b>Refuse</b> button.";

$MSG['268'] = "Egyéb beállítások";
$MSG['269'] = "A leadott licit regisztrálva";
$MSG['270'] = "Vissza";
$MSG['271'] = "A leadott licit feldolgozva";
$MSG['272'] = "Aukció:";
$MSG['273'] = "Kiemelté tesz";
$MSG['274'] = "Vastagít";
$MSG['275'] = "Hajrá!";
$MSG['276'] = "Kategóriák";
$MSG['277'] = "Minden kategória";
$MSG['278'] = "Utolsó aukciók";
$MSG['279'] = "Forró termékek";
$MSG['280'] = "A legközelebb lezáródó aukció!";
$MSG['281'] = "Segédoszlop";
$MSG['282'] = "Újdonság";
$MSG['283'] = "minimum";
$MSG['284'] = "Mennyiség";
$MSG['285'] = "Vissza";
$MSG['286'] = " adjon meg érvényes licitet.";
$MSG['287'] = "Kategória";
$MSG['288'] = "A keresési kulcsszó/szavak nem lehet üres";
$MSG['289'] = "Összes oldal:";
$MSG['290'] = "Összes termék:";
$MSG['291'] = "Tételek darabszáma oldalanként";
$MSG['292'] = "Kiemelté teszi";
$MSG['293'] = "ÁLNÉV";
$MSG['294'] = "NÉV";
$MSG['295'] = "ORSZÁG";
$MSG['296'] = "E-MAIL";
$MSG['297'] = "AUKCIÓ";
$MSG['298'] = "Szerkeszt";
$MSG['299'] = "Felhasználó aktiválása";
$MSG['300'] = "Felfüggeszt";
$MSG['301'] = " felhasználó található az adatbázisban";
$MSG['302'] = "Név";
$MSG['303'] = "E-mail";
$MSG['304'] = "Törölt felhasználó";
$MSG['305'] = "Felfüggesztett felhasználó";
$MSG['306'] = "Újraengedélyezett felhasználó";
$MSG['307'] = "Valóban törölni kívánja ezt a felhasználót?";
$MSG['308'] = "Valóban fel kívánja függeszteni ezt a felhasználót?";
$MSG['309'] = "Valóban újra engedélyezni kívánja ezt a felhasználót?";
$MSG['310'] = "Újraengedélyezés";
$MSG['311'] = "aukció található az adatbázisban";
$MSG['312'] = "Cím";
$MSG['313'] = "Felhasználó";
$MSG['314'] = "Dátum";
$MSG['319'] = "Szállítás";
$MSG['321'] = "Felfüggesztett aukció";
$MSG['322'] = "Újraengedélyezett aukció";
$MSG['323'] = "Valóban fel kívánja függeszteni az aukciót?";
$MSG['324'] = "Valóban újra engedélyezni kívánja az aukciót?";
$MSG['325'] = "Aukció törlése";
$MSG['326'] = "Valóban törölni kívánja az aukciót?";
$MSG['328'] = "Szín";
$MSG['329'] = "Kép helye";
$MSG['330'] = "Köszönjük, hogy megerõsítette a regisztrációját!<br>
A regisztrációs folyamata befejezõdött és most már használhatja oldalunk minden elõnyét.<br>";
$MSG['331'] = "Regisztrációját visszavonhatatlanul töröltük az adatbázisból.";
$MSG['332'] = "Cím";
$MSG['333'] = "Üzenet";
$MSG['334'] = "Kapcsolat vele ";
$MSG['335'] = "Kapcsolat tõle ";
$MSG['336'] = "üdvözöljük az aukcióját: ";
$MSG['337'] = "Az üzenetét elküldtük a ";
$MSG['337_1'] = "Itt található a letöltési link ";
$MSG['340'] = "Tól";
$MSG['341'] = "Minden újdonság megtekintése";
$MSG['342'] = " Újdonság";
$MSG['343'] = "Újdonság szerkesztése";
$MSG['344'] = "Idõ beállítások";
$MSG['345'] = "Válassza ki a kívánt idõzonát amit minden alkalommal használni kíván";
$MSG['346'] = "Idõzóna";
$MSG['347'] = "Idõbeállítások elmentve";
$MSG['348'] = "Kötegelt eljárások beállításai";
$MSG['349'] = "Válaszol";
$MSG['351'] = "Vissza a postaládába";
$MSG['352'] = "E-Mail formátum";
$MSG['354'] = "Lezárt aukciók";
$MSG['355'] = "Fixáras engedélyezése majd automatikus tíltása";
$MSG['356'] = "Fixáras de limitálva";
$MSG['357'] = "százalék";

$MSG['358'] = "The Buy it now auto disable feature will automatically disable the buy it now only option on the sell page when or if the percentage of buy it now only auctions reach the set value, you may want to use this as a deterrent to stop people making lots of spam buy it now only auctions";

$MSG['359'] = " Módosítva";
$MSG['363'] = "Dátum formátum";
$MSG['364'] = "Dátum";
$MSG['365'] = "Admin felhasználók";
$MSG['367'] = "Új Admin felhasználó";

$MSG['368'] = "Mass category add<br><span class=\"smallspan\">Just enter category names and put each category on a new line</span>";
$MSG['371'] = "uAuctions needs to periodically run <code>batch.php</code> to close expired auctions and send notification e-mails to the seller and/or the winner. The recommended way to run <code>batch.php</code> is to set up a <a href=\"http://www.aota.net/Script_Installation_Tips/cronhelp.php4\" target=\"_blank\">cronjob</a> if you run a Unix/Linux server.<br>If for any reason you can't run a cronjob on your server, you can choose the <b>Non-batch</b> option below to have <code>batch.php</code> run by uAuctions itself: in this case <code>cron.php</code> will be run each time someone access your home page.";

$MSG['372'] = "Ütemezett feladat futtatása";
$MSG['373'] = "Kötegelt";
$MSG['374'] = "Nem-kötegelt";
$MSG['375'] = "Alapértelmezett beállításokkal az uActions's a <code>cron.php</code> eljárással törli a 30 napnál régebbi aukciókat.
                        <br>Itt lehetõsége van módosítani a periódusokon.";
$MSG['376'] = "Az alábbi napnál régebbi aukciók törlése";
$MSG['377'] = " nap";
$MSG['378'] = "Kötegelt feladat beállítások módosítva.";
$MSG['379'] = "Válassza ki a kívánt dátumformátumot ami megjelenjen az oldalán.";
$MSG['382'] = "mm/dd/yyyy";
$MSG['383'] = "dd/mm/yyyy";
$MSG['384'] = "Dátumformátum módosítva.";
$MSG['385'] = "Utolsó értékelések";
$MSG['386'] = "Elmúlt hónap";
$MSG['387'] = "Elmúlt 6 hónap";
$MSG['388'] = "Elmúlt 12 hónap";
$MSG['389'] = "Ajánlások eladóként";
$MSG['390'] = "Ajánlások vásárlóként";
$MSG['391'] = "Összeg";
$MSG['392'] = "Díj típusa";
$MSG['393'] = "lapos";
$MSG['394'] = "Hozzáad";
$MSG['395'] = "Díjak engedélyezése/tíltása";
$MSG['396'] = "Díj beállítások elmentve";
$MSG['397'] = "Kívánja, hogy az aukciós oldal teljesen ingyenes vagy fizetõs legyen";
$MSG['398'] = "Hírlevél elküldve";
$MSG['399'] = "Keressük ezt!";
$MSG['400'] = "E-mail címek";
$MSG['401'] = "Adatvédelmi irányelvek";
$MSG['402'] = "Adatvédelmi irányelvek oldal";
$MSG['403'] = "Adatvédelmi irányelvek oldal engedéylezése?";
$MSG['404'] = "Adatvédelmi irányelv tartalma<br>(HTML engedélyezett)";
$MSG['405'] = "Engedélyezd ezt az opciót amennyiben szeretnéd, hogy az Adatvédelmi irányel link minden oldal alján megjelenjen.";
$MSG['406'] = "Adatvédelmi irányel beállítások elmentve";
$MSG['409'] = "Hibakezelés";
$MSG['410'] = "Végzetes hiba (általában MySQL hiba) esetén az Aukciós portál átirányítja a felhasználókat egy hiba oldalra.
Lehetõsége van ezt a hiba oldalt testre szabni ami ezekben az esetekben megjelenik.";
$MSG['411'] = "Hibaüzenet";
$MSG['412'] = "Hiba E-Mail cím";
$MSG['413'] = "Hibakezelési beállítások elmentve.";
$MSG['415'] = "Hiba";
$MSG['417'] = "Általános";
$MSG['418'] = "Valóban engedélyezni kívánja ezt a felhasználót?";
$MSG['419'] = "Valóban törölni kívánja ezt a felhasználót? Az összes aukciója és licitje visszavonhatatlanul törlésre kerül";
$MSG['420'] = "Ez a felhasználó ELADÓ az alábbi aukciókban:<br>";
$MSG['421'] = "A felhasználó %s aukcióban licitált.";
$MSG['422'] = "Kiemelt fizetések";
$MSG['423'] = "Fizetés sikeresen megtörtént";
$MSG['424'] = "Fizetését visszaigazoljuk.<br><br>Köszönjük, hogy igénybe vette szolgáltatásunkat.";
$MSG['425'] = "Sikertelen fizetés";
$MSG['426'] = "A fizetése nem érkezett be vagy nem történt meg.<br> <br> Elnézést kérünk a kellemetlenségért.";
$MSG['427'] = "Automatikus licit engedélyezése";
$MSG['428'] = "Automatikus licit funkció engedélyezése / tíltása";
$MSG['429'] = "Nem volt egy licit sem vagy a minimumár nem teljesült";
$MSG['430'] = "Felhasználói regisztráció díja";
$MSG['431'] = "Aukció díja";
$MSG['432'] = "Aukció összeállítás díja";
$MSG['433'] = "Fõlapi megjelenés díja";
$MSG['434'] = "Termék kiemelés díja";
$MSG['435'] = "Képfeltöltés díja";
$MSG['436'] = "Vásárlás azonnal díja";
$MSG['437'] = "Újraindítás díja";
$MSG['439'] = "Termék vastagítás díja";
$MSG['440'] = "Minimumár díja";
$MSG['441'] = "Kérem hozzon létre egy felhasználónevet és jelszót az ADMIN és a FELHASZNÁLÓI felület eléréséhez";
$MSG['442'] = "Kérem adja meg saját felhasználónevét és jelszavát";
$MSG['443'] = "A %s postaládája jelenleg televan kérem próbálja meg késõbb";
$MSG['444'] = "Üzenetek törölve";
$MSG['445'] = "Fizetési lehetõségek";
$MSG['446'] = "Szükséges?";
$MSG['447'] = "Engedélyezi?";
$MSG['448'] = "Felhasználói csoport";
$MSG['449'] = "Csoport azonosító";
$MSG['450'] = "Csoportnév";
$MSG['451'] = "Felhasználók száma";
$MSG['452'] = "Csoport hozzáadása/szerkesztése";
$MSG['453'] = "Gyõztes adatai";
$MSG['454'] = "Az aukciót megnyerted";
$MSG['455'] = "Gyõztes";
$MSG['456'] = "Gyõztes E-mail címe";
$MSG['457'] = "Gyõztes licit";
$MSG['458'] = "Aukció: ";
$MSG['460'] = "Eladó E-mail címe";
$MSG['461'] = "A legmagasabb licitje";
$MSG['464'] = "Fejlett keresõ";
$MSG['471'] = "Új aukciók értesítései";
$MSG['472'] = "Megfigyelt termékek";
$MSG['496'] = "Vásárlás most";
$MSG['497'] = "Villámár";
$MSG['498'] = "Sikeres termékvásárlás<br>";
$MSG['499'] = "Teljesen semleges értékelés érkezett: ";
$MSG['500'] = "Teljesen pozitív értékelés érkezett: ";
$MSG['501'] = "Teljesen negatív értékelés érkezett: ";
$MSG['502'] = "Teljes értékelés érkezett: ";
$MSG['503'] = "Értékelés";
$MSG['504'] = "megjegyzés";
$MSG['505'] = "Vissza a felhasználói adatlapra";
$MSG['506'] = "Elküldött értékelés: ";
$MSG['507'] = "Elõzmények rejtése";
$MSG['508'] = "Új üzenet";
$MSG['509'] = "Felhasználó adatai";
$MSG['510'] = "Jelenleg nincs megjelenítendõ Emlékeztetõ.";
$MSG['511'] = "Felhasználó szerkesztése";
$MSG['512'] = "Aukció szerkesztése";
$MSG['514'] = "<b>Minimumár nem teljesült</b>";
$MSG['515'] = 'Felhasználó engedélyezése';
$MSG['516'] = "Újdonságok kezelése";
$MSG['517'] = " újdonság található az adatbázisban.";
$MSG['518'] = "Új újdonság";
$MSG['519'] = "Cím";
$MSG['520'] = "Tartalom";
$MSG['521'] = "Engedélyez";
$MSG['522'] = "Vevõ által fizetett díjak";
$MSG['523'] = "Ellenszolgáltatási díjak";
$MSG['524'] = "BEÁLLÍTÁSOK";
$MSG['525'] = "Felhasználó kezelõ";
$MSG['526'] = "Általános beállítások";
$MSG['527'] = "Weboldal neve";
$MSG['528'] = "Weboldal URL";
$MSG['530'] = "Változások mentése";
$MSG['531'] = "Logo";
$MSG['532'] = "Bejelentkezõ megjelenítése?";
$MSG['533'] = "Újdonságok megjelenítése?";
$MSG['534'] = "Elfogadási szöveg megjelenítése?";
$MSG['535'] = "A Webhely neve megjelenik minden a portál által felhasználóknak küldött E-Mail-ben";
$MSG['536'] = "A teljes URL kell, hogy legyen (<b>http://</b>-kel kell, hogy kezdõdjön) a telepítéskor.<br> Bizonyosodjon meg, hogy az URL lezáró is megtalálható.";
$MSG['537'] = "Válassza az <b>Igen</b>-t ha szeretné megjeleníteni a belépési modult a fõoldalon. Ellenkezõ esetben <b>Nem</b>-t válassza";
$MSG['538'] = "Válassza az <b>Igen</b>-t ha szeretné megjeleníteni a újdonságok modult a fõoldalon. Ellenkezõ esetben <b>Nem</b>-t válassza";
$MSG['539'] = "Válassza az <b>Igen</b>-t ha szeretné megjeleníteni az elfogadói nyilatkozatot az új felhasználók regisztrációikor az ELKÜLD gomb elõtt.<br>
                       Ezt általában így használják, hogy megjelenítik a szabályzatot a regisztrációs úrlapon, amit el kell, hogy fogadjon a regisztráló.";
$MSG['540'] = "Adminisztrátor E-Mail";
$MSG['541'] = "Az Adminisztrátori E-Mail címet használjuk az automatikus E-Mail-k elküldésére";
$MSG['542'] = "Általános beállítások módosítva";
$MSG['543'] = "Nyilvánossá tesz ezt a kérdést";
$MSG['544'] = "Pénzformátum";
$MSG['545'] = "Amerikai stílus: 1,250.00";
$MSG['546'] = "Európai stílus: 1.250,00";
$MSG['547'] = "Írjon nullát vagy hagyja üresen annak megfelelõen, hogy hány tizedes jegyet kívánna megjeleníteni";
$MSG['548'] = "Tizedesjegyek";
$MSG['549'] = "Pénznem helye";
$MSG['550'] = "Az érték elõtt (pl. USD 200)";
$MSG['551'] = "Az érték után (pl. 200 USD)";
$MSG['552'] = "Kérdések";
$MSG['553'] = "Pénznem beállítások módosítva";
$MSG['554'] = "Mennyi újdonságot kíván megjeleníteni a fõlapon";
$MSG['555'] = "Kérdezõ";
$MSG['556'] = "Jelenlegi Logo";
$MSG['557'] = "Termék részletei";
$MSG['558'] = "Létrehozva";
$MSG['559'] = "Utolsó belépés";
$MSG['560'] = "Állapot";
$MSG['561'] = "TÖRÖL";
$MSG['562'] = "Adminisztrátor szerkesztése";
$MSG['563'] = "Amennyiben meg kívánja változtatni a felhasználó jelszavát kérem töltse ki a két mezõt. Ellenkezõ esetben hagyja üresen.";
$MSG['564'] = "Jelszó ismét";
$MSG['566'] = "Aktív";
$MSG['567'] = "Nem aktív";
$MSG['568'] = "plusz %s több";
$MSG['569'] = "Felhasználó beszúrása";
$MSG['570'] = "Soha nem jelentkezett be";
$MSG['578'] = "Tud eladni";
$MSG['579'] = "Tud vásárolni";

$MSG['580'] = "Auto Join";
$MSG['581'] = "Please proceed to one of the payment gateways listed below to pay the seller the amount of <b>%s</b>.";
$MSG['582'] = "Please proceed to one of the payment gateways listed below to credit your account by amount of <b>%s</b>.";
$MSG['583'] = "To activate your account please proceed to one of the payment gateways listed below to pay the fee of <b>%s</b>.";
$MSG['590'] = "To finalise your auction please proceed to one of the payment gateways listed below to pay the fee of <b>%s</b>.";
$MSG['591'] = "To finalise the relisting your auction(s) please proceed to one of the payment gateways listed below to pay the fee of <b>%s</b>.";

$MSG['592'] = "Belépve mint:";
$MSG['593'] = "Emlékeztetõk";
$MSG['594'] = "Elfogadó szöveg";
$MSG['597'] = "Reklámsávok használatának engedélyezése?";
$MSG['600'] = "Reklámsáv beállítások módosítva";
$MSG['602'] = "Új Logo feltöltése (max. 50 KB)";
$MSG['603'] = "Hírlevél fogadása?";
$MSG['604'] = "If you activate this option, users will be able to subscribe to your newsletter from the registration page.<br>The \"Newsletter management\" will let you send e-mail messages to the subscribed users";
$MSG['605'] = "Üzenet tartalma";
$MSG['606'] = "By clicking save changes the newsletter will be sent to all of the users who are signed up to receive the newsletter in the selected group.";
$MSG['607'] = "Hírlevél feliratkozás";
$MSG['608'] = "Szeretne feliratkozni hírlevelünkre?";
$MSG['609'] = "Válassza a NEM-t a hírlevelünk leiratkozásához";
$MSG['610'] = "Make a selection in the image below of what you want to show in your thumbnail";
$MSG['611'] = "<b>Ezt a terméket meglátogatták</b>";
$MSG['612'] = "<b>-szor</b>";
$MSG['613'] = "Miniatûr kép megtekintése";

$MSG['614'] = "Use the built-in proportional increments table";
$MSG['615'] = "Use your custom fixed increment";

$MSG['616'] = "Miniatûr mentése";

$MSG['617'] = "*NOTE*  If you want to change your password use the two fields below.<br>Otherwise leave them blank.";

$MSG['618'] = "Mégsem";
$MSG['619'] = "Aukció megnyitása";
$MSG['620'] = "Licitjei";
$MSG['621'] = "Személyes adatlap szerkesztése";
$MSG['622'] = "Saját vezérlõpult";
$MSG['623'] = "Üzenetek megtekintése";
$MSG['624'] = "Aukció címe";
$MSG['625'] = "Elindítva";
$MSG['626'] = "Vége";
$MSG['627'] = "Licitszám";
$MSG['628'] = "Max. Licit";

$MSG['629'] = "*If you click cancel the thumbnail image for your auction will be a squashed down version of the image you uploaded/set as default";

$MSG['630'] = "Újraindít";
$MSG['631'] = "Kiválasztott aukciók feldolgozása";
$MSG['631a'] = "Folyamatok kiválasztva";
$MSG['640'] = "*Megjegyzés* A holland aukció esetén nem állítható be minimumár, egyedi licitlépcsõ és nem használható a VÁSÁRLÁS MOST szolgáltatás.";
$MSG['641'] = "Holland aukció";
$MSG['642'] = "Általános aukció";
$MSG['645'] = "Kérdés küldése az eladónak";
$MSG['646'] = "Be kell lépni, hogy a kérdését fel tudja tenni az eladónak";
$MSG['647'] = "Kérdés";
$MSG['648'] = "Válaszok a kérdésekre";
$MSG['649'] = "Válasz:";
$MSG['650'] = "Kérdés:";
$MSG['651'] = "Kérdés az aukciójáról: %s";
$MSG['661'] = "Fényképalbumhoz hozzáad";
$MSG['662'] = "<h2>Miniatür kép létrehozása</h2>";
$MSG['663'] = "Fényképalbum";
$MSG['664'] = "Amennyiben engedélyezi ezt a funkciót akkor az eladók újabb képeket tudnak a megadott keretek alapján (lásd alább) feltölteni.";
$MSG['665'] = "Fényképalbum engedélyezése?";
$MSG['666'] = "Maximális fényképszám";
$MSG['667'] = "Viewing user: ";
$MSG['668'] = "Ez az aukció még nem lett megnyitva.";
$MSG['671'] = "Maximum képméretek";
$MSG['672'] = "Kbyte";
$MSG['673'] = "Feltölthet %s fényképet. A feltõltött képek méretei kisebbek kell, hogy legyenek %s byte-nál.";
$MSG['674'] = "Csak %s fényképet tölthet fel. Kérem távolítson el néhányat a listáról.";
$MSG['675'] = "A %s fénykép feltöltéséért fizetni kell.";
$MSG['677'] = "Fényképek feltöltése";
$MSG['678'] = "Bezár";
$MSG['679'] = "Kérem, kövesse az alábbi lépéseket.";
$MSG['680'] = "Válassza ki a feltöltendõ állományt";
$MSG['681'] = "Állományok feltöltése";
$MSG['682'] = "Az 1. és 2. lépést ismételje meg minden kép esetében. Amikor végzett akkor kattintson a <i>Album létrehozása</i> nyomógombra.<br>Az elsõ feltöltött kép vagy egy kiválasztott lesz az alapértelmezett akkor ahhoz létre kell hozni a miniatûr képet is";
$MSG['683'] = "&gt;&gt;&gt; Album létrehozása &lt;&lt;&lt;";
$MSG['684'] = "Állomány neve";
$MSG['685'] = "Mérete (byte)";
$MSG['686'] = "Alapértelmezett";
$MSG['687'] = "Feltöltött állományok";
$MSG['677_a'] = "Állományok feltöltése";
$MSG['688'] = "Már feltöltötte ";
$MSG['689'] = " állományok";
$MSG['694'] = "Album megtekintése";
$MSG['699'] = "Az alábbi licitjei ";
$MSG['700'] = " kerültek leadásra. ";
$MSG['701'] = " A licitje nem elegendõ ahhoz, hogy a legmagasabb legyen!<br>Kíván új licitet leadni?";
$MSG['718'] = "Aukció fajtája";
$MSG['719'] = "Fizetési részletek";
$MSG['720'] = "PayPal E-mail cím";
$MSG['724'] = "Egyéb információk";
$MSG['725'] = "Optimalizálás";
$MSG['726'] = "Sablon-gyorstár engedélyezése?";
$MSG['727'] = "Jelentõsen növelhetõ a weboldal sebessége. Ezt csak akkor tíltsa le ha jelenleg a sablonon dolgozik";
$MSG['728'] = "Optimalizálási beállítások módosítva";
$MSG['729'] = "Fizetési típusok beállítása";
$MSG['730'] = "Itt lehet beállítani azt, hogy milyen módon fizethetik ki a felhasználók a díjjakat. Az egyenleg mód-ban a felhasználók kiválaszthatják, hogy mikor fizetik ki a tartozásaikat melyeket a szolgáltatásokkal vettek igénybe";
$MSG['731'] = "Egyenleg mód";
$MSG['732'] = "Live payments";
$MSG['733'] = "Az alábbi beállítások csak Egyenleg módban használhatók";
$MSG['734'] = "Maximum tartozás";
$MSG['735'] = "Ez az maximum elérhetõ tartozás amit már mindenképpen vissza kell fizetni";
$MSG['736'] = "Egyenleg felírás";

$MSG['737'] = "The credit given to an account on its creation";

$MSG['738'] = "Felfüggesztett fiókok";
$MSG['739'] = "Felfüggesztett fiókok amelyek túllépték a maximum keretet";
$MSG['740'] = "Nincs";
$MSG['741'] = "Kép";
$MSG['742'] = "reCaptcha";
$MSG['743'] = "Captcha típusa a regisztrációs oldalon";
$MSG['744'] = "Captcha típusa az aukció küldése ismerõsnek oldalon";
$MSG['745'] = "A Captcha rendkívül alkalmas arra, hogy spam üzenetekkel és regisztrációkkal ellehetetlenítsék az oldalt";
$MSG['746'] = "Recaptcha nyilvános kulcs";
$MSG['747'] = "Recaptcha privát kulcs";
$MSG['748'] = 'A reCaptcha használatához létre kell hozni egy fiókot a <a href="http://recaptcha.net/" class="new-window">http://recaptcha.net/</a> oldalon, ahol megkapja a saját oldalához a kulcsokat';
$MSG['749'] = 'Spam beállítások';
$MSG['750'] = 'Spam beállítások módosítva';
$MSG['751'] = 'A reCaptcha a két kulcs nélkül nem használható';
$MSG['752'] = 'Az ellenõrzõ szöveg amit megaott nem egyezik a képen láthatóval.';
$MSG['753'] = 'Fiók felfüggesztve';
$MSG['754'] = 'A fiókhoz tartozó számla egyenleg elérte a maximális tartozást; fiókját zároltuk, míg nem egyenlíti ki a számlájának egyenlegét <br>Visszatérhet <a href="outstanding.php">ide</a>';
$MSG['755'] = 'Fizetve';
$MSG['756'] = 'Fizetés most';
$MSG['757'] = 'Biztonsági kód';
$MSG['758'] = 'Ellenõrzõ kód';
$MSG['759'] = 'Engedélyez';
$MSG['760'] = 'Tílt';
$MSG['761'] = 'Díj beállítások módosítva';
$MSG['762'] = 'Díjmód beállítások módosítva';
$MSG['763'] = 'Egyenleg';
$MSG['764'] = 'Fizetés emlékeztetõ küldése';
$MSG['765'] = 'Fizetési emlékeztetõ elküldve';
$MSG['766'] = 'Számla';
$MSG['767'] = 'Kattintson a jobb oldali gombra a PayPal mûvelethez';
$MSG['768'] = 'Regisztrációs díj';
$MSG['769'] = 'Közzétételi díj fizetése';
$MSG['770'] = 'Újraindítási díj fizetése';
$MSG['771'] = 'Alapértelmezett név';
$MSG['772'] = 'Fordítás';
$MSG['773'] = 'Authorize.net belépési azonosító';
$MSG['774'] = 'Authorize.net tranzakciós kulcs';
$MSG['775'] = 'Vevõi díj';
$MSG['776'] = "Az eladás véglegesítéséhez válasszon egy fizetési lehetõséget a listáról az <b>%s</b> összeg kiegyenlítéséhez.";
$MSG['777'] = 'Van egy lejárt fizetési kötelezettsége az alábbi termék megvásárlása miatt %s; fiókját zároltuk míg ezt a tartozást ki nem egyenlíti. Ezt megteheti most és <a href="%s">itt</a>';
$MSG['778'] = 'Amennyiben alternatív fizetési móddal kíván fizetni lépjen id <a href="profile.php?user_id=%s">%s</a> és megtudja hogyan.';
$MSG['779'] = 'Felhasználói regisztrációs mezõk módosítva';
$MSG['780'] = 'Jelenítse meg a regisztrációs oldalt';
$MSG['781'] = 'Születésnap kitöltése kötelezõ?';
$MSG['782'] = 'Postacím kitöltése kötelezõ?';
$MSG['783'] = 'Város kitöltése kötelezõ?';
$MSG['784'] = 'Állam/Megye kitöltése kötelezõ?';
$MSG['785'] = 'Ország kitöltése kötelezõ?';
$MSG['786'] = 'Postai irányítószám kitöltése kötelezõ?';
$MSG['787'] = 'Telefon kitöltése kötelezõ?';
$MSG['788'] = 'Megjelenítési beállítások';
$MSG['789'] = 'Eredmények száma oldalanként';
$MSG['790'] = 'Maximális tételszám oldaltörés elött';
$MSG['791'] = 'Végsõ ár díja';
$MSG['792'] = '%s-t kell fizetni a termékekért';
$MSG['793'] = ' hamarosan lezáruló aukciós tételek melyekre licitált';
$MSG['794'] = 'A %s termékre rálicitáltak';
$MSG['795'] = 'Megjelenítési beállítások módosítva';

$MSG['796'] = 'You have an outstanding payment of the final value fee for the item %s; your account will be suspended until this is paid. You can pay for this now at <a href="%s">Here</a>';

$MSG['797'] = 'Feliratok engedélyezése';
$MSG['798'] = 'Az eladónak engedélyezett, hogy feliratokat adjon az aukcióihoz, melyek megjelennek az aukciós listákon';
$MSG['799'] = 'Másodlagos kategória engedélyezése';
$MSG['800'] = 'Az eladónak engedélyezett, hogy extra kategóriához is párosítsa aukcióját';
$MSG['801'] = 'SSL URL megosztása';
$MSG['802'] = 'Amennyiben használni kívánja az SSL URL-k megosztását akkor azt adja meg itt';
$MSG['803'] = 'Feliratok díja';
$MSG['804'] = 'Extra kategória díja';
$MSG['805'] = 'Továbblépés';
$MSG['806'] = 'Termék felirata';
$MSG['807'] = 'Fõoldal beállítások';
$MSG['808'] = 'Ez a miniatûr kép az ami megjelenik az aukciós listákon mikor keresnek a felhasználók';

$MSG['809'] = 'A field must be shown if it\'s a required field';

$MSG['810'] = 'Valós PayPal E-Mail cím szükséges';
$MSG['811'] = 'Valós AuthNet felhasználónév és jelszó szükséges';
$MSG['812'] = 'Állomány neve:';
$MSG['813'] = 'Állomány tartalma:';
$MSG['814'] = 'Extra kategória';
$MSG['815'] = 'Valóban aktibálni kívánja ezt a felhasználót?';
$MSG['816'] = 'Fotóalbum';
$MSG['817'] = 'Árak';
$MSG['818'] = 'Nincs engedélye a termék listázására';
$MSG['819'] = 'Nincs engedélye a termék megvásárlásához';
$MSG['820'] = 'A fiók értékesítési engedély kérelme';
$MSG['821'] = 'Valós 2Checkout azonosító szükséges';
$MSG['822'] = 'Valós Skrill E-Mail cím szükséges';
$MSG['823'] = 'Valós Worldpay azonosító szükséges';
$MSG['824'] = 'Worldpay azonosító';
$MSG['825'] = 'Skrill E-Mail cím';
$MSG['826'] = '2Checkout azonosító';
$MSG['827'] = 'Heti jelentés';
$MSG['828'] = 'Hét';
$MSG['829'] = 'Megtekintve ';
$MSG['830'] = 'Hónap';
$MSG['831'] = 'Jelenleg nincs kitíltott IP cím.';
$MSG['832'] = 'Valóban törölni kívánja ezt az újdonságot \'%s\'';
$MSG['833'] = 'Valóban törölni kívánja ezt az aukciót \'%s\'';
$MSG['834'] = 'Valóban törölni kívánja ezt az üzenetet (Azonosító: %s)';
$MSG['835'] = 'Valóban törölni kívánja ezt a felhasználót \'%s\'';
$MSG['836'] = 'Tagsági típusok módosítva';
$MSG['837'] = '(%s elem a GYIK-ban)';
$MSG['838'] = 'Valóban fel szeretné dolgozni az alábbi kategóriákat: ';

$MSG['839'] = 'What do you want to do with the FAQs in the following categories';

$MSG['840'] = 'Mozgatás ide ';
$MSG['841'] = 'Adminisztrációs sablon';
$MSG['842'] = 'Díjak beállítása';
$MSG['843'] = 'What do you want to do with the auctions & subcategories in the following categories<br><small>(If you want to move them you must enter the category id of where you want them moved)</small>';
$MSG['844'] = 'Some categories selected to move could not be processed as no valid category ID was given to where they would be moved to';
$MSG['845'] = '<p><img src="' . $system->SETTINGS['siteurl'] . 'themes/admin/images/bullet_blue.png"> Alkategóriával rendelkezõ kategóriák megjelenítése</p><p><img src="' . $system->SETTINGS['siteurl'] . 'themes/admin/images/bullet_red.png"> Aukciót tartalmazó kategóriák megjelenítése</p>';
$MSG['846'] = 'Fiókegyenleg';
$MSG['847'] = 'Ár';
$MSG['848'] = 'Valóban törölni kívánja a felhasználónak ezt az értékelését (Azonosító: %s)';
$MSG['849'] = 'Automatikus újraindítás engedélyezése';
$MSG['850'] = 'Engedélyezi a felhasználóknak, hogy amennyiben az aukciónak nincs nyertese akkor azt újraindítsák';
$MSG['851'] = 'Maximális újraindítás';
$MSG['852'] = 'Annak száma, hogy hány alkalommal lehet az aukciót újraindítani';
$MSG['853'] = 'Jelenleg nincsen megfigyelt terméke';
$MSG['854'] = 'Fiók';
$MSG['855'] = 'Válasszon nézetet';
$MSG['856'] = 'Válasszon idõszakot';
$MSG['857'] = 'Összes kiszámlázott';
$MSG['858'] = 'Elolvasta és elfogadja a <a href="' . $system->SETTINGS['siteurl'] . 'contents.php?show=terms" data-fancybox-type="iframe" class="infoboxs">Általános mûködési és üzletszabályzatot</a>';
$MSG['858_a'] = 'Elolvasta és elfogadja a <a href="' . $system->SETTINGS['siteurl'] . 'contents.php?show=cookies" data-fancybox-type="iframe" class="infoboxs">Internetes süti szabályzatot</a>';
$MSG['859'] = 'Köszönjük  %s, hogy regisztrált oldalunkra.';
$MSG['016'] = 'Elküldtük a megerõsítõ levelet a %s címre. Ez a levél tartalmaz egy aktivációs linket amivel visszigazolhatja az E-Mail címét. Csak annyi a teendõje, hogy rákattint a levélben linke és aktiválja a %s fiókot. Amennyiben nem érkezne meg a megerõsítõ levél akkor kérem ellenõrizze a SPAM mappát.';
$MSG['016_a'] = "Az adminisztrátor rövidesen ellenõrzi a fiókját, ha elfogadja akkor be fog tudni lépni";
$MSG['016_b'] = "Most már be tud jelentkezni a felhasználónevével és jelszavával.";
$MSG['860'] = '<p>Olvasson néhány tippet a kezdéshez ' . $system->SETTINGS['sitename'] . '.</p>
<ul>
  <li>Amennyiben képeket is kíván használni a termékei eladásakor, akkor csökkentse a képek méretreit a gyorsabb feltöltés érdekében.</li>
  <li>Használja a Fõoldali kiemelési szolgáltatásunkat amivel jóval nagyobb az esélye az eladásra ' . $system->SETTINGS['sitename'] . ' Fõoldal.</li>
</ul>';
$MSG['861'] = 'Aukció keresése';
$MSG['862'] = 'Belépés a folytatáshoz';
$MSG['863'] = '<h1>Még nem tag?</h1>
        Regisztráljon most
        Csupán csak néhány perc
        <p> Amennyiben tag, akkor:</p>
        <ul>
            <li>Eladhat bármit</li>
            <li>Értesítést kap ha a keresett elem megjelenik</li>
            <li>Termékeket saját megfigyelõi listára tehet</li>
            <li>Licitálhat termékekre</li>
            <li>plusz még számos egyéb szolgáltatást vehet igénybe</li>
        </ul>
        <a style="color:white" class="btn btn-primary" href="'.$system->SETTINGS['siteurl'].'new_account">Regisztráljon most</a>';
$MSG['864'] = 'Szállítási adatok';
$MSG['865'] = 'Listázási adatok';

$MSG['867'] = 'Pick up only';

$MSG['868'] = ' mind';
$MSG['869'] = 'Eladás dátuma';

$MSG['888'] = 'A hibanapló jelenleg üres';
$MSG['889'] = "Hibanapló ürítése";
$MSG['890'] = "Napló ürítése";
$MSG['891'] = "Hibanapló";
$MSG['892'] = "Aktiváláshoz";
$MSG['893'] = "Teljes";
$MSG['894'] = "Felhasználói beállítások";
$MSG['895'] = "Felhasználói beállítások módosítva";
$MSG['896'] = "Ez a miniatûr méret ami meg fog jelenni az aukciós lista oldalakon";
$MSG['897'] = "Egyéb aukciós beállítások";
$MSG['898'] = "Fizetett";
$MSG['899'] = "Beállítás fizetettre";
$MSG['900'] = "Gyõztes megtekintése";
$MSG['901'] = "Tételek darabszáma";
$MSG['902'] = "HTML";
$MSG['903'] = "Milyen formátumban szeretné az E-Mail-ket megkapni";
$MSG['904'] = "Ez az aukció lezárult";
$MSG['905'] = "Valaki azt szeretné, hogy nézd meg ezt az aukciót";
$MSG['906'] = "A licitje tovább már nem a gyõztes-licit";
$MSG['907'] = " - A terméke elkelt!";
$MSG['908'] = " - Nincs gyõztes";
$MSG['909'] = " Az aukció lezárult - Megnyerte a terméket";
$MSG['910'] = "Ennek a felhasználónak nincs aukciója.";
$MSG['911'] = "Lezárt";
$MSG['912'] = "Segítség-kezelõ";
$MSG['913'] = "a téma megtalálható az adatbázisban";
$MSG['914'] = "Téma";
$MSG['915'] = "Szöveg";

$MSG['916'] = "Súgótéma kezelõ";
$MSG['917'] = "Súgótéma hozzáadása";
$MSG['918'] = "Egyéb súgótémák:";
$MSG['919'] = "Általános súgó";
$MSG['920'] = "Megvásárol most aktiválása?";
$MSG['921'] = "Amennyiben engedélyezi ezt az opciót, akkor a felhasználóknak lehetõsége nyílik azonnal megvenni a terméket annak ellenére, hogy nincs licit leadva. Ezt az opciót kell elsõnek engedélyezni az aukciók létrehozásakor.";
$MSG['922'] = "Kérdés küldése az eladónak";
$MSG['923'] = "Eladó földrajzi helye";
$MSG['924'] = "Mostanában nyitott";
$MSG['925'] = "Hamarosan befejezõdõ";

$MSG['926'] = "Big Ticket";
$MSG['927'] = "Nagyon drága";
$MSG['928'] = "Olcsó termékek";
$MSG['929'] = "Közkedvelt termékek";
$MSG['930'] = "Forró termékek";
$MSG['931'] = "Megvásárol most";

$MSG['932'] = "Listings posted by %s";

$MSG['933'] = "Azonnal megvásárolhatók csak";
$MSG['934'] = "Felhasználótól: ";

$MSG['935'] = 'Balance Payment';

$MSG['936'] = 'Néhány hiba történt a bekéréskor kérem próbálja ismét';
$MSG['937'] = "Hibás név";
$MSG['938'] = "Helytelen felhasználónév";
$MSG['939'] = "Helytelen jelszó";
$MSG['940'] = "Kérem adja meg a jelszavát kétszer";
$MSG['941'] = "Hiányzó E-Mail cím";
$MSG['942'] = "Hiányzó cím";
$MSG['943'] = "Hiányzó város";
$MSG['944'] = "Hiányzó tartomány";
$MSG['945'] = "Hiányzó ország";
$MSG['946'] = "Hiányzó irányítószám";
$MSG['947'] = "Hiányzó irányítószám";
$MSG['948'] = "Hiányzó vagy helytelen születési dátum";
$MSG['949'] = "Bezárva ";
$MSG['950'] = "%s licitek";
$MSG['951'] = "Nincs azonali megvásárlás<br>ennél a terméknél";
$MSG['1000'] = "Kulcsszavak vagy termékkódok keresése";
$MSG['1001'] = "Keresés címre <b>és</b> leírásra";
$MSG['1002'] = "Keresés a kategóriákban";
$MSG['1003'] = "Ártartomány";
$MSG['1004'] = "között";
$MSG['1005'] = " és ";
$MSG['1006'] = "Fizetés választása";
$MSG['1008'] = "Hely";
$MSG['1009'] = "Vége";
$MSG['1010'] = "Ma";
$MSG['1011'] = "Holnap";
$MSG['1012'] = "a 3 napban";
$MSG['1013'] = "az 5 napban";
$MSG['1014'] = "Sorrend";
$MSG['1015'] = "Hamarosan lezárulók elöl";
$MSG['1016'] = "Az újonnan feltöltöttek elöl";
$MSG['1017'] = "Ár szerint növekvõ";
$MSG['1018'] = "Ár szerint csökkenõ";
$MSG['1020'] = "Holland aukciók";
$MSG['1021'] = "Normál aukciók";
$MSG['1022'] = "SSL támogatás";
$MSG['1023'] = "SSL támogatás aktiválása?";
$MSG['1024'] = "<p>Az SSL támogatással elérheti, hogy az aukciós portált használók biztonságban legyenek.</p>Miután az SSL használat engedélyezésre került akkor a felhasználók bejelentkezése és regisztrációja automatikusan a titkosított csatornán keresztül történik.";
$MSG['1025'] = "SSL beállítások módosítva";

$MSG['1028'] = "Counteries updated";

$MSG['1029'] = "Számlálók módosítva";
$MSG['1030'] = "A felhasználók, aukciók és licit számlálók újraszinkronizálása";
$MSG['1031'] = "Számlálókl újraszinkronizálása";
$MSG['1032'] = "nincs gyõztes";

// Invoices & tax system
$MSG['1033'] = 'Fuvarlevél';
$MSG['1034'] = 'Aukciós azonosító';
$MSG['1035'] = 'Rendelés/Számla';
$MSG['1036'] = 'Rendelés dátuma';
$MSG['1037'] = 'VÁSÁRLÓ';
$MSG['1038'] = 'SZÁLLÍTÁS IDE ';
$MSG['1039'] = 'Számla információ';
$MSG['1040'] = 'Rendelési részletek';
$MSG['1041'] = 'Számla sorszám.';
$MSG['1042'] = 'Rendelés száma:';
$MSG['1043'] = 'Számla dátuma:';
$MSG['1044'] = 'Termék';
$MSG['1045'] = 'Adó';
$MSG['1046'] = 'Egységár (ÁFA nélkül)';
$MSG['1047'] = 'Egységár (ÁFA-val)';
$MSG['1048'] = 'Teljes (ÁFA nélkül)';
$MSG['1049'] = 'Teljes (ÁFA-val)';
$MSG['1050'] = 'Részösszeg:';

$MSG['1051'] = 'UK Shipping:';

$MSG['1052'] = 'ÁFA 20%:';
$MSG['1053'] = 'Teljes:';
$MSG['1054'] = 'Szállítási mód';
$MSG['1055'] = 'Fizetési mód';
$MSG['1056'] = 'Nem választott kis egy megrendelést sem a fuvarlevél nyomtatásához.';
$MSG['1057'] = 'Számlák listája';
$MSG['1058'] = 'Számla megtekintése';
$MSG['1059'] = 'Számlalista';
$MSG['1060'] = 'Hibás számla.';

// admin general
$MSG['1061'] = 'Megjegyzések';

// admin help page
$MSG['1062'] = 'Támogatás kérése';
$MSG['1063'] = 'Támogatói fórumok';
$MSG['1064'] = 'Amennyiben bármilyen problémája vagy kérdése lenne, látogasson el a támogatói fórumra, ahol segítséget kaphat az aktív felhasználóktól.';
$MSG['1065'] = 'Online dokumentáció';
$MSG['1066'] = 'Ha nem tudja biztosan, hogy az hogy mûködik vagy bármi akkor látogasson el az uAuctions online dokumentációs oldalára';
$MSG['1067'] = 'Gyakran ismételt kérdések és azok megoldásai';
$MSG['1068'] = 'uAuctions módosítása';
$MSG['1069'] = 'Sablon letöltése';
$MSG['1070'] = 'Böngésszen a számos sablonból amit a felhasználók küldtek be, amelyek azonnal megjelennek az oldalán';
$MSG['1071'] = 'Modulok letöltése';
$MSG['1072'] = 'Böngésszen a számos modulból amit a felhasználók küldtek be és hozzá is adhatja a weboldalához amivel új funkciókat indíthat el';
$MSG['1073'] = 'Nyelvi csomag letöltése';
$MSG['1074'] = 'Böngésszen a számos beküldött nyelvi csomagok között';
$MSG['1075'] = 'uAuctions támogatása';
$MSG['1076'] = 'Hiba beküldése';
$MSG['1077'] = 'Talált valami vicces funkciót? Vegye fel velünk a kapcsolatot és segítsen másoknak is';
$MSG['1078'] = 'Új funkció beküldése';
$MSG['1079'] = 'Van egy jó ötlete amivel javíthatjuk az uAuctions funkcionalitását, kérem vegye fel velünk a kapcsolatot mert várjuk a jó ötleteket';
$MSG['1080'] = 'Támogatás';
$MSG['1081'] = 'Segítse munkánkat és a jövõbeli fejlesztéseket';

// tax admin
$MSG['1082'] = 'Adó neve';
$MSG['1083'] = 'Adó mértéke';
$MSG['1084'] = 'Eladó honnan';
$MSG['1085'] = 'Vásárló honnan';
$MSG['1086'] = 'Weboldal adója';
$MSG['1087'] = 'Valóban törölni kívánja ezt az adótípust';
$MSG['1088'] = 'Adóbeállítások';
$MSG['1089'] = 'Adóbeállítások módosítva';
$MSG['1090'] = 'Adó engedélyezve';
$MSG['1091'] = 'Globálisan engedélyezi vagy letíltja az adót';
$MSG['1092'] = 'A felhasználók fizetik az adót';
$MSG['1093'] = 'Lehetõvé teszi, hogy a felhasználók módosítsák az adózást';

// admin invoice settings
$MSG['1094'] = 'Számla beállítások';
$MSG['1095'] = 'Számla beállítások módosítva';
$MSG['1096'] = 'Számla megjegyzések';

$MSG['1097'] = 'This will show in a yellow box above the end message on users invoices';
$MSG['1098'] = 'Invoice End Message';

$MSG['1099'] = 'Ez megjelenik minden felhasználói számla végén';

// list admin users
$MSG['1100'] = 'Nem tudja törölni a fiókot, mert jelenleg be vannak lépve';
$MSG['1101'] = 'Adminisztrációs fiók törölve';

// sell.php tax
$MSG['1102'] = 'Charge Tax';
$MSG['1103'] = 'Adü beleszámítva a végsõ teljes árba';

$MSG['1104'] = 'Add to balance';
$MSG['1105'] = 'Válassza ki a törlendõ képet <strong>(Ez a lépés visszavonhatatlan)</strong>';

$MSG['5003'] = "Weblap beállítások";
$MSG['5004'] = "Pénznem beállítások";
$MSG['5005'] = "Általános kinézeti beállítások";
$MSG['5006'] = "Fotóalbum beállítások módosítva";
$MSG['5008'] = "Weblap pénzneme";
$MSG['5010'] = "Pénznemváltó";
$MSG['5011'] = "Fõoldalon kiemelt termékek";
$MSG['5012'] = "Ez a szám határozza meg a nyitólapon kiemelt termékek darabszámát (MEGJEGYZÉS: CSAK A <b>kiemelt</b> termékek fognak megjelenni).<br>0 (nulla) is megengedett.";
$MSG['5013'] = "Utoljára feltöltött termékek";
$MSG['5014'] = "Ez a szám határozza meg a nyitólapon megjelenített, utoljára feltöltött termékek darabszámát.<br>0 (nulla) is megengedett.";
$MSG['5015'] = "Friss termékek";
$MSG['5016'] = "Ez a szám határozza meg a nyitólapon megjelenített, friss termékek darabszámát.<br>0 (nulla) is megengedett.";
$MSG['5017'] = "Hamarosan lezáruló";
$MSG['5018'] = "Ez a szám határozza meg a nyitólapon megjelenített, hamarosan lezáruló termékek darabszámát.<br>0 (nulla) is megengedett.";
$MSG['5019'] = "Általános kinézeti beállítások módosítva";
$MSG['5022'] = "FELHASZNÁLÓ KERESÉSE";
$MSG['5023'] = "Keresés &gt;&gt;";
$MSG['5024'] = "Név, felhasználónév vagy E-Mail";
$MSG['5025'] = "Fiók";
$MSG['5028'] = "Mûvelet";
$MSG['5029'] = "Mehet >>";
$MSG['5030'] = "Üzenõfal";
$MSG['5031'] = "Új üzenõfal";
$MSG['5032'] = "Üzenõfal kezelõ";
$MSG['5033'] = "Üzenõfal lista";
$MSG['5034'] = "Üzenõfal címe";
$MSG['5035'] = "Megjelenített üzenetek";
$MSG['5036'] = "Ez a szám határozza meg az üzenõfalan megjelenített legutolsó üzenetek darabszámát.";
$MSG['5038'] = "Aktív";
$MSG['5039'] = "Inaktív";
$MSG['5040'] = "MEGJEGYZÉS: egy üzenet törlése az üzenõfalról töröl minden hozzákapcsolt üzenetet is.";
$MSG['5043'] = "# Üzenetek";
$MSG['5044'] = "Kijelölt üzenõfalak eltávolítása";
$MSG['5046'] = "MEGJELENÍT";
$MSG['5047'] = "Üzenõfal beállítások";
$MSG['5048'] = "Üzenõfal szolgáltatás aktiválása?";
$MSG['5051'] = "Üzenõfal beállítások módosítva";
$MSG['5052'] = "Üzenõfal szerkesztése";
$MSG['5053'] = "Utolsó üzenet";
$MSG['5056'] = "Jelenleg nincs belépve.<br>Amennyiben küld egy üzenetet akkor az <b><i>Ismeretlen</i></b>-ként jelenik meg.";
$MSG['5057'] = "Üzenet küldése";
$MSG['5058'] = "Vissza az üzenõfalhoz";
$MSG['5059'] = "Üzenetek";
$MSG['5060'] = "Elküldte ";
$MSG['5061'] = "Ismeretlen";
$MSG['5062'] = "Minden üzenet megjelenítése";
$MSG['5063'] = "Üzenet megtekintése";
$MSG['5064'] = "Vissza az üzenõfalhoz";
$MSG['5065'] = "Minden üzenet törlése ami régebbi mint ";
$MSG['5067'] = "Számlálók frissítése ";
$MSG['5068'] = "Szószûrõ";
$MSG['5069'] = "A szószûrõ opció segíti kiszûrni a nem kívánt szavakat a:
<ul>
<li>Az aukciók CÍMEINEK és LEÍRÁSAINAK szövegébõl.</li>
<li>Üzenõfalra elküldött üzenetekbõl</li>
</ul>";
$MSG['5070'] = "Szószûrõ engedélyezése?";
$MSG['5071'] = "Nem kívánatos szavak listája";
$MSG['5072'] = "Megadhatja a nem kívánatos szavakat soronként (maximum 255 karakter egy sorban). Fontos, hogy minden soronként kerül feldolgozásra mint \"egy szó\".";
$MSG['5073'] = "Szószûrõ beállítások módosítva";
$MSG['5074'] = "Rólunk oldal";
$MSG['5075'] = "Szerzõdési &amp; feltételek oldal";
$MSG['5076'] = "Engedélyezze ezt a beállítást amennyiben azt akarja, hogy a <u>Rólunk</u> link megtalálható legyen minden oldal láblécében.";
$MSG['5077'] = "Aktiválja a Rólunk oldalt?";
$MSG['5078'] = "Rólunk oldal tartalma";
$MSG['5079'] = "Rólunk beállítások módosítva";
$MSG['5080'] = "Megjegyzés: minden új sor karakter <b>&lt;br&gt;</b> HTML tag-é kerül átkovertálásra.";
$MSG['5081'] = "Engedélyezze ezt a beállítást amennyiben azt akarja, hogy a <u>Szerzõdési &amp; feltételek</u> link megtalálható legyen minden oldal láblécében.";
$MSG['5082'] = "Engedélyezi a Szerzõdési &amp; feltételek oldalt?";
$MSG['5083'] = "Szerzõdési &amp; feltételek oldal tartalma<br>(HTML elfogadott)";
$MSG['5084'] = "Szerzõdési &amp; feltételek beállítások módosítva";
$MSG['5085'] = "Rólunk";
$MSG['5086'] = "Szerzõdési &amp; feltételek";
$MSG['5087'] = "Aukció beállításai";
$MSG['5088'] = "Aukció beállításai módosítva";
$MSG['5089'] = "A felhasználók egyedi idõpontot is beállíthatnak az aukciójuk megnyitására";
$MSG['5090'] = "Egyedi nyitódátum engedélyezése?";

$MSG['5091'] = "Hours until auction ends count-down";

$MSG['5092'] = "Aukció keresése";
$MSG['5093'] = "Cím, leírás";
$MSG['5094'] = "Aukciók megtekintése";

$MSG['5095'] = "Hours remaining on an auction until the time remaining becomes an automatic countdown timer";

$MSG['5113'] = "Módosít";
$MSG['5115'] = "napok";
$MSG['5117'] = "Oldal";
$MSG['5118'] = "a";
$MSG['5119'] = "&lt;&lt;Elõzõ";
$MSG['5120'] = "Következõ&gt;&gt;";
$MSG['5138'] = "Megjegyzés: használhatja a választott pénznemet az oldalon.<br>
Minden fizetendõ Paypal összeg automatikusan konmvertálásra kerül USA Dollárra az aktuális árfolyamon  mielõtt a fizetés el lenne küldve a PayPal szervernek.";
$MSG['5140'] = "Fiókkezelõ";
$MSG['5141'] = "Hozzáférési statisztika";
$MSG['5142'] = "Beállítások";
$MSG['5143'] = "Hozzáférési statisztikák megtekintése";
$MSG['5144'] = "Kérem válassza ki, hogy a szerver milyen hozzáférési statisztikát készítsen el.";
$MSG['5145'] = "Felhasználó hozzáférési statisztika készítése";
$MSG['5146'] = "Böngészõ és platform statisztika készítése";
$MSG['5148'] = "Statisztikai beállítások módosítva.";
$MSG['5149'] = "Engedélyezve a statisztikát?";
$MSG['5150'] = "Válassza ki a statisztika típusát amit el szeretne készíteni";
$MSG['5155'] = "Böngészõk";
$MSG['5156'] = "Platformok";
$MSG['5157'] = "Domain-k";
$MSG['5158'] = "Hozzáférési statisztika ";
$MSG['5159'] = "Nap";
$MSG['5160'] = "Elõzmény megjelenítése";
$MSG['5161'] = "Oldal megtekintések";
$MSG['5162'] = "Egyedi látogatók";
$MSG['5163'] = "Felhasználói munkamenetek";
$MSG['5164'] = "Összesen";
$MSG['5165'] = "Böngészõ statisztika megtekintése";
$MSG['5167'] = "Böngészõ statisztika ";
$MSG['5169'] = "Böngészõ";
$MSG['5170'] = "Domain";
$MSG['5180'] = "Felhasználó";
$MSG['5181'] = "Hozzáad &gt;&gt;";
$MSG['5182'] = "Felhasznál keresése (álnév, név vagy e-mail)";
$MSG['5183'] = "Felhasználók találhatók";
$MSG['5184'] = "VÁLASSZON";
$MSG['5185'] = "Álnév";
$MSG['5187'] = "Lista tartalmának szerkesztése: ";
$MSG['5188'] = "Kijelölt felhasználók törlése";
$MSG['5189'] = "Aukció elküldése";
$MSG['5190'] = "Alaphelyzet";
$MSG['5199'] = "Licit jóváhagyása";
$MSG['5200'] = "Kérdés elküldése";
$MSG['5201'] = "Üzenet elküldése";
$MSG['5202'] = "Megfigyeli";
$MSG['5202_0'] = "Megfigyelt tételekbõl törli";
$MSG['5204'] = "Beszúr";
$MSG['5205'] = "Engedélyez/Tílt";
$MSG['5220'] = "Max. 255 karakter";

$MSG['5221'] = "Highlighted items background";
$MSG['5222'] = "<b>NOTE: Home page featured items are shown in the home page, in rows of two auctions wide, so this number should be an even number.</b>";

$MSG['5223'] = "Miniatûr szélessége (A legjobb a 195 képpont vagy kevesebb)";
$MSG['5224'] = "képpont";
$MSG['5225'] = "Fõoldalra kiemelt aukciók";
$MSG['5227'] = "Felfüggesztett aukciók megtekintése";
$MSG['5228'] = "Logo megjelenítése a fõoldalon?";
$MSG['5230'] = "GYIK kategóriák";
$MSG['5231'] = "Új GYIK";
$MSG['5232'] = "GYIK-k kezelése";
$MSG['5233'] = "Egyéb beállítások";
$MSG['5234'] = "Új kategória beszúrása";
$MSG['5235'] = "<b>Megjegyzése</b>: csak olyan kategória törölhetõ amiben nincs GYIK elem.";
$MSG['5236'] = "GYIK";
$MSG['5237'] = "Kategória ID";
$MSG['5238'] = "GYIK kategória";
$MSG['5239'] = "Kérdés";
$MSG['5240'] = "Válasz<br>(HTML elfogadott)";
$MSG['5241'] = "GYIK szerkesztése";
$MSG['5243'] = "Súgó tartalom";
$MSG['5244'] = "AUKCIÓ KEZELÕ";
$MSG['5245'] = "Teteje";
$MSG['5276'] = "Üzenet törlése";
$MSG['5277'] = "Vissza az üzenet listához";
$MSG['5278'] = "Üzenet szerkesztése";
$MSG['5279'] = "Vissza a felhasználói listához";
$MSG['5280'] = "Évek/Hónapok";
$MSG['5281'] = "Havi jelentés";
$MSG['5282'] = "Aktuális hónap megjelenítése";
$MSG['5283'] = "GYIK kategória szerkesztése";
$MSG['5284'] = "Kategóra neve";
$MSG['5285'] = "Napi jelentés";
$MSG['5291'] = "Aktív felhasználók";
$MSG['5292'] = "Soha sem visszaigazolt fiókok";
$MSG['5293'] = "Regisztrációd díjat ki nem fizetett";
$MSG['5294'] = "Adminisztrátor által felfüggesztett";
$MSG['5295'] = "Megtekint";
$MSG['5296'] = "Minden felhasználó";
$MSG['5297'] = "Adósság limitet elértek";

$MSG['5299'] = "Limit submission to";

$MSG['5300'] = " üzenetek elküldve.";
$MSG['5318'] = "Platform statisztika megtekintése";
$MSG['5322'] = "Alapértelmezett ország";
$MSG['5321'] = "You can select a default country for your site.<br>It will automatically appear as the selected country in the countries select box throughout the site.";
$MSG['5323'] = "Alapértelmezett ország módosítva";
$MSG['5408'] = "Max. ";
$MSG['5431'] = "Új jelszó kiküldése számomra";
$MSG['5436'] = "Eszközök";
$MSG['5438'] = "Platform statisztika ";
$MSG['5492'] = "Termékek";
$MSG['5493'] = "licit";
$MSG['5494'] = "licitálta ";
$MSG['5495'] = "minden egyéb ";
$MSG['5506'] = "Pozitív értékelés: ";
$MSG['5507'] = '<span style="color:#CD0000;">Negatív értékelés:</span> ';
$MSG['5508'] = "Mióta tag ";
$MSG['5509'] = "Értékelések száma ";
$MSG['_0001'] = "Karbantartás alatt oldal";
$MSG['_0002'] = "Átmenetileg letilthatja a weboldalhoz a hozzáférést.<br>
                        Karbantartási módban a felhasználó csak ehhet fér hozzá. Miután regisztrált az <a target=\"_new\" href=\"../new_account\">a szokásos regisztrációs oldalon</a>
                        , beszúrta alulra a felhasználónevet. A karbantartási mód után <a target=\"_new\" href=\"../user_login.php\">belépés itt</a> oldalon hozzáfér a weboldalhoz.";
$MSG['_0004'] = "Karbantartási alatt HTML üzenet";
$MSG['_0005'] = "Karbantartási alatt beállítások módosítva";
$MSG['_0006'] = "Váltás a \"Karbantartás alatt\" módra?";
$MSG['_0008'] = "Reklámsávok adminisztrációja";
$MSG['_0010'] = "REKLÁMHÍRDETÉSEK";
$MSG['_0012'] = "Felhasználó kezelõ";
$MSG['_0014'] = "Az uAuction reklámhírdetési redszere véletlenül választ reklámot az adatbázisból, amint engedélyezi
                        a szûrõt akkor szabad csak reklámot beilleszteni
                        <br>Az elsõ dolog a reklámcsíkok méretének beállítása legyen amit használni kíván:";
$MSG['_0015'] = "Bármilyen méret";
$MSG['_0016'] = "Kötött méret (kérem adja meg)";
$MSG['_0017'] = "Szélesség";
$MSG['_0018'] = "Magasság";
$MSG['_0022'] = "Cég";
$MSG['_0024'] = "Reklámcsíkok kezelése";
$MSG['_0025'] = "Reklámcsíkok";
$MSG['_0026'] = "Felhasználót hozzáad";
$MSG['_0028'] = "Kiválasztott felhasználók törlése (Reklámok is törlõdnek)";
$MSG['_0029'] = "kiválasztott reklámok";
$MSG['_0030'] = "URL";
$MSG['_0031'] = "Szöveg a reklámhírdetés alatt";
$MSG['_0032'] = "ALTERNATÍV szöveg";
$MSG['_0033'] = "<b>Szûrõk</b>";
$MSG['_0035'] = "Kulcsszavak";
$MSG['_0036'] = "Elfogadott formátumok: GIF, JPG, PNG, SWF";
$MSG['_0037'] = "Teljes URL beleértve a http:// -t";
$MSG['_0038'] = "Üresen hagyható";
$MSG['_0039'] = "Van lehetõsége arra, hogy kiszûrje a reklámok állandó ismétlõdését:
                        <ul>
                        <li><b>Kategóriák</b>: válasszon egyet vagy többet az alábbi kategóriából. A reklám csak akkor jelenik meg ha a kiválasztott kategória látható (azaz kategória listázása, aukciós oldal)
                        <li><b>Kulcsszavak</b>: válasszon egy vagy több kulcsszót (soronként egy). A reklám csak azokon az aukciós oldalakon jelenik meg ahol a termék neve vagy leírása tartalmazza a megadott kulcsszavakból egyet.
                        </ul>
                        A <b>Kategóriák</b> szûrõt alkalmazzuk a Kategóriák böngészésekor és a Termék oldalakon.<br>
                        A <b>Kulcsszavak</b> szûrõt csak az aukciós oldalakon alkalmazzuk.<br>
                        Amennyiben egy szûrõ sem ad eredményt akkor véletlen reklám kerül megjelenítésre (köztük olyanok akiknek nincs szûrõ beállítva).";
$MSG['_0040'] = "Reklámhírdetések hozzáadása";
$MSG['_0041'] = "<b>Új reklámhírdetés</b>";
$MSG['_0042'] = " (szükséges)";
$MSG['_0043'] = "<b>Felhasználói reklámhírdetések</b>";
$MSG['_0044'] = "Kérem adjon meg valós URL-t";
$MSG['_0045'] = "Vásárlások megjelenítése";
$MSG['_0046'] = "A nulla vagy üres az korlátlan megjelenítést jelent";
$MSG['_0047'] = "%s már létezik";
$MSG['_0048'] = "Hibás állománytípus. Elfogadott formátumok: GIF, JPG, PNG, SWF";
$MSG['_0049'] = "Megtekintések:";
$MSG['_0050'] = "URL:";
$MSG['_0051'] = "Kattintások:";
$MSG['_0052'] = "Szûrõk megjelenítése";
$MSG['_0053'] = "<b>Kategóriák</b>";
$MSG['_0054'] = "<b>Kulcsszavak</b>";
$MSG['_0055'] = "Reklámhírdtések szerkesztése";
$MSG['_0056'] = "Új Reklámhírdetés";
$MSG['_0148'] = "Aukció újraindítása";
$MSG['_0151'] = " szor";
$MSG['_0153'] = "Újraindítás / <br>Újraindítva";
$MSG['_0161'] = "Automatikus újraindítás";
$MSG['_0162'] = "Választhatja az automatikus újraindítást az aukcióin, ha nincs érvényes licit rajta.";
$MSG['_0163'] = "Nyertesek megjelenítése";

$MSG['2_0004'] = "Felhasználók IP címeinek megjelenítése";
$MSG['2_0005'] = "Regisztrációs IP";
$MSG['2_0006'] = "Kitílt";
$MSG['2_0007'] = "Elfogad";
$MSG['2_0009'] = "IP cím";
$MSG['2_0012'] = '<span style="color:#A2CD5A;"><b>Elfogadva</b></span>';
$MSG['2_0013'] = '<span style="color:#CD0000;"><b>Kitíltva</b></span>';
$MSG['2_0015'] = "Kijelöltek feldolgozása";
$MSG['2_0017'] = "IP címek";
$MSG['2_0020'] = "IP tíltások kezelése";
$MSG['2_0021'] = "Ennek az IP címnek a tíltása: ";
$MSG['2_0024'] = "(Egészítse ki az IP címet - pl.: 185.39.51.63)";
$MSG['2_0025'] = "Kézi megadás";
$MSG['2_0026'] = "Nagyon sajnáljuk de egy vagy több okból letíltottuk a hozzáférését az oldalhoz<br>
                                Amennyiben volt nyitott aukciója, akkor töröltük az összes licitet és a
                                terméket az adatbázisból.
                                <br><br>
                                Köszönjük";
$MSG['2_0027'] = "Az Ön IP címe tíltólistán szerepel";
$MSG['2_0032'] = "Aukció hosszabbító beállítások";
$MSG['2_0034'] = "Automatikus aukció hosszabbító engedélyezése?";
$MSG['2_0035'] = "Aukció hosszabbító ";
$MSG['2_0036'] = " másodpercek, ha valaki licitált az utolsó  ";
$MSG['2_0037'] = " másodpercek";
$MSG['2_0038'] = "Kérem adjon meg valós értéket";
$MSG['2_0039'] = "Az automatikus aukció hosszabító lehetõséget biztosít arra, hogy <b>X</b> másodperccel
                                hosszabbítsuk az aukció idõtartamát ha valaki licitált <b>Y</b> másodperccel a vége elõtt.<br>";
$MSG['2__0001'] = "Válasszon nyelvet";
$MSG['2__0002'] = "Többnyelvûség támogatása";
$MSG['2__0003'] = "<br>The default language is English.<br>
                                If you want to enable multilingual support or change the default language you must follow the steps below:
                                <ul>
                                <li>If you want to add a new language make a duplicate of language/EN and name it with the
                                appropriate name: i.e. if you are translating to French you will need to name the directory FR
                                <br>
                                The first thing is to define the characters encoding your language requires at the top of the messages file.
                                The variable you must edit is <i>\$CHARSET</i>. UTF-8 encoding should work with almost all the languages.<br>
                                Next you'll have to define the document reading direction. The variable to modify is <i>\$DOCDIR</i>
                                and can have two possible values:
                                <ul>
                                <li><b>ltr</b> (left-to-right): this is the default and means the text must be read from left to right
                                <li><b>rtl</b> (right-to-left): means the text must be read from right to left (i.e. Arabian, Hebrew, etc)
                                </ul>
                                Once changed <i>\$CHARSET</i> and <i>\$DOCDIR</i> according to the language you are translating into,
                                 you will have to translate all the error messages and user interface messages contained in the messages file.

                                <li>You will then need the flag(s) GIFs for the languages you are going to use, in the inc/flags directory.
                                Get the flag(s) you need and change the file name(s) to be XX.gif, where XX is the language code for your country language.
                                <br>Copy the renamed flag file(s) in the inc/flags/ directory.
                                <br><b>Note:</b> for each translation, you need the corresponding XX.gif file in inc/flags/
                                <li>Select the default language below. All the other available languages will be available in the home page (the corresponding flags will be shown).
                                </ul>
                                ";
$MSG['2__0004'] = "Alapértelmezett nyelv";
$MSG['2__0005'] = '<span style="color:#CD0000;"><b>Jelenlegi alapértelmezett nyelv</b></span>';
$MSG['25_0011'] = "Reklám";
$MSG['2__0016'] = "Kezdés dátuma";
$MSG['2__0025'] = "Fixáras";
$MSG['2__0027'] = "Minden";
$MSG['2__0028'] = "Kiválaszt mind";
$MSG['2__0029'] = "Nincs üzenete";
$MSG['2__0030'] = " azt jelenti, hogy a bejegyzés nem törölhetõ, mert éppen használatban van.";
$MSG['2__0031'] = "Valóban törölni kívánja ezeket az üzeneteket?";
$MSG['2__0037'] = "Aukció elküldése";
$MSG['2__0038'] = "Válassza ki saját kategóriáját";
$MSG['2__0039'] = "Ha elfelejtette jelszavát akkor adja meg felhasználónevét és E-Mail címét.";
$MSG['2__0041'] = "Válasszon második kategóriát";
$MSG['2__0045'] = " képpont ";
$MSG['2__0047'] = "KATEGÓRIA VÁLASZTÁSA &gt;&gt;";
$MSG['2__0048'] = "Bezárja most!";
$MSG['2__0050'] = "Hasonló eladás";
$MSG['2__0051'] = "Újraindít";
$MSG['2__0054'] = '<span style="#CD0000;"><b>Már ki lett választva</b></span>';
$MSG['2__0056'] = "Felfüggesztett aukciók";
$MSG['2__0057'] = "Számlálók megjelenítése";
$MSG['2__0058'] = "Ön el tudja dönteni, hogy az oldal fejlécén megjelenítse-e a számlálókat.<br>
                                Négyféle számláló érhetõ el jelenleg:
                                <ul>
                                <li>Aktív aukciók</li>
                                <li>Regisztrált felhasználók</li>
                                <li>Online vendégek</li>
                                <li>Online felhasználók</li>
                                </ul>
                                Engedélyezheti/tílthatja a számlálókat itt";
$MSG['2__0059'] = "Online vendégek";
$MSG['2__10059'] = "ONLINE FELHASZNÁLÓK";
$MSG['2__100591'] = "Felhasználók";
$MSG['2__0060'] = "Aktív aukciók";
$MSG['2__0061'] = "Regisztrált felhasználók";
$MSG['2__0062'] = "Számlálók amiket meg szeretne jeleníteni";
$MSG['2__0063'] = "Számláló beállítások módosítva";
$MSG['2__0064'] = "ONLINE VENDÉGEK";
$MSG['2__00642'] = "Vendégek";
$MSG['2__0066'] = "Engedélyez";
$MSG['2__0067'] = "Tílt";
$MSG['25_0001'] = "NYERTES";
$MSG['25_0002'] = "ELADÓ";
$MSG['25_0004'] = "Felhasználó neve";
$MSG['25_0005'] = 'Felhasználüi regisztrációs mezõk';
$MSG['25_0006'] = "Utolsó licit";
$MSG['25_0008'] = "Tulajdonságok";
$MSG['25_0009'] = "Felület";
$MSG['25_0010'] = "Felhasználók";
$MSG['25_0011'] = "Reklám";
$MSG['25_0012'] = "Díjak";
$MSG['25_0015'] = "Hírújság elküldése";
$MSG['25_0018'] = "Tartalma";
$MSG['25_0023'] = "Statisztika";
$MSG['25_0025'] = "Beállítások áttekintõje";
$MSG['25_0026'] = "Ütemezett feladat (cron.php)";
$MSG['25_0027'] = "Kérem ellenõrizze, hogy az ütemezett feladat idõközönként futtatja a cron.php-t (a 15 perces intervallum az megfelelõ).";
$MSG['25_0031'] = "Statisztika";
$MSG['25_0032'] = " perc";
$MSG['25_0033'] = " másodperc";
$MSG['25_0035'] = "Óra korrekció";
$MSG['25_0036'] = "GMT";
$MSG['25_0037'] = " óra";
$MSG['25_0038'] = "Számlálók megjelenítése a fejlécben:<br>";
$MSG['25_0040'] = "Oldaltájolás";
$MSG['25_0041'] = "Megjeleníti a fõoldalon";
$MSG['25_0042'] = "Belépési modul";
$MSG['25_0043'] = "Újdonság modul";
$MSG['25_0044'] = "Újdonság megjelenítése";
$MSG['25_0045'] = "Miniatûr szélessége";
$MSG['25_0048'] = "Egyéb miniatûr: ";
$MSG['25_0049'] = "Hírlevél feliratkozás";
$MSG['25_0055'] = "Regisztrált aktív felhasználó";
$MSG['25_0056'] = "Felfüggesztett felhasználók";
$MSG['25_0057'] = "Élõ aukciók";
$MSG['25_0059'] = "Licitek élõ aukciókon";
$MSG['25_0063'] = "Mai hozzáférések";
$MSG['25_0071'] = "Termék leírások";
$MSG['25_0074'] = "E-Mail újraküldése";
$MSG['25_0075'] = "Regisztrációt megerõsítõ E-Mail-k újraküldése";
$MSG['25_0076'] = "E-mail újraküldése";
$MSG['25_0077'] = "HTML nem elfogadott";
$MSG['25_0078'] = "E-mail elküldve a ";
$MSG['25_0079'] = "Hírlevél";
$MSG['25_0080'] = "Összesítés";
$MSG['25_0081'] = "Fiókom";
$MSG['25_0082'] = "Eladás";
$MSG['25_0083'] = "Vásárlás";
$MSG['25_0084'] = "Új termék kulcsszó hozzáadása";
$MSG['25_0085'] = "Emlékezzen rám";
$MSG['25_0086'] = "Kattintson az alábbi gombra a megvásárolandó termék teljes összegének a kifizetéséhet amennyiben Ön az aukció nyertese";
$MSG['25_0087'] = "Önre rálicitáltak";
$MSG['25_0088'] = "Jelenleg Öné a legmagasabb licit";
$MSG['25_0089'] = "Gratulálunk, Öné a termék. Kérem fizesse ki most";
$MSG['25_0107'] = "Miniatûrök méretei";
$MSG['25_0108'] = "Fõoldal miniatûre";
$MSG['25_0109'] = "Kategóriák miniatûrei";
$MSG['25_0110'] = "Elfogadási szöveg";
$MSG['25_0115'] = "Függõ aukciók";
$MSG['25_0116'] = "Ekkor indult";
$MSG['25_0117'] = "Ekkor zárul";
$MSG['25_0118'] = "Indítás most!";
$MSG['25_0119'] = "Eladott termékek";
$MSG['25_0121'] = "Lezárva";
$MSG['25_0133'] = "Regisztrálni szeretném";
$MSG['25_0134'] = "<b>Eladó</b> (eladhat és licitálhat termékekre)";
$MSG['25_0135'] = "<b>Csak vásárló</b> (csak licitálhat termékekre)";
$MSG['25_0136'] = "Adminisztrátori jóváhagyás szükséges";
$MSG['25_0137'] = "Választania kell a fióktípust (Eladó vagy Vásárló)";
$MSG['25_0138'] = "Eladók";
$MSG['25_0139'] = "Vásárlók";
$MSG['25_0140'] = "A Fióktípusa Vásárló. A termékek eladása nem engedélyezett.<br> Amennyiben váltani szeretne <b>Eladói fiókra</b> ";
$MSG['25_0141'] = "küldjön egy kérést az adminisztrátornak";
$MSG['25_0142'] = "Kérés elküldve az webolda-adminisztrátornak.";
$MSG['25_0143'] = "Önnek Vásárló típusú fiókja van. Az eladás nem engedélyezett.<br> A  kérelem már elküldésre került a <b>Eladó típusú</b> fiókra váltáshoz: kérelme feldolgozás alatt van. ";
$MSG['25_0146'] = "Kategóriák sorrendje";
$MSG['25_0147'] = "A kategória listát a fõoldalon sorrendbe teheti, <b>ABC soorend</b> vagy az aukciók darabszáma alapján (<b>kategória számlálók</b>).<br>
                        Válasszon az alábbi sorrendezlsi lehetõségek között";
$MSG['25_0148'] = "ABC sorrend";
$MSG['25_0149'] = "Kategória számlálók";
$MSG['25_0150']= "Kategória sorrend beállítások módosítva";
$MSG['25_0151'] = "Felhasználók hitelesítése";
$MSG['25_0151_a'] = "Felhasznói megerösítési mód";
$MSG['25_0152'] = "Amennyiben ezt engedélyezi akkor a felhasználóktól jelszót kérünk az aukciók elhagyása értékelés miatt, aukciók elküldése vagy licitálás esetén";
$MSG['25_0152_a'] = "Az uAuctions minden felhasználótól megerösítést kér a fiók aktiválásához. Kérem válassza ki, melyik módot kívánja használni";
$MSG['25_0152_b'] = "Az adminisztrátor engedélyezheti csak a fiókokat.";
$MSG['25_0152_c'] = "A felhasználó maga engedélyezheti a fiókját";
$MSG['25_0152_d'] = "A felhasználói fiók automatikusan aktiválódik a regisztrációkor (nem ajánlott)";
$MSG['25_0155'] = "Eladói kapcsolt módosítva";
$MSG['25_0157'] = "Az Ön háttérképe";
$MSG['25_0166'] = "Vissza az aukcióhoz";
$MSG['25_0167'] = "ikon";
$MSG['25_0169'] = "Tagásgi szint";
$MSG['25_0169a'] = "Verzió ellenõrzése";
$MSG['25_0170'] = "Az alábbi ûrlap segítségével szerkesztheti, törölhet, vagy hozzáadhat tagsági szintet. \"Pontok\" limitet jelentenek  (minimum szintet beleértve), \"tagság\" az adott szint megnevezése, \"ikon\" a névhez és szinthez tartozó megjelenített ikon, relatív elérési \"images/icons/\" úttal";
$MSG['25_0171'] = "Pont tulajdonosa";
$MSG['25_0172'] = "Tagság típusa";
$MSG['25_0176'] = "Átalakít";

$MSG['25_0177'] = "for";

$MSG['25_0178'] = "HTML cimkék";
$MSG['25_0180'] = "Meta leírások cimkéi";
$MSG['25_0181'] = "Meta kulcsszavak cimkéi";
$MSG['25_0182'] = "A Meta leírások cimkéket a keresõkben használjuk, hogy az oldalát meg tudjuk jeleníteni a találati listákban.<br>
                                Kérem adjon szavakat, melyek a legjobban jellemzik az oldalát.";
$MSG['25_0184'] = "A Meta kulcsszavak cimkéit a keresõmotor adatbázisban rögzíti és sorrendezi.<br>
                                Kérem kulcsszavakat adjon meg vesszõvel elválasztva (i.e. könyv, könyvaukció, könyv eladás).";
$MSG['25_0185'] = "Meta cimkék beállítások módosítva";
$MSG['25_0186'] = "Képfeltöltések";
$MSG['25_0187'] = "Adja meg a képek maximális méretét (KByte-ban) amit az eladók feltölthetnek az aukcióikhoz.";
$MSG['25_0188'] = "Aukció emlékeztetõ E-Mail-k";
$MSG['25_0189'] = "Eladóként kérhet egy emlékeztetõ E-Mail-t minden befejezett aukcióról vagy napi rendszerességgel jelentést kaphat az aznap lezárult aukciókról.<br>
                                A második beállítás elõnyösebb ha nagyon magas az aukciók száma.<br>Végezetül választhatja, hogy nincs emlékeztetõ E-Mail de ez nem célszerû.";
$MSG['25_0190'] = "<b>Minden</b> aukcióról külön email kerül kiküldésre";
$MSG['25_0191'] = "Napi rendszerességgel egy összesített E-Mail kerül kiküldésre";
$MSG['25_0192'] = "E-Mail emlékeztetõk beállítása";
$MSG['25_0193'] = "Nincs emlékeztetõ E-Mail";
$MSG['25_0195'] = "Választhat, hogy minden aukcióról kapjon emlékeztetõ E-Mail-t vagy nem.";
$MSG['25_0196'] = "Kapjon <b>aukció elindítását visszaigazoló E-Mail-t</b>.";
$MSG['25_0197'] = "Nem kíván kapni <b>aukció elindítását visszaigazoló E-Mail-t</b>.";
$MSG['25_0199'] = "Lezáruló aukció folytatódik";
$MSG['25_0209'] = "Eladás ár alatt";
$MSG['25_0214'] = "Keresés a lezárult aukciókban: ";
$MSG['25_0215'] = "Szállítási feltételek";
$MSG['25_0216'] = "Kapcsolat az eladóval";

$MSG['25_0217'] = "Annak beállítása, hogy bárki szabadon kapcsolatba léphessen az ELADÓ-val a weboldalon keresztül. Az uAuctions lehetõséget
                                biztosít, hogy az ELADÓ milyen módon (csak a weboldalon keresztül vagy nem) legyen elérhetõ.";
$MSG['25_0218'] = "Bármilyen látogató kapcsolatba léphessen az eladóval (annak lehetõsége, hogy az ELADÓ minden esetben látható legyen)";
$MSG['25_0219'] = "Csak a bejelentkezett felhasználók léphetnek kapcsolatba az eladóval (annak lehetõsége, hogy az ELADÓ csak a regisztrált és belépett felhasználók részére jelenjen meg)";
$MSG['25_0220'] = "Senki sem tud kapcsolatba lépni az eladóval (az eladó SOSEM jelenik meg a weboldalon)";
$MSG['25_0223'] = "Értékelést kapott";
$MSG['25_0224'] = "Elõnézet";

// multi-language months
$MSG['MON_001'] = "Jan";
$MSG['MON_001E'] = "Január";
$MSG['MON_002'] = "Feb";
$MSG['MON_002E'] = "Február";
$MSG['MON_003'] = "Márc";
$MSG['MON_003E'] = "Március";
$MSG['MON_004'] = "Ápr";
$MSG['MON_004E'] = "Április";
$MSG['MON_005'] = "Máj";
$MSG['MON_005E'] = "Május";
$MSG['MON_006'] = "Jún";
$MSG['MON_006E'] = "Június";
$MSG['MON_007'] = "Júl";
$MSG['MON_007E'] = "Július";
$MSG['MON_008'] = "Aug";
$MSG['MON_008E'] = "Augusztus";
$MSG['MON_009'] = "Szep";
$MSG['MON_009E'] = "Szeptember";
$MSG['MON_010'] = "Okt";
$MSG['MON_010E'] = "Október";
$MSG['MON_011'] = "Nov";
$MSG['MON_011E'] = "November";
$MSG['MON_012'] = "Dec";
$MSG['MON_012E'] = "December";

//time zones
$MSG['TZ_12'] = '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka';
$MSG['TZ_11'] = '(GMT +11:00) Magadan, Solomon Islands, New Caledonia';
$MSG['TZ_10'] = '(GMT +10:00) Eastern Australia, Guam, Vladivostok';
$MSG['TZ_9'] = '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk';
$MSG['TZ_8'] = '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong';
$MSG['TZ_7'] = '(GMT +7:00) Bangkok, Hanoi, Jakarta';
$MSG['TZ_6'] = '(GMT +6:00) Almaty, Dhaka, Colombo';
$MSG['TZ_5'] = '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent';
$MSG['TZ_4'] = '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi';
$MSG['TZ_3'] = '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg';
$MSG['TZ_2'] = '(GMT +2:00) Kaliningrad, South Africa';
$MSG['TZ_1'] = '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris';
$MSG['TZ_-12'] = '(GMT -12:00) Eniwetok, Kwajalein';
$MSG['TZ_-11'] = '(GMT -11:00) Midway Island, Samoa';
$MSG['TZ_-10'] = '(GMT -10:00) Hawaii';
$MSG['TZ_-9'] = '(GMT -9:00) Alaska';
$MSG['TZ_-8'] = '(GMT -8:00) Pacific Time (US &amp; Canada)';
$MSG['TZ_-7'] = '(GMT -7:00) Mountain Time (US &amp; Canada)';
$MSG['TZ_-6'] = '(GMT -6:00) Central Time (US &amp; Canada), Mexico City';
$MSG['TZ_-5'] = '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima';
$MSG['TZ_-4'] = '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz';
$MSG['TZ_-3'] = '(GMT -3:00) Brazil, Buenos Aires, Georgetown';
$MSG['TZ_-2'] = '(GMT -2:00) Mid-Atlantic';
$MSG['TZ_-1'] = '(GMT -1:00) Azores, Cape Verde Islands';
$MSG['TZ_0'] = '(GMT) Western Europe Time, London, Lisbon, Casablanca';

$MSG['26_0000'] = "Alapértelmést beállít";
$MSG['26_0001'] = "Licit elõzmény";
$MSG['26_0002'] = "Sablonkezelõ";
$MSG['26_0003'] = "Sablon állományok szerkesztése";
$MSG['26_0004'] = "Sablon állomány hozzáadása";
$MSG['26_0005'] = "Alapértelmezett sablon módosítva";
$MSG['30_0029'] = "Megadható a fõoldal bal oldalán megjelenített kategórák száma";
$MSG['30_0030'] = "Megjelenített kategóriák: ";
$MSG['30_0031'] = "Gyorstár törlése";
$MSG['30_0031a'] = "Weboldal logo";
$MSG['30_0032'] = "Minden sablon gyorstár törlése, ami akkor szükséges ha sokszor módosítja a sablon-állományokat";
$MSG['30_0033'] = "Gyorstár törölve";
$MSG['30_0049'] = "Hírlevél beállítások módosítva";
$MSG['30_0053'] = "<p>Néhány ingyenes E-Mail szolgáltatót ki kell tiltani a weboldalról. Kérem ne adjon meg E-Mail címet az alábbi szolgáltatóktól:</p>";
$MSG['30_0055'] = "Az uAuction automatikusan küld ki hírlevelet <b>HTML</b> formában,<br /> ezért szükséges, hogy használja a <code>&lt;BR&gt;</code> cimkéket minden új sor esetén.<br /> Ellenkezõ esetben a kiküldött üzenet egy soros formázatlan szövegként fog megjelenni.";
$MSG['30_0062'] = "Kérem adjon meg legalább 4 karaktert";
$MSG['30_0063'] = "Csak Fixárasak?";
$MSG['30_0064'] = " <b>Fixáras</b> aukciók engedélyezése?";
$MSG['30_0065'] = "A <b>Fixáras</b> beállítás lehetõséget biztosít az eladóknak, hogy létrehozzanak olyan aukciókat ahol nem lehet licitálni és csak a <b>Megvásárol most</b> jelenik meg (kötött a termék ára). <br><b>Megjegyzés:</b> A <b>Csak Fixáras</b> beállítás csak akkor mûködik ha a <b>Fixáras</b> engedélyezve van.";
$MSG['30_0066'] = "Fixáras beállítások módosítva";
$MSG['30_0067'] = "<b>Fixáras</b> aukció";
$MSG['30_0069'] = "Eladó: módosítja ezt az aukciót";
$MSG['30_0070'] = "Keresés csak ebben a kategóriában";
$MSG['30_0080'] = "Fizetési beállítások";
$MSG['30_0081'] = "Megtekintve ";
$MSG['30_0084'] = "Eldöntheti,hogy a felhasználók elrejtik/megjelenítik egymás elõl az E-Mail címeiket és minden kommunikáció csak privát üzenetben történik";
$MSG['30_0085'] = "Felhasználói E-Mail elrejtve";
$MSG['30_0086'] = "Címek: ";
$MSG['30_0087'] = "Valóban alkalmazni kívánja a kiválasztott aukciókra?";
$MSG['30_0098'] = "&nbsp; = Rálicitálva";
$MSG['30_0100'] = "<b>Fixáras</b> aukciók";
$MSG['30_0101'] = "<b>Csak fixáras</b> aukciók";
$MSG['30_0102'] = "Mindent kijelöl/levesz";
$MSG['30_0176'] = "Nyertes megtekintése";
$MSG['30_0177'] = "Aukció vége";
$MSG['30_0178'] = "&nbsp;&nbsp;Ennek az aukciónak nincs nyertese";
$MSG['30_0179'] = "Nyertes licit";
$MSG['30_0180'] = "Teljes licitelõzmény";
$MSG['30_0181'] = "Üzenõfal";
$MSG['30_0208'] = "Licit helye >>";
$MSG['30_0209'] = "Eladó megismerése";
$MSG['30_0210'] = "*Amint a kulcsszavakkal találunk terméket akkor értesítjük E-Mail-ben";
$MSG['30_0211'] = "Régi uActions verzót használ kérjük látogassa meg a <a href='http://u-auctions.com' target=_blank>oldalt</a>";
$MSG['30_0212'] = "A legfrissebb verziót használja";
$MSG['30_0213'] = "Nincs értékelendõ tranzakciója";
$MSG['30_0214'] = "Fizetett uAuctions verzió";
$MSG['30_0215'] = "Név";
$MSG['30_0216'] = "Bank neve";
$MSG['30_0217'] = "Bankszámlaszám";
$MSG['30_0218'] = "SWIFT kód";
$MSG['30_0219'] = "Átutalás";
$MSG['30_0220'] = 'Valós banknév szükséges';
$MSG['30_0221'] = 'Valós bankszámlaszám szükséges';
$MSG['30_0222'] = 'Valós banki SWIFT kód szükséges';
$MSG['30_0223'] = 'Valós banknév, bankszámlaszám és SWIFT kód szükséges';
$MSG['30_0224'] = 'Al adminisztrátor';
$MSG['30_0225'] = 'Fõadminisztrátor';
$MSG['30_0226'] = 'Ez a felhasználó egy adminisztrátor';
$MSG['30_0227'] = 'Ez a felhasználó a fõadminisztrátor';
$MSG['30_0228'] = 'Eladói adatok';
$MSG['30_0229'] = 'Adminisztrátor mappa elérésri út/név';
$MSG['30_0230'] = '<span style="color:red">Az Elérési út/név -t ne zárja le / jellel</span><br>Maximálisan 100 karakter adható meg';
$MSG['30_0231'] = 'Az Adminisztrációs mappa átnevezésre került';
$MSG['30_0232'] = 'Aukciókra-Vissza';

$MSG['30_0233'] = "Internetes süti szabályzat oldal";
$MSG['30_0234'] = "Aktiválja az Interenetes sütik szabályzati oldalt?";
$MSG['30_0235'] = "Interenetes sütik szabályzat tartalma<br>(HTML elfogadott)";
$MSG['30_0236'] = "Ez a beállítás engedélyezi az Internetes sütik szabályzata linket melyet minden oldal alján megjelenítünk.";
$MSG['30_0237'] = "Internetes sütik szabályzata beállítások módostíva";
$MSG['30_0238'] = "Interenetes sütik szabályzat tartalma<br>(HTML allowed)";
$MSG['30_0239'] = "Interenetes sütik szabályzata";
$MSG['30_0240'] = 'Ez az oldal sütiket használ kérjük olvassa el és fogadja el <a href="' . $system->SETTINGS['siteurl'] . 'contents.php?show=cookies" class="new-window">Internetes sütik szabályzata</a>';


$MSG['30_0110'] = "Szállítás";
$MSG['350_1004'] = "Mennyiség";
$MSG['350_1008'] = "Egyéb szállítás";
$MSG['350_1009'] = "Egyéb szállítási költségek";
$MSG['350_10111'] = "Felhasználó belépve";
$MSG['350_10112'] = "Felhasználó kijelentkezve";
$MSG['350_10114'] = "Az Ön bejelentkezésének elrejtése";
$MSG['350_10115'] = "Bejelentkezési állapot be/ki kapcsolása";
$MSG['350_10116'] = "Válassza ki, hogy be/ki akarja kapcsolni az Ön bejelentkezési állapotát. Ezutn kattintson a Mentés gombra.";
$MSG['350_10025'] = "Avatar feltöltése";
$MSG['350_10017'] = "Szállítva";
$MSG['350_10018'] = "Szállítási állapot";
$MSG['350_10019'] = "Megjelölve, hogy szállítva";
$MSG['350_10021'] = "Megjelölve, hogy megérkezett";
$MSG['350_10020'] = "A termék még nincs szállítva";
$MSG['350_10022'] = "Termék megérkezett";
$MSG['350_10023'] = "Megérkezett";
$MSG['350_10024'] = "Fizetésre vár";
$MSG['350_10026'] = "Digitális termék";

///Digital Item auction
$MSG['350_1010'] = "Digitális termék";
$MSG['350_10171'] = "Digitális termék feltöltése";
$MSG['350_10172'] = "Állomány mentése:&nbsp;&nbsp;&nbsp;";
$MSG['350_10173'] = "Új állomány feltöltve:&nbsp;&nbsp;&nbsp;";
$MSG['350_10174'] = "flv,&nbsp;&nbsp; mp3,&nbsp;&nbsp; pdf,&nbsp;&nbsp; rar,&nbsp;&nbsp; zip <br>";
$MSG['350_10175'] = "Állománytípusok amelyeket elfogadunk";
$MSG['350_10176'] = "Maximálisan elfogadott állományméret (KByte-ban)";
$MSG['350_10177'] = "Letöltés";
$MSG['350_10179'] = "Digitális termék beállítások";
$MSG['350_10180'] = "Maximális digitális mérete";
$MSG['350_10181'] = "Adja meg az eladó által feltölthetõ maximális digitális termék file-méretét";
$MSG['350_10182'] = "Elfogadott terméktípusok";
$MSG['350_10183'] = "Adja meg az elfogadott file-ok típusát (pl.flv, mp3, pdf, rar, zip)";
$MSG['350_10184'] = "Digitális termék aukciók engedélyezése";

$MSG['350_10185'] = "<span style='color:red'>To use the digital item auction feacher you will need to turn on the buy now only feacher.</span><br>If you activate this option, the seller will be able to sell digital items.";
///End of Digital Item auction

///Login with facebook
$MSG['350_10167'] = "Megosztás Facebook-n";
$MSG['350_10168'] = "Megosztás Twitter-n";
$MSG['350_10169'] = "Megosztás Google+-n";
$MSG['350_10186'] = "Facebook segítségével kapcsolódott <br> Kérem adja meg a hiányzó adatokat a fiók regisztrációjához";
$MSG['350_10187'] = "Nem Facebook-al kapcsolódott";
$MSG['350_10188'] = "Amennyiben a Kapcsolódás Facebook-al gombot kívánja használni, kérem kövesse az alábbi lépéseket
<br>1. Kattintson a Facebook nyomógombra
<br>2. pótolja a hiányzó személyes adatokat
<br>3. Kattintson a Regisztrál most gombra
<br>4. Aktiválja fiókját
<br>5. Most már használhatja a Facebook gombot a weboldalra történõ belépéshez.";
$MSG['350_10189'] = "Személyes adatok";
$MSG['350_10190'] = "Fiókja össze van kapcsolva a Facebook-al";
$MSG['350_10191'] = "Fiókja nincs összekapcsolva a Facebook-al";
$MSG['350_10192'] = "Amennyiben fiókját össze kívánja kapcsolni a Facebook-al, kövesse az alábbi lépéseket.
<br>1. Kattintson a Facebook nyomógombra
<br>2. Kattintson a Mentés gombra
<br>3. Most már használhatja a Kapcsolódás Facebbok-al gombot az oldalra történõ belépéshez.";
$MSG['350_10194'] = "Facebook app id";
$MSG['350_10195'] = "Facebook app secret";
$MSG['350_10196'] = "Facebook belépés";
$MSG['350_10197'] = "Akarja, hogy a felhasználók bejelentkezhessen a Facebook segítségével";
$MSG['350_10198'] = "Adja meg facebook app id-t";
$MSG['350_10199'] = "Adja meg facebook app secret-t";
$MSG['350_10200'] = "lehetõvé teszi, hogy a felhasználók a Facebook fiókjukat használják belépéshez és regisztrácihoz, ehhez kell egy Facebook app id és secret key.<br> <a href='https://developers.facebook.com'>Kattintson id</a> a Facebook app adatok elkészítéséhez.";
$MSG['350_10201'] = "Facebook fejlesztõk";
$MSG['350_10202'] = "Kattintson a Kapcsolódás Facebook-al nyomógombra a fiók összekapcsolásához a Facebook-al.";
$MSG['350_10203'] = "Kattintson a Mentés gombra, hogy véglegesítse a Facebook fiók és a saját fiók összepárosításához.";
$MSG['350_10204'] = "Kell, hogy rendelkezzen érvényes fiókkal, hogy használja a belépéshez a Facebook fiókját.";
/// End of Login with facebook

$MSG['350_10205'] = "Tallózás";
$MSG['350_10206'] = "Kiemelt aukciók";
$MSG['350_10207'] = "Kapcsolat";
$MSG['103600'] = "Termék állapota";
$MSG['103700'] = "Gyártó";
$MSG['103800'] = "Model";
$MSG['103900'] = "Szín";
$MSG['104000'] = "Gyártás éve";
$MSG['104100'] = "Állapottábla";
$MSG['104200'] = "Az alábbi ûrlappal szerkesztheti, törölheti vagy hozzáadhat állapotokat.";
$MSG['104300'] = "Állapot információk módosítva!";
$MSG['104400'] = "Termék leírás";

$MSG['OPENBUBBLE']='&nbsp;&nbsp;&nbsp;<a href="#" class="tt"><img src="images/info_button_s.png" height="18" width="18" align="top" border="0"><span class="tooltip"><span class="top"></span><span class="middle">';
$MSG['CLOSEBUBBLE']='</span><span class="bottom"></span></span></a>';
$MSG['REQUIRED']='<font color="red"><b>* Szükséges információk</b></font>';
$MSG['ASTERIX']='<font color="red">*&nbsp;&nbsp;</font>';

$MSG['1044'] ="What does each Item Condition mean?";

$MSG['1045a']="Ez lesz az aukció címe. Használjon szavakat amelyekre rákereshetnek a vásárlók.";
$MSG['1046']="egyébb információk megjelenítése a cím alatt, amivel még érdekessebbé teheti termékét";
$MSG['1047']="Gyõzõdjön meg arról, hogy megfelelõ állapotot választott ki. Nagyon fontos, hogy megfelelõen írja le termékét";
$MSG['1048']="írja le a terméket részletesen";
$MSG['1049']="Képek feltöltése az aukciójához - automatikusan elkészülnek a miniatûr képek, így gyorsabban megjelennek a vásárlói listán";
$MSG['1400']="<font color='red'><b>Bejelentések</b></font>";
$MSG['1401']="Bejelentések megtekintése";
$MSG['1402']="Bejelentés információja";
$MSG['1403']="Jelentett ok/hozzászólás";
$MSG['1404']="Termék címe/azonosítója";
$MSG['1405']="Bejelentett okok";
$MSG['1406']="Jelentés okainak beállítása";
$MSG['1407'] = "Az alábbi ûrlappal szerkesztheti, törölheti és hozzáadhat a bejelentések okait.";
$MSG['1408'] = "Bejelentési okok módosítva!";
$MSG['1409']="Válassza ki a bejelentés okát";
$MSG['1410']="Bejelentési lista";
$MSG['1411']="Azonnali hatályú felfüggesztések listája vagy csak értesítés az adminisztrátornak";
$MSG['1412']="<font color='white'> FIGYELEM ! használja a bejelentéskor az 1-t a normálhoz, vagy 2-t a automatikus felfüggesztéshez</font>";
$MSG['1413']="Nem megfelelõ tartalmat talált az oldalon? Mondja el nekünk";
$MSG['1414']="Eladó azonosítója:&nbsp;&nbsp;";
$MSG['1415']="Eladó neve:&nbsp;&nbsp;";
$MSG['1416']="Bejelentõ neve:&nbsp;";
$MSG['1417']="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Azonosítója:";
$MSG['1418']="Bejelentve:&nbsp;";
$MSG['1419']="Aukció címe: ";
$MSG['1420']="Bejelentõ azonosítója:&nbsp;";
$MSG['1421']="Bejelentés oka:&nbsp;";
$MSG['1422']="Bejelentés megjegyzése:&nbsp;";
$MSG['1423']="Bejelentés dátuma:&nbsp;";
$MSG['1424']="Bejelentés lezárása";
$MSG['1425']="Kapcsolat az eladóval";
$MSG['1426']="Kapcsolat a bejelentõvel";
$MSG['1427']="Bejelentõ E-Mail címe:&nbsp;";
$MSG['1428']="Tárgy";
$MSG['1429']="Megjegyzés hozzáadása";
$MSG['1430']="Gyõzõdjön meg arról, hogy a megfelelõ okot állította be, mert az határozza meg, hogy milyen gyorsan reagálunk a bejentésére";
$MSG['1431']="Aukció bejelentése:&nbsp;";
$MSG['1432']="Aukció azonosítója:&nbsp; ";
$MSG['1433']="Az aukciót már bejelentették";
$MSG['1434'] = "<br />
The email sent through uAuctions will be sent automatically in <b>HTML</b>, so,<br /> it is necessary to add a tag <code>&lt;BR&gt;</code> tags for each new line you are going to add.<br /> Otherwise, the message will look like a single line of text without format.<br />";
$MSG['1435']="Fõoldal";
$MSG['1436']="Adminisztrátor értesítése csak";
$MSG['1437']="<b>Megjegyzés: Minden bejelentést adatai bizalmasan kezeljük és nem adjuk ki az Eladónak <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A bejelentésekkel történt visszaéléseket büntetjük</b>";

$MSG['350_10208']="Tárgy:";
$MSG['350_10209']="Google Adsense";
$MSG['350_10210']="Felhasználói aktivitás";
$MSG['30_0181152'] = "Elrejt vagy megjelenít";
$MSG['30_0181153'] = "Kiemelt aukciók";
$MSG['30_0181154'] = "Forró termékek";
$MSG['30_0181155'] = "Hamarosan lezáruló";
$MSG['30_0181156'] = "Legújabb aukciók";
$MSG['30_0181157'] = "Kiemelt aukciók";
$MSG['30_0181158'] = "Ez az aukció bejelentése";
$MSG['30_0181159'] = "Általános aukció";
$MSG['277_1'] = "minden-kategória";
$MSG['277_2'] = "kategória";
$MSG['277_3'] = "keresés";
$MSG['138_1'] = "vissza-az-aukciókhoz";

/// Favourite Sellers
$MSG['FSM1'] = "Hozzáad a Kedvenc eladókhoz";
$MSG['FSM2'] = "Ahhoz hogy a Kedvenc eladókhoz hozzáadja be kell, hogy jelentkezve legyen";
$MSG['FSM3'] = "Ez a személy már szerepel a Kedvenc eladók között";
$MSG['FSM4'] = "Ez a személy most már a Kedvenc Eladók között van. Mostantól a vezérlõpultról megtinthetõ az eladó termékeit";
$MSG['FSM6'] = "Kedvenc eladók megjelenítése";
$MSG['FSM7'] = "Az Ön Kedvenc eladói";
$MSG['FSM8'] = "Nem tudtuk hozzáadni ezt a személyt";
$MSG['30_0087a'] = "Valóban érvényesíteni akarja ezt a kiválasztott eladókon?";
/// End Favourite Sellers

/* Banner system */
$MSG['350_10129'] = "Extra reklámhírdetés díjért cserébe állandó reklámot vásárolhat a fiókjához.<br> Az extra reklámhírdetési díj kifizetése után egy extra reklámhírdetést tölthet fel fiókjához.";
$MSG['350_10130'] = "Új fiók felvétele";
$MSG['350_10131'] = "Kiválasztott fiókok törlése (a hírdetések is törlésre kerülnek)";
$MSG['350_10132'] = "Hírdetés regisztriós díja";
$MSG['350_10133'] = "Reklámhírdetési sávok regisztrációs díja";
$MSG['350_10134'] = "Extra Reklámhírdetések díja";
$MSG['350_10135'] = "Új reklámfiók";
$MSG['350_10136'] = "Extra reklámhírdetés díja";
$MSG['350_10137'] = "Ahhoz, hogy feltöltsön egy reklámot, kérem egy kiválasztott fizetési móddal fizesse ki a díjat <b>%s</b>.";
$MSG['350_10138'] = "Extra reklámcsík díja <b>%s</b>";
$MSG['350_10139'] = "A hírdetési-számla fiók aktiválásához válasszon ki egyet az alábbi fizetési módok közül ahol a <b>%s</b> díjat kívánja kiegyenlíteni .";
$MSG['350_10149'] = "Van egy egyszeri regisztrációs díj melyet a (Új fiók létrehozása) gomb lenyomásával elfogad és ezáltal automatikusan átlép a fizetési oldalra majd az összeg kifizetése után a fiók létrejön és aktiválódik.";
$MSG['350_1012111'] = "Reklámaim";
$MSG['350_1012212'] = "Saját reklámhírdetések kezelése";
$MSG['350_1012222'] = "Vissza";
$MSG['350_1012535'] = "Fiókinformációk kezelése";
$MSG['350_1012333'] = "A maximum szélesség és magasság nem lehet nagyobb 400x100 képpontnál";
$MSG['350_101244'] = "Új fiók létrehozása";
$MSG['350_1012555'] = "Fiók aktiválása";
$MSG['350_10126'] = "Fiók aktiválva";
$MSG['350_10127'] = "Regisztrációs díj";
$MSG['350_10128'] = "Extra reklámhírdetés díja";
$MSG['350_10150'] = "Információk szerkesztése";
$MSG['350_10151'] = "<b>Kulcsszavak</b>: adjon meg egy vagy több szót (soronként egyet). A reklám csak akkor jelenik meg ha legalább egy kulcsszó megegyezik az oldal címével vagy leírásával";
$MSG['350_10152'] = "Belépés a licitáláshoz";
$MSG['350_10153'] = "A felhasználó fizetett az extra reklámhírdetésért";
$MSG['350_1015444'] = "Extra reklémhírdetés engedélyezése";
$MSG['350_1015446'] = "Fiók regisztrációs díja";
$MSG['350_1015445'] = "Extra reklámhírdetés";
/* END Banner system */

$MSG['025_00'] = "Csak kézbe";
$MSG['025_A'] = "Visszaküldést elfogad";
$MSG['025_B'] = "Visszaküldést elfogad.<br> A vásárló a vásárlást követõ 7 napon belül visszaküldheti a terméket.<br>A vásárló fizeti a visszaszállítás költségét.";
$MSG['025_C'] = "Visszaküldés";
$MSG['025_D'] = "Nincs visszaküldésre és visszafizetésre lehetõség";
$MSG['025_E'] = "A vásárlõ 7 napon belül visszaküldheti a megkapott terméket.";

$MSG['350_1015401'] = "Be kell ahhoz lépnie, hogy a terméket megvásárolhassa";
$MSG['350_1015402'] = "Megvásárolja most";
$MSG['350_10193'] = "Érvényes fiókkal kell rendelkeznie a weboldalhoz, hogy használhassa a Kapcsolódás Facebook segítségével nyomógombot.";
$MSG['350_1015403'] = "Licitál most";



$MSG['heading_title']         = 'Fuvarlevél';
$MSG['text_packingslip']      = 'Fuvarlevél/Számla';
$MSG['text_invoice']      = 'számla';
$MSG['text_auction_id']         = 'Aukció azonosító';
$MSG['text_order_invoice']         = 'Rendelés/Számla';
$MSG['text_date_added']       = 'Rendelés dátuma';
$MSG['text_telephone']        = 'Telefonszám:';
$MSG['text_fax']              = 'Fax:';
$MSG['text_to']               = 'Csomagfeladás';
$MSG['text_ship_to']          = 'Szállítás';
$MSG['text_account']          = 'Fiók';
$MSG['text_order']            = 'Rendelési információk';
$MSG['text_order_detail']     = 'Rendelési részletek';
$MSG['text_invoice_no']       = 'Számlaszám:';
$MSG['text_order_id']         = 'Rendelés száma:';
$MSG['text_invoice_date']         = 'Számla dátuma:';
$MSG['text_seller']              = 'Eladó:';
$MSG['350_10119'] = "Paypal";
$MSG['350_10120'] = "Moneyorder";
$MSG['350_10121'] = "Paypal vagy Moneyorder";
$MSG['350_10122'] = "Authorize.net";
$MSG['350_10123'] = "Worldpay";
$MSG['350_10124'] = "2Checkout";
$MSG['350_10125'] = "skrill";
$MSG['column_product']        = 'Termék';
$MSG['column_model']          = 'Model';
$MSG['column_tax']            = 'Adó';
$MSG['column_quantity']          = 'Mennyiség';
$MSG['column_unit_price']         = 'Egységár';
$MSG['column_shipping']          = 'Szállítás dátuma';
$MSG['column_total']         = 'Összesen';
$MSG['unit_price']         = 'Egységár:';
$MSG['shipping_text']          = 'Szállítás:';
$MSG['vat_value_total']        = 'Adó:';
$MSG['total_text']             = 'Összes tartozás:';
$MSG['entry_shipping_method'] = 'Szállítás módja';
$MSG['entry_payment_method']  = 'Fizetés módja';
$MSG['entry_yellow_line']        = 'Yellow line text. It is in the language file.';
$MSG['entry_thankyou']           = 'Köszönjük, hogy nálunk vásárolt, reméljük hamarosan visszatér!';
$MSG['entry_no_orders_selected'] = 'Ön nem választott ki rendelést a fuvarlevél nyomtatásához.';
$MSG['350_562'] = "Összesen";
$MSG['350_10160'] = "Fiók regisztrálása";
$MSG['350_1015404'] = "Ingyenes regisztrációs egyenleg";
$MSG['350_1015405'] = "Hírdetési reklámcsík";

$MSG['3500_1015405'] = "Adminisztráció vezérlõpult";
$MSG['3500_1015406'] = "Vendég!";
$MSG['3500_1015407'] = "Panel bezására";
$MSG['3500_1015408'] = "uAuctions script szerkesztõ";
$MSG['3500_1015409'] = "Összes aukció nyertes megtekintése";
$MSG['3500_1015410'] = "A termék elkelt és fizetésre vár";
$MSG['3500_1015411'] = "A termék elkelt és kifizetve";
$MSG['3500_1015412'] = "A termék csak eladva";
$MSG['3500_1015413'] = "A Facebook az Ön fiókjával összekapcsolásra került. <br>(Kérem nyomja le a Mentés gombot)";
$MSG['3500_1015414'] = "Alapértelmezett séma beállítása";
$MSG['3500_1015415'] = "Szerkesztheti vagy törölheti a botok által használt E-Mail tartományokat.<br>
Pl.: bot@bot_email_block.com<br>
Ahhoz, hogy hozzáad egy új E-Mail tartományr csak másolja át a @ utáni szövegrészt. A fenti példa alapján ezt bot_email_block.com";
$MSG['3500_1015416'] = "E-Mail feketelist";
$MSG['3500_1015417'] = "Tömeges E-Mail tartományok hozzáadása<br>
Adjon soronként egy E-Mail tartományt";
$MSG['3500_1015418'] = "Levélszemét";
$MSG['3500_1015419'] = "Sorrendezés <b>(A)</b>-val kezdve";
$MSG['3500_1015420'] = "Sorrendezés <b>(Z)</b>-vel kezdve";
$MSG['3500_1015421'] = "Kategóriák";
$MSG['3500_1015422'] = "Hirdetési csík maximális szélessége";
$MSG['3500_1015423'] = "Hirdetési csík maximális magassága";
$MSG['3500_1015424'] = "Beállíthatja az elfogadott maximális szélességet. (Méret megadása képpontban)";
$MSG['3500_1015425'] = "Beállíthatja az elfogadott maximális magasságot. (Méret megadása képpontban)";
$MSG['3500_1015426'] = "Hírdetési csíkok típusai";
$MSG['3500_1015427'] = "Elfogadott hírdetéscsíkok (gif,&nbsp; jpg,&nbsp; jpeg,&nbsp; png,&nbsp; swf)";
$MSG['3500_1015428'] = "Paypal tesztelõ";
$MSG['3500_1015429'] = "Bekapcsolhatja a Paypal tesztelõjét, hogy ellenõrizhesse a PayPal IPN-t";
$MSG['3500_1015430'] = "Letöltéseim";
$MSG['3500_1015431'] = "Ön képes letölteni a tartalmat a fizetés után";

///Support Mod
$MSG['3500_1015432'] = "Támogatás";
$MSG['3500_1015433'] = "Beszélgetés";
$MSG['3500_1015434'] = "Felhasználói támogatás";
$MSG['3500_1015435'] = "Ön választ kapott a támogatási csoporttól";
$MSG['3500_1015436'] = "Támogatási csoport";
$MSG['3500_1015437'] = "Felhasználó";
$MSG['3500_1015438'] = "Üzenet";
$MSG['3500_1015439'] = "Küldés idõpontja";
/// End Support Mod

$MSG['3500_1015440'] = "Ön az eladó és nem vásárolhatja ezt a terméket";
$MSG['3500_1015441'] = "Válassszon témát";
$MSG['3500_1015442'] = "Kérjen új kategóriát";
$MSG['3500_1015443'] = "Honlap javaslatok";
$MSG['3500_1015444'] = "Általános érdeklõdés";
$MSG['3500_1015445'] = "Hiba";
$MSG['3500_1015446'] = "Téma";

$MSG['3500_1015447'] = "Ez a modul már telepítve";
$MSG['3500_1015448'] = "Ennek a modulnak a telepítése";
$MSG['3500_1015449'] = "Biztonsági mentés mappa";
$MSG['3500_1015450'] = "Ez a biztonsági mentés mappa";
$MSG['3500_1015451'] = "AutoMod";
$MSG['3500_1015452'] = '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/new_mods.php">Kattintson ide</a>, hogy keressen és telepítsen új modult vagy témát az Ön u-Auction script rendszeréhez.';
$MSG['3500_1015453'] = "Ez a modulok letöltési mappája";
$MSG['3500_1015454'] = "Modul letöltése";
$MSG['3500_1015455'] = '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/mods.php">Kattintson ide</a>, hogy megjelenítse a letöltött modulokat.';
$MSG['3500_1015456'] = "<b>Modul mappa:</b> ";
$MSG['3500_1015457'] = "Ez a modul már le lett töltve";
$MSG['3500_1015458'] = "Modul törlése";
$MSG['3500_1015459'] = "1. rész: Adminisztrációs terület";
$MSG['3500_1015460'] = "2. rész: Felhasználói terület";
$MSG['3500_1015461'] = "Kérjük ne felejtse el kitölteni adataival a saját vezérlõpultját a Felhasználói terület oldalon.";
$MSG['3500_1015462'] = "Válasszon színt";
$MSG['3500_1015463'] = "Oldal megtekintése";
$MSG['3500_1015464'] = "Üzenetek";
$MSG['3500_1015465'] = "Folyamat";
$MSG['3500_1015466'] = "Gyorstár törlése";
$MSG['3500_1015467'] = "Google Adsense kód hozzáadása";
$MSG['3500_1015468'] = "Utolsó belépés";
$MSG['3500_1015469'] = "BE / KI";
$MSG['3500_1015470'] = "Valóban bezárja ezt a bejelentést?";
$MSG['3500_1015471'] = "Valóban be kívánja zárni ezt a bejelentést és minden mást is ami ezzel az aukcióval kapcsolatos?";
$MSG['3500_1015472'] = "Bejelentett aukciók";
$MSG['3500_1015473'] = "Saját verzió";
$MSG['3500_1015474'] = "Aktuális verzió";
$MSG['3500_1015475'] = "Szerzõ";
$MSG['3500_1015476'] = "Modul";
$MSG['3500_1015477'] = "Verzió";
$MSG['3500_1015478'] = "Mappa";
$MSG['3500_1015479'] = "Oldal";
$MSG['3500_1015480'] = "Ennek a kódnak a keresése";
$MSG['3500_1015481'] = "Kód cseréje";
$MSG['3500_1015482'] = "Hozzáad a megtalált kód után";
$MSG['3500_1015483'] = "Hozzáad a megtalált kód elé";
$MSG['3500_1015484'] = "Részletek";
$MSG['3500_1015485'] = "Szállítás részletei";
$MSG['3500_1015486'] = "Eladó adatai";
$MSG['3500_1015487'] = "Egyéb részletek";
$MSG['3500_1015488'] = "Termék állapota";
$MSG['3500_1015489'] = "Kérdések";
$MSG['3500_1015490'] = "Elõzmény";
$MSG['3500_1015491'] = "Aukció részletei";
$MSG['3500_1015492'] = "Engedjük, hogy az eladók saját állapotot adhassanak meg amivel pontosabb leírhatják termékeiket.";
$MSG['3500_1015493'] = "Az Ön adatait SSL titkosítással küldtük el és szigorú bizalmasan kezeljük. Nem adjuk át 'spam'-nek és nem szolgáltatjuk ki harmadik fél számára sem.";
$MSG['3500_1015494'] = "Fénykép feltöltés";
$MSG['3500_1015495'] = "Állomány hozzáadása";
$MSG['3500_1015496'] = "Feltöltés indítása";
$MSG['3500_1015497'] = "Feltöltés megszakítása";
$MSG['3500_1015498'] = "Ablak bezárása";
$MSG['3500_1015499'] = "Méret";
$MSG['3500_1015500'] = "Alapértelmezettként beállít";
$MSG['3500_1015501'] = "Aukció bezárása";
$MSG['3500_1015502'] = "Google Adsense kód beszúrása a szövegdobozba";
$MSG['3500_1015503'] = "Kód elküldve";
$MSG['3500_1015504'] = "Adsense beállítások módosítva";
$MSG['3500_1015505'] = "Nincs eladó vagy bejelentõ megjelelölve";
$MSG['3500_1015506'] = "Normál tag";
$MSG['3500_1015507'] = "Ez a felhasználó már normál tag.";
$MSG['3500_1015508'] = "Aladminisztrátorként beállít";
$MSG['3500_1015509'] = "Fõadminisztrátorként beállít";
$MSG['3500_1015510'] = "Adminisztrátori beállítások";
$MSG['3500_1015511'] = "The Main Admin is used to display the Administrator button at the bottom of the page in the Front End of the u-Auctions script and it is also used for the bank transfers gateway so it can get the correct user info for the bank transfers so other members can pay there fees with a bank transfer.<p style='color:red'>Please only have 1 user as a main admin.</p>";
$MSG['3500_1015512'] = "Csak az Aladminisztrátornak jelenik meg az Adminisztrátor nyomógomb az uAuction script Vezérlõpulti részleg oldalainak alján.";
$MSG['3500_1015513'] = "A Normál-tagok nem látják az Adminisztrátor nyomógombot a Vezérlõpulti felület oldalainak alján.";



?>