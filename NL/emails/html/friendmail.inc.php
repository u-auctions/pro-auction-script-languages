<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Uw vriend of kennis, heeft u een veiling doorgestuurd van {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Hallo <b>{F_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
        Uw vriend of kennis, {S_NAME} (<a href="mailto:{S_EMAIL}">{S_EMAIL}</a>), heeft u een veiling doorgestuurd van {SITENAME}
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px; line-height: 0.6cm;">
			<b>Veiling titel:</b> 	{TITLE} <br />
			<b>Commentaar:</b> {S_COMMENT}<br />
		</td>
		<td width="34%" style="font-size: 12px;">Neem een kijkje!</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{URL}"><img src="{SITEURL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
		</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">Met vriendelijke groet,<br />Uw veilng team.
		</td>
	</tr>
</table>