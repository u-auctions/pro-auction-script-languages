Hallo {C_NAME},<br>

Dit is een factuur over betaling van de vergoeding voor {ITEM_NAME}.<br>

Totaal te betalen: {BALANCE}<br>

Klik op de link hieronder voor toegang tot de betaalpagina:<br>
<a href="{LINK}">{LINK}</a><br>

Notitie: Alle activiteiten op uw verkoopsite zullen worden opgeschort totdat het bedrag is betaald.

Met vriendelijke groet,
Uw veilng team.
