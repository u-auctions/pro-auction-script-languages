Hallo {NAME},<br><br>

Uw account op {SITENAME} is opgeschort omdat u de maximaal toegestane negatief saldo heeft overschreden.<br><br>

Uw saldo is: {BALANCE}<br><br>

Om uw account opnieuw te activeren, moet u uw negatief saldo eerst aanvullen.<br><br>
 
Klik op de link hieronder voor toegang tot de betaalpagina:<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a><br><br>

Met vriendelijke groet,<br>
Uw veilng team.<br>
