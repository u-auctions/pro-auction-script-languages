Hallo {W_NAME},<br>

Je hebt de volgende veiling op {SITENAME} gewonnen.<br><br>

Veiligngegevens<br><br>

Titel: {A_TITLE}<br>
Omschrijving: {A_DESCRIPTION}<br><br>

Uw bod: {A_CURRENTBID} ({W_WANTED} items)<br>
U bereikte: {W_GOT} items<br>
Veiling einddatum: {A_ENDS}<br>
URL: <a href="{A_URL}">{A_URL}</a><br>
<br>
=============<br>
Info verkoper<br>
=============<br>
<br>
{S_NICK}<br>
{S_EMAIL}<br><br>
Betalingsgegevens verkoper:<br>
{S_PAYMENT}
<br>
<br>
Als u dit bericht ten onrechte hebt ontvangen, reageer dan op deze e-mail,
schrijf naar <a href="mailto:{ADMINEMAIL}">{ADMINEMAIL}</a>, of bezoekt <a href="{SITE_URL}">{SITENAME}</a> op <a href="{SITE_URL}">{SITE_URL}</a>.<br>
<br>
<br>
Met vriendelijke groet,<br>
Uw veilng team.<br>


