Hallo {C_NAME},<br><br>

Dit is een factuur over gemaakte kosten ten behoeve van diensten geleverd door onze site, {SITENAME}.<br><br>

Uw saldo is: {BALANCE}<br><br>

Klik op de link hieronder voor toegang tot de betaalpagina:<br>
[ <a href="{LINK}">klik hier</a> ]<br><br>


Met vriendelijke groet,<br>
Uw veilng team.<br>
