Hallo {C_NAME},<br><br>

Dit is een factuur over betaling van de vergoeding voor {ITEM_NAME}.<br><br>

Totaal te betalen: {BALANCE}<br><br>

Klik op de link hieronder voor toegang tot de betaalpagina:
<a href="{LINK}">{LINK}</a><br><br>

Notitie: Alle activiteiten op uw verkoopsite zullen worden opgeschort totdat het bedrag is betaald.<br><br>


Met vriendelijke groet,<br>
Uw veilng team.<br>
