Hallo {REALNAME},<br />

Op uw verzoek hebben we een nieuw wachtwoord voor uw account aangemaakt.<br /><br />

Uw nieuwe wachtwoord: {NEWPASS}<br /><br />

Gebruik dit nieuwe wachtwoord om in te loggen op {SITENAME}, vergeet niet om het wachtwoord te veranderen in iets wat u kunt onthouden.<br /><br />


Met vriendelijke groet,<br />
Uw veilng team.<br />
