<table border="0" width="100%">
  <tr>
    <td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Voltooi uw registratie op {SITENAME}!</div></td>
  </tr>
  <tr>
    <td colspan="2" style="font-size: 12px;">Hallo <b>{C_NAME}</b>,</td>
  </tr>
  <tr>
    <td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;"> Klik op "Activeren" om uw registratie te voltooien. Zodra u dat gedaan hebt bent u klaar om te 
      beginnen met het kopen/verkopen op {SITENAME}. </td>
  </tr>
  <tr>
    <td width="55%" rowspan="2" valign="top"><br />
      <br />
      <table border="0" width="100%">
        <tr>
          <td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Als de link niet werkt:</b></td>
        </tr>
        <tr>
          <td style="font-size: 12px;">Mail dan naar <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a></td>
        </tr>
        <tr>
          <td width="34%" height="176" valign="top"> Met vriendelijke groet, <br />
            Uw veilng team. </td>
        </tr>
      </table></td>
    <td width="34%" style="font-size: 12px;">Ga verder om te bevestigen</td>
  </tr>
  <tr>
    <td width="34%" height="176" valign="top"><a href="{CONFIRMURL}"> <img border="0" src="{SITEURL}images/email_alerts/Active_Acct_Btn.jpg" width="120" height="32"></a></td>
  </tr>
</table>
