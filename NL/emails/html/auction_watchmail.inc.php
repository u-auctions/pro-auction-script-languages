<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Dit is uw veilingvolger voor {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Hallo <b>{REALNAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		Dit is uw veiling die u heeft opgevraagd. De volgende veiling is geopend met uw zoekwoord(en):: <b>{KWORD}</b>

		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top"><br />
		<table border="0" width="100%">
			<tr>
				<td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Veiling:</b>&nbsp;{TITLE}</td>

			</tr>
			<tr>
				<td style="font-size: 12px;"><b>Veiling URL:</b>&nbsp;<a href="{URL}">{URL}</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">Met vriendelijke groet,<br />Uw veilng team.
		</td>
	</tr>
</table>