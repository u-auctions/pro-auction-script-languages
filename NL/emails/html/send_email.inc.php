Hallo {SELLER_NICK},<br><br>

Dit bericht is verzonden van {SITENAME}.<br><br>

{SENDER_NAME} met <a href="mailto:{SENDER_EMAIL}">{SENDER_EMAIL}</a> heeft een vraag voor u met betrekking tot uw veiling {TITLE}.<br><br>

Vraag:<br>
{SENDER_QUESTION}<br><br>

Veiling URL: <a href="{SITEURL}item.php?id={AID}">{SITEURL}item.php?id={AID}</a><br><br>


Met vriendelijke groet,<br>
<a href="{SITEURL}">Uw veilng team</a>.<br>
