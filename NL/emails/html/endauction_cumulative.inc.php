Hallo {S_NAME},<br>

De volgende veilingen geplaatst op {SITENAME} zijn gisteren gesloten.<br><br>

{REPORT}<br><br>


Een e-mail is verzonden naar de winnaar (s) met uw e-mailadres.<br><br>

Als u dit bericht ten onrechte hebt ontvangen, reageer dan op deze e-mail,
schrijf naar <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>, of bezoekt {SITENAME} op <a href="{SITE_URL}">{SITE_URL}</a>.<br><br>


Met vriendelijke groet,
Uw veilng team.

