<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Dank u voor uw registratie op {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Hello <b>{C_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		Met het oog op de verkoop en/of het kopen op {SITENAME}, moet uw account worden
geactiveerd door de beheerder van onze site. U ontvangt een e-mail als uw account actief is.
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td colspan="2" style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Uw gegevens zijn:</b></td>

			</tr>
			<tr>
				<td width="17%" style="font-size: 12px;">Naam:</td>
				<td align="left" style="font-size: 12px;">{C_NAME}</td>
			</tr>
			<tr>
				<td width="17%" style="font-size: 12px;">Gebruikernaam:</td>
				<td align="left" style="font-size: 12px;">{C_NICK}</td>
			</tr>
<!-- IF C_ADDRESS ne '' -->
			<tr>
				<td width="17%" style="font-size: 12px;">Adres:</td>
				<td align="left" style="font-size: 12px;">{C_ADDRESS}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
			<tr>
				<td width="17%" style="font-size: 12px;">Plaats:</td>
				<td align="left" style="font-size: 12px;">{C_CITY}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
			<tr>
				<td width="17%" style="font-size: 12px;">Provincie:</td>
				<td align="left" style="font-size: 12px;">{C_PROV}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
			<tr>
				<td width="17%" style="font-size: 12px;">Land:</td>
				<td align="left" style="font-size: 12px;">{C_COUNTRY}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
			<tr>
				<td width="17%" style="font-size: 12px;">Postcode:</td>
				<td align="left" style="font-size: 12px;">{C_ZIP}</td>
			</tr>
<!-- ENDIF -->
			<tr>
				<td width="17%" style="font-size: 12px;">E-mail:</td>
				<td align="left" style="font-size: 12px;">{C_EMAIL}</td>
			</tr>
			<tr>
				<td width="17%" style="font-size: 12px;"></td>
				<td align="left" style="font-size: 12px;">&nbsp;</td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">Met vriendelijke groet,<br />Uw veilng team.
		</td>
	</tr>
</table>