<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
 
 <p>Guten Tag {C_NAME},<br>
   <br>
   <br>
   Dies ist eine Rechnung für die Kaufgebühr des folgenden Artikels: <strong>{ITEM_NAME}</strong>.<br>
  </p>
 <p><br>
   Total zu bezahlen: <strong>{BALANCE}</strong><br>
   <br>
   Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsmöglichkeiten zu gelangen:<br>
   <a href="{LINK}">{LINK}</a><br>
  </p>
 <p><br>
   Bitte beachten Sie:<br>
   Bis zur vollständigen Bezahlung der Gebühr werden Sie zu keinen Aktivitäten im Auktionshaus mehr zugelassen.
   
  <br>
  </p>
 <p>&nbsp;</p>
 <p><br>
   Besten Dank und freundliche Grüsse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>