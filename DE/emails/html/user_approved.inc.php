<html>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Herzliche Gratulation, Sie sind jetzt Mitglied auf &laquo;{SITENAME}&raquo;!</div></td>
	</tr>
	<tr>
		<td height="58" colspan="2" style="font-size: 12px;">Guten Tag {C_NAME},</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		 Mit diesem E-Mail best�tigen wir, dass Ihre Registrierung auf &laquo;{SITENAME}&raquo; erfolgreich und vollst�ndig abgeschlossen wurde!<br>
		 <br />
		Sie k�nnen sich ab sofort mit Ihrem Login und Ihrem Passwort anmelden und mit Verkaufen und Bieten beginnen. Danke, dass Sie das Auktionshaus &laquo;{SITENAME}&raquo; gew�hlt haben!
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px;"><p>&nbsp;</p>
		  <p><b>M�chten Sie gleich starten?</b><br /><br />
    <a href="{SITE_URL}"><img src="{SITE_URL}images/email_alerts/Take_Me_There.jpg" border="0"></a></p></td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="134" valign="top">&nbsp;
		</td>
	</tr>
 </table>
  <p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
</body>
</html>