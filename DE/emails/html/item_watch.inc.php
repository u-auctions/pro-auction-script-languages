<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

 <p>Guten Tag {REALNAME},<br>
  <br>
   Dieses E-Mail wurde Ihnen wie gew�nscht vom Auktionshaus &laquo;{SITENAME}&raquo; gesandt, da jemand auf einen Artikel geboten hat, den Sie beobachten.  </p>
 <p><br>
   Titel: {TITLE}<br>
  <br>
   Aktuelles Gebot: <strong>{BID}</strong><br>
  <br>
   Link zur Auktion: <a href="{AUCTION_URL}">{AUCTION_URL}</a>  </p>
 <p><br>
  <br>
   Besten Dank und freundliche Gr�sse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>