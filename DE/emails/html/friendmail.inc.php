<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
 
<table border="0" width="100%">
	<tr>
		<td colspan="2" height="53"><div style="font-size: 14px; font-weight: bold;">Interessante Auktion auf &laquo;{SITENAME}&raquo;!</div></td>
	</tr>
	<tr>
		<td height="36" colspan="2" style="font-size: 12px;">Guten Tag {F_NAME},</td>
	</tr>
	<tr>
		<td colspan="2" height="90" style="font-size: 12px; padding-right: 6px;">
		 Ihr Freund, Ihre Freundin {S_NAME} (<a href="mailto:{S_EMAIL}">{S_EMAIL}</a>) schickt Ihnen einen Link zu einer interessanten Auktion auf der Site &laquo;{SITENAME}&raquo;.
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px; line-height: 0.6cm;">
			<b>Titel:</b> 	{TITLE} <br>
		  <br />
			<b>Bemerkungen:</b> {S_COMMENT}<br />
		</td>
		<td width="34%" height="44" style="font-size: 12px;">Link zur Auktion: klicken Sie einfach auf die Schaltfäche!</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{URL}"><img src="{SITEURL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
		</td>
	</tr>
	
</table>
 
 <p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
 
 </body>
</html>