Liebes Support Team,<br>
<br>
Sie haben ein Support-Ticket erhalten.<br>
<strong>User:</strong> {USER}<br>
<br>
<strong>Webseite:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Betreff:</strong><br>
{SUBJECT}
<br><br>
<strong>Mitteilung:</strong><br>
{MESSAGE}