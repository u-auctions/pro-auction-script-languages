<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
 	
<table border="0" width="100%">
	<tr>
		<td colspan="3" height="50"><div style="font-size: 14px; font-weight: bold;">Herzliche Gratulation, Ihr Artikel wurde soeben verkauft!</div></td>
	</tr>
	<tr>
		<td colspan="3" style="font-size: 12px;">Guten Tag {S_NAME},</td>
	</tr>
	<tr>
		<td colspan="3" height="67" style="font-size: 12px; padding-right: 6px;"><br>
		  Herzliche Gratulation, Ihr Artikel wurde soeben verkauft!<br>
		  <br>
	    Details:</td>
	</tr>
	<tr>
		<td width="9%" rowspan="2"><img border="0" src="{SITE_URL}{A_PICURL}"></td>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td height="33" colspan="2" style="font-size: 12px;"><a href="{A_URL}"><strong>{A_TITLE}</strong></a></td>

			</tr>
			<tr>
				<td width="34%" height="33" style="font-size: 12px;">Verkaufspreis:</td>
				<td width="66%" align="left" style="font-size: 12px;">{A_CURRENTBID}</td>
			</tr>
			<tr>
				<td width="34%" height="29" style="font-size: 12px;">Anzahl:</td>
				<td align="left" style="font-size: 12px;">{A_QTY}</td>
			</tr>
			<tr>
				<td width="34%" height="30" style="font-size: 12px;">Auktionsende:</td>
				<td align="left" style="font-size: 12px;">{A_ENDS}</td>
			</tr>
			<tr>
				<td width="34%" height="33" style="font-size: 12px;">Link zur Auktion:</td>
				<td align="left" style="font-size: 12px;"><a href="{A_URL}">{A_URL}</a></td>
			</tr>
			<tr>
				<td width="34%" height="66" style="font-size: 12px;"></td>
				<td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Gehe zu &laquo;{SITENAME}&raquo;</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="90" valign="top">
		<br>
		<span style="font-size: 12px;">Zum &Uuml;berpr&uuml;fen der Zahlungsdetails<br>
		bitte hier klicken:
		</span><br>
		<br>
		<a href="{SITE_URL}buying.php">
		<img border="0" src="{SITE_URL}images/email_alerts/Total_Due_Btn.jpg" width="120" height="32"></a></td>
	</tr>
 </table><br />
 
<table border="0" width="100%">
	<tr>
		<td height="39" style="font-size: 12px;"><b>Informationen zum K�ufer</b></td>
	</tr>
	<tr>
		<td style="font-size: 12px;">{B_REPORT}</td>
	</tr>
</table>
<br />
<div style="font-size: 12px;"><br>
  <br>
 <strong>An den Gewinner der Auktion wurde eine E-Mail mit Ihrer E-Mail-Adresse gesandt.</strong></div>
<br>
<p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
 </body>
</html>

