<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

<table border="0" width="100%">
	<tr>
		<td colspan="3" height="48"><div style="font-size: 14px; font-weight: bold;">Herzliche Gratulation, Sie haben die Auktion gewonnen!</div></td>
	</tr>
	<tr>
		<td height="46" colspan="3" style="font-size: 12px;">Guten Tag {W_NAME},</td>
	</tr>
	<tr>
		<td colspan="3" height="50" style="font-size: 12px; padding-right: 6px;">Sie haben die folgende Auktion auf &laquo;{SITENAME}&raquo; gewonnen: <a href="{A_URL}"><strong>{A_TITLE}</strong></a> <br></td>
	</tr>
	<tr>
		<td width="9%" rowspan="2"><img border="0" src="{SITE_URL}{A_PICURL}"></td>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td height="29" colspan="2" style="font-size: 12px;"><strong><a href="{A_URL}">{A_TITLE}</a></strong></td>
			</tr>
			<tr>
				<td width="33%" height="27" style="font-size: 12px;">Ihr h�chstes Gebot:</td>
				<td width="67%" align="left" style="font-size: 12px;">{A_CURRENTBID}</td>
			</tr>
			<tr>
				<td width="33%" height="28" style="font-size: 12px;">Auktionsende:</td>
				<td align="left" style="font-size: 12px;">{A_ENDS}</td>
			</tr>
			
			<tr>
			  <td height="27" style="font-size: 12px;">Link zur Auktion:</td>
			  <td align="left" style="font-size: 12px;"><a href="{A_URL}">{A_URL}</a></td>
		  </tr>
			<tr>
				<td width="33%" height="51" style="font-size: 12px;"></td>
				<td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Gehe zu &laquo;{SITENAME}&raquo;</a></td>
			</tr>
		</table>
	  </td>
		<td width="34%" style="font-size: 12px;">Zahlungsdetails �berpr�fen</td>
	</tr>
	<tr>
		<td width="34%" height="157" valign="top">
		<a href="{SITE_URL}buying.php">
		<img border="0" src="{SITE_URL}images/email_alerts/Total_Due_Btn.jpg" width="120" height="32"></a></td>
	</tr>
 </table>
 
<table border="0" width="100%">
	<tr>
		<td height="47" style="font-size: 12px;"><b>Informationen zum Verk�ufer</b></td>
	</tr>
	<tr>
		<td style="font-size: 12px;">{S_NICK} - <a href="mailto:{S_EMAIL}">{S_EMAIL}</a></td>
	</tr>
</table>
<br />
<div style="font-size: 12px;"><br>
  <br>
 <strong>An den Gewinner der Auktion wurde eine E-Mail mit Ihrer E-Mail-Adresse gesandt.</strong></div>
<br>
<p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
</body>
</html>
