<!doctype html>
<html>
<head>
<meta charset="utf-8">
 <style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

 <p>Guten Tag {W_NAME},<br>
  <br>
   Sie haben die folgende Auktion auf &laquo;{SITENAME}&raquo; gewonnen: <a href="{A_URL}"><strong>{A_URL}</strong></a> <br>
  <br>
  </p>
 <p><br>
   ------------------------------------<br>
   Titel: {A_TITLE}<br>
   Beschreibung: {A_DESCRIPTION}<br>
  <br>
   Ihr h�chstes Gebot: {A_CURRENTBID} ({W_WANTED} St&uuml;ck)<br>
   Sie erhalten: {W_GOT} St�ck<br>
   Auktionsende: {A_ENDS}<br>
   Link zur Auktion: <a href="{A_URL}">{A_URL}</a><br>
  <br>
  </p>
 <p>&nbsp;</p>
 <p><br>
   =============<br>
   Informationen zum Verk�ufer<br>
   =============<br>
  <br>
   {S_NICK}<br>
  <a href="mailto:{S_EMAIL}">{S_EMAIL}</a></p>
 <p><br>
   Zahlungsdetails:<br>
   {S_PAYMENT}<br>		 
  <br>
 </p>
 <p><br>
  <br>
   Wenn Sie dieses Mail irrt�mlich erhalten haben, so wenden Sie sich bitte an den Administrator (<a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>) oder besuchen Sie das Auktionshaus &laquo;{SITENAME}&raquo; unter <a href="{SITE_URL}">{SITE_URL}</a>.
  <br>
  </p>
 <p>&nbsp;</p>
 <p><br>
   Besten Dank und freundliche Gr�sse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
</body>
</html>


