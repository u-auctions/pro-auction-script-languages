<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

Guten Tag {REALNAME},<br>
<br>
Diese Mail wurde Ihnen wie gew�nscht vom Auktionsbeobachter des Auktionshauses &laquo;{SITENAME}&raquo; gesandt.<br>
Die unten stehende, von Ihnen beobachtete Auktion wurde geschlossen.<br>
<br>
<br>
Auktion: {TITLE}<br>
Link zur Auktion: <a href="{URL}">{URL}</a>
<br>
<br>
<br>
<br>
Besten Dank und freundliche Gr�sse<br>
<br>
Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;

 
 </body>
</html>
