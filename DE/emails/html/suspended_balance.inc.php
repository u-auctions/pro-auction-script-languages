<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

Guten Tag {NAME},<br>
<br>
Ihr Benutzerkonto beim Auktionshaus &laquo;{SITENAME}&raquo; wurde gesperrt, weil das Limit für die maximalen Ausstände erreicht wurde.<br>
Um Ihr Benutzerkonto zu reaktivieren müssen Sie Ihre Ausstände vollständig begleichen. 
<br>
<br>
Total zu bezahlen: <strong>{BALANCE}</strong><br>
<br>
<br>
Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsmöglichkeiten zu gelangen:<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a>
<br>
<br>
Bitte beachten Sie:<br>
Bis zur vollständigen Bezahlung der Gebühr werden Sie zu keinen Aktivitäten im Auktionshaus mehr zugelassen.
<br>
<br><br>

<br>
Besten Dank und freundliche Grüsse<br>
<br>
Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;

 </body>
</html>
