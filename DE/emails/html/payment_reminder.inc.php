<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

 <p>Guten Tag {C_NAME},<br>
  <br>
   Dies ist eine Rechnung zum Begleichen der offenen Gebühren auf &laquo;{SITENAME}&raquo;.<br>
  </p>
 <p><br>
   Total zu bezahlen: <strong>{BALANCE}</strong><br>
  <br>
   Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsmöglichkeiten zu gelangen:<br>
  <a href="{LINK}">{LINK}</a><br>
  </p>
 <p><br>
  <br>
   Besten Dank und freundliche Grüsse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>
