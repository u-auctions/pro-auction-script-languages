<!doctype html>
<html>
<head>
<meta charset="utf-8">
 <style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

 <p>Guten Tag {C_NAME},</p>
 <p><br>
  <br>
   Dies ist eine Rechnung f�r die Umsatzgeb�hr des folgenden Artikels: {ITEM_NAME}.<br>
  <br>
   Total zu bezahlen: {BALANCE}<br>
  <br>
   Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsm�glichkeiten zu gelangen:<br>
  <a href="{LINK}">{LINK}</a><br>
  <br>
   Bitte beachten Sie:<br>
   Bis zur vollst�ndigen Bezahlung der Geb�hr werden Sie zu keinen Aktivit�ten im Auktionshaus mehr zugelassen.  </p>
 <p>&nbsp;</p>
 <p><br>
  <br>
   Besten Dank und freundliche Gr�sse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>
