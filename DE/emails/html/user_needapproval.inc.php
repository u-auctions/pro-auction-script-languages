<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

<table border="0" width="100%">
	<tr>
		<td colspan="2" height="54"><div style="font-size: 14px; font-weight: bold;">Danke f�r Ihre Registrierung beim Auktionshaus &laquo;{SITENAME}&raquo;!</div></td>
	</tr>
	<tr>
		<td height="48" colspan="2" style="font-size: 12px;">Guten Tag {C_NAME},</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
	Damit Sie sich mit Ihrem Login und Ihrem Passwort anmelden und mit Verkaufen und Bieten beginnen k�nnen muss Ihr Benutzerkonto von einem Administrator auf &laquo;{SITENAME}&raquo; genehmigt werden.<br>
	<br>
	Dieser Prozess ist momentan in Bearbeitung und Sie werden umgehend per E-Mail benachrichtigt, sobald Ihr Benutzerkonto freigeschaltet und aktiviert wurde.<br>
		</td>
	</tr>
	<tr>
		<td width="66%" rowspan="2">
		<table border="0" width="110%">
			<tr>
				<td colspan="2" style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Ihre Konto-Informationen:</b></td>

			</tr>
			<tr>
				<td width="35%" style="font-size: 12px;">Name:</td>
				<td width="65%" align="left" style="font-size: 12px;">{C_NAME}</td>
			</tr>
			<tr>
				<td width="35%" style="font-size: 12px;">Benutzername:</td>
				<td align="left" style="font-size: 12px;">{C_NICK}</td>
			</tr>
<!-- IF C_ADDRESS ne '' -->
			<tr>
				<td width="35%" style="font-size: 12px;">Adresse:</td>
				<td align="left" style="font-size: 12px;">{C_ADDRESS}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
			<tr>
				<td width="35%" style="font-size: 12px;">Stadt:</td>
				<td align="left" style="font-size: 12px;">{C_CITY}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
			<tr>
				<td width="35%" style="font-size: 12px;">Region:</td>
				<td align="left" style="font-size: 12px;">{C_PROV}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
			<tr>
				<td width="35%" style="font-size: 12px;">Land:</td>
				<td align="left" style="font-size: 12px;">{C_COUNTRY}</td>
			</tr>
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
			<tr>
				<td width="35%" style="font-size: 12px;">Postleitzahl:</td>
				<td align="left" style="font-size: 12px;">{C_ZIP}</td>
			</tr>
<!-- ENDIF -->
			<tr>
				<td width="35%" style="font-size: 12px;">E-Mail-Adresse:</td>
				<td align="left" style="font-size: 12px;">{C_EMAIL}</td>
			</tr>
			<tr>
				<td width="35%" height="24" style="font-size: 12px;"></td>
				<td align="left" style="font-size: 12px;">&nbsp;</td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">&nbsp;
		</td>
	</tr>
 </table>
 <p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
 
</body>
</html>