<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
 
 <table border="0" width="100%">
	<tr>
		<td colspan="3" height="51"><div style="font-size: 14px; font-weight: bold;">Ihr Artikel wurde bei &laquo;{SITENAME}&raquo; aufgenommen!</div></td>
	</tr>
	<tr>
		<td height="38" colspan="3" style="font-size: 12px;">Guten Tag {C_NAME},</td>
	</tr>
	<tr>
		<td colspan="3" height="63" style="font-size: 12px; padding-right: 6px;"><p><br>
		  Ihr Artikel wurde bei &laquo;{SITENAME}&raquo; erfolgreich aufgenommen. Der Artikel wird umgehend in allen Suchresultaten und im Auktionshaus angezeigt.</p>
		  <p><br>
	    Die Details dazu:</p></td>
	</tr>
	<tr>
		<td width="9%" rowspan="2"><img border="0" src="{SITE_URL}{A_PICURL}"></td>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td colspan="2" style="font-size: 12px;"><a href="{SITE_URL}item.php?id={A_ID}"><strong>{A_TITLE}</strong></a></td>

			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;">Auktionstyp:</td>
				<td width="66%" align="left" style="font-size: 12px;">{A_TYPE}</td>
			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;">Artikel-Nummer:</td>
				<td align="left" style="font-size: 12px;">{A_ID}</td>
			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;">Mindestgebot:</td>
				<td align="left" style="font-size: 12px;">{A_MINBID}</td>
			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;">Reservierungspreis:</td>
				<td align="left" style="font-size: 12px;">{A_RESERVE}</td>
			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;">Sofort Kaufen Preis:</td>
				<td align="left" style="font-size: 12px;">{A_BNPRICE}</td>
			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;">Auktionsende:</td>
				<td align="left" style="font-size: 12px;">{A_ENDS}</td>
			</tr>
			<tr>
				<td width="34%" style="font-size: 12px;"></td>
				<td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Gehe zu  &laquo;{SITENAME}&raquo;</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		  <p><span style="font-size: 12px;">Wollen Sie weitere Artikel einstellen?</span><br>
	      <span style="font-size: 12px;">Dann gleich hier klicken...</span></p>
<p><a href="{SITE_URL}select_category.php"><img border="0" src="{SITE_URL}images/email_alerts/Sell_More_Btn.jpg" width="120" height="32">
	    </a></p></td>
	</tr>
</table>
<p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo;</span></p>
 <p>&nbsp;</p>
 
 </body>
</html>