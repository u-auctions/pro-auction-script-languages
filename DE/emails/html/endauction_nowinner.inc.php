<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

<table border="0" width="100%">
	<tr>
		<td colspan="3" height="35"><div style="font-size: 14px; font-weight: bold;">Ihre Auktion endete ohne Gewinner</div></td>
	</tr>
	<tr>
		<td colspan="3" style="font-size: 12px;"><br>
	    Guten Tag {S_NAME},<br>
	    <br>
	    <br>
	    <br></td>
	</tr>
	<tr>
		<td colspan="3" height="40" style="font-size: 12px; padding-right: 6px;">
		Ihre Auktion auf &laquo;{SITENAME}&raquo; endete ohne Gewinner.<br>
<br>
</td>
	</tr>
	<tr>
		<td width="9%" rowspan="2">&nbsp;</td>
		<td width="55%" rowspan="2">
		<table border="0" width="100%">
			<tr>
				<td colspan="2" style="font-size: 12px;"><a href="{A_URL}"><strong>{A_TITLE}</strong></a></td>

			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Artikel-Nummer:</td>
				<td align="left" style="font-size: 12px;">{A_ID}</td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Auktionsende:</td>
				<td align="left" style="font-size: 12px;">{A_END}</td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">Link zur Auktion:</td>
				<td align="left" style="font-size: 12px;"><a href="{A_URL}">{A_URL}</a></td>
			</tr>
			<tr>
				<td width="22%" style="font-size: 12px;">&nbsp;</td>
				<td align="left" style="font-size: 12px;"><a href="{SITE_URL}user_menu.php?">Gehe zu &laquo;{SITENAME}&raquo;</a></td>
			</tr>
		</table>
		
        <p>&nbsp;</p>
        <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
 
 </body>
</html>