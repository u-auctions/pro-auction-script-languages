<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
<table border="0" width="100%">
	<tr>
		<td colspan="3" height="50"><div style="font-size: 14px; font-weight: bold;">
		  <p>Bitte schliessen Sie Ihre Registrierung beim Auktionshaus &laquo;{SITENAME}&raquo; ab!      </p>
	  </div></td>
	</tr>
	<tr>
		<td height="31" colspan="3" style="font-size: 12px;">Guten Tag {C_NAME},</td>
	</tr>
	<tr>
		<td colspan="3" height="50" style="font-size: 12px; padding-right: 6px;">
		Bitte klicken Sie zum Abschliessen der Registrierung auf die Schaltfläche <b>&laquo;Aktivieren&raquo;</b>. Nach Abschluss der Bestätigung des Benutzerkontos können sich mit Ihrem Login und Ihrem Passwort anmelden und mit Verkaufen und Bieten beginnen.
		</td>
	</tr>
	<tr>
	  <td width="65%" rowspan="3" valign="top"><br /><br />
		<table border="0" width="100%">
			<tr>
				<td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Sollte der Link nicht funktionieren, so senden Sie uns bitte ein E-Mail:</b></td>

			</tr>
			<tr>
				<td style="font-size: 12px;">e-Mail-Adresse: <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a></td>
			</tr>
		<tr>
				<td style="font-size: 12px;">oder verwenden Sie <a href="{CONFIRMURL}"><b>diesen Link.</b></a></td>
		  </tr>
		</table>
		</td>
	  <td width="5%" style="font-size: 12px;">&nbsp;</td>
		<td width="30%" height="43" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
	  <td valign="top">&nbsp;</td>
	  <td height="33" valign="top"><span style="font-size: 12px;">Hier klicken zum Best&auml;tigen</span></td>
  </tr>
	<tr>
	  <td width="5%" valign="top">&nbsp;</td>
		<td width="30%" height="99" valign="top">
	  <p><a href="{CONFIRMURL}"><img border="0" src="{SITEURL}images/email_alerts/Active_Acct_Btn.jpg" width="120" height="32"></a></p></td>
	</tr>
	
 </table>
 <br>
<p><br>
 <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
<p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>

</body>
</html>