<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
 
<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Der Auktionsbeobachter von &laquo;{SITENAME}&raquo; hat eine passende Auktion zu Ihrem Suchauftrag gefunden!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Guten Tag {REALNAME},</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;"><br>

		Dieses E-Mail wurde Ihnen wie gew�nscht vom Auktionsbeobachter des Auktionshauses &laquo;{SITENAME}&raquo; gesandt. <br><br>

Eine Auktion wurde er�ffnet, die die folgenden Stichworte enth�lt: <b>{KWORD}</b>

		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top"><br />
		<table border="0" width="100%">
			<tr>
				<td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Auktion:</b>&nbsp;{TITLE}</td>

			</tr>
			<tr>
				<td style="font-size: 12px;"><b>Link zur Auktion:</b>&nbsp;<a href="{URL}">{URL}</a></td>
			</tr>
			<tr>
				<td style="font-size: 12px;">&nbsp;</td>
			</tr>
		  </table>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="96" valign="top">&nbsp;
		</td>
	</tr>
</table>
 <p>&nbsp;</p>
 <p><br>
   <span style="font-size: 12px; padding-right: 6px;"> Besten Dank und freundliche Gr&uuml;sse</span></p>
 <p><span style="font-size: 12px; padding-right: 6px;">Die Administration des Auktionhauses &laquo;{SITENAME}&raquo; </span></p>
 <p>&nbsp;</p>
<html>
 
 </body>
</html>