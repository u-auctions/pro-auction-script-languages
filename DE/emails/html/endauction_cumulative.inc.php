<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>
 
 <p>Guten Tag {S_NAME},<br>
  <br>
   </p>
 <p>Die folgende Auktion auf <a href="{SITE_URL}">&laquo;{SITENAME}&raquo;</a> wurde gestern geschlossen.<br>
   <br>
   {REPORT}
  <br>
  </p>
 <p><br>
   Ein E-Mail mit Ihrer E-Mail-Adresse wurde an den oder die Gewinner der Auktion gesandt.<br>
  <br>
   Wenn Sie dieses Mail irrt�mlich erhalten haben, so wenden Sie sich bitte an den Administrator (<a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>) oder besuchen Sie das Auktionshaus &laquo;{SITENAME}&raquo; unter <a href="{SITE_URL}">{SITE_URL}</a>.
  <br>
  </p>
 <p><br>
   Besten Dank und freundliche Gr�sse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>


