Geehrte/r {NAME},<br>
<br>
Sie haben eine Antwort vom Support-Team erhalten haben.<br>
<br>
<strong>{SITENAME} Support:</strong> <a href="{A_URL}">Mein Support</a><br>
<br>
<strong>Webseite:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Betreff:</strong><br>
{SUBJECT}
<br><br>
<strong>Mitteilung:</strong><br>
{MESSAGE}