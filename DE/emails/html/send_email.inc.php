<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

 <p>Guten Tag {SELLER_NICK},<br>
  <br>
   Sie erhalten diese Benachrichtigung von &laquo;{SITENAME}&raquo;.<br>
  <br>
  </p>
 <p>{SENDER_NAME} von <a href="mailto:{SENDER_EMAIL}">{SENDER_EMAIL}</a> hat Ihnen eine Frage gestellt zur folgenden Auktion: {TITLE}.<br>
  <br>
 </p>
 <p><b>Frage:</b><br><br>
   {SENDER_QUESTION}<br>
  <br><br>
   Link zur Auktion: <a href="{SITEURL}item.php?id={AID}">{SITEURL}item.php?id={AID}</a><br>
  </p>
 <p><br>
  <br>
  <br>
   Besten Dank und freundliche Gr�sse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>