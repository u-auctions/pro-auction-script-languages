<!doctype html>
<html>
<head>
<meta charset="utf-8">
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		font-size: 12px;
 		}
 	</style>
 </head>
 <body>

 <p>Guten Tag {REALNAME},</p>
 <p><br>
   F�r Ihr Benutzerkonto wurde wie gew�nscht ein neues Passwort erstellt.<br>
  <br>
   Ihr neues Passwort: <strong>{NEWPASS}</strong><br>
  </p>
 <p><br>
   Ben�tzen Sie es, um sich auf &laquo;{SITENAME}&raquo; einzuloggen und �ndern Sie es aus Sicherheitsgr�nden wieder auf ein idividuelles Passwort Ihrer Wahl.
  <br>
 </p>
 <p><br>
   Besten Dank und freundliche Gr�sse<br>
  <br>
   Die Administration Ihres Auktionshauses &laquo;{SITENAME}&raquo;</p>
 </body>
</html>
