Guten Tag {W_NAME},

Sie haben die folgende Auktion auf �{SITENAME}� gewonnen:


Titel: {A_TITLE}
Beschreibung: {A_DESCRIPTION}

Ihr h�chstes Gebot: {A_CURRENTBID} ({W_WANTED} items)
Sie erhalten: {W_GOT} St�ck
Auktionsende: {A_ENDS}
Link zur Auktion: {A_URL}


Informationen zum Verk�ufer:

{S_NICK}
{S_EMAIL}
Zahlungsdetails:
{S_PAYMENT}


Wenn Sie dieses Mail irrt�mlich erhalten haben, so wenden Sie sich bitte an den Administrator ({ADMINMAIL}) oder besuchen Sie das Auktionshaus �{SITENAME}� unter {SITE_URL}.



Besten Dank und freundliche Gr�sse

Die Administration Ihres Auktionshauses �{SITENAME}�
