Guten Tag {NAME},

Ihr Benutzerkonto beim Auktionshaus „{SITENAME}“ wurde gesperrt, weil das Limit für die maximalen Ausstände erreicht wurde.
Um Ihr Benutzerkonto zu reaktivieren müssen Sie Ihre Ausstände vollständig begleichen. 


Total zu bezahlen: {BALANCE}


Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsmöglichkeiten zu gelangen:
{OUTSTANDING}


Bitte beachten Sie: 
Bis zur vollständigen Bezahlung der Gebühr werden Sie zu keinen Aktivitäten im Auktionshaus mehr zugelassen.



Besten Dank und freundliche Grüsse

Die Administration Ihres Auktionshauses „{SITENAME}“
