Guten Tag {S_NAME},

Die folgende Auktion auf �{SITENAME}� wurde gestern geschlossen.

{REPORT}


Ein E-Mail mit Ihrer E-Mail-Adresse wurde an den oder die Gewinner der Auktion gesandt.

Wenn Sie dieses Mail irrt�mlich erhalten haben, so wenden Sie sich bitte an den Administrator ({ADMINMAIL}) oder besuchen Sie das Auktionshaus �{SITENAME}� unter {SITE_URL}.



Besten Dank und freundliche Gr�sse

Die Administration Ihres Auktionshauses �{SITENAME}�
