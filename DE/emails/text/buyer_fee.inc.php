Guten Tag {C_NAME},

Dies ist eine Rechnung für die Kaufgebühr des folgenden Artikels: {ITEM_NAME}.

Total zu bezahlen: {BALANCE}

Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsmöglichkeiten zu gelangen:
{LINK}

Bitte beachten Sie: 
Bis zur vollständigen Bezahlung der Gebühr werden Sie zu keinen Aktivitäten im Auktionshaus mehr zugelassen.




Besten Dank und freundliche Grüsse

Die Administration Ihres Auktionshauses „{SITENAME}“