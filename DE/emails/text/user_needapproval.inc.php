Guten Tag {C_NAME},

Damit Sie sich mit Ihrem Login und Ihrem Passwort anmelden und mit Verkaufen und Bieten beginnen k�nnen muss Ihr Benutzerkonto von einem Administrator auf �{SITENAME}� genehmigt werden.

Dieser Prozess ist momentan in Bearbeitung und Sie werden umgehend per E-Mail benachrichtigt, sobald Ihr Benutzerkonto freigeschaltet und aktiviert wurde.



Ihre Konto-Informationen:

Name: {C_NAME}
Benutzername: {C_NICK}
<!-- IF C_ADDRESS ne '' -->
Adresse: {C_ADDRESS}
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
Stadt: {C_CITY}
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
Region: {C_PROV}
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
Land: {C_COUNTRY}
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
Postleitzahl: {C_ZIP}
<!-- ENDIF -->
E-Mail-Adresse: {C_EMAIL}





Besten Dank und freundliche Gr�sse

Die Administration Ihres Auktionshauses �{SITENAME}�