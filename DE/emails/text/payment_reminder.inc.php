Guten Tag {C_NAME},

Dies ist eine Rechnung zum Begleichen der offenen Gebühren auf „{SITENAME}“.

Total zu bezahlen: {BALANCE}

Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsmöglichkeiten zu gelangen:
{LINK}




Besten Dank und freundliche Grüsse

Die Administration Ihres Auktionshauses „{SITENAME}“