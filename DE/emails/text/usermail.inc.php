Guten Tag {C_NAME},

Bitte schliessen Sie Ihre Registrierung beim Auktionshaus �{SITENAME}� ab!

Klicken Sie dazu auf den unten stehenden Link. Nach Abschluss der Best�tigung des Benutzerkontos k�nnen sich mit Ihrem Login und Ihrem Passwort anmelden und mit Verkaufen und Bieten beginnen.


Link zur Best�tigung (Klicken oder kopieren und in die Adresszeile Ihres Webbrowsers einf�gen):

{CONFIRMURL}



Sollte der Link nicht funktionieren, so senden Sie uns bitte ein E-Mail auf {ADMINMAIL}.




Besten Dank und freundliche Gr�sse

Die Administration Ihres Auktionshauses �{SITENAME}�
