Guten Tag {C_NAME},

Ihr Artikel wurde bei �{SITENAME}� erfolgreich aufgenommen. Der Artikel wird umgehend in allen Suchresultaten und im Auktionshaus angezeigt.

Die Details dazu:

Titel: {A_TITLE}
Auktionstyp: {A_TYPE}
Artikel-Nummer: {A_ID}
Mindestgebot: {A_MINBID}
Reservierungspreis: {A_RESERVE}
Sofort Kaufen Preis: {A_BNPRICE}
Auktionsende: {A_ENDS}

{SITE_URL}item.php?id={A_ID}

				
Wollen Sie weitere Artikel einstellen?
{SITE_URL}select_category.php