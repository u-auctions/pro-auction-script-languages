Guten Tag {C_NAME}

Dies ist eine Rechnung f�r die Umsatzgeb�hr des folgenden Artikels: {ITEM_NAME}.

Total zu bezahlen: {BALANCE}

Klicken Sie bitte auf den folgenden Link, um zur Seite mit den Zahlungsm�glichkeiten zu gelangen:
{LINK}

Bitte beachten Sie: 
Bis zur vollst�ndigen Bezahlung der Geb�hr werden Sie zu keinen Aktivit�ten im Auktionshaus mehr zugelassen.




Besten Dank und freundliche Gr�sse

Die Administration Ihres Auktionshauses �{SITENAME}�