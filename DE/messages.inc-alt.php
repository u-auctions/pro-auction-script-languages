<?php
/*******************************************************************************
 *   copyright				: (C) 2011 - 2014 uAuctions
 *   site					: http://www.u-Auctions.com
 *******************************************************************************/

/*******************************************************************************
 *   This uAuctions is a Paid version of the Free version that comes with 
 *   extra feachers that the Free version dose not included in it.
 *   You are not allowed to resell/sell this script this auction script is 
 *   copyrighted to uAuctions.
 *   If you have been sold this script from a 3rd party and not from the 
 *   ubidzz.com website ask for a refund.
*******************************************************************************
******************************************************************************
 * If you bought this script from the ubidzz.com  
 * Please register at u-Auctions.com and contact the uAuctions admin  
 * at u-Auctions.com with your order number and name and member name that 
 * you used on the forums so we can change your group to Paid so you can view the
 * paid area on the forums.
*******************************************************************************/

if (!defined('InuAuctions')) exit();

// CHARSET ENCODING
// Change the charset according to the language used in this file.
$CHARSET = "{CHARSET}";
// DOCUMENT DIRECTION
// Change the $DOCDIR variable below according to the document direction needed
// by the language you are using.
// Possible values are:
// - ltr (default) - means left-to-right document (almost any language)
// - rtl - means right-to-left document (i.e. arabic, hebrew, ect).
$DOCDIR = "ltr";
// Error messages and user interface messages are below. Translate them taking care of leaving
// The PHP and HTML tags unchanged.
// Error messages =============================================================
$ERR_001 = "Keine Verbindung zur Datenbank. Bitte kontaktieren Sie den Administrator.";
$ERR_002 = "Bitte Name angeben";
$ERR_003 = "Bitte Benutzername eingeben";
$ERR_004 = "Bitte Passwort einngeben";
$ERR_005 = "Passwort wiederholen";
$ERR_006 = "Passworte stimmen nicht �berein";
$ERR_007 = "Bitte E-Mail-Adresse angeben";
$ERR_008 = "Bitte g�ltige E-Mail-Adresse angeben";
$ERR_009 = "Benutzername schon vorhanden, bitte einen anderen w�hlen";
$ERR_010 = "Benutzername zu kurz (mindestens 6 Zeichen)";
$ERR_011 = "Passwort zu kurz (mindestens 6 Zeichen)";
$ERR_012 = "Adresse fehlt";
$ERR_013 = "Stadt fehlt";
$ERR_014 = "Land fehlt";
$ERR_015 = "Postleitzahl fehlt";
$ERR_016 = "Bitte g�ltige Postleitzahl eingeben";
$ERR_017 = "Bitte Titel Ihres Artikel angeben";
$ERR_018 = "Beschreibung des Artikels fehlt";
$ERR_019 = "Startpreis fehlt";
$ERR_020 = "Minimale Anzahl ist nicht korrekt";
$ERR_021 = "Mindestpreis fehlt";
$ERR_022 = "der eingegebene Mindestpreis ist nicht korrekt";
$ERR_023 = "Bitte eine Kategorie w�hlen";
$ERR_024 = "Bitte eine Zahlungsmethode w�hlen";
$ERR_025 = "Unbekannter Benutzername";
$ERR_026 = "Falsches Passwort";
$ERR_027 = "W�hrungssymbol fehlt";
$ERR_028 = "Bitte g�ltige E-Mail-Adresse angeben";
$ERR_029 = "Benutzerdaten sind bereits registriert";
$ERR_030 = "Felder m�ssen numerisch in im Format nnnn.nn sein";
$ERR_031 = "Das Formular ist noch nicht vollst�ndig";
$ERR_032 = "eine oder beide E-Mail-Adressen sind nicht korrekt";
$ERR_033 = "Ung�ltiger Best�tigungs-Link";
$ERR_034 = "Ihr Angebot muss mindestens sein: ";
$ERR_035 = "Tage m�ssen in numerischem Format sein";
$ERR_036 = "Der Verk�ufer kann f�r seine eigenen Angebote keine Gebote abgeben";
$ERR_037 = "Das Suche-Feld darf nicht leer sein";
$ERR_038 = "Falsches Login";
$ERR_039 = "Ihre Registrierung wurde bereits best�tigt.";
$ERR_040 = "Sie sind bereits der H�chstbietende und k�nnen daher keine Gebote abgeben, die tiefer sind als Ihr bestehendes H�chstgebot.";
$ERR_041 = "Bitte w�hlen Sie eine Bewertung zwischen 1 und 5";
$ERR_042 = "Ihr Kommentar fehlt";
$ERR_043 = "Ung�ltiges Feld-Format: muss numerisch sein";
$ERR_044 = "Ein Benutzer muss mindestens einer Benutzergruppe angeh�ren";
$ERR_047 = "Erforderliches Feld fehlt";
$ERR_048 = "Ung�ltiges Login";
$ERR_049 = "Der Name der Kategorie darf nicht leer sein";
$ERR_050 = "Mindestens eine Benutzergruppe muss *Automatisch beitreten* aktiviert haben";
$ERR_051 = "Bitte eine g�ltiger Anzahl Ziffern eintragen";
$ERR_054 = "BItte beide Passwort-Felder ausf�llen";
$ERR_055 = "Benutzername <i>%s</i> ist bereits in der Datenbank vorhanden";
$ERR_056 = "Wert f�r Erh�hungsschritt fehlt";
$ERR_057 = "Werte f�r Erh�hungsschritte m�ssen numerisch sein";
$ERR_058 = "Ung�ltiges W�hrungsformat.";
$ERR_059 = "Ihr bereits bestehendes Gebot f�r diese Auktion ist h�her als Ihr jetziges Gebot.<br>  Bei R�ckw�rtsauktionen k�nnen Sie keine Gebote abgeben, wenn Sie bereits h�her geboten haben als jetzt. ";
$ERR_060 = "Startdatum kann nicht in der Vergangenheit liegen.";
$ERR_061 = "Der eingegebene *Sofort Kaufen* Preis ist ung�ltig";
$ERR_062 = "Bei R�ckw�rtsauktionen k�nnen keine Mindestpreise angegeben werden";
$ERR_063 = "Bei R�ckw�rtsauktionen k�nnen keine individuellen Erh�hungsschritte angegeben werden";
$ERR_064 = "Bei R�ckw�rtsauktionen kann die Option *Sofort Kaufen* nicht verwendet werden";
$ERR_065 = "Fehler beim Hochladen der Informationen";
$ERR_066 = "Fehler beim L�chen der Informationen";
$ERR_067 = "Erforderliche Felder fehlen (Alle Felder sind erforderlich).";
$ERR_068 = "Das gew�hlte Theme existiert nicht";
$ERR_069 = "W�hrungsumrechnung fehlgeschlagen";
$ERR_070 = "Diese Mitteilung existiert nicht";
$ERR_071 = "Benutzername ung�ltig. Darf nur Buchstaben und Zahlen enthalten";
$ERR_072 = "Bitte einen Betrag f�r Ihr Gebot eintragen";
$ERR_073 = "Sie k�nnen keine Gebote abgeben f�r Auktionen, die noch nicht begonnen haben";
$ERR_074 = "F�r diese Transaktion haben Sie bereits eine Bewertung abgegeben";
$ERR_075 = 'Keine Gebote';
$ERR_076 = 'Ung�ltige Benutzername/E-Mail-Kombination';
$ERR_077 = 'G�ltiges Token abgelaufen';
$ERR_078 = 'Sie m�ssen mit den Nutzungsbedingungen einverstanden sein';
$ERR_079 = 'Sie m�ssen mit den Cookiesbedingungen einverstanden sein';

$ERR_100 = "Benutzer existiert nicht";
$ERR_101 = "Falsches Passwort";
$ERR_102 = "Benutzer existiert nicht";
$ERR_103 = "Sie k�nnen sich nicht selbst bewerten";
$ERR_104 = "Alle Felder erforderlich";
$ERR_105 = "Benutzername existiert nicht";
$ERR_106 = "<br><br>Kein Benutzer angegeben";
$ERR_107 = "Benutzername zu kurz (min. 6 Zeichen)";
$ERR_108 = "Passwort zu kurz (min. 6 Zeichen)";
$ERR_109 = "Passworte stimmen nicht �berein";
$ERR_110 = "Ung�ltige E-Mail-Adresse";
$ERR_111 = "Dieser Benutzername existiert bereits";
$ERR_112 = "Fehlende Daten";
$ERR_113 = "Sie m�ssen mindestens 18 Jahre alt sein, um sich zu registrieren";
$ERR_114 = "Keine aktiven Auktionen f�r diese Kategorie gefunden";
$ERR_115 = "Diese E-Mail-Adresse wird bereits verwendet";
$ERR_115_a = "Die von Ihnen eingegebene E-Mail-Domain-Adresse ist nicht zul�ssig";
$ERR_115_b = "Die Zahlungs info wird bereits verwendet,";
$ERR_117 = "Ung�ltiges Geburtsdatum";
$ERR_122 = "Keine Auktionen gefunden";

$ERR_600 = 'Ung�ltiger Auktionstyp';
$ERR_601 = "Feld *Anzahl* ist nicht korrekt";
$ERR_602 = "Bilder m�ssen im Format GIF oder JPG sein";
$ERR_603 = "Dieses Bild ist zu gross.";
$ERR_606 = "Die angegebene Auktions-ID ist nicht korrekt";
$ERR_607 = "Ihr Gebot ist unter dem Minimalgebot";
$ERR_608 = "Die angegebene Anzahl ist ung�ltig, Sie k�nnen h�chstens f�r die angebotene Anzahl Artikel bieten";
$ERR_608a = "Die angegebene Menge ist nicht g�ltig. Bitte geben Sie eine g�ltige Anzahl ein.";
$ERR_609 = "Benutzer existiert nicht";
$ERR_610 = "Bitte Benutzername und Passwort eintragen";
$ERR_611 = "Falsches Passwort";
$ERR_612 = "Sie k�nnen keine Gebote abgeben, da Sie der Verk�ufer sind!";
$ERR_614 = "Diese Auktion ist geschlossen";
$ERR_616 = "Postleitzahl ist zu kurz";
$ERR_617 = "Telefonnummer ist ung�ltig";
$ERR_618 = "Ihr Konto wurde vom Administrator gesperrt.";
$ERR_619 = "Diese Auktion wurde gesperrt";
$ERR_620 = "Ihr Konto muss zuerst aktiviert werden; bitte sehen Sie dazu in Ihren E-Mails nach. Der Aktivierungs-Link wurde Ihnen per E-Mail geschickt.";
$ERR_621 = "Ihr Konto muss zuerst vom Administrator aktiviert werden.";
$ERR_622 = "Keine solche Auktion.";
$ERR_623 = "Die von Ihnen gesuchte Auktion wurde entweder entfernt oder hat nie existiert.";
$ERR_624 = "Keine Nachricht zum posten";

$ERR_700 = "Ung�ltiges Datumsformat";
$ERR_701 = "Ung�ltige Anzahl (muss >0 sein).";
$ERR_702 = "Gebot muss h�her sein als das Minimalgebot.";
$ERR_704 = "<br>Sie k�nnen f�r diesen Benutzer keine Bewertung abgeben! <br>Diese Auktion ist noch nicht komplett abgeschlossen (in der Regel noch nicht bezahlt)!";
$ERR_705 = "Sie k�nnen f�r diesen Benutzer nur eine Bewertung abgeben, wenn Sie mit ihm Transaktionen get�tigt und komplett abgeschlossen haben!";
$ERR_706 = "<i>Max. Anzahl Bilder</i> muss numerisch sein.";
$ERR_707 = "<i>Max. Bildgr�sse</i> kann nicht 0 sein.";
$ERR_708 = "<i>Max. Bildgr�sse</i> muss numerisch sein.";
$ERR_709 = "Das Bild, das Sie hochladen m�chten ist zu gross. Bilder d�rfen nicht gr�sser sein als ";
$ERR_710 = "Falsches Dateiformat. Erlaubte Dateiformate sind: GIF, PNG and JPEG";
$ERR_711 = "Sie k�nnen diesen Artikel nicht kaufen, Sie sind der Verk�ufer!";
$ERR_712 = "<b>*Sofort kaufen*</b> ist f�r diese Auktion nicht verf�gbar";
$ERR_713 = 'Der Wert im Feld *von* muss kleiner sein als der Wert im Feld *bis*';
$ERR_714 = 'Der Wert f�r das Feld *Angebot reaktivieren* muss numerisch sein';
$ERR_715 = 'Der Wert f�r *Angebot reaktivieren* ist zu hoch';

$ERR_5000 = "Angezeigte Meldungen m�ssen numerisch sein";
$ERR_5001 = "Angezeigte Meldungen k�nnen nicht 0 sein";
$ERR_5002 = "Sie m�ssen mindestens einen Statistiktyp w�hlen (Zugriffe, Browser &amp; Plattform, nach Land)";

$ERR_5014 = "Betreff oder Mitteilung fehlt";

$ERR_5029 = "Name fehlt";
$ERR_5030 = "Benutzername fehlt";
$ERR_5031 = "Passwort fehlt";
$ERR_5032 = "Bitte geben Sie Ihr Passwort zweimal ein";
$ERR_5033 = "E-Mail-Adresse fehlt";
$ERR_5034 = "Adresse fehlt";
$ERR_5035 = "Stadt fehlt";
$ERR_5036 = "Gebiet fehlt";
$ERR_5037 = "Land fehlt";
$ERR_5038 = "Postleitzahl fehlt";
$ERR_5039 = "Telefonnummer fehlt";
$ERR_5040 = "Geburtsdatum fehlt oder unvollst�ndig";
$ERR_5045 = "Der Mindestpreis kann nicht niedriger sein als das Minimalgebot";
$ERR_5046 = "Der *Sofort Kaufen* Preis kann nicht niedriger sein als das Minimalgebot oder der Mindestpreis";

$ERR_25_0001 = "Bitte eine Unterkategorie ausw�hlen";
$ERR_25_0002 = "<p>URL file-Access ist auf Ihrem Server deaktiviert, daher kann uAuctions die Versionskontrolle nicht ausf�hren</p>";

// UI Messages =============================================================
$MSG['001'] = "Registrieren als neuer Benutzer";
$MSG['002'] = "Name";
$MSG['003'] = "Benutzer";
$MSG['004'] = "Passwort";
$MSG['005'] = "Passwort wiederholen";
$MSG['006'] = "E-Mail-Adresse";
$MSG['007'] = "Senden";
$MSG['008'] = "L�schen";
$MSG['009'] = "Adresse";
$MSG['010'] = "Stadt";
$MSG['011'] = "Region";
$MSG['012'] = "Postleitzahl";
$MSG['013'] = "Telefon";
$MSG['014'] = "Land";
$MSG['015'] = "--bitte w�hlen";
$MSG['016'] = "Registrierung abgeschlossen. Ihre Daten wurden gespeichert.<br>Eine Best�tigungs-E-Mail wurde an folgende Adresse gesandt: <i>%s</i>";
$MSG['016_a'] = "Registrierung abgeschlossen. Ihre Daten wurden gespeichert.<br>Ein Administrator wird in K�rze Ihr Konto �berpr�fen. Nach der Freigabe durch den Administrator k�nnen Sie sich einloggen.";
$MSG['016_b'] = "Registrierung abgeschlossen. Ihre Daten wurden gespeichert.<br>Sie k�nnen sich jetzt mit Ihrem Benutzernamen und Passwort einloggen.";
$MSG['017'] = "Titel";
$MSG['018'] = "Beschreibung";
$MSG['019'] = "Standardbild";
$MSG['020'] = "Auktion beginnt mit";
$MSG['021'] = "Mindestpreis";
$MSG['022'] = "Dauer";
$MSG['023'] = "Versandkosten";
$MSG['024'] = "Neues Passwort";
$MSG['025'] = "Versandkonditionen";
$MSG['026'] = "Zahlungsm�glichkeiten";
$MSG['027'] = "Bitte Kategorie w�hlen";
$MSG['028'] = "Verkaufen";
$MSG['029'] = "Nein";
$MSG['030'] = "Ja";
$MSG['031'] = "K�ufer tr�gt die Versandkosten";
$MSG['032'] = "Verk�ufer tr�gt die Versandkosten";
$MSG['033'] = "Internationaler Versand";
$MSG['034'] = "Vorschau auf die Auktion";
$MSG['035'] = "Eingabefelder zur�cksetzen";
$MSG['036'] = "Daten absenden";
$MSG['037'] = "Kein Bild vorhanden";
$MSG['038'] = "Preis anfragen";
$MSG['039'] = "Kein Mindestpreis";
$MSG['040'] = "Auktion absenden";
$MSG['041'] = "Kategorie";
$MSG['043'] = "Kein internationaler Versand";
$MSG['044'] = "Bitte Benutzernamen und Passwort eingeben und Formular absenden.";
$MSG['045'] = "Benutzerverwaltung";
$MSG['046'] = "Sie k�nnen <b><a href='sell.php?mode=recall'>Ihre Auktion hier noch �ndern.</a></b>";
$MSG['047'] = " Neu";
$MSG['048'] = "Felder f�r die Benutzerregistrierung";
$MSG['049'] = "Wenn Sie nicht registriert sind, ";
$MSG['050'] = "(mindestens 6 Zeichen)";
$MSG['051'] = "Hauptseite";
$MSG['052'] = "Login";
$MSG['053'] = "Admin E-Mail bearbeiten";
$MSG['054'] = "Neue E-Mail-Adresse angeben";
$MSG['055'] = "Admin E-Mail unten bearbeiten";
$MSG['056'] = "E-Mail-Adresse wurde aktualisiert";
$MSG['057'] = "Bitte W�hrungssymbol unten bearbeiten";
$MSG['058'] = "Neues W�hrungssymbol eingeben";
$MSG['059'] = "E-Mail gesandt";
$MSG['060'] = "W�hrungssymbol wurde aktualisiert";
$MSG['067'] = "laufende Auktionen anzeigen";
$MSG['068'] = "Individuelle Erh�hungsschritte zulassen";
$MSG['069'] = "Auktionsdauer";
$MSG['070'] = "Benutzer k�nnen f�r Ihre Auktionen individuelle Erh�hungsschritte einstellen (Erh�hungsschritt = minimale Differenz zwischen zwei Geboten)";
$MSG['071'] = "Aktualisieren";
$MSG['072'] = " Benutzer warten auf Bewertungen von Ihnen";
$MSG['073'] = "Linien l�schen";
$MSG['074'] = "Bitte ben�tzen Sie das Kontrollk�stchen *L�schen* und die Schaltfl�che *L�schen*, um Zeilen zu l�schen. Bearbeiten Sie die Textfelder und w�hlen sie die Schaltfl�che *Aktualisieren* um die �nderungen zu speichern.";
$MSG['075'] = "Zahlungsarten";
$MSG['076'] = "W�hrungssymbol";
$MSG['077'] = "Admin E-Mail-Adresse bearbeiten";
$MSG['078'] = "Kategorien-Tabelle";
$MSG['079'] = "Ihre Auktion ist beendet";
$MSG['080'] = "Z�hler neu synchronisieren oder Cache leeren";
$MSG['081'] = "L�ndertabelle";
$MSG['082'] = "Umrechnen";
$MSG['083'] = "von dieser W�hrung";
$MSG['084'] = "Mitteilung angebracht";
$MSG['085'] = "::: W�hrungsumrechner :::";
$MSG['086'] = "Kategorientabelle aktualisiert";
$MSG['087'] = "Beschreibung";
$MSG['088'] = "in diese W�hrung";
$MSG['089'] = "�nderungen speichern";
$MSG['090'] = "L�ndertabelle aktualisiert";
$MSG['091'] = "Sprache �ndern";
$MSG['092'] = '�ndern, L�schen oder Hinzuf�gen von Zahlungsarten mittels des unten stehenden Formulars. Dies sind alternative weitere Zahlungsoptionen. Die eingebauten Zahlungs-Gateways k�nnen Sie bei den <b><a href="fee_gateways.php">Zahlungs-Gateway-Einstellungen</a></b> freischalten oder deaktivieren.';
$MSG['093'] = "Zahlungsarten-Tabelle aktualisiert";
$MSG['094'] = "�ndern, L�schen oder Hinzuf�gen von L�ndern mittels des unten stehenden Formulars.";
$MSG['095'] = "Willkommen, Sie sind nun ein Mitglied!";
$MSG['096'] = "Aktuelle Sprache";
$MSG['097'] = "Tage";
$MSG['098'] = "Registrierungsbest�tigung";
$MSG['099'] = "Ihre Auktion wurde best�tigt";

$MSG['100'] = "Ihre Auktion wurde ordnungsgem�ss erhalten.<br>Eine Best�tigungs-E-Mail wurde an Ihre E-Mail-Adresse gesandt.<br>";
$MSG['101'] = "URL zu Ihrer Auktion: ";
$MSG['103'] = " Suche ";
$MSG['104'] = "Auktionen ";
$MSG['105'] = "Verlauf ansehen";
$MSG['106'] = "Einem Freund senden";
$MSG['107'] = "E-Mail-Adresse des Benutzers";
$MSG['108'] = "Bild ansehen";
$MSG['109'] = "Tag";
$MSG['110'] = "Admin";
$MSG['111'] = "Auktion gestartet";
$MSG['112'] = "Auktion endet";
$MSG['112a'] = "-- Ihre Auktion endete ohne Gewinner";
$MSG['113'] = "Auktions-ID";
$MSG['114'] = "Kein Bild verf�gbar";
$MSG['115'] = "Gebot jetzt abgeben!";
$MSG['116'] = "Aktuelles Gebot";
$MSG['117'] = "Bieter mit h�chstem Gebot";
$MSG['118'] = "Endet in";
$MSG['119'] = "Anzahl Gebote";
$MSG['120'] = "Erh�hungsschritte";
$MSG['121'] = "Gebot hier abgeben:";
$MSG['122'] = "�ndern, L�schen oder Hinzuf�gen von Auktionsdauern mittels des unten stehenden Formulars.";
$MSG['123'] = "Auktionsdauertabelle aktualisiert";
$MSG['124'] = "Minimales Gebot";
$MSG['125'] = "Verk�ufer";
$MSG['126'] = " Tage, ";
$MSG['126b'] = " Tag, ";
$MSG['126a'] = "Tage her";
$MSG['127'] = "Startgebot";
$MSG['128'] = "Erh�hungsschritte";
$MSG['129'] = "ID";
$MSG['130'] = "Gebot";
$MSG['131'] = "K�ufer";
$MSG['132'] = "�bersetzungstabelle f�r Kategorien";
$MSG['133'] = "Erh�hungsschritte-Tabelle";
$MSG['135'] = "�ndern, L�schen oder Hinzuf�gen der Erh�hungsschritte mittels des unten stehenden Formulars.<br>
			Bitte �nderungen vorsichtig vornehmen, die Kongruenz der eingetragenen Werte wir nicht gepr�ft.
			Sie muss vom Administrator selbst �berpr�ft werden. Es wird nur gepr�ft, ob die Eintr�ge im numerischen Format sind.";
$MSG['136'] = "und";
$MSG['137'] = "Erh�hungsschritt";
$MSG['138'] = "Zur�ck zur Auktion";
$MSG['139'] = "Diese Auktion einem Freund senden";
$MSG['140'] = "Name Ihres Freundes";
$MSG['141'] = "E-Mail-Adresse Ihres Freundes";
$MSG['142'] = "Option *Top-Angebot* aktivieren";
$MSG['143'] = "Ihre E-Mail-Adresse";
$MSG['144'] = "Einen Kommentar hinzuf�gen";
$MSG['145'] = "Ihrem Freund senden";
$MSG['146'] = "Diese Auktion wurde an folgende Adresse versandt: ";
$MSG['147'] = "Einem weiteren Freund senden";
$MSG['148'] = "Hilfe";
$MSG['148a'] = "Zee Webseite";
$MSG['148ab'] = "Auktion Start";
$MSG['149'] = "Sie k�nnen diesen Benutzer mittels des untenstehenden Formulars kontaktieren.";
$MSG['150'] = "Anfrage senden";
$MSG['151'] = " Die angefragte E-Mail-Adresse lautet ";
$MSG['152'] = "Bitte Ihr Gebot best�tigen";
$MSG['153'] = "Sie m�ssen registriert sein, um Gebote abzugeben.";
$MSG['154'] = "Sie bieten f�r:";
$MSG['155'] = "Artikel:";
$MSG['156'] = "Ihr Gebot:";
$MSG['157'] = "Erm�glicht es den Verk�ufern, Ihre Auktionen auf der Startseite und den Kategorienseiten als *Top-Angebot* einzustellen";
$MSG['158'] = "Gebot abgeben";
$MSG['159'] = "Ihr Gebot wurde empfangen";
$MSG['159'] = "Bieter:";
$MSG['160'] = "Erh�hungsschritt-Tabelle aktualisiert";
$MSG['161'] = "�ndern, L�schen oder Hinzuf�gen von Kategorien mittels des unten stehenden Formulars.";
$MSG['162'] = "Option *Hervorheben* aktivieren";
$MSG['163'] = "Registrieren!";
$MSG['164'] = "Erm�glicht es Verk�ufern, Ihre Auktionen als hervorgehoben zu markieren (Anzeige in anderer Farbe bei Listen aus z.B. Suchresultaten usw.)";
$MSG['165'] = "Kategorie: ";
$MSG['166'] = "Start";
$MSG['167'] = "Bild";
$MSG['168'] = "Auktion";
$MSG['169'] = "Aktuelles Gebot";
$MSG['170'] = "Anzahl Gebote";
$MSG['171'] = "Endet in";
$MSG['171a'] = "Beendet";
$MSG['172'] = "Keine laufenden Auktionen in dieser Kategorie";
$MSG['173'] = "Suchresultat: ";
$MSG['174'] = "Option *Fett* aktivieren";
$MSG['175'] = "Datum und Uhrzeit";
$MSG['176'] = "Bieter";
$MSG['177'] = "Kategorienliste";
$MSG['178'] = "Bieter kontaktieren";
$MSG['179'] = "Um die E-Mail-Adresse eines anderen Benutzers zu erhalten, tragen Sie hier bitte Ihren Benutzernamen und Ihr Passwort ein.";
$MSG['180'] = " ist:";
$MSG['181'] = "Benutzer-Login";
$MSG['182'] = "Pers�nliche Daten bearbeiten";
$MSG['183'] = "Ihre Daten wurden aktualisiert";
$MSG['184'] = "Die Kategorien-Tabelle wurde aktualisiert.";
$MSG['185'] = "Sie betrachten momentan die Bewertung f�r ";
$MSG['186'] = "<a href=\"javascript:history.back()\">Zur�ck</a>";
$MSG['187'] = "Ihr Benutzername";
$MSG['188'] = "Ihr Passwort";
$MSG['189'] = "Total f�llig";
$MSG['190'] = "Kategorie Ihres Artikels";
$MSG['191'] = "Ihre Copyright-Mitteilung";
$MSG['192'] = "Das ist die Copyright-Mitteilung, die am Ende jeder Seite angezeigt wird";
$MSG['193'] = "Auktionsdauer";
$MSG['194'] = "Erm�glicht es Verk�ufern, Ihre Auktionen als fett zu markieren (Anzeige in fetter Schriftart bei Listen aus z.B. Suchresultaten usw.)";
$MSG['195'] = "Bild-URL";
$MSG['196'] = "Artikelbeschreibung";
$MSG['197'] = "Titel";
$MSG['198'] = "Keine Artikel gefunden";
$MSG['199'] = "Suche";
$MSG['200'] = "Hallo ";
$MSG['201'] = "Neuer Benutzer";
$MSG['202'] = "Benutzerdaten";
$MSG['203'] = "Laufende Auktionen";
$MSG['204'] = "Geschlossene Auktionen";
$MSG['205'] = "Pers�nliche Einstellungen";
$MSG['206'] = "Benutzerprofil";
$MSG['207'] = "Bewertung abgeben";
$MSG['208'] = "Bewertung ansehen";
$MSG['209'] = "Registrierter Benutzer seit: ";
$MSG['210'] = "Kontakt ";
$MSG['211'] = "Auktion jetzt beginnen";
$MSG['212'] = "Auktionen:";
$MSG['213'] = "<b>Weitere Auktionen des Verk�ufers</b>";
$MSG['214'] = "Geschlossene Auktionen anzeigen";
$MSG['215'] = "Passwort vergessen?";
$MSG['216'] = "Wenn Sie Ihr Passwort vergessen oder verloren haben, so tragen Sie bitte Ihren Benutzernamen <strong>und</strong> Ihre E-Mail-Adresse in die Felder unten ein.<br>Ein neues Passwort wird f�r Sie generiert";
$MSG['217'] = "Ein neues Passwort wurde an Ihre E-Mail-Adresse gesandt.";
$MSG['218'] = "Benutzerprofil anzeigen";
$MSG['219'] = "Laufende Auktionen: ";
$MSG['220'] = "Geschlossene Auktionen: ";
$MSG['221'] = "Benutzer-Login";
$MSG['222'] = "Bewertungen";
$MSG['223'] = "Einen Kommentar abgeben";
$MSG['224'] = "W�hlen Sie eine Beurteilung zwischen 1 und 5";
$MSG['225'] = "Danke f�r Ihren Kommentar!";
$MSG['226'] = "Ihre Beurteilung ";
$MSG['227'] = "Ihr Kommentar ";
$MSG['228'] = "Bewertet von ";
$MSG['229'] = "Neueste Bewertung:";
$MSG['230'] = "Alle Bewertungen anzeigen";
$MSG['231'] = "REGISTRIERTE BENUTZER";
$MSG['232'] = "laufende AUKTIONEN ";
$MSG['233'] = "Mehr...";
$MSG['234'] = "Zur�ck &gt;";
$MSG['235'] = "Registrieren";
$MSG['236'] = "Bieterdaten verstecken";
$MSG['237'] = "Aktivieren der Option *Bieterdaten verstecken*?";
$MSG['238'] = "Wenn Sie diese Option aktivieren, dann wird die Identit�t des Bieters nur dem Verk�ufer angezeigt. F�r alle anderen Benutzer bleibt die Identit�t versteckt.";
$MSG['239'] = "Auktionen";
$MSG['240'] = "Von";
$MSG['241'] = "Bis";
$MSG['242'] = "Gesendet";
$MSG['243'] = "Wenn Sie Ihr Passwort �ndern m�chten, dann f�llen Sie bitte die beiden unten stehenden Felder aus. Ansonsten die Felder einfach leer lassen.";
$MSG['244'] = "Daten bearbeiten";
$MSG['245'] = "Ausloggen";
$MSG['246'] = "Eingeloggt";
$MSG['247'] = "Bieterdaten verstecken: Einstellungen aktualisiert";
$MSG['248'] = "Registrierung best�tigen";
$MSG['249'] = "Best�tigen";
$MSG['250'] = "Ablehnen";
$MSG['251'] = "---- hier ausw�hlen";
$MSG['252'] = "Geburtsdatum (tt/mm/yyyy)";
$MSG['253'] = "(tt/mm/jjjj)";
$MSG['254'] = "Neue Kategorie vorschlagen";
$MSG['255'] = "Auktions-ID";
$MSG['256'] = "Oder Bild zum Hochladen ausw�hlen (optional)";
$MSG['257'] = "Auktionstyp";
$MSG['258'] = "Artikel Anzahl";
$MSG['259'] = "Artikel";
$MSG['260'] = "Oder Beginn am:";
$MSG['261'] = "Auktionstyp";
$MSG['262'] = "Ihr Vorschlag";
$MSG['263'] = "Geb�hrentotal";
$MSG['264'] = "Sie k�nnen noch ";
$MSG['265'] = "�nderungen";
$MSG['266'] = " an dieser Auktion vornehmen";
$MSG['267'] = "Sie haben sich f�r diese Seite angemeldet.
			<br>Um Ihre Registrierung zu best�tigen dr�cken Sie einfach auf die Schaltfl�che <b>Best�tigen</b> gleich unten.
			<br>Wenn Sie sich nicht registrieren und Ihre Daten von der Datenbank l�schen m�chten, so w�hlen Sie einfach die Schaltfl�che <b>Ablehnen</b>.";
$MSG['268'] = "Zus�tzliche Optionen";
$MSG['269'] = "Ihr Gebot wurde angenommen";
$MSG['270'] = "Zur�ck";
$MSG['271'] = "Ihr Gebot wurde verarbeitet";
$MSG['272'] = "Auktion:";
$MSG['273'] = "Als *Top-Angebot* einstellen";
$MSG['274'] = "Als *Fett* markieren";
$MSG['275'] = "Los!";
$MSG['276'] = "Kategorien";
$MSG['277'] = "Alle Kategorien";
$MSG['278'] = "Neueste Auktionen";
$MSG['279'] = "H�chste Gebote";
$MSG['280'] = "Kurz vor Ende!";
$MSG['281'] = "Hilfe und FAQ";
$MSG['282'] = "News und Werbung";
$MSG['283'] = "Minimum";
$MSG['284'] = "Menge";
$MSG['285'] = "Zur�ck";
$MSG['286'] = " und geben Sie ein g�ltigen Gebot ab.";
$MSG['287'] = "Kategorie";
$MSG['288'] = "Feld f�r Suchbegriffe kann nicht leer sein";
$MSG['289'] = "Total der Seiten:";
$MSG['290'] = "Total der Artikel:";
$MSG['291'] = "Anzahl Artikel pro Seite";
$MSG['292'] = "Als *Hervorgehoben* markieren";
$MSG['293'] = "BENUTZERNAME";
$MSG['294'] = "NAME";
$MSG['295'] = "LAND";
$MSG['296'] = "E-MAIL";
$MSG['297'] = "AKTION";
$MSG['298'] = "Bearbeiten";
$MSG['299'] = "Benutzer aktivieren";
$MSG['300'] = "Sperren";
$MSG['301'] = "Benutzer in der Datenbank";
$MSG['302'] = "Name";
$MSG['303'] = "E-Mail";
$MSG['304'] = "Benutzer l�schen";
$MSG['305'] = "Benutzer sperren";
$MSG['306'] = "Benutzer reaktivieren";
$MSG['307'] = "Sind Sie sicher, dass Sie diesen Benutzer l�schen wollen?";
$MSG['308'] = "Sind Sie sicher, dass Sie diesen Benutzer sperren wollen?";
$MSG['309'] = "Sind Sie sicher, dass Sie diesen Benutzer reaktivieren wollen?";
$MSG['310'] = "Reaktivieren";
$MSG['311'] = "Auktionen in der Datenbank";
$MSG['312'] = "Titel";
$MSG['313'] = "Benutzer";
$MSG['314'] = "Datum";
$MSG['319'] = "Versand";
$MSG['321'] = "Auktion sperren";
$MSG['322'] = "Auktion reaktivieren";
$MSG['323'] = "Sind Sie sicher, dass Sie diese Auktion sperren wollen?";
$MSG['324'] = "Sind Sie sicher, dass Sie diese Auktion reaktivieren wollen?";
$MSG['325'] = "Auktion l�schen";
$MSG['326'] = "Sind Sie sicher, dass Sie diese Auktion l�schen wollen?";
$MSG['328'] = "Farbe";
$MSG['329'] = "Bild-Ablage";
$MSG['330'] = "Danke f�rs Best�tigen Ihrer Registrierung!<br>Der Registrierungsprozess ist damit abgeschlossen und Sie k�nnen nun an den Aktivit�ten der Website aktiv teilnehmen.<br>";
$MSG['331'] = "Ihre Registrierung wurde von der Datenbank definitiv gel�scht.";
$MSG['332'] = "Betreff";
$MSG['333'] = "Mitteilung";
$MSG['334'] = "Kontakt mit";
$MSG['335'] = "Kontakt von ";
$MSG['336'] = "Betreffend Ihre Auktion: ";
$MSG['337'] = "Ihre Mitteilung wurde gesendet an ";
$MSG['340'] = "Von";
$MSG['341'] = "Alle News anzeigen";
$MSG['342'] = " News";
$MSG['343'] = "News bearbeiten";
$MSG['344'] = "Zeit-Einstellungen";
$MSG['345'] = "Bitte w�hlen Sie die Zeitzone, die f�r die Zeitangaben auf Ihrer gesamten Website g�ltig sein soll";


//time zones
$MSG['TZ_12'] = '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka';
$MSG['TZ_11'] = '(GMT +11:00) Magadan, Solomon Islands, New Caledonia';
$MSG['TZ_10'] = '(GMT +10:00) Eastern Australia, Guam, Vladivostok';
$MSG['TZ_9'] = '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk';
$MSG['TZ_8'] = '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong';
$MSG['TZ_7'] = '(GMT +7:00) Bangkok, Hanoi, Jakarta';
$MSG['TZ_6'] = '(GMT +6:00) Almaty, Dhaka, Colombo';
$MSG['TZ_5'] = '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent';
$MSG['TZ_4'] = '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi';
$MSG['TZ_3'] = '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg';
$MSG['TZ_2'] = '(GMT +2:00) Kaliningrad, South Africa';
$MSG['TZ_1'] = '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris';
$MSG['TZ_-12'] = '(GMT -12:00) Eniwetok, Kwajalein';
$MSG['TZ_-11'] = '(GMT -11:00) Midway Island, Samoa';
$MSG['TZ_-10'] = '(GMT -10:00) Hawaii';
$MSG['TZ_-9'] = '(GMT -9:00) Alaska';
$MSG['TZ_-8'] = '(GMT -8:00) Pacific Time (US &amp; Canada)';
$MSG['TZ_-7'] = '(GMT -7:00) Mountain Time (US &amp; Canada)';
$MSG['TZ_-6'] = '(GMT -6:00) Central Time (US &amp; Canada), Mexico City';
$MSG['TZ_-5'] = '(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima';
$MSG['TZ_-4'] = '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz';
$MSG['TZ_-3'] = '(GMT -3:00) Brazil, Buenos Aires, Georgetown';
$MSG['TZ_-2'] = '(GMT -2:00) Mid-Atlantic';
$MSG['TZ_-1'] = '(GMT -1:00) Azores, Cape Verde Islands';
$MSG['TZ_0'] = '(GMT) Western Europe Time, London, Lisbon, Casablanca';

$MSG['346'] = "Zeitzone";
$MSG['347'] = "Zeit-Einstellungen aktualisiert";
$MSG['348'] = "Einstellungen f�r die Stapelverarbeitung (Batch)";
$MSG['349'] = "Antwort";
$MSG['351'] = "Zur�ck zum Posteingang";
$MSG['352'] = "E-Mail-Format";
$MSG['354'] = "Geschlossene Auktionen";
$MSG['355'] = "Aktivieren der Option *Sofort Kaufen automatisch abschalten*";
$MSG['356'] = "Limit f�r *Sofort Kaufen*-Auktionen eines Benutzers";
$MSG['357'] = "Prozent";
$MSG['358'] = "Die Option *Sofort Kaufen automatisch abschalten* schaltet die M�glichkeit *Sofort Kaufen* selbst�ndig ab, sobald die *Sofort Kaufen*-Auktionen eines Benutzers den eingestellten Wert erreichen. Diese Option dient dazu, Benutzer davon abzuhalten, viele *Sofort Kaufen*-Auktionen im Sinne von Spam einzustellen.";
$MSG['359'] = " Aktualisiert";
$MSG['363'] = "Datumsformat";
$MSG['364'] = "Datum";
$MSG['365'] = "Admin-Benutzer";
$MSG['367'] = "Neuen Admin-Benutzer hinzuf�gen";
$MSG['368'] = "Mehrere Kategorien hinzuf�gen<br><span class=\"smallspan\">Kategorienamen eingeben. Jede Kategorie bitte auf eine eigene, neue Zeile!</span>";
$MSG['371'] = "uAuctions muss periodisch den Code in der Datei <code>batch.php</code> ausf�hren, um abgelaufene Auktionen abzuschliessen und E-Mail-Benachrichtigungen an Verk�ufer und Gewinner einer Auktion zu versenden. Wenn Sie einen Linux-/Unix-Server betreiben ist das empfohlene Vorgehen, den Code in der Datei <code>batch.php</code> mittels eines <b><a href=\"http://www.aota.net/Script_Installation_Tips/cronhelp.php4\" target=\"_blank\">cronjobs</a></b> abarbeiten zu lassen.<br>Wenn Sie auf Ihrem Server keinen cronjob laufen lassen k�nnen, so haben Sie die M�glichkeit, die Option <b>Non-batch</b> zu w�hlen. In diesem Fall wird der Code in der Datei <code>batch.php</code> von uAuctions selbst ausgef�hrt, und zwar jedes Mal dann, wenn jemand Ihre Startseite aufruft.";
$MSG['372'] = "cron ausf�hren";
$MSG['373'] = "Stapelverarbeitung (Batch)";
$MSG['374'] = "Non-batch";
$MSG['375'] = "Gem�ss den Standardeinstellungen in uAuctions l�scht <code>cron.php</code> automatisch alle Auktionen, die �lter als 30 Tage sind.
			<br>Diese Einstellung kann im Feld unten ge�ndert werden.";
$MSG['376'] = "Auktionen l�schen, die �lter sind als";
$MSG['377'] = " Tage";
$MSG['378'] = "Einstellungen f�r die Stapelverarbeitung aktualisiert.";
$MSG['379'] = "W�hlen Sie das Format, in dem Daten auf der Website angezeigt werden sollen.";
$MSG['382'] = "mm/tt/jjjj";
$MSG['383'] = "tt/mm/jjjj";
$MSG['384'] = "Datumsformat aktualisiert.";
$MSG['385'] = "Neuste Bewertungen";
$MSG['386'] = "Letzter Monat";
$MSG['387'] = "Letzte 6 Monate";
$MSG['388'] = "Letzte 12 Monate";
$MSG['389'] = "Bewertungen als Verk�ufer";
$MSG['390'] = "Bewertungen als K�ufer";
$MSG['391'] = "Betrag";
$MSG['392'] = "Geb�hrenart";
$MSG['393'] = "pauschal";
$MSG['394'] = "Hinzuf�gen";
$MSG['395'] = "Geb�hren aktivieren / deaktivieren";
$MSG['396'] = "Einstellungen zu den Geb�hren aktualisiert";
$MSG['397'] = "Wollen Sie Geb�hren erheben (Aktivieren) oder m�chten Sie, dass Ihre Site komplett geb�hrenfrei ist (Deaktivieren)?";
$MSG['398'] = "Newsletter versenden";
$MSG['399'] = "Suchen";
$MSG['400'] = "E-Mail-Adresse";
$MSG['401'] = "Datenschutzrichtlinie";
$MSG['402'] = "Datenschutzrichtlinien Seite";
$MSG['403'] = "Seite mit den Datenschutzrichtlinien aktivieren?";
$MSG['404'] = "Inhalt der Datenschutzrichtlinie<br>(HTML erlaubt)";
$MSG['405'] = "Aktivieren Sie diese Option, wenn Sie einen Link zur Datenschutzrichtlinie in der Fusszeile aller Seiten angezeigt haben wollen.";
$MSG['406'] = "Einstellungen zur Datenschutzrichtlinie aktualisiert";
$MSG['409'] = "Fehler-Handling";
$MSG['410'] = "Bei schwerwiegenden Fehlern w�hrend der Ausf�hrung (in der Regel MySQL-Fehler) werden die Benutzer auf eine spezielle Fehler-Seite umgeleitet.
			Sie k�nnen den Text dieser Seite im unten stehenden Feld individualisieren.";
$MSG['411'] = "Fehler-Text";
$MSG['412'] = "E-Mail-Adresse bei schwerwiegenden Fehlern";
$MSG['413'] = "Einstellungen f�rs Fehler-Handling aktualisiert.";
$MSG['415'] = "Fehler";
$MSG['417'] = "Allgemein";
$MSG['418'] = "Sind Sie sicher, dass Sie diesen Benutzer aktivieren m�chten?";
$MSG['419'] = "Sind Sie sicher, dass Sie diesen Benutzer l�schen m�chten? Alle seine Auktionen und Gebote werden ebenfalls gel�scht und k�nnen nicht wiederhergestellt werden!";
$MSG['420'] = "Der Benutzer ist Verk�ufer in folgenden Auktionen:<br>";
$MSG['421'] = "Der Benutzer hat Gebote in  %s Auktion(en).";
$MSG['422'] = "Ausstehende Zahlungen";
$MSG['423'] = "Zahlung erfolgreich";
$MSG['424'] = "Ihre Zahlung wurde best�tigt.<br><br>Danke f�r Ihren Einkauf.";
$MSG['425'] = "Zahlung fehlgeschlagen";
$MSG['426'] = "Ihre Zahlung wurde nicht best�tigt oder ist ung�ltig.<br> <br> Bitte entschuldigen Sie die Unannehmlichkeiten.";
$MSG['427'] = "Aktivieren der Option *Gebots-Assistent*";
$MSG['428'] = "Option *Gebots-Assistent* aktivieren / deaktivieren";
$MSG['429'] = "Keine Gebote oder der Mindestpreis wurde nicht erreicht";
$MSG['430'] = "Geb�hr f�r Benutzerregistrierung";
$MSG['431'] = "Geb�hr f�r Auktionen";
$MSG['432'] = "Geb�hr f�rs Erstellen von Auktionen";
$MSG['433'] = "Geb�hr f�r das Einstellen als *Top-Angebot* auf der Startseite";
$MSG['434'] = "Geb�hr f�r das Einstellen als *Hervorgehoben*";
$MSG['435'] = "Geb�hr f�rs Hochladen von Bildern";
$MSG['436'] = "Geb�hr f�r die Option *Sofort Kaufen*";
$MSG['437'] = "Geb�hr f�r die Option *Angebot reaktivieren*";
$MSG['439'] = "Geb�hr f�r das Einstellen als *Fett*";
$MSG['440'] = "Geb�hr f�r die Option *Mindestpreis*";
$MSG['441'] = "Bitte Benutzernamen und Passwort erstellen";
$MSG['442'] = "Bitte Benutzernamen und Passwort eingeben";
$MSG['443'] = "%s hat zurzeit einen vollen Posteingang. Bitte versuchen Sie es sp�ter nochmals.";
$MSG['444'] = "Nachricht gel�scht";
$MSG['445'] = "Zahlungs-Gateways";
$MSG['446'] = "Erforderlich?";
$MSG['447'] = "Aktiviert?";
$MSG['448'] = "Benutzergruppen";
$MSG['449'] = "Gruppen-ID";
$MSG['450'] = "Name der Benutzergruppe";
$MSG['451'] = "Anzahl Benutzer";
$MSG['452'] = "Benutzergruppe hinzuf�gen / bearbeiten";
$MSG['453'] = "Details zum Gewinner der Auktion";
$MSG['454'] = "Auktionen, die Sie gewonnen haben";
$MSG['455'] = "Gewinner";
$MSG['456'] = "E-Mail-Adresse des Gewinners der Auktion";
$MSG['457'] = "Gebot des Gewinners der Auktion";
$MSG['458'] = "Auktion: ";

$MSG['460'] = "E-Mail-Adresse des Verk�ufers";
$MSG['461'] = "Ihr Gebot";

$MSG['464'] = "Erweiterte Suche";

$MSG['471'] = "Auktionsbeobachter (Benachrichtigung, sobald Auktionen mit den eingegebenen Stichworten eingestellt werden)";
$MSG['472'] = "Artikel die ich beobachte";

$MSG['496'] = "Sofort Kaufen";
$MSG['497'] = "Sofort-Kaufen-Preis";
$MSG['498'] = "Erfolgreich ersteigerte Artikel<br>";
$MSG['499'] = "Total erhaltene Bewertungen neutral: ";

$MSG['500'] = "Total erhaltene Bewertungen positiv: ";
$MSG['501'] = "Total erhaltene Bewertungen negativ: ";
$MSG['502'] = "Total erhaltene Bewertungen: ";
$MSG['503'] = "Bewertung";
$MSG['504'] = "Kommentar";
$MSG['505'] = "Zur�ck zum Benutzerprofil";
$MSG['506'] = "Bewertung gesandt zu: ";
$MSG['507'] = "Verlauf verstecken";
$MSG['508'] = "Neue Mitteilungen";
$MSG['509'] = "Benutzerdaten";
$MSG['510'] = "Es gibt momentan keine Erinnerungen anzuzeigen.";
$MSG['511'] = "Benutzer bearbeiten";
$MSG['512'] = "Auktion bearbeiten";
$MSG['514'] = "<b>Mindestpreis nicht erreicht</b>";
$MSG['515'] = 'Benutzer aktivieren';
$MSG['516'] = "News-Management";
$MSG['517'] = " News in der Datenbank gefunden.";
$MSG['518'] = "Neu hinzuf�gen";
$MSG['519'] = "Titel";
$MSG['520'] = "Inhalt";
$MSG['521'] = "Aktivieren";
$MSG['522'] = "K�ufer-Geb�hren f�llig";
$MSG['523'] = "Gesamtpreis-Geb�hren f�llig";
$MSG['524'] = "EINSTELLUNGEN";
$MSG['525'] = "Admin-Benutzer-Management";
$MSG['526'] = "Allgemeine Einstellungen";
$MSG['527'] = "Name der Site";
$MSG['528'] = "URL der Site";
$MSG['530'] = "�nderungen speichern";
$MSG['531'] = "Ihr Logo";
$MSG['532'] = "Login-M�glichkeit auf der Startseite anzeigen?";
$MSG['533'] = "News-Box auf der Startseite anzeigen?";
$MSG['534'] = "*Akzeptieren*-Text anzeigen?";
$MSG['535'] = "Der Name der Site erscheint auf allen E-Mail-Nachrichten, die uAuctions an die Benutzer sendet.";
$MSG['536'] = "Das muss die komplette URL (beginnend mit <b>http://</b>) Ihrer uAuctions -Installation sein.<br>Bitte sicherstellen, dass <b>die URL am Ende mit einem Schr�gstrich abgeschlossen ist.</b>";
$MSG['537'] = "W�hlen Sie <b>Ja</b>, wenn die Login-M�glichkeit auf der Startseite angezeigt werden soll. Ansonsten w�hlen Sie <b>Nein</b>";
$MSG['538'] = "W�hlen Sie <b>Ja</b>, wenn die News-Box auf der Startseite angezeigt werden soll. Ansonsten w�hlen Sie <b>Nein</b>";
$MSG['539'] = "Wenn Sie unten die Option <b>Ja</b> w�hlen, so zeigt uAuctions jedem Benutzer vor dem Absenden der Registrierung den Text an, den Sie in die Textbox unten eingeben.<br>
			Dies wird typischerweise verwendet, um rechtliche Hinweise einzublenden, die Benutzer mit dem Absenden Ihrer Registrierung akzeptieren (m�ssen).";
$MSG['540'] = "Admin E-Mail-Adresse";
$MSG['541'] = "Die Admin E-Mail-Adresse wird bei allen automatisch versandten E-Mail-Nachrichten ben�tzt";
$MSG['542'] = "Allgemeine Einstellungen aktualisiert";
$MSG['543'] = "Diese Frage �ffentlich machen";
$MSG['544'] = "W�hrungsformat";
$MSG['545'] = "Amerikanisch: 1,250.00";
$MSG['546'] = "Europ�isch: 1.250,00";
$MSG['547'] = "0 (Null) einsetzen oder leer lassen f�r W�hrungsformat ohne Dezimaltrennzeichen";
$MSG['548'] = "Dezimalstellen";
$MSG['549'] = "Position des W�hrungssymbols";
$MSG['550'] = "Vor dem Betrag (d.h. USD 200)";
$MSG['551'] = "Nach den Betrag (d.h. 200 USD)";
$MSG['552'] = "Fragen";
$MSG['553'] = "W�hrungseinstellungen aktualisiert";
$MSG['554'] = "Anzahl sichtbare News-Eintr�ge";
$MSG['555'] = "Fragesteller";
$MSG['556'] = "Momentanes Logo";
$MSG['557'] = "Artikel-Detail";
$MSG['558'] = "Erstellt";
$MSG['559'] = "Letztes Login";
$MSG['560'] = "Status";
$MSG['561'] = "L�SCHEN";
$MSG['562'] = "Admin-Benutzer bearbeiten";
$MSG['563'] = "Wenn Sie das Benutzerpasswort �ndern m�chten bitte die zwei unten stehenden Felder verwenden. Zum Beibehalten des aktuellen Passworts beide Felder einfach leer lassen.";
$MSG['564'] = "Passwort wiederholen";
$MSG['566'] = "Aktiv";
$MSG['567'] = "Nicht aktiv";
$MSG['568'] = "und %s mehr";
$MSG['569'] = "Benutzer hinzuf�gen";
$MSG['570'] = "Nie eingeloggt";

$MSG['578'] = "Kann verkaufen";
$MSG['579'] = "Kann kaufen";
$MSG['580'] = "Automatisch beitreten";
$MSG['581'] = "Bitte w�hlen Sie aus einem der unten aufgelisteten Zahlungs-Gateways aus und bezahlen Sie dem Verk�ufer den Betrag von <b>%s</b>.";
$MSG['582'] = "Bitte w�hlen Sie aus einem der unten aufgelisteten Zahlungs-Gateways aus und f�gen Sie Ihrem Konto folgenden Betrag hinzu <b>%s</b>.";
$MSG['583'] = "Bitte w�hlen Sie zur Aktivierung Ihres Kontos aus einem der unten aufgelisteten Zahlungs-Gateways aus und bezahlen Sie die Geb�hren im Betrag von <b>%s</b>.";

$MSG['590'] = "Bitte w�hlen Sie zum Abschliessen Ihrer Auktion aus einem der unten aufgelisteten Zahlungs-Gateways aus und bezahlen Sie die Geb�hren im Betrag von <b>%s</b>.";
$MSG['591'] = "Bitte w�hlen Sie zum Abschliessen Ihrer Auktion und f�r die Option *Angebot reaktivieren* aus einem der unten aufgelisteten Zahlungs-Gateways aus und bezahlen Sie die Geb�hren im Betrag von <b>%s</b>.";
$MSG['592'] = "Eingeloggt als:";
$MSG['593'] = "Erinnerungen";
$MSG['594'] = "*Akzeptieren*-Text";

$MSG['597'] = "Banner-Unterst�tzung aktivieren?";

$MSG['600'] = "Banner-Einstellungen aktualisiert";
$MSG['602'] = "Neues Logo hochladen (max. 50 KB)";
$MSG['603'] = "Newsletter abonnieren?";
$MSG['604'] = "Durch Aktivieren dieser Option erhalten die Benutzer bei der Registrierung die M�glichkeit, den Newsletter zu abonnieren.<br>W�hlen Sie *Newsletter erstellen* aus dem Men� in der linken Randspalte um Newsletter zu erstellen und per E-Mail an die Abonnenten zu versenden.<br>";
$MSG['605'] = "Nachrichtentext";
$MSG['606'] = "Durch Klicken auf *�nderungen speichern* wird der Newsletter per E-Mail an alle Abonnenten der gew�hlten Benutzergruppe versandt.";
$MSG['607'] = "Newsletter erstellen";
$MSG['608'] = "M�chten Sie unseren Newsletter erhalten?";
$MSG['609'] = "Zum Abbestellen des Newsletters bitte *Nein* ankreuzen";
$MSG['610'] = "Bitte im Bild unten eine Auswahl treffen, die im Vorschaubild angezeigt werden soll";
$MSG['611'] = "<b>Dieser Artikel wurde</b>";
$MSG['612'] = "<b>mal angesehen</b>";
$MSG['613'] = "Vorschaubild ansehen";
$MSG['613tp'] = "Vorschau Bild";
$MSG['614'] = "Die eingebaute Tabelle mit den proportionalen Erh�hungsschritten verwenden";
$MSG['615'] = "Eigene fixe Erh�hungsschritte verwenden";
$MSG['616'] = "Vorschaubild speichern";
$MSG['617'] = "*Bitte beachten Sie*:  Zum �ndern des Passwortes bitte ein neues Passwort zweimal in die untenstehenden Felder eingeben.<br>Wenn keine �nderung gew�nscht: leer lassen.";
$MSG['618'] = "Abbrechen";
$MSG['619'] = "Laufende Auktionen";
$MSG['620'] = "Ihre Gebote";
$MSG['621'] = "Pers�nliches Profil anpassen";
$MSG['622'] = "Mein Konto";
$MSG['623'] = "Mitteilungen ansehen";
$MSG['624'] = "Titel";
$MSG['625'] = "Startdatum";
$MSG['626'] = "Enddatum";
$MSG['627'] = "Anzahl Gebote";
$MSG['628'] = "H�chstes Gebot";
$MSG['629'] = "*Wenn Sie Abbrechen dr�cken, so wird das gew�hlte / hochgeladene Standard-Bild f�r Ihre Auktion verwendet";
$MSG['630'] = "Angebot reaktivieren";
$MSG['631'] = "Ausgew�hlte Auktionen verarbeiten";
$MSG['631a'] = "Ausgew�hlte verarbeiten";

$MSG['640'] = "*Bitte beachten* Wenn R�ckw�rts-Auktion gew�hlt ist, dann sind einige Optionen nicht verf�gbar.</br>  <b>bedingt durch Zahlungsgateways:</b>  Bei Preisen bitte unbedingt einen Punkt als Dezimaltrennung benutzen (Kommas sind nicht zul�ssig!!) ";
$MSG['641'] = "R�ckw�rts-Auktion";
$MSG['642'] = "Standard-Auktion";

$MSG['645'] = "Frage an den Verk�ufer richten";
$MSG['646'] = "Sie m�ssen eingeloggt sein, um Fragen an den Verk�ufer richten";
$MSG['647'] = "Fragen";
$MSG['648'] = "Auf Fragen antworten";
$MSG['649'] = "Antwort:";
$MSG['650'] = "Frage:";
$MSG['651'] = "Frage zu Ihrer Auktion: %s";

$MSG['662'] = "<h2>Vorschaubild erzeugen</h2>";
$MSG['663'] = "Bildergalerie";
$MSG['664'] = "Wenn Sie diese Option aktivieren, dann k�nnen Verk�ufer Bilder bis zur maximalen Anzahl (siehe unten) hochladen.";
$MSG['665'] = "Bildergalerie aktivieren?";
$MSG['666'] = "Max. Anzahl Bilder";
$MSG['667'] = "Betrachtender Benutzer: ";
$MSG['668'] = "Diese Auktion hat noch nicht begonnen.";

$MSG['671'] = "Max. Gr�sse von Bildern";
$MSG['672'] = "KBytes";
$MSG['673'] = "Es sind bis zu %s Bilder erlaubt. Jedes Bild muss kleiner als %s bytes sein. <br>Ein Standard-Miniaturbild muss erzeugt werden!! (Make default)";
$MSG['674'] = "Sie k�nnen nur %s Bilder hochladen. Bitte entfernen Sie Bilder";
$MSG['675'] = "Es werden Geb�hren in H�he von %s erhoben f�r jedes Bild.";
$MSG['677'] = "Bilder hochladen";
$MSG['677_a'] = "Bilder hochladen";
$MSG['678'] = "Schliessen";
$MSG['679'] = "Bitte folgen Sie den Anweisungen.";
$MSG['680'] = "Bild zum Hochladen aussuchen";
$MSG['681'] = "Bild hochladen";
$MSG['682'] = "Wiederholen Sie bitte Schritt 1 und 2 f�r jedes Bild. Nach Beenden klicken Sie bitte auf den <i>Gallerie erzeugen</i> .<br>Sie m�ssen ein Standard-Miniaturbild erzeugen lassen!! (Schaltfl�che: Make default)";
$MSG['683'] = "&gt;&gt;&gt; Create Gallery &lt;&lt;&lt;";
$MSG['684'] = "Filename";
$MSG['685'] = "Size (bytes)";
$MSG['686'] = "Standard";
$MSG['687'] = "Hochgeladene Dateien";
$MSG['688'] = "Sie haben bereits hochgeladen ";
$MSG['689'] = " Dateien";

$MSG['694'] = "Galerie anzeigen";

$MSG['699'] = "Ihr Gebot von ";

$MSG['700'] = " wurde eingetragen. ";
$MSG['701'] = " Ihr Gebot war nicht hoch genug, um Sie zum H�chstbietenden zu machen!<br>M�chten Sie weiterbieten?";

$MSG['718'] = "Auktionstyp";
$MSG['719'] = "Zahlungsdetails (optional)";
$MSG['720'] = "PayPal E-Mail-Adresse";
$MSG['724'] = "Zus�tzliche Information";
$MSG['725'] = "Optimierung";
$MSG['726'] = "Vorlagen-Cache aktivieren?";
$MSG['727'] = "Das Aktivieren dieser Option erh�ht die Geschwindigkeit Ihrer Website massiv. Es wird empfohlen, die Option nur dann zu deaktivieren, wenn Sie Updates und �nderungen an Ihren Vorlagen-Dateien vornehmen";
$MSG['728'] = "Optimierungs-Einstellungen aktualisiert";
$MSG['729'] = "Zahlungstyp einstellen";
$MSG['730'] = "Hier kann eingestellt werden, wie Benutzer ihre Geb�hren bezahlen. Im Kontostands-Modus k�nnen die Benutzer entscheiden, wann sie ihre Ausst�nde bezahlen, im Sofort-Modus m�ssen Benutzer jede geb�hrenpflichtige Aktion sofort bezahlen";
$MSG['731'] = "Kontostands-Modus";
$MSG['732'] = "Sofort-Modus";
$MSG['733'] = "Die folgenden Einstellungen sind nur im Kontostands-Modus relevant";
$MSG['734'] = "Maximaler Ausstand";
$MSG['735'] = "Maximaler Ausstand, den ein Konto aufweisen darf, bevor eine Zahlung erfolgen muss";
$MSG['736'] = "Registrierungs Geschenk";
$MSG['737'] = "Start-Kredit, der einem neu erstellten Konto gutgeschrieben wird";
$MSG['738'] = "Konto sperren";
$MSG['739'] = "Konto sperren, dessen Ausstands-Limite �berschritten wurde";
$MSG['740'] = "Keiner";
$MSG['741'] = "Bild";
$MSG['742'] = "reCaptcha";
$MSG['743'] = "Captcha-Typ bei der Registrierung";
$MSG['744'] = "Captcha-Type auf der Seite *Auktion an einen Freund senden*";
$MSG['745'] = "Captcha werden verwendet, um Spam zu verhindern. Es wird empfohlen, irgend eine Art Captcha zu verwenden";
$MSG['746'] = "reCaptcha public key";
$MSG['747'] = "reCaptcha private key";
$MSG['748'] = 'Um reCaptcha zu verwenden m�ssen Sie ein Konto bei <b><a href="http://recaptcha.net/" class="new-window">http://recaptcha.net/</a></b> er�ffnen und f�r die Site und die Domain sogenannte Schl�ssel (public key [�ffentlicher Schl�ssel], private key [privater Schl�ssel]) beantragen';
$MSG['749'] = 'Spam Einstellungen';
$MSG['750'] = 'Spam Einstellungen aktualisiert';
$MSG['751'] = 'Sie k�nnen reCaptcha nicht ohne die beiden Schl�ssel verwenden (public key [�ffentlicher Schl�ssel], private key [privater Schl�ssel])';
$MSG['752'] = 'Die eingegebene Zeichenfolge stimmt nicht mit der im Bild gezeigten �berein.';
$MSG['753'] = 'Konto gesperrt';
$MSG['754'] = 'Das Limit f�r die maximalen Ausst�nde auf Ihrem Konto wurde erreicht. Ihr Benutzerkonto ist gesperrt, bis Sie Ihre Ausst�nde vollst�ndig beglichen haben.<br>Klicken Sie bitte auf den folgenden Link, um <b><a href="outstanding.php">zur Seite mit den Zahlungsm�glichkeiten</a></b> zu gelangen.';
$MSG['755'] = 'Bezahlt';
$MSG['756'] = 'Jetzt Bezahlen';
$MSG['757'] = 'Sicherheitscode';
$MSG['758'] = 'Sicherheitscode best�tigen';
$MSG['759'] = 'Aktivieren';
$MSG['760'] = 'Deaktivieren';
$MSG['761'] = 'Einstellungen zu den Geb�hren aktualisiert';
$MSG['762'] = 'Einstellungen zu den Zahlungs-Gateways aktualisiert';
$MSG['763'] = 'Kontostand';
$MSG['764'] = 'Zahlungserinnerung senden';
$MSG['765'] = 'Zahlungserinnerung gesendet';
$MSG['766'] = 'Rechnung';
$MSG['767'] = 'Klicken Sie auf die Schaltfl�che rechts, um zu PayPal zu gelangen';
$MSG['768'] = 'Registrierungsgeb�hr';
$MSG['769'] = 'Bezahlen der Geb�hr f�r Auktionen';
$MSG['770'] = 'Bezahlen der Geb�hr f�r *Angebot reaktivieren*';
$MSG['771'] = 'Standardname';
$MSG['772'] = '�bersetzung';
$MSG['773'] = 'Authorize.net Login ID';
$MSG['774'] = 'Authorize.net Transaction Key (Transaktionsschl�ssel)';
$MSG['775'] = 'K�ufergeb�hr';
$MSG['776'] = "Zum Abschliessen des Verkaufs w�hlen Sie bitte einen der unten aufgef�hrten Zahlungs-Gateways und bezahlen Sie folgende Geb�hr: <b>%s</b>.";
$MSG['777'] = 'Sie haben noch ausstehende Zahlungen f�r die K�ufergeb�hren des folgenden Artikels: %s; Ihr Konto bleibt gesperrt, bis alle Ausst�nde beglichen sind. Zum Bezahlen <b><a href="%s">w�hlen Sie jetzt diesen Link.</a></b>';
$MSG['778'] = 'Wenn Sie mehr �ber Alternativen dazu wissen m�chten (z.B. wenn Sie keine der Zahlungs-Gateways ben�tzen k�nnen <br>oder wenn keine Zahlungs-Gateways aufgef�hrt sind, weil die Artikel nur mittels Barzahlung, Einzahlungsschein, Bankanweisung oder �hnlichem beglichen werden k�nnen), <br>so kontaktieren Sie bitte den Verk�ufer <b><a href="profile.php?user_id=%s">%s</a></b>.';
$MSG['779'] = 'Benutzer-Registrierungs-Felder aktualisiert';
$MSG['780'] = 'Auf der Registrierungsseite anzeigen';
$MSG['781'] = 'Geburtstags-Feld erforderlich?';
$MSG['782'] = 'Adress-Feld erforderlich?';
$MSG['783'] = 'Stadt-Feld erforderlich?';
$MSG['784'] = 'Region-Feld erforderlich?';
$MSG['785'] = 'Land-Feld erforderlich?';
$MSG['786'] = 'Postleitzahl-Feld erforderlich?';
$MSG['787'] = 'Telefon-Feld erforderlich?';
$MSG['788'] = 'Anzeige-Einstellungen';
$MSG['789'] = 'Anzahl Resultate pro Seite';
$MSG['790'] = 'Maximale Anzahl Artikel bis zum Seitenumbruch';
$MSG['791'] = 'Umsatzgeb�hr';
$MSG['792'] = 'Bezahlen Sie bitte f�r %s Artikel';
$MSG['793'] = ' Artikel, auf die Sie bieten erreichen bald das Auktionsende';
$MSG['794'] = 'Sie wurden bei  %s Artikeln �berboten';
$MSG['795'] = 'Anzeige-Einstellungen aktualisiert';
$MSG['796'] = 'Sie haben noch ausstehende Zahlungen f�r die Umsatzgeb�hren des folgenden Artikels: %s; Ihr Konto bleibt gesperrt, bis alle Ausst�nde beglichen sind. Zum Bezahlen <b><a href="%s">w�hlen Sie jetzt diesen Link.</a></b>';
$MSG['797'] = 'Option *Untertitel* aktivieren';
$MSG['798'] = 'Erm�glichst es Verk�ufern, Auktionen einen Untertitel hinzuzuf�gen, der auf allen Auktionslisten angezeigt wird';
$MSG['799'] = 'Option *Weitere Kategorie* aktivieren';

$MSG['800'] = 'Erm�glicht es Verk�ufern, Auktionen mehreren Kategorien zuzuordnen';
$MSG['801'] = 'Gemeinsames SSL (Shared SSL URL)';
$MSG['802'] = 'Wenn Sie Gemeinsames SSL (Shared SSL) verwenden, dann tragen Sie bitte hier die URL ein';
$MSG['803'] = 'Geb�hr f�r die Option *Untertitel*';
$MSG['804'] = 'Geb�hr f�r die Option *Weitere Kategorie*';
$MSG['805'] = 'Diesen Schritt �berspringen';
$MSG['806'] = 'Untertitel';
$MSG['807'] = 'Einstellungen f�r die Startseite';
$MSG['808'] = 'Das ist die Gr�sse der Vorschaubilder, die in den Ergebnislisten angezeigt werden, wenn Benutzer eine Suche durchf�hren';
$MSG['809'] = 'Felder m�ssen angezeigt werden, wenn sie als *erforderliches Feld* definiert wurden';
$MSG['810'] = 'Eine g�ltige PayPal E-Mail-Adresse ist erforderlich';
$MSG['811'] = 'Eine g�ltige AuthNet ID und Passwort ist erforderlich';
$MSG['812'] = 'Dateiname:';
$MSG['813'] = 'Inhalt der Datei:';
$MSG['814'] = 'Weitere Kategorie';
$MSG['815'] = 'Sind Sie sicher, dass Sie diesen Benutzer aktivieren m�chten?';
$MSG['816'] = 'Galerie';
$MSG['817'] = 'Preise';
$MSG['818'] = 'Sie haben keine Berechtigung, Artikel-Listen anzuzeigen';
$MSG['819'] = 'Sie haben keine Berechtigung, Artikel zu kaufen';
$MSG['820'] = 'Beantragen Sie f�r Ihr Benutzerkonto eine Berechtigung zum Verkaufen von Artikeln';
$MSG['821'] = 'Eine g�ltige 2Checkout ID ist erforderlich';
$MSG['822'] = 'Eine g�ltige Skrill (moneybookers) E-Mail-Adresse ist erforderlich';
$MSG['823'] = 'Eine g�ltige Worldpay ID ist erforderlich';
$MSG['824'] = 'Worldpay ID';
$MSG['825'] = 'Skrill (moneybookers) E-Mail-Adresse';
$MSG['826'] = '2Checkout ID';
$MSG['827'] = 'W�chentlicher Bericht';
$MSG['828'] = 'Woche';
$MSG['829'] = 'Anzeige nach ';
$MSG['830'] = 'Monat';
$MSG['831'] = 'Bisher wurden keine IP-Adressen gesperrt.';
$MSG['832'] = 'Sind Sie sicher, dass Sie den folgenden News-Eintrag l�schen m�chten: *%s*';
$MSG['833'] = 'Sind Sie sicher, dass Sie die folgende Auktion l�schen m�chten: *%s*';
$MSG['834'] = 'Sind Sie sicher, dass Sie die folgende Nachricht l�schen m�chten: ID: *%s*';
$MSG['835'] = 'Sind Sie sicher, dass Sie den folgenden Benutzer l�schen m�chten: *%s*';
$MSG['836'] = 'Mitgliedschafts-Arten aktualisiert';
$MSG['837'] = '(enth�lt %s FAQ)';
$MSG['838'] = 'Sind Sie sicher, dass Sie die folgenden Kategorien bearbeiten m�chten: ';
$MSG['839'] = 'Was m�chten Sie mit den FAQ in den folgenden Kategorien tun';
$MSG['840'] = 'Verschieben nach ';
$MSG['841'] = 'Admin Vorlagen';
$MSG['842'] = 'Geb�hren festlegen';
$MSG['843'] = 'Was m�chten Sie mit den Auktionen und Unterkategorien in den folgenden Kategorien tun?<br><small>(Wenn Sie sie verschieben m�chten, dann m�ssen Sie die ID der Kategorie angeben, zu der die verschoben werden sollen)</small>';
$MSG['844'] = 'Einige der zu verschiebenden Kategorien konnten nicht verarbeitet und verschoben werden, da keine g�ltige ID einer Kategorie angegeben wurde, zu der verschoben werden soll';
$MSG['845'] = '<p><img src="' . $system->SETTINGS['siteurl'] . 'themes/admin/images/bullet_blue.png"> Bezeichnet Kategorien, die Unterkategorien enthalten</p><p><img src="' . $system->SETTINGS['siteurl'] . 'themes/admin/images/bullet_red.png"> Bezeichnet Kategorien, die Auktionen enthalten</p>';
$MSG['846'] = 'Kontostand Ihres Benutzerkontos';
$MSG['847'] = 'Preis';
$MSG['848'] = 'Sind Sie sicher, dass Sie die folgende Bewertung l�schen wollen?  (ID: %s)';
$MSG['849'] = 'Option *Angebot reaktivieren* aktivieren';
$MSG['850'] = 'Benutzern erm�glichen, ihre Auktionen automatisch zu reaktivieren, wenn sie ohne Gewinner enden';
$MSG['851'] = 'Maximale Anzahl, die eine Auktion reaktiviert werden kann';
$MSG['852'] = 'Festlegen der maximalen Anzahl, die eine Auktion reaktiviert werden kann';
$MSG['853'] = 'Sie beobachten momentan keine Angebote';
$MSG['854'] = 'Konti';
$MSG['855'] = 'Ansicht w�hlen';
$MSG['856'] = 'Zeitspanne w�hlen';
$MSG['857'] = 'Total in Rechnung gestellt';
$MSG['858'] = 'Ich habe gelesen und best�tige die: &nbsp;<a href="' . $system->SETTINGS['siteurl'] . 'contents.php?show=terms">Nutzungsbedingungen (AGB)</a>';
$MSG['858_a'] = 'Ich habe gelesen und best�tige die: <a href="' . $system->SETTINGS['siteurl'] . 'contents.php?show=cookies" data-fancybox-type="iframe" class="infoboxs">Cookies Policy</a>';
$MSG['859'] = 'Vielen Dank %s f�r die Registrierung.';
$MSG['860'] = '<p>In der Zwischenzeit kannst Du dir eineige Tipps ansehen ' . $system->SETTINGS['sitename'] . '.</p>
<li>Halte die gew�nschten Fotos bereit wenn Du Artikel einstellst, denke an die Gr��enanpassung.</li>
  <li>FTop Auktionen sind viel eher zu verkaufen, wird Ihre Auktion zuf�llig auch hier zu sehen sein: ' . $system->SETTINGS['sitename'] . ' Webseite.</li>
</ul>';
$MSG['861'] = 'Auktionen suchen';
$MSG['862'] = 'Bitte einloggen';
$MSG['863'] = '<h1>Noch kein Mitglied?</h1>       
        Es dauert nur eine Minute</h3>
        <p> Als Mitglied kannst Du:</p>
        <ul>
            <li>Alles m�gliche verkaufen</li>
            <li>Du wirst benachrichtigt, wenn ein Artikel auf Deiner Beobachtungsliste sich ver�ndert</li>
            <li>Artikel beobachten</li>
            <li>Auf Artikel bieten</li>
            <li>und eine ganze Menge mehr</li>
        </ul>
        <a style="color:white" class="btn btn-primary" href="'.$system->SETTINGS['siteurl'].'register.php">Jetzt registrieren</a>';
$MSG['864'] = 'Versandbedingungen';
$MSG['865'] = 'Listen Details';
$MSG['867'] = 'Nur aufnehmen';
$MSG['868'] = ' jeder';
$MSG['869'] = 'Verkaufsdatum';

$MSG['888'] = 'Fehler-Log ist momentan leer';
$MSG['889'] = "Fehler-Log geleert";
$MSG['890'] = "Fehler-Log leeren";
$MSG['891'] = "Fehler-Log";
$MSG['892'] = "Zu aktivieren";
$MSG['893'] = "Total";
$MSG['894'] = "Benutzereinstellungen";
$MSG['895'] = "Benutzereinstellungen aktualisiert";
$MSG['896'] = "Das ist die Gr�sse der Vorschaubilder, die in den Auktionslisten angezeigt werden";
$MSG['897'] = "Zus�tzliche Optionen f�r Auktionen";
$MSG['898'] = "Bezahlt";
$MSG['899'] = "Als bezahlt markieren";
$MSG['900'] = "Gewinner anzeigen";
$MSG['901'] = "Anzahl Artikel";
$MSG['902'] = "HTML";
$MSG['903'] = "In welchem Format wollen Sie Ihre E-Mails erhalten?";
$MSG['904'] = "Diese Auktion ist geschlossen";
$MSG['905'] = "Jemand l�dt Sie ein, sich eine interessante Auktion anzusehen";
$MSG['906'] = "Ihr Gebot ist nicht mehr das H�chstgebot";
$MSG['907'] = " - Ihr Artikel wurde verkauft!";
$MSG['908'] = " - Kein Gewinner";
$MSG['909'] = " Auktion geschlossen - Sie haben diesen Artikel ersteigert";
$MSG['910'] = "Dieser Benutzer hat keine Auktionen.";
$MSG['911'] = "Geschlossen";
$MSG['912'] = "Hilfe-Management";
$MSG['913'] = "Themen in der Datenbank gefunden";
$MSG['914'] = "Thema";
$MSG['915'] = "Text";
$MSG['916'] = "Hilfethemen-Management";
$MSG['917'] = "Hilfethema hinzuf�gen";
$MSG['918'] = "Andere Hilfethemen:";
$MSG['919'] = "Allgemeine Hilfe";
$MSG['920'] = "Option *Sofort Kaufen* aktivieren?";
$MSG['921'] = "Wenn Sie diese Option aktivieren, dann k�nnen Benutzer den Artikel Ihrer Auktion sofort kaufen, sofern noch keine Gebote abgegeben wurden. Diese Option muss durch den Verk�ufer beim Er�ffnen der Auktion aktiviert werden.";
$MSG['922'] = "<b>Frage an den Verk�ufer senden</b>";
$MSG['923'] = "Standort des Verk�ufers";
$MSG['924'] = "Neue Angebote";
$MSG['925'] = "Kurz vor Ende";
$MSG['926'] = "*Big Ticket*";
$MSG['927'] = "Sehr teuer";
$MSG['928'] = "G�nstige Artikel";
$MSG['929'] = "Popul�re Artikel";
$MSG['930'] = "Gefragte Artikel";
$MSG['931'] = "Sofort Kaufen";
$MSG['932'] = "Auktionen des Benutzers %s";
$MSG['933'] = "Nur *Sofort Kaufen*";
$MSG['934'] = "F�r Benutzer: ";
$MSG['935'] = 'Zahlung der Ausst�nde';
$MSG['936'] = 'Es ist ein Fehler aufgetreten. Bitte erneut versuchen';
$MSG['937'] = "Name fehlt";
$MSG['938'] = "Username fehlt";
$MSG['939'] = "Passwort fehlt";
$MSG['940'] = "Passwort bitte doppelt eingeben";
$MSG['941'] = "E-mail fehlt";
$MSG['942'] = "Addresse fehlt";
$MSG['943'] = "tadt fehlt";
$MSG['944'] = "Landkreis fehlt";
$MSG['945'] = "Land fehlt";
$MSG['946'] = "PLZ fehlt";
$MSG['947'] = "Telefonnummer fehlt";
$MSG['948'] = "Geburtsdatum fehlt oder falsche Eingabe";
$MSG['949'] = "Schliesst am ";
$MSG['950'] = "%s bids";
$MSG['951'] = "Kein Sofortkauf<br>f�r diesen Artikel";

$MSG['1000'] = "Nach Stichworten oder Artikelnummer suchen";
$MSG['1001'] = "Titel <b>und</b> Beschreibung durchsuchen";
$MSG['1002'] = "In folgenden Kategorien suchen";
$MSG['1003'] = "Preisspanne";
$MSG['1004'] = "Zwischen";
$MSG['1005'] = " und ";
$MSG['1006'] = "Zahlungsoptionen";
$MSG['1006T'] = "�berweisung, <br>Barzahlung bei Abholung<br>oder:<br>";
$MSG['1008'] = "mit Standort in";
$MSG['1009'] = "Endend...";
$MSG['1010'] = "Heute";
$MSG['1011'] = "Morgen";
$MSG['1012'] = "in 3 Tagen";
$MSG['1013'] = "in 5 Tagen";
$MSG['1014'] = "Sortieren nach";
$MSG['1015'] = "Kurz vor Ende";
$MSG['1016'] = "Neue Angebote";
$MSG['1017'] = "Geringster Preis";
$MSG['1018'] = "H�chster Preis";
$MSG['1020'] = "R�ckw�rtsauktionen";
$MSG['1021'] = "normale Auktionen";
$MSG['1022'] = "SSL-Unterst�tzung";
$MSG['1023'] = "SSL-Unterst�tzung aktivieren?";
$MSG['1024'] = "<p>Sie k�nnen Ihren Kunden eine sicherere Umgebung anbieten, sofern Ihr Server SSL-Unterst�tzung anbietet.</p>Sobald die SSL-Unterst�tzung aktiviert wurde, werden alle Verbindungen eingeloggter oder registrierter Benutzer in einer gesch�tzten HTTPS-Umgebung stattfinden.";
$MSG['1025'] = "SSL-Einstellungen aktualisiert";
$MSG['1028'] = "L�nder aktualisiert";
$MSG['1029'] = "Z�hler aktualisiert";
$MSG['1030'] = "Benutzer-, Auktions- und Gebote-Z�hler neu synchronisieren";
$MSG['1031'] = "Z�hler neu synchronisieren";
$MSG['1032'] = "Kein Gewinner";

// Invoices & tax system
$MSG['1033'] = 'Lieferschein';
$MSG['1034'] = 'Auktion ID';
$MSG['1035'] = 'Bestellung/Rechnung';
$MSG['1036'] = 'Bestellungs Datum';
$MSG['1037'] = 'K�ufer';
$MSG['1038'] = 'Versand nach';
$MSG['1039'] = 'Rechnungs Information';
$MSG['1040'] = 'Bestellungs Details';
$MSG['1041'] = 'Bestellungs Nr.';
$MSG['1042'] = 'Bestellung ID:';
$MSG['1043'] = 'Bestellungs Datum:';
$MSG['1044'] = 'Produkt';
$MSG['1045'] = 'Geb�hr';
$MSG['1046'] = 'VK-Preis (exkl.)';
$MSG['1047'] = 'VK-Preis (inkl.)';
$MSG['1048'] = 'Total (exkl.)';
$MSG['1049'] = 'Total (inkl.)';
$MSG['1050'] = 'Zwischensumme:';
$MSG['1051'] = 'DHL Versand:';
$MSG['1052'] = 'MwSt 19%:';
$MSG['1053'] = 'Total:';
$MSG['1054'] = 'Versand Methode';
$MSG['1055'] = 'Zahlungs Methode';
$MSG['1056'] = 'Keine Rechnung zum Lieferschein ausdrucken gew�hlt.';
$MSG['1057'] = 'Rechnungen Liste';
$MSG['1058'] = 'Rechnung zeigen';
$MSG['1059'] = 'Rechnung auflisten';
$MSG['1060'] = 'Ung�ltige Rechnung.';

// admin general
$MSG['1061'] = 'Notizen';

// admin help page
$MSG['1062'] = 'Support';
$MSG['1063'] = 'Support Foren';
$MSG['1064'] = 'Wenn du irgendwelche Fragen hast finde Hilfe in unseren Foren.';
$MSG['1065'] = 'Online Dokumentation';
$MSG['1066'] = 'Dokumentation dar�ber wie alles funktioniert und wo sich was befindet';
$MSG['1067'] = 'H�ufig gestellte Fragen (FAQ) und deren L�sungen';
$MSG['1068'] = 'uAuctions modifizieren';
$MSG['1069'] = 'Download Themes';
$MSG['1070'] = 'Gro�e Auswahl an Beutzer erstellten Themes';
$MSG['1071'] = 'Download Mods';
$MSG['1072'] = 'Gro�e Auswahl an Benutzer erstellten Modifikationen';
$MSG['1073'] = 'Download Sprach Packet';
$MSG['1074'] = 'Gro�e Auswahl an Sprach Paketen f�r verschiedene Versionen';
$MSG['1075'] = 'WeBid Unterst�tzen';
$MSG['1076'] = 'Fehler Mitteilen';
$MSG['1077'] = 'Eine Ungereimtheit gefunden? Teile es mit, um auch anderen zu helfen';
$MSG['1078'] = 'Eine Funktion vorschlagen';
$MSG['1079'] = 'Ideen mitteilen die in WeBid intergriert werden k�nnten';
$MSG['1080'] = 'Spende';
$MSG['1081'] = 'den uAuction Progammierer mit einer Spende unterst�tzen';

// tax admin
$MSG['1082'] = 'Geb�hren Name';
$MSG['1083'] = 'Geb�hren Raten';
$MSG['1084'] = 'Verk�ufer von';
$MSG['1085'] = 'K�ufer von';
$MSG['1086'] = 'Seiten Geb�hr';
$MSG['1087'] = 'Wollen sie diese Geb�hren Rate wirklich l�schen?';
$MSG['1088'] = 'Geb�hren Einstellungen';
$MSG['1089'] = 'Geb�hren Einstellung Aktualisierung';
$MSG['1090'] = 'Geb�hren Aktiv';
$MSG['1091'] = 'Globale Einstellung zum aktivieren oder deaktivieren von Geb�hren';
$MSG['1092'] = 'Benutzer k�nnen Geb�hren �ndern';
$MSG['1093'] = 'Benutzer Geb�hren �nderung erm�glichen';

// admin invoice settings
$MSG['1094'] = 'Rechnungs Einstellungen';
$MSG['1095'] = 'Rechnungs Einstellungen aktualisiert';
$MSG['1096'] = 'Rechnungs Hinweise';
$MSG['1097'] = 'Dies zeigt in einer gelben Box die End Mitteilung zu Benutzer Rechnungen an';
$MSG['1098'] = 'Rechnungs End Mitteilung';
$MSG['1099'] = 'Anzeigen von End Mitteilungen aller Benutzer Rechnungen';

// list admin users
$MSG['1100'] = 'You cannot delete the account you are currently logged in from';
$MSG['1101'] = 'Admin accounts deleted';

// sell.php tax
$MSG['1102'] = 'Charge Tax';
$MSG['1103'] = 'Steuern im endg�ltigen Verkaufspreis';
$MSG['1104'] = 'Add to balance';
$MSG['1105'] = 'Markiere die Bilder, die Du l�schen m�chtest <strong>(Dieses kann ncht r�ckg�ngig gemacht werden</strong>';

$MSG['3232'] = "Lightbox Einstellungen";

//Seiteneinstellungen
$MSG['5003'] = "Globale Einstellungen";
$MSG['5004'] = "W�hrungs-Einstellungen";
$MSG['5005'] = "Layout-Einstellungen";
$MSG['5006'] = "Einstellungen zur Bildergalerie aktualisiert";
$MSG['5008'] = "Generell verwendete W�hrung";
$MSG['5010'] = "W�hrungsumrechner";
$MSG['5011'] = "*Top-Angebot* Artikel auf der Startseite";
$MSG['5012'] = "So viele *Top-Angebote* werden auf der Startseite angezeigt(Bitte beachten: Nur <b>*Top-Angebote*</b> werden angezeigt).<br>0 (Null) ist erlaubt.";
$MSG['5013'] = "Neueste Auktionen";
$MSG['5014'] = "So viele *Neueste Auktionen* werden auf der Startseite angezeigt.<br>0 (Null) ist erlaubt.";
$MSG['5015'] = "H�chste Gebote";
$MSG['5016'] = "So viele *H�chste Gebote* werden auf der Startseite angezeigt.<br>0 (Null) ist erlaubt.";
$MSG['5017'] = "Kurz vor Ende";
$MSG['5018'] = "So viele *Kurz vor Ende* werden auf der Startseite angezeigt.<br>0 (Null) ist erlaubt.";
$MSG['5019'] = "Layout-Einstellungen aktualisiert";
$MSG['5022'] = "SUCHE NACH BENUTZERN";
$MSG['5023'] = "Suche &gt;";
$MSG['5024'] = "Name, Benutzername oder E-Mail-Adresse";
$MSG['5025'] = "Konto";
$MSG['5028'] = "Aktion";
$MSG['5029'] = "Suche starten!";
$MSG['5030'] = "Pinnwand";
$MSG['5031'] = "Neue Pinnwand";
$MSG['5032'] = "Pinnwand-Management";
$MSG['5033'] = "VERF�GBARE Pinnw�nde";
$MSG['5034'] = "Titel der Pinnwand";
$MSG['5035'] = "Anzahl Mitteilungen";
$MSG['5036'] = "Anzahl der anzuzeigenden Mitteilungen in dieser Pinnwand (aktuellste Mitteilungen zuerst).";
$MSG['5038'] = "Aktiv";
$MSG['5039'] = "Nicht aktiv";
$MSG['5040'] = "BITTE BEACHTEN: Das L�schen einer Pinnwand l�scht auch alle dazu geh�renden Mitteilungen!";
$MSG['5043'] = "Anzahl Mitteilungen";
$MSG['5044'] = "Ausgew�hlte Pinnwand wurde entfernt";
$MSG['5046'] = "ANZEIGEN";
$MSG['5047'] = "Einstellungen zu Pinnw�nden";
$MSG['5048'] = "Option *Pinnwand* aktivieren?";

$MSG['5051'] = "Einstellungen zu Pinnw�nden aktualisiert";
$MSG['5052'] = "Pinnwand bearbeiten";
$MSG['5053'] = "Neueste Mitteilung";

$MSG['5056'] = "Sie sind nicht eingeloggt.<br>Wenn Sie eine Mitteilung anbringen, so erscheint sie als angebracht von <b><i>Unbekannter Benutzer</i></b>.";
$MSG['5057'] = "Mitteilung anbringen";
$MSG['5058'] = "Zur�ck zu den Pinnw�nden";
$MSG['5059'] = "Mitteilungen";
$MSG['5060'] = "Angebracht von ";
$MSG['5061'] = "Unbekannter Benutzer";
$MSG['5062'] = "Alle Mitteilungen anzeigen";
$MSG['5063'] = "Mitteilungen anzeigen";
$MSG['5064'] = "Zur�ck zur Pinnwand";
$MSG['5065'] = "Alle Mitteilungen l�schen, die �lter sind als";
$MSG['5067'] = "Z�hler aktualisieren ";
$MSG['5068'] = "Wort-Filter";
$MSG['5069'] = "Der Wort-Filter erm�glicht es, unerw�nschte Worte zu eliminieren von:
<ul>
<li>TITEL und BESCHREIBUNG von Auktionen.</li>
<li>an Pinnw�nden angebrachte Mitteilungen</li>
</ul>";
$MSG['5070'] = "Wort-Filter aktivieren?";
$MSG['5071'] = "Liste unerw�nschter Worte";
$MSG['5072'] = "Geben Sie unerw�nschte Worte ein, eines auf jeder Linie (max. 255 Zeichen pro Linie). Beachten Sie, dass jede Linie gesamthaft als *ein Wort* betrachtet wird.";
$MSG['5073'] = "Einstellungen f�r den Wort-Filter aktualisiert";
$MSG['5074'] = "*�ber uns*-Seite";
$MSG['5075'] = "Nutzungsbedingungen Seite";
$MSG['5076'] = "Aktivieren Sie diese Option, wenn in der Fusszeile jeder Seite ein Link zur Seite <u>�ber uns</u> angezeigt werden soll.";
$MSG['5077'] = "*�ber uns*-Seite aktivieren?";
$MSG['5078'] = "Inhalt der *�ber uns*-Seite";
$MSG['5079'] = "Einstellungen zu *�ber uns* aktualisiert";
$MSG['5080'] = "Bitte beachten Sie: jede Absatzmarke (Zeilenvorschub) wird in einen <b>&lt;br&gt;</b> HTML-Tag umgewandelt.";
$MSG['5081'] = "Aktivieren Sie diese Option, wenn in der Fusszeile jeder Seite ein Link zur Seite <u>Nutzungsbedingungen</u> angezeigt werden soll.";
$MSG['5082'] = "Nutzungsbedingungen-Seite aktivieren?";
$MSG['5083'] = "Inhalt der Nutzungsbedingungen-Seite<br>(HTML erlaubt)";
$MSG['5084'] = "Einstellungen zu den Nutzungsbedingungen aktualisiert";
$MSG['5085'] = "Impressum";
$MSG['5085a'] = "Kontakt";
$MSG['5086'] = "Nutzungsbedingungen";
$MSG['5087'] = "Einstellungen zu den Auktionen";
$MSG['5088'] = "Einstellungen zu den Auktionen aktualisiert";
$MSG['5089'] = "Benutzer k�nnen ein individuelles Startdatum f�r Auktionen festlegen";
$MSG['5090'] = "Individuelle Startdaten erlauben?";
$MSG['5091'] = "Count-down-Z�hler mit Stunden bis zum Auktionsende";
$MSG['5092'] = "Auktions-Suche";
$MSG['5093'] = "Titel, Beschreibung";
$MSG['5094'] = "Auktionen anzeigen";
$MSG['5095'] = "Anzahl Stunden, die eine Auktion noch laufen muss, bevor die verbleibende Zeit automatisch als Count-down-Z�hler angezeigt wird";

$MSG['5113'] = "�ndern";

$MSG['5115'] = "Tage";

$MSG['5117'] = "Seite";
$MSG['5118'] = "von";
$MSG['5119'] = "&lt;Zur�ck";
$MSG['5120'] = "Vor&gt;";

$MSG['5138'] = "Bitte beachten Sie: Sie k�nnen die W�hrung Ihrer Wahl f�r die gesamte Site verwenden.<br>
Alle Betr�ge, die Benutzer �ber PayPal bezahlen m�chten werden automatisch zum Tageskurs in US-Dollar (USD) umgerechnet, bevor Sie zum PayPal-Server gesandt werden.";

$MSG['5140'] = "Benutzerkonto-Einstellungen";
$MSG['5141'] = "Zugriffsstatistik";
$MSG['5142'] = "Einstellungen";
$MSG['5142a'] = "Passwort �ndern";
$MSG['5143'] = "Zugriffsstatistik ansehen";
$MSG['5144'] = "W�hlen Sie bitte aus den unten stehenden Optionen, wenn uAuctions Zugriffsstatistiken f�r Ihre Site generieren soll.";
$MSG['5145'] = "Benutzer-Zugriffsstatistik generieren";
$MSG['5146'] = "Browser- und Plattformen-Statistik generieren";
$MSG['5148'] = "Einstellungen zur Zugriffsstatistik aktualisiert.";
$MSG['5149'] = "Zugriffsstatistik aktivieren?";
$MSG['5150'] = "Bitte w�hlen Sie die zu generierenden Statistiken";

$MSG['5155'] = "Browser";
$MSG['5156'] = "Plattformen";
$MSG['5157'] = "Domains";
$MSG['5158'] = "Zugriffsstatistik f�r ";
$MSG['5159'] = "Tag";
$MSG['5160'] = "Verlauf anzeigen";
$MSG['5161'] = "Seitenaufrufe";
$MSG['5162'] = "Eindeutige Besucher";
$MSG['5163'] = "Benutzer-Sessions";
$MSG['5164'] = "Gesamt";
$MSG['5165'] = "Browser-Statistik anzeigen";
$MSG['5167'] = "Browser-Statistik f�r ";
$MSG['5169'] = "Browser";
$MSG['5170'] = "Domain";

$MSG['5180'] = "Benutzer";
$MSG['5181'] = "Hinzuf�gen &gt;";
$MSG['5182'] = "Benutzer suchen (Benutzername, Name oder E-Mail-Adresse)";
$MSG['5183'] = "Benutzer gefunden";
$MSG['5184'] = "W�HLEN";
$MSG['5185'] = "Benutzername";
$MSG['5187'] = "Inhalt folgender Liste bearbeiten: ";
$MSG['5188'] = "Gew�hlten Benutzer l�schen";
$MSG['5189'] = "zur Vorschau";
$MSG['5190'] = "Felder zur�cksetzen";
$MSG['5199'] = "Gebot best�tigen";


$MSG['5200'] = "Frage stellen";
$MSG['5201'] = "Nachricht senden";
$MSG['5202'] = "Zur Beobachtungsliste hinzuf�gen";
$MSG['5202_0'] = "Von der Beobachtungsliste entfernen";

$MSG['5204'] = "Einf�gen";
$MSG['5205'] = "Aktivieren/Deaktivieren";

$MSG['5220'] = "Max. 255 Zeichen";
$MSG['5221'] = "Hintergrund f�r Artikel mit der Option *Hervorgehoben*";
$MSG['5222'] = "<b>Bitte beachten Sie: Die *Top-Angebote* auf der Startseite werden in zwei Reihen angezeigt, daher sollte diese Zahl eine gerade Zahl sein.</b>";
$MSG['5223'] = "Breite der Vorschaubilder (f�r optimale Ergebnisse w�hlen Sie 195 Pixel oder weniger)";
$MSG['5224'] = "Pixel";
$MSG['5225'] = "*Top-Angebote* auf der Startseite";
$MSG['5227'] = "Gesperrte Auktionen anzeigen";
$MSG['5228'] = "Logo auf der Startseite anzeigen?";

$MSG['5230'] = "Kategorien f�r FAQ";
$MSG['5231'] = "Neue FAQ";
$MSG['5232'] = "FAQ verwalten";
$MSG['5233'] = "Weitere Einstellungen";
$MSG['5234'] = "Neue Kategorie hinzuf�gen";
$MSG['5235'] = "<b>Bitte beachten Sie</b>: Nur Kategorien ohne FAQ k�nnen gel�scht werden.";
$MSG['5236'] = "FAQ";
$MSG['5237'] = "KATEGORIEN-ID";
$MSG['5238'] = "FAQ-Kategorie";
$MSG['5239'] = "Frage";
$MSG['5240'] = "Antwort<br>(HTML erlaubt)";
$MSG['5241'] = "FAQ bearbeiten";
$MSG['5243'] = "Hilfe Index";
$MSG['5244'] = "AUKTIONEN-VERWALTUNG";
$MSG['5245'] = "Nach Oben";

$MSG['5276'] = "Nachricht l�schen";
$MSG['5277'] = "Zur�ck zur Nachrichtenliste";
$MSG['5278'] = "Nachricht bearbeiten";
$MSG['5279'] = "Zur�ck zur Benutzerliste";
$MSG['5280'] = "Jahre/Monate";
$MSG['5281'] = "Monatlicher Bericht";
$MSG['5282'] = "Aktuellen Monat anzeigen";
$MSG['5283'] = "FAQ-Kategorien bearbeiten";
$MSG['5284'] = "Name der Kategorie";
$MSG['5285'] = "T�glicher Bericht";

$MSG['5291'] = "Aktive Benutzer";
$MSG['5292'] = "Benutzerkonto nie best�tigt";
$MSG['5293'] = "Registrierungsgeb�hr nicht bezahlt";
$MSG['5294'] = "Vom Administrator gesperrt";
$MSG['5295'] = "Anzeigen";
$MSG['5296'] = "Alle Benutzer";
$MSG['5297'] = "Ausst�nde �ber der Kredit-Limite";
$MSG['5299'] = "Eingabe begrenzen auf";

$MSG['5300'] = " Nachrichten gesandt.";

$MSG['5318'] = "Plattformen-Statistik anzeigen";

$MSG['5322'] = "Standard-Land";
$MSG['5321'] = "Sie k�nnen ein Land als Standard f�r die gesamte Site definieren.<br>Dieses Land wird bei jeder L�nderauswahl-M�glichkeit automatisch als Standard-Vorgabe eingesetzt.";
$MSG['5323'] = "Standard-Land aktualisiert";

$MSG['5408'] = "Max. ";

$MSG['5431'] = "Neues Passwort zusenden";

$MSG['5436'] = "Werkzeuge";

$MSG['5438'] = "Plattformen-Statistik f�r ";

$MSG['5492'] = "Artikel";
$MSG['5493'] = "Gebot";
$MSG['5494'] = "Gebot ";
$MSG['5495'] = "f�r jede(n) ";

$MSG['5506'] = "Positive Bewertungen: ";
$MSG['5507'] = '<span style="color:#CD0000;">Negative Bewertungen:</span> ';
$MSG['5508'] = "Mitglied seit ";
$MSG['5509'] = "Anzahl Bewertungen ";

// Tips and Infos
$MSG['5555Titel'] = "Kurzinfos";
$MSG['5555'] = "Willkommen im Administrations-Bereich. Sie k�nnen die meisten Einstellungen von hier aus vornehmen wie Website-Konfiguration, Zahlungen, Auktionen, Nachrichten, Banner, Geb�hren Einstellungen, Kategorien, Sprachen und etliches weitere verwalten.";
$MSG['5555E'] = "<p>Beachten Sie sorgf�ltig jedes einzelne Feld und verwalten Sie Ihre Webseiten-Einstellungen. Alle Links sind selbsterkl�rend. Sie k�nnen die wichtigsten Einstellungen, wie Website-URL, Copyright Fu�zeile Info, Kontakt per E-Mail, Webseiten Anzeigeeinstellungen so wie die Anzahl der anzuzeigenden Elemente, Thumbnail-Gr��e �ndern, usw. bearbeiten</p><p>Sie k�nnen auch Ketegorien bearbeiten, hinzuf�gen oder l�schen, , einschlie�lich �bersetzungen, wenn Sie eine mehrsprachige Webseite verwenden. Andere Optionen wie Standard-Landes-, W�hrungs-Einstellungen und so weiter ..</p>";
$MSG['5555G'] = "Hier verwalten Sie Geb�hren und Zahlungs Einstellungen. Sie k�nnen Geb�hren aktivieren oder deaktivieren f�r kostenlose Auktionen. Zahlungen von Kunden �ber Paypal, Authorize.net, WorldPay, Moneybookers und 2checkout akzeptieren und etliche weitere Einstellungen vornehmen...";
$MSG['5555T'] = "Hier k�nnen Sie Theme-Dateien bearbeiten, w�hlen Sie das gew�nschte Theme. Sie k�nnen Theme-Ordner kopieren und umbenennen. Bearbeiten Sie die tpl-Dateien um ein neues Theme zu erstellen. Wenn Sie ein spezifisches Standard Theme haben m�chten - kontaktieren Sie uns unter zeescripts.com und teilen uns ihre Design Details mit um ein Angebot zu erhalten.";
$MSG['5555W'] = "<p>Das vorliegende Theme ist konfiguriert um Banner im gew�nschten Bereich anzeigen zu k�nnen - optimale Breite des Banners sind 554px. Wenn das Banner an anderen Positionen angezeigt werden soll, k�nnen Sie aus der home.tpl den entsprechenden Bereich ausschneiden und wo immer sie m�chten einf�gen, z.B. in Kopf- oder Fu�zeile. Wenn Sie einen zentralen Banner-Server mit mehreren Zonen unterschiedlicher Gr��e verwenden m�chten, ben�tigen Sie eventuell eine erweiterte L�sung wie Php Adserver-Script (Link siehe unten).</p><p>
Klicken Sie auf die Banner die Sie verwalten oder l�schen m�chten. 
W�hlen Sie alle Kategorien oder einzelne bzw. mehrere Kategorien durch Halten der STRG-Taste aus.</p>";
$MSG['5555B'] = "Bearbeiten, L�schen , deaktivieren oder aktivieren von Benutzerkonten. Verwalten von Newsletter, verbieten von Hacker IP-Adressen etc. Sie k�nnen auch Benutzergruppen mit bestimmten Privilegien wie Eink�ufer, Verk�ufer usw. versehen. Sie k�nnen auch neue Administratoren anlegen die Ihre Webseite verwalten k�nnen.";
$MSG['5555A'] = "Hier k�nnen sie Auktionen verwalten. Sie haben hier die Berechtigung alles an den laufenden oder auch beendeten Auktionen zu bearbeiten. So k�nnen z.B. zu viele externe Verlinkungen aus der Beschreibung entfernt werden.";
$MSG['5555I'] = "In diesem Abschnitt k�nnen statische Seiten erstellt, bearbeitet oder gel�scht werden. �ber uns inklusive Kontaktdaten oder Code f�r ein Kontaktformular, AGB's, Datenschutz usw., so auch FAQs  bzw. Hilfeseiten erstellen oder Foren f�r Diskussionen.";
$MSG['5555S'] = "Anzeige verschiedener Statitiken �ber Server Zugriffe usw. �ber 'Einstellungen' k�nnen sie auch w�hlen ob Statistiken �berhaupt aktiviert sein sollen und wenn ja welche.";
$MSG['5555W2'] = "Sie k�nnen Ihre Website in den Wartungsmodus oder Benutzer Wortfilter setzen um unerw�nschte Worte zu unterdr�cken. Au�erdem k�nnen Sie das Fehlerprotokoll einsehen und leeren.";
// Ende Tipps und Infos


$MSG['_0001'] = "*Ausser Betrieb* Seite";
$MSG['_0002'] = "Sie k�nnen, falls n�tig oder gew�nscht, den Zugang zu Ihrer Site vor�bergehend sperren.<br>
			<b>Bitte beachten Sie: im Modus *Vor�bergehend ausser Betrieb* hat <i>nur ein einziger Benutzer</i>&nbsp;&nbsp;Zugang zur Site.</b> Nachdem Sie dazu einen neuen Benutzer <b><a target=\"_new\" href=\"../register.php\">�ber die �bliche Registrierungsseite</a></b> erfasst haben
			, tragen Sie diesen Benutzernamen ins Feld unten ein. Nach dem Umschalten in den Modus *Vor�bergehend ausser Betrieb* k�nnen Sie sich <b><a target=\"_new\" href=\"../user_login.php\">hier einloggen</a></b> um Zugriff auf die Site zu erhalten.";
			
$MSG['_0004'] = "HTML-Code f�r die Seite *Vor�bergehend ausser Betrieb*";
$MSG['_0005'] = "Einstellungen f�r den Modus *Vor�bergehend ausser Betrieb* aktualisiert";
$MSG['_0006'] = "M�chten Sie die Site in den Modus *Vor�bergehend ausser Betrieb* umschalten?";

$MSG['_0008'] = "Banner-Verwaltung";

$MSG['_0010'] = "BANNER";

$MSG['_0012'] = "Benutzerverwaltung";

$MSG['_0014'] = "Das Banner-System wird je nach gew�hlten Filtern beim Erstellen der Banner auf zuf�lliger Basis Banner aus der Datenbank auslesen.
			<br>Zun�chst muss festgelegt werden, welche Bannergr�sse verwendet werden soll:";
$MSG['_0015'] = "Jede Gr�sse";
$MSG['_0016'] = "Fixe Gr�sse (bitte angeben)";
$MSG['_0017'] = "Breite";
$MSG['_0018'] = "H�he";

$MSG['_0022'] = "Firma";

$MSG['_0024'] = "Banner verwalten";
$MSG['_0025'] = "Banner";
$MSG['_0026'] = "Benutzer hinzuf�gen";

$MSG['_0028'] = "Gew�hlte Benutzer l�schen (Banner werden ebenfalls gel�scht)";
$MSG['_0029'] = "Banner ausw�hlen";
$MSG['_0030'] = "URL";
$MSG['_0031'] = "Text unterhalb des Banners";
$MSG['_0032'] = "Alternativer Text (f�r den *ALT*-Tag in HTML)";
$MSG['_0033'] = "<b>Filter</b>";
$MSG['_0035'] = "Stichworte";
$MSG['_0036'] = "Zugelassene Formate: GIF, JPG, PNG, SWF";
$MSG['_0037'] = "Vollst�ndige URL inklusive http://";
$MSG['_0038'] = "Kann leer gelassen werden";
$MSG['_0039'] = "Sie k�nnen die Banner-Rotation auf der Basis von zwei verschiedenen Kriterien filtern:
			<ul>
			<li><b>Kategorien</b>: W�hlen Sie unten eine oder mehrere Kategorien. Das Banner wird nur angezeigt, wenn die gew�hlte Kategorie sichtbar ist (z.B. Beim Durchsehen von Kategorien, beim Auflisten von Auktionen)
			<li><b>Stichworte</b>: Geben Sie ein oder mehrere Stichworte ein (ein Wort pro Zeile). Das Banner wird nur bei Auktionen angezeigt, die mindestens ein Stichwort in Titel oder Beschreibung enthalten
			</ul>
			Der <b>Kategorien-Filter</b> wird angewendet, wenn Kategorien durchsucht oder Artikel der gew�hlten Kategorie angezeigt werden.<br>
			Der <b>Stichworte-Filter</b> wird nur auf der Auktionsseite angewendet.<br>
			Wenn keine Filter-Kriterien zutreffen, so wird ein zuf�llig ausgew�hltes Banner gezeigt (aus der Gruppe von Bannern ohne zugeordneten Filter).";
$MSG['_0040'] = "Banner hinzuf�gen";
$MSG['_0041'] = "<b>Neues Banner</b>";
$MSG['_0042'] = " (erforderlich)";
$MSG['_0043'] = "<b>Benutzer-Banner</b>";
$MSG['_0044'] = "Bitte geben Sie eine g�ltige URL an";
$MSG['_0045'] = "Anzahl gekaufte Einblendungen";
$MSG['_0046'] = "0 (Null) oder leer = unlimitierte Einblendungen";
$MSG['_0047'] = "%s bereits vorhanden";
$MSG['_0048'] = "Falsches Dateiformat. Erlaubte Formate: GIF, JPG, PNG, SWF";
$MSG['_0049'] = "Einblendungen:";
$MSG['_0050'] = "URL:";
$MSG['_0051'] = "Klicks:";
$MSG['_0052'] = "Filter f�r Einblendungen";
$MSG['_0053'] = "<b>Kategorien</b>";
$MSG['_0054'] = "<b>Stichworte</b>";
$MSG['_0055'] = "Banner bearbeiten";
$MSG['_0056'] = "Neues Banner";

$MSG['_0148'] = "Auktion mit *Angebot reaktivieren*";

$MSG['_0151'] = " Mal";

$MSG['_0153'] = "Reaktivierungen / <br>Reaktiviert";

$MSG['_0161'] = "Automatisch reaktivieren";
$MSG['_0162'] = "Ihre Auktion kann automatisch reaktiviert werden, wenn keine Gebote abgegeben wurden. (1,00� pro Reaktivierung)";
$MSG['_0163'] = "Gewinner anzeigen";


$MSG['2_0004'] = "Zeige die IP-Adressen des Benutzers";
$MSG['2_0005'] = "IP-Adresse bei Registrierung";
$MSG['2_0006'] = "Sperren";
$MSG['2_0007'] = "Zulassen";

$MSG['2_0009'] = "IP-Adresse";

$MSG['2_0012'] = '<span style="color:#A2CD5A;"><b>Zugelassen</b></span>';
$MSG['2_0013'] = '<span style="color:#CD0000;"><b>Gesperrt</b></span>';

$MSG['2_0015'] = "Ausgew�hlte Elemente: �nderungen ausf�hren";

$MSG['2_0017'] = "IP-Adressen";

$MSG['2_0020'] = "Gesperrte IP-Adressen verwalten";
$MSG['2_0021'] = "Diese IP-Adresse sperren: ";

$MSG['2_0024'] = "(Vollst�ndige IP-Adresse - Beispiel: 185.39.51.63)";
$MSG['2_0025'] = "Von Hand eingegeben";
$MSG['2_0026'] = "Ihr Zugang zu dieser Seite wurde gesperrt.<br>
				Die Gebote zu Ihren aktiven Auktionen wurden gel�scht und Ihre Artikel aus der Datenbank entfernt.
				<br><br>
				Danke f�r Ihren Besuch.";
$MSG['2_0027'] = "Ihre IP-Adresse wurde gesperrt";

$MSG['2_0032'] = "Einstellungen f�r die Option *Auktion verl�ngern*";

$MSG['2_0034'] = "Option *Auktion verl�ngern* aktivieren?";
$_CUSTOM_0032 = "Mit der Option *Auktion verl�ngern* wird eine Auktion vor Ihrem Ende automatisch um <b>X</b> Sekunden verl�ngert,
				sofern jemand w�hrend der letzten <b>Y</b> Sekunden der Laufzeit einer Auktion ein Gebot abgibt.<br>";
$MSG['2_0035'] = "Auktion verl�ngern um ";
$MSG['2_0036'] = " Sekunden, wenn jemand ein Gebot abgibt w�hrend der letzten ";
$MSG['2_0037'] = " Sekunden";
$MSG['2_0038'] = "Bitte geben Sie g�ltige numerische Werte ein";
$MSG['2_0039'] = "Auktionen Auto Erweiterung bietet Ihnen die M�glichkeit, automatisch nach <b>X</b> Sekunden die Auktionen Zeit Ende, erweitern, wenn jemand in den letzten Sekunden der <b>Y</b> der Auktion Lebensdauer bietet.<br>";

$MSG['2__0001'] = "Sprache w�hlen";
$MSG['2__0002'] = "Sprachen Unterst�tzung";

$MSG['2__0003'] = "<br>Die Standardsprache ist Englisch.<br>
				F�hren Sie bitte die folgenden Schritte aus, wenn Sie die Unterst�tzung f�r mehrere Sprachen aktivieren oder die Standardsprache �ndern m�chten:
				<ul>
				<li>Zum Hinzuf�gen neuer Sprachen erstellen Sie zun�chst eine vollst�ndige Kopie des Ordners *language/EN* und benennen ihn mit dem
				entsprechenden Namen: z.B. �bersetzung in Deutsch: Ordner *DE* benennen (ohne die Anf�hrungen), f�r Franz�sisch: FR usw.
				<br>
				Danach m�ssen Sie in den obersten Zeilen der Datei *messages.inc.php* das Zeichen-Codierung (character encoding) entsprechend Ihrer Sprache einstellen.
				Die zu �ndernde Variable heisst <i>\$CHARSET</i>. UTF-8 sollte f�r die meisten Sprachen geeignet sein.<br>
				Sodann ist die Leserichtung im Dokument zu definieren. Die zu �ndernde Variable heisst <i>\$DOCDIR</i>
				und kann zwei m�gliche Werte annehmen:
				<ul>
				<li><b>ltr</b> (links-nach-rechts [left-to-right]): Dies ist der Standardwert und bedeutet, dass Text von links nach rechts gelesen wird
				<li><b>rtl</b> (rechts-nach-links [right-to-left]): bedeutet, dass Text von rechts nach links gelesen wird (i.e. Arabische Sprachen, Hebr�isch, usw.)
				</ul>
				Sobald <i>\$CHARSET</i> und <i>\$DOCDIR</i> gem�ss der Sprache, in die Sie �bersetzen wollen, ge�ndert wurde,
				 m�ssen Sie alle Fehlermeldungstexte und alle Benutzeroberfl�chentexte in der Datei *messages.inc.php* in die gew�nschte Sprache �bersetzen.
				
				<li>Im Weiteren ben�tigen Sie Flaggensymbole im Dateiformat GIF f�r die gew�nschte Sprache. Diese m�ssen im Ordner *includes/flags* abgelegt werden. 
				Die GIF-Dateien m�ssen denselben Namen haben wie Ihr Sprachcode, also z.B. DE.gif f�r den deutschen Sprachcode DE.
				<br>Kopieren Sie die Flaggensymbole (GIF-Dateien) in den Ordner *includes/flags*.
				<br><b>Bitte beachten Sie:</b> f�r jede �bersetzte Sprache muss eine entsprechende Datei XX.gif im Ordner *includes/flags* vorhanden sein
				<li>W�hlen Sie unten die Standardsprache. Alle anderen verf�gbaren Sprachen werden auf der Startseite mit ihren Flaggensymbolen angezeigt und k�nnen so gew�hlt werden.
				</ul>
				";
$MSG['2_0003map'] = "Kartenansicht der Benutzer";				
$MSG['2__0004'] = "Standardsprache";
$MSG['2__0005'] = '<span style="color:#CD0000;"><b>Aktuelle Standardsprache</b></span>';

$MSG['25_0011'] = "Werbung";

$MSG['2__0016'] = "Startdatum";

$MSG['2__0025'] = "*Sofort Kaufen*";

$MSG['2__0027'] = "Alle";
$MSG['2__0028'] = "Alle ausw�hlen";
$MSG['2__0029'] = "Sie haben keine Nachrichten";
$MSG['2__0030'] = " bedeutet, dass der Eintrag nicht gel�scht werden kann, da er in Verwendung ist.";
$MSG['2__0031'] = "Sind sie sicher, dass Sie diese Nachrichten l�schen m�chten?";
$MSG['2__0037'] = "Auktion einstellen (kostenpflichtig)";
$MSG['2__0038'] = "W�hlen Sie eine Kategorie";
$MSG['2__0039'] = "Wenn Sie Ihr Passwort nicht mehr wissen, geben Sie bitte in den Feldern unten Ihren Benutzernamen <strong>und</strong> Ihre E-Mail-Adresse ein.";
$MSG['2__0041'] = "Wahlen Sie eine weitere Kategorie";
$MSG['2__0045'] = " Pixel ";
$MSG['2__0047'] = "WEITER &gt;";
$MSG['2__0048'] = "Jetzt schliessen!";

$MSG['2__0050'] = "�hnliches verkaufen";
$MSG['2__0051'] = "Angebot reaktivieren";
$MSG['2__0054'] = '<span style="#CD0000;"><b>Bereits ausgew�hlt</b></span>';

$MSG['2__0056'] = "Gesperrte Auktionen";
$MSG['2__0057'] = "Z�hler anzeigen";
$MSG['2__0058'] = "W�hlen Sie, welche Z�hler im Kopfbereich der Seiten angezeigt werden sollen.<br>
				Die folgenden drei Z�hler sind verf�gbar:
				<ul>
				<li>Aktive Auktionen</li>
				<li>Registrierte Benutzer</li>
				<li>Benutzer, die momentan online sind</li>
				</ul>
				Sie k�nnen jeden Z�hler unten aktivieren oder deaktivieren";
$MSG['2__0059'] = "Benutzer online";
$MSG['2__10059'] = "BENUTZER ONLINE";
$MSG['2__100591'] = "Benutzer";
$MSG['2__0060'] = "Aktive Auktionen";
$MSG['2__0061'] = "Registrierte Benutzer";
$MSG['2__0062'] = "Z�hler, die angezeigt werden sollen";
$MSG['2__0063'] = "Z�hler-Einstellungen aktualisiert";
$MSG['2__0064'] = "G�STE ONLINE";
$MSG['2__00642'] = "G�ste";

$MSG['2__0066'] = "Aktiviert";
$MSG['2__0067'] = "Deaktiviert";


$MSG['25_0001'] = "GEWINNER";
$MSG['25_0002'] = "VERK�UFER";

$MSG['25_0004'] = "Benutzername";
$MSG['25_0005'] = 'Felder der Benutzerregistrierung';
$MSG['25_0006'] = "H�chstes Gebot";
$MSG['25_0008'] = "Voreinstellungen";
$MSG['25_0009'] = "Themes";
$MSG['25_0010'] = "Benutzer";
$MSG['25_0011'] = "Werbung";
$MSG['25_0012'] = "Geb�hren";

$MSG['25_0015'] = "Newsletter versenden";

$MSG['25_0018'] = "Inhalt";

$MSG['25_0023'] = "Statistik";

$MSG['25_0025'] = "�bersicht Einstellungen";
$MSG['25_0026'] = "Stapelverarbeitung (cron.php)";
$MSG['25_0027'] = "Stellen Sie sicher, dass die Datei <code>cron.php</code> periodisch mittels eines cron-jobs gestartet wird (15 Minuten ist in der Regel ein geeignetes Intervall).";

$MSG['25_0031'] = "Statistik";
$MSG['25_0032'] = " Minuten";
$MSG['25_0033'] = " Sekunden";

$MSG['25_0035'] = "Zeitkorrektur";
$MSG['25_0036'] = "GMT";
$MSG['25_0037'] = " Stunden";
$MSG['25_0038'] = "Im Kopfbereich angezeigte Z�hler:<br>";

$MSG['25_0040'] = "Seitenausrichtung";
$MSG['25_0041'] = "Auf der Starseite angezeigt";
$MSG['25_0042'] = "Login-Box";
$MSG['25_0043'] = "News-Box";
$MSG['25_0044'] = "News";
$MSG['25_0045'] = "Breite der Vorschaubilder";

$MSG['25_0048'] = "Andere Vorschaubilder: ";
$MSG['25_0049'] = "Newsletter-Abonnemente";

$MSG['25_0055'] = "Registrierte aktive Benutzer";
$MSG['25_0056'] = "Gesperrte Benutzer";
$MSG['25_0057'] = "Anzahl aktive Auktionen";

$MSG['25_0059'] = "Anzahl Gebote in aktiven Auktionen";

$MSG['25_0063'] = "Zugriffe heute";

$MSG['25_0071'] = "Artikel-Beschreibung";

$MSG['25_0074'] = "E-Mail nochmals senden";
$MSG['25_0075'] = "Registrierungsbest�tigungs-E-Mail nochmals senden";
$MSG['25_0076'] = "E-Mail nochmals senden";
$MSG['25_0077'] = "HTML ist nicht erlaubt";
$MSG['25_0078'] = "E-Mail gesendet an ";
$MSG['25_0079'] = "Newsletter";
$MSG['25_0080'] = "�bersicht";
$MSG['25_0081'] = "Mein Konto";
$MSG['25_0082'] = "Verkaufen";
$MSG['25_0083'] = "Kaufen";
$MSG['25_0084'] = "Stichwort zum Artikel hinzuf�gen";
$MSG['25_0085'] = "Mich bei jedem Besuch automatisch anmelden";
$MSG['25_0086'] = "Bitte best�tigen Sie Ihr Gebot, indem Sie auf die unten stehende Schaltfl�che klicken. Beachten Sie, dass Ihr Gebot verbindlich ist und nicht mehr annulliert werden kann. Sie verpflichten sich damit zur Zahlung von <b id=\"bidcost\">%s</b> an den Verk�ufer.";
$MSG['25_0087'] = "Sie wurden �berboten";
$MSG['25_0088'] = "Sie sind momentan der H�chstbietende";
$MSG['25_0089'] = "Herzliche Gratulation, sie haben diesen Artikel gekauft. Bitte bezahlen Sie ihn jetzt.";

$MSG['25_0107'] = "Gr�sse der Vorschaubilder";
$MSG['25_0108'] = "Vorschaubilder auf der Startseite";
$MSG['25_0109'] = "Vorschaubilder f�r Kategorien";
$MSG['25_0110'] = "*Akzeptieren*-Text";

$MSG['25_0115'] = "Anstehende Auktionen";
$MSG['25_0116'] = "Beginnt";
$MSG['25_0117'] = "Endet";
$MSG['25_0118'] = "Jetzt beginnen!";
$MSG['25_0119'] = "Verkaufte Artikel";

$MSG['25_0121'] = "Geschlossen am";

$MSG['25_0133'] = "Ich m�chte mich registrieren als";
$MSG['25_0134'] = "<b>Verk�ufer</b> (Kann Artikel kaufen und verkaufen)";
$MSG['25_0135'] = "<b>Nur K�ufer</b> (Kann Artikel nur kaufen)";
$MSG['25_0136'] = "Ben�tigt Genehmigung durch den Administrator";
$MSG['25_0137'] = "Sie m�ssen einen Kontotyp w�hlen (Verk�ufer oder K�ufer)";
$MSG['25_0138'] = "Verk�ufer";
$MSG['25_0139'] = "K�ufer";
$MSG['25_0140'] = "Ihr Konto ist ein K�ufer-Konto. Verkaufsaktivit�ten sind Ihnen daher nicht erlaubt.<br> Wenn Sie zu einem <b>Verk�ufer-Konto</b> wechseln m�chten";
$MSG['25_0141'] = "schicken Sie bitte einen Antrag an den Administrator.";
$MSG['25_0142'] = "Ihr Antrag wurde an den Administrator gesandt.";
$MSG['25_0143'] = "Ihr Konto ist ein K�ufer-Konto. Verkaufsaktivit�ten sind Ihnen daher nicht zugelassen.<br> Sie haben bereits einen Antrag f�r ein <b>Verk�ufer-Konto</b> gestellt. Ihr Antrag wird momentan bearbeitet. ";

$MSG['25_0146'] = "Kategorien-Sortierung";
$MSG['25_0147'] = "Die Kategorienliste in der linken Spalte der Startseite kann <b>alphabetisch</b> sortiert werden oder basierend auf der Anzahl Auktionen in jeder Kategorie (<b>Kategorien-Z�hler</b>).<br>
			W�hlen Sie unten die gew�nschte Sortierung";
$MSG['25_0148'] = "Alphabetisch";
$MSG['25_0149'] = "Kategorien-Z�hler";
$MSG['25_0150']= "Einstellungen zur Kategorien-Sortierung aktualisiert";
$MSG['25_0151'] = "Benutzer-Authentifizierung";
$MSG['25_0151_a'] = "Best�tigungs-Methode";
$MSG['25_0152'] = "Wenn diese Option aktiviert ist, dann m�ssen Benutzer Ihr Passwort eingeben, bevor Sie Aktivit�ten ausf�hren k�nnen wie z.B. Bewertungen abgeben, Auktionen einstellen oder Gebote abgeben.";
$MSG['25_0152_a'] = "In uAuctions m�ssen alle Benutzerkonten vor dem ersten Gebrauch aktiviert werden. W�hlen Sie die Aktivierungsmethode.";
$MSG['25_0152_b'] = "Der Administrator muss jedes Konto aktivieren.";
$MSG['25_0152_c'] = "Benutzer m�ssen ihr Konto selbst aktivieren.";
$MSG['25_0152_d'] = "Benutzerkonti werden bei Registrierung automatisch aktiviert (nicht empfohlen)";

$MSG['25_0155'] = "Verk�uferkontakt erneuert";

$MSG['25_0157'] = "Ihr Hintergrundbild";

$MSG['25_0166'] = "Zur�ck zur Auktion";
$MSG['25_0167'] = "Icon";

$MSG['25_0169'] = "Mitgliedschafts-Stufen";
$MSG['25_0169a'] = "Version pr�fen";
$MSG['25_0170'] = "Bearbeiten, l�schen oder hinzuf�gen von Mitgliedschafts-Stufen. *Punkte* = oberes Limit, *Mitgliedschaft* = Name der Stufe, *Icon* = Name des Icons, das die Stufe repr�sentiert (relativ zum Ordner *images/icons/*)";
$MSG['25_0171'] = "verdiente Punkte";
$MSG['25_0172'] = "Mitgliedschafts-Arten";

$MSG['25_0176'] = "JETZT UMWANDELN!";
$MSG['25_0177'] = "f�r";
$MSG['25_0178'] = "HTML Meta-Tags";

$MSG['25_0180'] = "Meta-Description-Tag";
$MSG['25_0181'] = "Meta-Keywords-Tag";
$MSG['25_0182'] = "Der Meta-Description-Tag wird �blicherweise verwendet, um Ihre Seiten in den Resultaten von Suchmaschinen zu beschreiben.<br>
				Geben Sie unten den Text ein, der Ihre Site m�glichst pr�gnant beschreibt.";
				
$MSG['25_0184'] = "Der Meta-Keywords-Tag bietet gewissen Suchmaschinen weitere Informationen, um Ihre Site zu indexieren.<br>
				Geben Sie unten Stichworte ein (mit Kommas abgetrennt, z.B. B�cher, B�cher-Auktion, Buchverkauf).";
$MSG['25_0185'] = "Meta-Tags-Einstellungen aktualisiert";
$MSG['25_0186'] = "Bilder hochladen";
$MSG['25_0187'] = "Geben die maximal zugelassene Gr�sse von Bildern ein (in KBytes), die Benutzer pro Auktion hochladen d�rfen.";
$MSG['25_0188'] = "Benachrichtigungs-E-Mails f�r Auktionen";
$MSG['25_0189'] = "Als Verk�ufer k�nnen Sie w�hlen, ob Sie f�r jede Auktion, die geschlossen wird, eine E-Mail-Benachrichtigung erhalten wollen oder ob Sie eine E-Mail-Benachrichtigung pro Tag erhalten m�chten, die alle geschlossenen Auktionen des Tages enth�lt.<br>
				Die zweite Option ist normalerweise geeignet, wenn Sie eine grosse Anzahl an laufenden Auktionen haben.<br>Sie k�nnen auf E-Mail-Benachrichtigungen auch ganz verzichten; diese Option wird nicht empfohlen.";
$MSG['25_0190'] = "<b>Eine</b> E-Mail-Benachrichtigung erhalten f�r jede Auktion, die geschlossen wird";
$MSG['25_0191'] = "Eine E-Mail-Benachrichtigung pro Tag erhalten, die alle geschlossenen Auktionen des Tages enth�lt";
$MSG['25_0192'] = "E-Mail-Benachrichtigungs-Optionen aktualisiert";
$MSG['25_0193'] = "<b>Keine</b> E-Mail-Benachrichtigungen erhalten";

$MSG['25_0195'] = "Sie k�nnen w�hlen, ob Sie f�r jede eingestellte Auktion eine E-Mail-Best�tigung erhalten m�chten oder nicht.";
$MSG['25_0196'] = "E-Mail-Best�tigung f�r jede eingestellte Auktion <b>erhalten</b>.";
$MSG['25_0197'] = "Auf E-Mail-Best�tigung f�r jede eingestellte Auktion <b>verzichten</b>.";

$MSG['25_0199'] = "Auktionen, die geschlossen werden, fortsetzen";

$MSG['25_0209'] = "Unter dem Preis verkaufen";

$MSG['25_0214'] = "Auch geschlossene Auktionen durchsuchen: ";
$MSG['25_0215'] = "Versandkonditionen";
$MSG['25_0216'] = "Verk�ufer kontaktieren";
$MSG['25_0217'] = "Es wird nicht empfohlen, nicht registrierten Besuchern die M�glichkeit zu geben, Verk�ufer zu kontaktieren. Daher
				k�nnen Sie entscheiden, ob Sie nicht registrierten Besuchern die M�glichkeit geben wollen, Verk�ufer zu kontaktieren oder nicht.";
$MSG['25_0218'] = "Jeder Besucher kann Verk�ufer kontaktieren (die M�glichkeit, Verk�ufer zu kontaktieren, wird IMMER angezeigt)";
$MSG['25_0219'] = "Nur registrierte und eingeloggte Benutzer k�nnen Verk�ufer kontaktieren (die M�glichkeit, Verk�ufer zu kontaktieren, wird nur registrierten und eingeloggten Benutzern angezeigt)";
$MSG['25_0220'] = "Niemand kann Verk�ufer kontaktieren (die M�glichkeit, Verk�ufer zu kontaktieren, wird NIE angezeigt)";
$MSG['25_0223'] = "Erhaltene Bewertungen";
$MSG['25_0224'] = "Vorschau";

// multi-language months
$MSG['MON_001'] = "Jan";
$MSG['MON_001E'] = "Januar";
$MSG['MON_002'] = "Feb";
$MSG['MON_002E'] = "Februar";
$MSG['MON_003'] = "Mar";
$MSG['MON_003E'] = "M�rz";
$MSG['MON_004'] = "Apr";
$MSG['MON_004E'] = "April";
$MSG['MON_005'] = "Mai";
$MSG['MON_005E'] = "Mai";
$MSG['MON_006'] = "Jun";
$MSG['MON_006E'] = "Juni";
$MSG['MON_007'] = "Jul";
$MSG['MON_007E'] = "Juli";
$MSG['MON_008'] = "Aug";
$MSG['MON_008E'] = "August";
$MSG['MON_009'] = "Sep";
$MSG['MON_009E'] = "September";
$MSG['MON_010'] = "Okt";
$MSG['MON_010E'] = "Oktober";
$MSG['MON_011'] = "Nov";
$MSG['MON_011E'] = "November";
$MSG['MON_012'] = "Dez";
$MSG['MON_012E'] = "Dezember";

$MSG['26_0000'] = "Als Standard festlegen";
$MSG['26_0001'] = "Gebots�bersicht";
$MSG['26_0002'] = "Themes verwalten";
$MSG['26_0003'] = "Theme-Dateien bearbeiten";
$MSG['26_0004'] = "Theme-Datei hinzuf�gen";
$MSG['26_0005'] = "Standard-Theme aktualisiert";

$MSG['30_0029'] = "Stellen Sie unten die Anzahl Kategorien ein, die in der Kategorienliste in der linken Spalte der Startseite angezeigt werden soll";
$MSG['30_0030'] = "Anzuzeigende Kategorien: ";
$MSG['30_0031'] = "Cache leeren";
$MSG['30_0031a'] = "Site Logo";  
$MSG['30_0032'] = "L�scht alle Dateien aus dem Cache. Der Cache muss jedes Mal gel�scht werden, wenn Sie �nderungen an den Vorlagen-Dateien vornehmen";
$MSG['30_0033'] = "Cache geleert";

$MSG['30_0049'] = "Newsletter-Einstellungen aktualisiert";

$MSG['30_0053'] = "<p>Einige kostenlose E-Mail-Dienste wurden f�r diese Website gesperrt. Bitte verwenden Sie keine E-Mail-Adressen von folgenden Anbietern:</p>";

$MSG['30_0055'] = "Der Newsletter wird automatisch im <b>HTML-Format</b> versandt, daher muss f�r jede neue Zeile ein <code>&lt;BR&gt;</code>-Tag eingef�gt werden; ansonsten erscheint der Text ohne Zeilenumbr�che und ohne Formatierung.";

$MSG['30_0062'] = "Bitte mindestens 4 Zeichen eingeben";
$MSG['30_0063'] = "Nur *Sofort Kaufen*?";
$MSG['30_0064'] = "<b>*Nur Sofort Kaufen*</b>-Auktionen aktivieren?";
$MSG['30_0065'] = "Das Aktivieren der Option <b>*Nur Sofort Kaufen*</b> erm�glicht es Verk�ufern, Auktionen einzustellen, die keine Gebote zulassen, sondern nur die Option <b>*Sofort Kaufen*</b> (Fixpreis-Auktionen).<br><b>Bitte beachten Sie:</b> Die Option <b>Nur *Sofort Kaufen*</b> kann nur verwendet werden, wenn die Option <b>*Sofort Kaufen*</b> aktiviert ist.";
$MSG['30_0066'] = "Einstellungen f�r *Nur Sofort Kaufen* aktualisiert";
$MSG['30_0067'] = "<b>Nur *Sofort Kaufen*</b>-Auktion";

$MSG['30_0069'] = "Verk�ufer: diese Auktion bearbeiten";
$MSG['30_0070'] = "Nur in dieser Kategorie suchen";

$MSG['30_0080'] = "Zahlungsoptionen";
$MSG['30_0081'] = "Betrachtet ";

$MSG['30_0084'] = "Entscheiden Sie, ob die Adresse des Gewinners einer Auktion in die Benachrichtigungs-E-Mail an den Verk�ufer eingef�gt werden soll. Option unten aktivieren oder deaktivieren.";
$MSG['30_0085'] = "Adresse des Gewinners in die E-Mail-Benachrichtigung einf�gen?";
$MSG['30_0086'] = "Adresse: ";
$MSG['30_0087'] = "Sind Sie sicher, dass Sie die gew�hlten Auktionen verarbeiten wollen?";

$MSG['30_0098'] = "&nbsp; = �berboten";

$MSG['30_0100'] = "<b>*Sofort Kaufen*</b>-Auktionen";
$MSG['30_0101'] = "<b>Nur *Sofort Kaufen*</b>-Auktionen";
$MSG['30_0102'] = "Alle markieren / Alle Markierungen entfernen";

$MSG['30_0176'] = "Gewinner anzeigen";
$MSG['30_0177'] = "Auktion geschlossen";
$MSG['30_0178'] = "&nbsp;&nbsp;F�r diese Auktion wurden keine Gebote abgegeben";
$MSG['30_0179'] = "H�chstes Gebot";
$MSG['30_0180'] = "Gesamter Gebots Verlauf";
$MSG['30_0181'] = "Pinnwandtitel";

$MSG['30_0208'] = "Gebot abgeben >";
$MSG['30_0209'] = "Informationen zum Verk�ufer";
$MSG['30_0210'] = "Wenn Artikel mit diesen Stichworten eingestellt werden, so erhalten Sie eine Benachrichtigung per E-Mail";
$MSG['30_0211'] = "Sie verwenden eine alte Version. Die aktuellste Version erhalten Sie <b><a href='http://u-auctions.com' target='_blank'>hier.</a></b>";
$MSG['30_0212'] = "Sie verwenden die aktuellste Version";
$MSG['30_0213'] = "Keine Bewertung ausstehend";
$MSG['30_0214'] = "benutzte Version";
$MSG['30_0215'] = "Name";
$MSG['30_0216'] = "Name der Bank";
$MSG['30_0217'] = "IBAN";
$MSG['30_0218'] = "BIC";
$MSG['30_0219'] = "Bank�berweisung";
$MSG['30_0220'] = 'Ein g�ltiger Bankname ist erforderlich';
$MSG['30_0221'] = 'Ein g�ltiges Bank Konto ist erforderlich ';
$MSG['30_0222'] = 'Eine g�ltige BLZ ist erforderlich';
$MSG['30_0223'] = 'Ein g�ltiger Name der Bank, Kontonummer und BLZ werden ben�tigt';
$MSG['30_0224'] = 'Sub Admin';
$MSG['30_0225'] = 'Haupt Admin';
$MSG['30_0226'] = 'Der Benutzer ist ein Sub Admin';
$MSG['30_0227'] = 'Der Benutzer ist ein Haupt Admin';
$MSG['30_0228'] = 'Verk�ufer Info';
$MSG['30_0229'] = 'Admin Ordner Pfad/Name';
$MSG['30_0230'] = 'Am Ende des Admin Ordners Pfad/Name kein /';
$MSG['30_0231'] = 'Der Admin-Ordner wurde erfolgreich umbenannt';
$MSG['30_0232'] = 'Zur�ck-Zur-Auktion';

$MSG['30_0233'] = "Cookies Richtlinien Seite";
$MSG['30_0234'] = "Cookies Richtlinien Seite aktivieren?";
$MSG['30_0235'] = "Cookies Richtlinien Inhalt<br>(HTML erlaubt)";
$MSG['30_0236'] = "Aktivieren Sie diese Option, wenn ein Link zur Cookies Richtlinien Seite in der Fu�zeile der Seiten erscheinen soll.";
$MSG['30_0237'] = "Cookies Richtlinieneinstellungen aktualisiert";
$MSG['30_0238'] = "Cookies Richtlinien Inhalt<br>(HTML erlaubt)";
$MSG['30_0239'] = "Cookies Richtlinien";
$MSG['30_0240'] = 'Diese Seite benutzt Cookies m�ssen die Richtlinien lesen und akzeptieren: <a href="' . $system->SETTINGS['siteurl'] . 'contents.php?show=cookies" class="new-window">Cookies Richtlinien</a>';  


$MSG['30_0110'] = "Versand";
$MSG['350_1004'] = "Menge";
$MSG['350_1008'] = "zus�tzlich zum Versand";
$MSG['350_1009'] = "zus�tzliche Versandkosten";
$MSG['350_10111'] = "Benutzer is Online";
$MSG['350_10112'] = "Benutzer is Offline";
$MSG['350_10114'] = "Meinen online Status verbergen";
$MSG['350_10115'] = "Online Status An/Aus";
$MSG['350_10116'] = "W�hlen Sie per Ein oder Aus-Taste, um Ihren Online-Status verstecken. Klicken Sie auf die Schaltfl�che �nderungen speichern.";
$MSG['350_10017'] = "Verschickt";
$MSG['350_10018'] = "Versand Status";
$MSG['350_10019'] = "Als verschickt setzen";
$MSG['350_10021'] = "Als empfangen";
$MSG['350_10020'] = "Artikel wurde nicht verschickt";
$MSG['350_10022'] = "Artikel wurde empfangen";
$MSG['350_10023'] = "Empfangen";
$MSG['350_10024'] = "Warten auf Zahlung";
$MSG['350_10025'] = "Avatar hochladen";
$MSG['350_10026'] = "Digitales Produkt";

//Digitale Produkte
$MSG['350_1010'] = "Digitales Produkt";
$MSG['350_10171'] = "Digitales Produkt jetzt hochladen";
$MSG['350_10172'] = "Datei gespeichert:&nbsp;&nbsp;&nbsp;";
$MSG['350_10173'] = "Neue Datei hochgeladen:&nbsp;&nbsp;&nbsp;";
$MSG['350_10174'] = "flv,&nbsp;&nbsp; mp3,&nbsp;&nbsp; pdf,&nbsp;&nbsp; rar,&nbsp;&nbsp; zip <br>";
$MSG['350_10175'] = "erlaubte Dateitypen";
$MSG['350_10176'] = "maximal erlaubte Dateigr�sse (in KB)";
$MSG['350_10177'] = "Download";
$MSG['350_10179'] = "Digital item options";
$MSG['350_10180'] = "Max. Upload-Gr�sse f�r digitale Produkte";
$MSG['350_10181'] = "Geben Sie die maximale Gr��e der digitalen Artikel-Datei an (in KB), die die Verk�ufer hochladen k�nnen";
$MSG['350_10182'] = "erlaubte Dateitypen";
$MSG['350_10183'] = "Geben Sie ein, welche Dateitypen Sie erlauben wollen.(Beispiele: flv, mp3, pdf, rar, zip)";
$MSG['350_10184'] = "Auktion f�r digitale Produkte aktivieren.";
$MSG['350_10185'] = "Wenn Sie diese Option aktivieren, k�nnen Auktionen f�r digitale G�ter benutzt werden";
//Ende Digitale Produkte

///Login with facebook
$MSG['350_10167'] = "Teilen auf Facebook";
$MSG['350_10168'] = "Teilen auf Twitter";
$MSG['350_10169'] = "Teilen auf Google+";
$MSG['350_10186'] = "Sie sind jetzt mit Facebook verbunden <br>Bitte f�llen Sie Ihre pers�nliche Info komplett aus um einen neuen Account zu registrieren.";
$MSG['350_10187'] = "Sie  sind nicht mit Facebook verbunden.";
$MSG['350_10188'] = "Wenn Sie die Verbinden mit Facebook Schaltfl�che verwenden m�chten um sich auf unserer Webseite einloggen zu k�nnen, gehen Sie folgenderma�en vor.
<br>1. Klicken Sie den Facebook Button. 
<br>2. geben Sie ihre pers�nlichen Infos komplett ein.
<br>3. Jetzt den Registrierungs Button dr�cken.
<br>4. Ihren neuen Account aktivieren. 
<br>5. Sie k�nnen nun die Connect with Facebook-Taste benutzen, um sich auf unserer Webseite anzumelden.";
$MSG['350_10189'] = "Personal info";
$MSG['350_10190'] = "Ihr Konto ist nun mit Facebook verbunden";
$MSG['350_10191'] = "Ihr Konto ist nicht mit Facebook verbunden";
$MSG['350_10192'] = "Wenn Sie die Verbinden mit Facebook Schaltfl�che verwenden m�chten um sich auf Ihrer Webseite einloggen zu k�nnen, gehen Sie folgenderma�en vor.
<br>1. Klicken Sie auf die Schaltfl�che Verbinden mit Facebook 
<br>2. Klicken Sie auf die Schaltfl�che �nderungen speichern 
<br>3. Sie k�nnen nun die Connect with Facebook-Taste benutzen, um sich auf unserer Webseite anzumelden.";
$MSG['350_10194'] = "Facebook app id";
$MSG['350_10195'] = "Facebook app secret";
$MSG['350_10196'] = "Facebook login";
$MSG['350_10197'] = "Wollen Sie zulassen, dass sich User mit ihrem Facebooklogin anmelden k�nnen?";
$MSG['350_10198'] = "F�ge Deine facebook app id hier hinzu";
$MSG['350_10199'] = "F�ge Dein facebook app secret hier hinzu";
$MSG['350_10200'] = "You can allow people to login and register with there facebook account you will need the facebook app id and app secret.<br> <a href='https://developers.facebook.com'>Click Here</a> to get the facebook app id and app secret.";
$MSG['350_10201'] = "Facebook developer";
$MSG['350_10202'] = "Klicken Sie auf die Schaltfl�che Verbinden mit Facebook, um Ihr Facebook-Konto mit Ihrem Account zu verkn�pfen.";
$MSG['350_10203'] = "Klicken Sie auf die Schaltfl�che �nderungen speichern, um die Verkn�pfung Ihres Facebook-Kontos auf Ihr Konto zu beenden.";
$MSG['350_10204'] = "Sie m�ssen ein Konto auf unserer Website haben, um den Login mit Facebook zu nutzen.";
/// End of Login with facebook

$MSG['350_10205'] = "Durchsuchen";
$MSG['350_10206'] = "TOP Auktionen";
$MSG['350_10207'] = "Kontakt";
$MSG['103600'] = "Artikel Konditionen";
$MSG['103700'] = "Hersteller";
$MSG['103800'] = "Modell";
$MSG['103900'] = "Farbe";
$MSG['104000'] = "Herstellungsjahr";
$MSG['104100'] = "Konditionen Tabelle";
$MSG['104200'] = "Editieren, l�schen oder hinzuf�gen von Artikel Konditionen.";
$MSG['104300'] = "Konditionen Tabelle ge�ndert !";
$MSG['104400'] = "Artikelbeschreibung";

$MSG['OPENBUBBLE']='&nbsp;&nbsp;&nbsp;<a href="#" class="tt"><img src="images/info_button_s.png" height="18" width="18" align="top" border="0"><span class="tooltip"><span class="top"></span><span class="middle">';
$MSG['CLOSEBUBBLE']='</span><span class="bottom"></span></span></a>';
$MSG['REQUIRED']='<font color="red"><b>* Erforderlich !</b></font>';
$MSG['ASTERIX']='<font color="red">*&nbsp;&nbsp;</font>';
$MSG['1044'] ="Was bedeutet Artikel-Zustand?";
$MSG['1045']="Dieses wird der Titel Eintrag sein. Verwenden Sie W�rter im Titel, welche die K�ufer f�r die Suche verwenden w�rden.";
$MSG['1046']="Das Anzeigen weiterer Informationen unter Ihrem Titel, machen Ihren Eintrag interessanter";
$MSG['1047']="Stellen Sie sicher, dass Sie den richtigen Zustand w�hlen, es ist wichtig, Ihre Artikel genau zu beschreiben,";
$MSG['1048']="Beschreiben Sie Ihren Artikel im Detail";
$MSG['1049']="Laden Sie Bilder f�r Ihren Eintrag hoch. Miniaturen (thumbnails) werden automatisch erstellt. Machen Sie Ihren Eintrag durch Bilder interessant!!";
$MSG['1400']="<font color='red'><b>Gemeldete Auktionen</b></font>";
$MSG['1401']="Gemeldete Auktionen ansehen";
$MSG['1402']="Gemeldet von Info";
$MSG['1403']="Meldungs Gr�nde/Kommentare";
$MSG['1404']="Artikel Titel/ID";
$MSG['1405']="Meldungsgrund";
$MSG['1406']="Meldungsgr�nde definieren";
$MSG['1407'] = "Bearbeiten, L�schen oder Hinzuf�gen der Meldungsgr�nde (Formular unten).";
$MSG['1408'] = "Tabelle der Meldungsgr�nde wurde erneuert!";
$MSG['1409']="W�hle Meldungsgrund";
$MSG['1410']="Melde Artikel";
$MSG['1411']="Eintrag sofort suspendieren oder lediglich eine Mitteilung an den Administrator senden";
$MSG['1412']="<font color='white'> WICHTIG!! setze 1 f�r normal, setze 2 f�r 2 f�r die automatische Suspendierung der Auktion, wenn gemeldet</font>";
$MSG['1413']="Finden Sie Inhalte auf dieser Seite unangebracht? Sagen Sie es uns bitte";
$MSG['1414']="Verk�ufer ID:&nbsp;&nbsp;";
$MSG['1415']="Verk�ufer Name:&nbsp;&nbsp;";
$MSG['1416']="Melder Name:&nbsp;";
$MSG['1417']="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID:";
$MSG['1418']="Gemeldet von:&nbsp;";
$MSG['1419']="Artikel Titel: ";
$MSG['1420']="Melder ID:&nbsp;";
$MSG['1421']="Meldungsgrund:&nbsp;";
$MSG['1422']="Meldungskommentar:&nbsp;";
$MSG['1423']="Meldungsdatum:&nbsp;";
$MSG['1424']="Close Report";
$MSG['1425']="Verk�ufer kontaktieren";
$MSG['1426']="Melder kontaktieren";
$MSG['1427']="Melder Email:&nbsp;";
$MSG['1428']="Subject";
$MSG['1429']="Kommentar hinzuf�gen";
$MSG['1430']="Stellen Sie sicher, dass Sie den richtigen Meldungsgrund w�hlen, er bestimmt wie schnell wir auf den Bericht reagieren";
$MSG['1431']="Reporting Listing:&nbsp;";
$MSG['1432']="Artikel ID:&nbsp; ";
$MSG['1433']="Eine Auktion wurde gemeldet";
$MSG['1434'] = "<br />Die durch uAuctions gesendeten E-Mails werden automatisch in <b>HTML</b> gesendet werden,<br /> Es ist notwendig einen tag <code>&lt;BR&gt;</code> zu setzen. F�r jede Zeile, die Sie neu hinzuf�gen.<br /> Andernfalls wird die Nachricht wie eine einzige Textzeile Text ohne Format erscheinen.<br />";
$MSG['1435']="Home";
$MSG['1436']="Nur Administrator informieren";
$MSG['1437']="<b>Bitte beachten Sie: Alle Berichte sind vertraulich!! Ihre Daten werden NICHT an den Verk�ufer weitergegeben werden <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Missbrauch des Meldesystems wird nicht leicht genommen</b>";

$MSG['350_10208']="Subject:";
$MSG['350_10209']="Google Adsense";
$MSG['350_10210']="Benutzeraktivit�t";
$MSG['30_0181152'] = "Ausblenden oder anzeigen";
$MSG['30_0181153'] = "Hervorgehobene Auktionen";
$MSG['30_0181154'] = "Hot Items";
$MSG['30_0181155'] = "Enden in K�rze";
$MSG['30_0181156'] = "Letzte Auktionen";
$MSG['30_0181157'] = "Hervorgehobene Auktionen";
$MSG['30_0181158'] = "Diese Auktion melden";
$MSG['30_0181159'] = "Standard Auktionen";
$MSG['277_1'] = "Alle-Kategorien";
$MSG['277_2'] = "Kategorien";
$MSG['277_3'] = "suchen";
$MSG['138_1'] = "zur�ck zur Auktion";

/// Favourite Sellers
$MSG['FSM1'] = "F�gen Sie einen Lieblings Verk�ufer hinzu";
$MSG['FSM2'] = "Sie m�ssen eingeloggt sein, um einen Lieblingsverk�ufer hinzuzuf�gen";
$MSG['FSM3'] = "Diese Person ist bereits einer Ihrer bevorzugten Verk�ufer";
$MSG['FSM4'] = "Diese Person ist nun ein Lieblingsverk�ufer. Sie k�nnen alle Elemente dieser Person vom K�ufer Control Panel ansehen";
$MSG['FSM6'] = "Sehen Sie Ihre Lieblingsverk�ufer"; 
$MSG['FSM7'] = "Ihre Lieblingsverk�ufer";
$MSG['FSM8'] = "Wir konnten diese Person nicht hinzuf�gen";
$MSG['30_0087a'] = "Sind Sie sicher, dass Sie die ausgew�hlten Verk�ufer bearbeiten wollen?"; 
/// End Favourite Sellers

/* Banner system */
$MSG['350_10129'] = "Es wird eine Geb�hr f�r jedes zus�tzliche Banner erhoben.<br> Nachdem Sie die Geb�hr f�r ein zus�tzliches Banner bezahlt haben, k�nnen Sie dieses in Ihren Account hochladen."; 
$MSG['350_10130'] = "Neuen Account hinzuf�gen"; 
$MSG['350_10131'] = "L�sche ausgew�hlte Accounts (Banners werden gel�scht)"; 
$MSG['350_10132'] = "Banner Setup Geb�hr"; 
$MSG['350_10133'] = "Werbungs Banner Setup Geb�hr"; 
$MSG['350_10134'] = "Extra Werbungs Banner Geb�hr"; 
$MSG['350_10135'] = "Neuer Werbungs Account"; 
$MSG['350_10136'] = "Extra Banner Geb�hr"; 
$MSG['350_10137'] = "Um Ihren Banner hochladen zu k�nnen w�hlen Sie bitte eine der unten aufgef�hrten Zahlungs-Gateways aus, um die Gebuhr von <b>%s</b> zu zahlen.";
$MSG['350_10138'] = "Extra Banner Geb�hr von <b>%s</b>"; 
$MSG['350_10139'] = "Um Ihr Bannerkonto zu aktivieren w�hlen Sie bitte eine der unten aufgef�hrten Zahlungs-Gateways aus, um die Gebuhr von <b>%s</b> zu zahlen.";
$MSG['350_10149'] = "Es wird eine Setup-Gebuhr erhoben. Durch Klicken auf die (Add New Account)-Taste stimmen Sie zu, die Setup Gebuhr zu zahlen. Sie werden auf die Zahlungsseite weitergeleitet. Nachdem die Zahlung erfolgt ist, wird Ihr neues Konto aktiviert.";
$MSG['350_1012111'] = "Meine Werbungen"; 
$MSG['350_1012212'] = "Ihr Banner Control Panel"; 
$MSG['350_1012222'] = "Zur�ck"; 
$MSG['350_1012535'] = "Account Info bearbeiten"; 
$MSG['350_1012333'] = "Die max. Breite und H�he darf nicht 400x100 pixel �berschreiten";
$MSG['350_101244'] = "Neuen Account hinzuf�gen"; 
$MSG['350_1012555'] = "Account aktivieren";
$MSG['350_10126'] = "Account ist aktiviert";
$MSG['350_10127'] = "Setup Geb�hr";
$MSG['350_10128'] = "Extra Banner Geb�hr";
$MSG['350_10150'] = "Edit Info";
$MSG['350_10151'] = "<b>Keywords</b>: eines oder mehrere Keywords eingeben (eines pro Linie). Das Banner wird nur in den Auktionen-Seiten erscheinen, die mindestens ein zutreffendes Keyword im Titel oder in der Beschreibung des Artikels enthalten";
$MSG['350_10152'] = "Bitte einloggen um bieten zu k�nnen";
$MSG['350_10153'] = "User hat f�r eine zus�tzliches Banner bezahlt";
$MSG['350_1015444'] = "Extra Banner aktivieren";
$MSG['350_1015446'] = "Erstes Banner Geb�hr";
$MSG['350_1015445'] = "Extra Banner";
/* END Banner system */

$MSG['025_00'] = "Nur Selbstabholung"; 
$MSG['025_A'] = "R�ckgabe akzeptieren"; 
$MSG['025_B'] = "R�ckgaben akzeptieren.<br>K�ufer hat 7 Tage R�ckgaberecht.<br>K�ufer zahlt die R�cksendung."; 
$MSG['025_C'] = "R�ckgabe "; 
$MSG['025_D'] = "Keine R�ckgabe"; 
$MSG['025_E'] = "K�ufer hat 7 Tage R�ckgaberecht.";

$MSG['350_1015401'] = "Sie m�ssen eingeloggt sein um etwas kaufen zu k�nnen";
$MSG['350_1015402'] = "Jetzt Kaufen";
$MSG['350_10193'] = "Sie m�ssen ein Konto auf unserer Website haben, um die Schaltfl�che Login with Facebook  benutzen zu k�nnen.";
$MSG['350_1015403'] = "Jetzt bieten";


$MSG['heading_title']         = 'Packzettel';
$MSG['text_packingslip']      = 'Packzettel/Rechnung';
$MSG['text_invoice']      = 'Rechnung';
$MSG['text_auction_id']         = 'Auction ID';
$MSG['text_order_invoice']         = 'Bestellung/Rechnung';
$MSG['text_date_added']       = 'Bestelldatum';
$MSG['text_telephone']        = 'Telephon:';
$MSG['text_fax']              = 'Fax:';
$MSG['text_to']               = 'Versenden von';
$MSG['text_ship_to']          = 'Versenden nach';
$MSG['text_account']          = 'Account';
$MSG['text_order']            = 'Bestellinformation';
$MSG['text_order_detail']     = 'Bestell Details';
$MSG['text_invoice_no']       = 'Rechnung No.:';
$MSG['text_order_id']         = 'Bestellungs ID:';
$MSG['text_invoice_date']         = 'Rechnungsdatum:';
$MSG['text_seller']              = 'Verk�ufer:';
$MSG['350_10119'] = "Paypal";
$MSG['350_10120'] = "Moneyorder";
$MSG['350_10121'] = "Paypal or Moneyorder";
$MSG['350_10122'] = "Authorize.net";
$MSG['350_10123'] = "Worldpay";
$MSG['350_10124'] = "2Checkout";
$MSG['350_10125'] = "Skrill (Moneybookers)";
$MSG['column_product']        = 'Produkt';
$MSG['column_model']          = 'Modell';
$MSG['column_tax']            = 'Steuer';
$MSG['column_quantity']          = 'Menge';
$MSG['column_unit_price'] 	= 'Preis pro Einheit';
$MSG['column_shipping']          = 'Versandkosten';
$MSG['column_total'] 	= 'Total';
$MSG['unit_price']         = 'Preis einer Einheit:';
$MSG['shipping_text']          = 'Versand:';
$MSG['vat_value_total']        = 'Steuern:';
$MSG['total_text']             = 'Gesamtbetrag:';
$MSG['entry_shipping_method'] = 'Versand Methode';
$MSG['entry_payment_method']  = 'Zahlungs Methode';
$MSG['entry_yellow_line']        = 'Text in der gelben Linie. Es ist in der Sprachdatei zu finden.';
$MSG['entry_thankyou']           = 'Danke f�r den Einkauf bei uns und wir hoffen, wir sehen uns bald wieder!';
$MSG['entry_no_orders_selected'] = 'Sie haben keine Bestellung ausgew�hlt, um einen Packzettel zu drucken.';
$MSG['350_562'] = "Total";
$MSG['350_10160'] = "Account Registrierung";
$MSG['350_1015404'] = "Unser Registrierungs Geschenk";
$MSG['350_1015405'] = "Werbungs Banner";

$MSG['3500_1015405'] = "Administrator Control Panel";
$MSG['3500_1015406'] = "Gast!";
$MSG['3500_1015407'] = "Panel schliessen";
$MSG['3500_1015408'] = "uAuctions script editor";
$MSG['3500_1015409'] = "Zeige alle Auktionen-Gewinner";
$MSG['3500_1015410'] = "Artikel wurde verkauft und Zahlung wird erwartet";
$MSG['3500_1015411'] = "Artikel wurde verkauft und Zahlung erhalten";
$MSG['3500_1015412'] = "Artikel ist verkauft";
$MSG['3500_1015413'] = "Ihr Facebook-Konto wurde erfolgreich mit Ihrem Konto verkn�pft. <br>(Bitte klicken Sie auf die Schaltfl�che �nderungen speichern)";
$MSG['3500_1015414'] = "Stellen Sie Ihr Standard-Theme ein";
$MSG['3500_1015415'] = "Sie k�nnen die E-Mail-Dom�nen, die von Bots verwendet werden, bearbeiten oder l�schen.<br>
Beispiel: bot@bot_email_block.com<br>
Um eine neue E-Mail-Dom�ne hinzuf�gen kopieren Sie einfach die E-Mail-Dom�ne, die nach dem @ ist und setzen diese ein.";
$MSG['3500_1015416'] = "Email Blacklist";
$MSG['3500_1015417'] = "Einf�gen mehrerer E-Mailadressen<br>
F�r jede E-Mailadresse eine neue Zeile benutzen";
$MSG['3500_1015418'] = "Spam";
$MSG['3500_1015419'] = "Sortierung starten mit <b>(A)</b>";
$MSG['3500_1015420'] = "Sortierung starten mit <b>(Z)</b>";
$MSG['3500_1015421'] = "Kategorien";
$MSG['3500_1015422'] = "Banner Max Breite";
$MSG['3500_1015423'] = "Banner Max H�he";
$MSG['3500_1015424'] = "Legen Sie die maximale Breite die erlaubt ist fest. (Die Gr��e wird in Pixel eingestellt)";
$MSG['3500_1015425'] = "Legen Sie die maximale H�he die erlaubt ist fest. (Die Gr��e wird in Pixel eingestellt)";
$MSG['3500_1015426'] = "Banner Typen";
$MSG['3500_1015427'] = "erlaubte Banner Typen (gif,&nbsp; jpg,&nbsp; jpeg,&nbsp; png,&nbsp; swf)";
$MSG['3500_1015428'] = "Paypal Sandbox";
$MSG['3500_1015429'] = "Sie k�nnen PayPal in den Testmodus (Sandbox) setzen um die PayPal IPN zu testen";
$MSG['3500_1015430'] = "Meine Downloads";
$MSG['3500_1015431'] = "Sie werden in der Lage sein, diesen Artikel herunterzuladen, nachdem eine Zahlung geleistet wurde";

///Support Mod
$MSG['3500_1015432'] = "Mein Support";
$MSG['3500_1015433'] = "Konversationen";
$MSG['3500_1015434'] = "Mitglieder Support";
$MSG['3500_1015435'] = "Sie haben eine Antwort vom Support-Team erhalten";
$MSG['3500_1015436'] = "Support Team";
$MSG['3500_1015437'] = "Mitglied";
$MSG['3500_1015438'] = "Mitteilung";
$MSG['3500_1015439'] = "Zeit der Absendung";
/// End Support Mod


$MSG['3500_1015440'] = "Sie sind der Verk�ufer und k�nnen diesen Artikel nicht selbst kaufen";
$MSG['3500_1015441'] = "W�hle Betreff";
$MSG['3500_1015442'] = "Neue Kategorie";
$MSG['3500_1015443'] = "Verbesserungen zur Seite";
$MSG['3500_1015444'] = "Allgemeines";
$MSG['3500_1015445'] = "Fehler";
$MSG['3500_1015446'] = "Betreff";

$MSG['3500_1015447'] = "Dieses MOD ist schon installiert";
$MSG['3500_1015448'] = "Dieses MOD installieren";
$MSG['3500_1015449'] = "Backup Verzeichnis";
$MSG['3500_1015450'] = "Dieses ist ein backup Verzeichnis";
$MSG['3500_1015451'] = "AutoMod";
$MSG['3500_1015452'] = '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/automod.php?mods=new_mod">Hier klicken</a> um ein neues MOD oder neues Theme f�r ihr u-Auctions script zu finden.';
$MSG['3500_1015453'] = "Dieses ist das Downloadverzeichnis f�r die MODs";
$MSG['3500_1015454'] = "MOD herunterladen";
$MSG['3500_1015455'] = '<a href="' . $system->SETTINGS['siteurl'] . $system->SETTINGS['admin_folder'] . '/mods.php">Hier klicken</a> um alle heruntergeladenen MODs anzuzeigen.';
$MSG['3500_1015456'] = "<b>MOD Verzeichnis:</b> ";
$MSG['3500_1015457'] = "Dieses MOD wurde schon heruntergeladen";
$MSG['3500_1015458'] = "Dieses MOD l�schen";

///  Areas 
$MSG['3500_1015459'] = "Teil 1: Admin Bereich";
$MSG['3500_1015460'] = "Teil 2: Benutzer Bereich";
$MSG['3500_1015461'] = "Bitte vergessen Sie nicht das Ausf�llen Ihrer Informationen in Ihrem pers�nlichen Bereich auf der Seite Benutzer Bereich zu vervollst�ndigen.";
$MSG['3500_1015462'] = "W�hle Farbe"

/// 11.Aug.2014
// $MSG['3500_1015464'] = "Mitteilungen";
// $MSG['3500_1015465'] = "Fortschritt";
// $MSG['3500_1015466'] = "Caches zur�cksetzen";
// $MSG['3500_1015467'] = "Google Adsense Code hinzuf�gen";
// $MSG['3500_1015468'] = "Letztes Login";
// $MSG['3500_1015469'] = "AN / AUS";
// $MSG['3500_1015470'] = "Sind Sie sicher, dass Sie diesen Bericht schlie�en wollen?";
// $MSG['3500_1015471'] = "M�chten Sie diesen Bericht und alle anderen Berichte f�r dieselbe Auktion schlie�en?";
// $MSG['3500_1015472'] = "Gemeldete Auktion";
// $MSG['3500_1015473'] = "Ihre Version";
// $MSG['3500_1015474'] = "Aktuelle Version";
// $MSG['3500_1015475'] = "Autor";
// $MSG['3500_1015476'] = "Mod";
// $MSG['3500_1015477'] = "Version";
// $MSG['3500_1015478'] = "Verzeichnis";
// $MSG['3500_1015479'] = "Seite";
// $MSG['3500_1015480'] = "Finde diesen Code";
// $MSG['3500_1015481'] = "Ersetze den Code";
// $MSG['3500_1015482'] = "Hinter dem Code einf�gen";
// $MSG['3500_1015483'] = "Vor dem Code einf�gen";

//missed



?>