Gentile {S_NAME},

Le seguenti aste pubblicate su {SITENAME} si sono concluse ieri.

{REPORT}


Una e-mail è stata inviata al vincitore col tuo indirizzo email.

Se avete ricevuto questo messaggio per errore, vi preghiamo di rispondere a questa email,
scrivete a {ADMINMAIL}, o visita {SITENAME} su {SITE_URL}.