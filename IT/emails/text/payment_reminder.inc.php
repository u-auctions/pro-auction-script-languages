Gentile {C_NAME},

Questa è una fattura per saldare il conto con il nostro sito, {SITENAME}.

Il tuo saldo è: {BALANCE}

Clicca sul link sottostante per accedere alla pagina di pagamento:
{LINK}