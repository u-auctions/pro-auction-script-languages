Ciao {REALNAME},
Come richiesto, abbiamo creato una nuova password per il tuo account.

La tua nuova password: {NEWPASS}

Usala per accedere a {SITENAME} e ricordati di cambiarla con un altra che puoi ricordare.