Gentile {NAME},

Il tuo account su {SITENAME} è stato sospeso perché si è superato il limite massimo consentito di debito.

Il Bilancio è: {BALANCE}

Per riattivare il tuo account, è necessario saldare il conto.
Si prega di visitare il link sottostante per accedere alla pagina di pagamento.
{OUTSTANDING}