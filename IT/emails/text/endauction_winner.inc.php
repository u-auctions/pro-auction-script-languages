Gentile {S_NAME},
Complimenti il tuo oggetto è appena stato venduto!

Di seguito sono riportati i dettagli.

Titolo Asta: {A_TITLE}
Prezzo di Vendita: {A_CURRENTBID}
Quantità: {A_QTY}
Data fine: {A_ENDS}
URL Asta: {A_URL}

Vai a {SITENAME}: {SITE_URL}user_menu.php

Informazioni Acquirente
{B_REPORT}