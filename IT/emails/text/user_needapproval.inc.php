Gentile {C_NAME},
Al fine di iniziare a vendere e/o acquistare su {SITENAME}, il tuo account deve essere
accettato dall'amministratore del nostro sito ed è attualmente in fase di revisione.

Riceverai una e-mail a breve quando il tuo account sarà attivo.

Le tue informazioni sono qui di seguito:

Nome Completo: {C_NAME}
Username: {C_NICK}
<!-- IF C_ADDRESS ne '' -->
Indirizzo: {C_ADDRESS}
<!-- ENDIF -->
<!-- IF C_CITY ne '' -->
Città: {C_CITY}
<!-- ENDIF -->
<!-- IF C_PROV ne '' -->
Provincia: {C_PROV}
<!-- ENDIF -->
<!-- IF C_COUNTRY ne '' -->
Stato: {C_COUNTRY}
<!-- ENDIF -->
<!-- IF C_ZIP ne '' -->
CAP: {C_ZIP}
<!-- ENDIF -->
Email: {C_EMAIL}