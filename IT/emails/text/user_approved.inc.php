<html>
 <head>
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		}
 	</style>
 </head>
 <body>
<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Congratulazioni, sei un nuovo membro di {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Ciao <b>{C_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		 Questa è un email per confermare che la tua registrazione su {SITENAME} è stata approvata! <br />
		È ora possibile effettuare il login al proprio account e iniziare a vendere e/o comprare. Grazie per essere stati con noi!!
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px;"><b>Pronto per iniziare a cercare?</b><br /><br />
		<a href="{SITE_URL}"><img src="{SITE_URL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">&nbsp;
		</td>
	</tr>
 </table>
</body>
</html>