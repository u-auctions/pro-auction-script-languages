Gentile {NAME},<br>
<br>
Hai ricevuto una risposta dal team di supporto.<br>
<br>
<strong>{SITENAME} supporto:</strong> <a href="{A_URL}">Il mio supporto</a><br>
<br>
<strong>Sito web:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Oggetto:</strong><br>
{SUBJECT}
<br><br>
<strong>Messaggio:</strong><br>
{MESSAGE}
