Gentile {C_NAME},
Il tuo articolo è stato inserito con successo su {SITENAME}. L'elemento verrà visualizzato immediatamente nei risultati di ricerca.

Ecco i dettagli dell'annuncio:

Titolo Asta: {A_TITLE}
Tipo Asta: {A_TYPE}
Oggetto #: {A_ID}
Offerta di Partenza: {A_MINBID}
Prezzo di Riserva: {A_RESERVE}
Prezzo Compra Ora: {A_BNPRICE}
Fine: {A_ENDS}

{SITE_URL}item.php?id={A_ID}
				
Hai un altro oggetto da vendere?
{SITE_URL}select_category.php