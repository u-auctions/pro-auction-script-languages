<html>
 <head>
 	<style type="text/css">
 		body {
 		font-family: Verdana;
 		}
 	</style>
 </head>
 <body>
<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Completa la registrazione di {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Ciao <b>{C_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		Basta cliccare su "Attiva" per completare la registrazione. Una volta fatto questo si è pronti a  
		iniziare a comprare/vendere su {SITENAME}
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top"><br /><br />
		<table border="0" width="100%">
			<tr>
				<td style="font-size: 12px; padding-bottom: 7px; padding-top: 5px;"><b>Se il link non funziona:</b></td>

			</tr>
			<tr>
				<td style="font-size: 12px;">Scriveteci all'indirizzo <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a></td>
			</tr>
		</table>
		</td>
		<td width="34%" style="font-size: 12px;">Continua per confermare</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">
		<a href="{CONFIRMURL}">
		<img border="0" src="{SITEURL}images/email_alerts/Active_Acct_Btn.jpg" width="120" height="32"></a></td>
	</tr>
 </table>
</body>
</html>