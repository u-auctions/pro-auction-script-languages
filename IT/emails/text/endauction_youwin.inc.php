Gentile {W_NAME},

Hai vinto la seguente asta su {SITENAME}

Dati Asta

Titolo: {A_TITLE}
Descrizione: {A_DESCRIPTION}

La vostra offerta: {A_CURRENTBID} ({W_WANTED} oggetti)
Hai: {W_GOT} oggetti
Data fine Asta: {A_ENDS}
URL: {A_URL}

Informazioni Venditore

{S_NICK}
{S_EMAIL}
Dettagli pagamento venditore:
{S_PAYMENT}


Se avete ricevuto questo messaggio per errore, vi preghiamo di rispondere a questa email,
scrivi a {ADMINEMAIL}, o visita {SITENAME} su {SITE_URL}.
