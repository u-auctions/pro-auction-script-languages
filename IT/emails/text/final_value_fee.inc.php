Gentile {C_NAME},

Questa è una fattura per il pagamento della commissione per l'acquisto di {ITEM_NAME}.

Totale da pagare: {BALANCE}

Cliccate sul link sottostante per accedere alla pagina di pagamento:
{LINK}

Tutte le attività sul tuo sito verranno sospese fino a quando questo non verrà saldato