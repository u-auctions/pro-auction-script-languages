Gentile {C_NAME},

Questa è una fattura per il pagamento della commessione di acquisto di {ITEM_NAME}.

Totale da Pagare: {BALANCE}

Clicca sul link sottostante per accedere alla pagina di pagamento:
{LINK}

Tutte le attività sul tuo sito verranno sospese fino al saldo