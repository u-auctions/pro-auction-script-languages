Gentile Team di Supporto,<br>
<br>
Hai ricevuto un ticket di supporto.<br>
<strong>Utente:</strong> {USER}<br>
<br>
<strong>Sito:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Oggetto:</strong><br>
{SUBJECT}
<br><br>
<strong>Messaggio:</strong><br>
{MESSAGE}