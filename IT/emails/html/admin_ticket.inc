Gentile Tema di Supporto,<br>
<br>
Avete ricevuto una richiesta di assistenza.<br>
<strong>Utente:</strong> {USER}<br>
<br>
<strong>Website:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Oggetto:</strong><br>
{SUBJECT}
<br><br>
<strong>Messaggio:</strong><br>
{MESSAGE}