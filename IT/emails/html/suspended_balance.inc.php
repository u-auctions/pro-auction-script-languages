Gentile {NAME},<br>
<br>
Il tuo account su {SITENAME} è stato sospeso perché si è superato il limite massimo consentito debito.<br>
<br>
Il tuo Bilancio è: {BALANCE}<br>
<br>
Per riattivare il tuo account, è necessario azzerare il saldo del conto.<br>
Si prega di visitare il link sottostante per accedere alla pagina di pagamento.<br>
<a href="{OUTSTANDING}">{OUTSTANDING}</a>