Gentile {S_NAME},<br>
<br>
La seguente asta che ha postato <a href="{SITE_URL}">{SITENAME}</a> si è chiusa ieri.<br>
<br>
{REPORT}
<br>
<br>
Una e-mail è stata inviata al vincitore con il vostro indirizzo e-mail.<br>
<br>
Se avete ricevuto questo messaggio per errore, vi preghiamo di rispondere a questa email,
scrivere a <a href="mailto:{ADMINMAIL}">{ADMINMAIL}</a>, o visitate {SITENAME} a questo indirizzo <a href="{SITE_URL}">{SITE_URL}</a>.
