Gentile {REALNAME},<br>
<br>
Questa è una risposta automatica da un Oggetto Osservato. <br>
Qualcuno ha fatto un'offerta per un Oggetto da lei aggiunto alla sua lista di Oggetti Osservati.<br>
<br>
Titolo Asta: {TITLE}<br>
<br>
Offerta corrente: {BID}<br>
<br>
Asta URL: <a href="{AUCTION_URL}">{AUCTION_URL}</a>