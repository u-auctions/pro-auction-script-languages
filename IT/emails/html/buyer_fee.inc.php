Gentile {C_NAME},<br>
<br>
Si tratta di una fattura per il pagamento della tassa di acquisto di {ITEM_NAME}.<br>
<br>
Totale da pagare: {BALANCE}<br>
<br>
Cliccate sul link sottostante per accedere alla pagina di pagamento:<br>
<a href="{LINK}">{LINK}</a><br>
<br>
Tutte le attività sul suo sito verranno sospese fino a quando il pagamento non verrà regolarizzato