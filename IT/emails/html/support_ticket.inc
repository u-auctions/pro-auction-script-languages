Gentile {NAME},<br>
<br>
Avete ricevuto una risposta dal team di supporto.<br>
<br>
<strong>{SITENAME} Supporto:</strong> <a href="{A_URL}">Mio Supporto</a><br>
<br>
<strong>Website:</strong> <a href="{SITE_URL}">{SITENAME}</a>
<br><br>
<strong>Oggetto:</strong><br>
{SUBJECT}
<br><br>
<strong>Messaggio:</strong><br>
{MESSAGE}