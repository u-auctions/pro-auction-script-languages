Gentile {W_NAME},<br>
<br>
Hai vinto la seguente asta su <a href="{SITE_URL}">{SITENAME}</a> <br>
<br>
Data Asta<br>
------------------------------------<br>
Titolo: {A_TITLE}<br>
Descrizione: {A_DESCRIPTION}<br>
<br>
La sua offerta: {A_CURRENTBID} ({W_WANTED} items)<br>
Hai acquitato: {W_GOT} oggetti<br>
Data di chiusura Asta: {A_ENDS}<br>
URL: <a href="{A_URL}">{A_URL}</a><br>
<br>
=============<br>
INFORMAZIONI DEL VENDITORE<br>
=============<br>
<br>
{S_NICK}<br>
<a href="mailto:{S_EMAIL}">{S_EMAIL}</a><br>
Dettagli di pagamento del venditore:<br>
{S_PAYMENT}<br>		 
<br>
<br>
Se avete ricevuto questo messaggio per errore, vi preghiamo di rispondere a questa email,<br>
scrivere a <a href="mailto:{ADMINEMAIL}">{ADMINEMAIL}</a>, o visitare <a href="{SITE_URL}">{SITENAME}</a> al seguente indirizzo <a href="{SITE_URL}">{SITE_URL}</a>.
