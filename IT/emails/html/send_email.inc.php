Gentile {SELLER_NICK},<br>
<br>
Questo messaggio viene inviato da {SITENAME}.<br>
<br>
{SENDER_NAME} a <a href="mailto:{SENDER_EMAIL}">{SENDER_EMAIL}</a> ha una domanda per voi per quanto riguarda la vostra asta {TITLE}.<br>
<br>
Domanda:<br>
{SENDER_QUESTION}<br>
<br>
Asta URL: <a href="{SITEURL}item.php?id={AID}">{SITEURL}item.php?id={AID}</a><br>
<br>
Grazie per partecipare a {SITENAME}<br>
<a href="{SITEURL}">{SITEURL}</a><br>