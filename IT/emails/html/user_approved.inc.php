<table border="0" width="100%">
	<tr>
		<td colspan="2" height="35"><div style="font-size: 14px; font-weight: bold;">Congratulazioni, Ora sei un membro di {SITENAME}!</div></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 12px;">Salve <b>{C_NAME}</b>,</td>
	</tr>
	<tr>
		<td colspan="2" height="50" style="font-size: 12px; padding-right: 6px;">
		 Questa e-mail ha lo scopo di confermare la sua registrazione su {SITENAME}! <br />
		È ora possibile effettuare il login al proprio account e iniziare a vendere e/o di acquistare. Grazie per esservi uniti a noi!!
		</td>
	</tr>
	<tr>
		<td width="55%" rowspan="2" valign="top" style="font-size: 12px;"><b>Pronto per dare un'occhiata?</b><br /><br />
		<a href="{SITE_URL}"><img src="{SITE_URL}images/email_alerts/Take_Me_There.jpg" border="0"></a>
		</td>
		<td width="34%" style="font-size: 12px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="34%" height="176" valign="top">&nbsp;
		</td>
	</tr>
</table>