<b>Questa una copia del modulo di contatto che hai inviato a noi.</b><br><br>

Gentile {SELLER_NICK},<br>
<br>
Questo messaggio viene inviato da {SITENAME}.<br>
<br>
L'utente {SENDER_NAME} Ha una domanda per voi.<br>
<br>
E-mail del mittente:<br>
{SENDER_EMAIL}<br>
<br>
Oggetto:<br>
{SUBJECT}<br><br>
Domanda:<br>
{SENDER_QUESTION}<br>
