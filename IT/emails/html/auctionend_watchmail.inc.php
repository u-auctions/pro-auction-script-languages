Gentile {NAME},<br>
<br>
Questa è una risposta automatica da un oggetto osservato.<br>
L'asta che si trova nella lista degli oggetti osservati si è chiusa.<br>
<br>
Titolo Asta: {TITLE}<br>
Asta URL: <a href="{URL}">{URL}</a>